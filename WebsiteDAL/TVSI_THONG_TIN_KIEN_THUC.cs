using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_KIEN_THUC
    {
        public const string thong_tin_kien_thucid = "thong_tin_kien_thucid"; public const string danh_muc_kien_thucid = "danh_muc_kien_thucid"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string file_dinh_kem = "file_dinh_kem"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_KIEN_THUC"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_danh_muc_kien_thucid(int ngon_nguid,int danh_muc_kien_thucid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_kien_thucid = new SqlParameter("@danh_muc_kien_thucid", danh_muc_kien_thucid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_KIEN_THUC_By_danh_muc_kien_thucid", param_ngon_nguid, param_danh_muc_kien_thucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_KIEN_THUC_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Layra_chi_tiet_tin_dau_tien(int ngon_nguid, int danh_muc_kien_thucid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_kien_thucid = new SqlParameter("@danh_muc_kien_thucid", danh_muc_kien_thucid);
           
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_KIEN_THUC_TIN_DAU_TIEN", param_ngon_nguid, param_danh_muc_kien_thucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Layra_chi_tiet_tin(int ngon_nguid, int thong_tin_kien_thucid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_kien_thucid = new SqlParameter("@thong_tin_kien_thucid", thong_tin_kien_thucid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_KIEN_THUC_CHI_TIET_TIN", param_ngon_nguid, param_thong_tin_kien_thucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}