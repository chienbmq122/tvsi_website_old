using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using TVSI.Common;
 
namespace TVSI.WebsiteDAL
{
   public class TVSI_Chart
    {

        public static DataTable TVSI_INDEX_THI_TRUONG(string sMaTT, string sTuNgay, string sDenNgay)
        {

            DataSet ds = null;
            string DataInfo =  ConnectionString.ActionQuery_PriceInfo;
            string StoredForChart = "TVSI_sKET_QUA_GIAO_DICH_THI_TRUONG";
 
            ds = SqlHelper.ExecuteDataset(DataInfo, StoredForChart, sMaTT, sTuNgay, sDenNgay);
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            else
            {
                return new DataTable();
            }

        }
        public static DataTable TVSI_REALTIME_INDEX(string sMaTT)
        {
            DataSet ds = null;
            string DataInfo = ConnectionString.ActionQuery_PriceInfo;
            string StoredForChart = "TVSI_sTHONG_TIN_THI_TRUONG";
            ds = SqlHelper.ExecuteDataset(DataInfo, StoredForChart, sMaTT);
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            else
            {
                return new DataTable();
            }
        }
       public static DataTable TVSI_PRICE_STOCK(string sMaCK, string sTuNgay, string sDenNgay)
       {
           DataSet ds = null;
           string DataInfo = TVSI.Common.ConnectionString.ActionQuery_PriceInfo;
           string StoredForChart = "TVSI_sKET_QUA_GIAO_DICH";
           ds = SqlHelper.ExecuteDataset(DataInfo, StoredForChart, sMaCK, sTuNgay, sDenNgay);
           if (ds.Tables.Count > 0)
           {
               return ds.Tables[0];
           }
           else
           {
               return new DataTable();
           }
       }
    }
}
