
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_THONG_TIN_CHUNG
    {
        public const string DOANH_NGHIEP_THONG_TIN_CHUNGID = "DOANH_NGHIEP_THONG_TIN_CHUNGID"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ten_doanh_nghiep = "ten_doanh_nghiep"; public const string ten_giao_dich = "ten_giao_dich"; public const string ten_viet_tat = "ten_viet_tat"; public const string tru_so_chinh = "tru_so_chinh"; public const string dien_thoai = "dien_thoai"; public const string fax = "fax"; public const string dia_chi_web = "dia_chi_web"; public const string so_giay_phep_thanh_lap = "so_giay_phep_thanh_lap"; public const string ngay_cap_giay_phep_thanh_lap = "ngay_cap_giay_phep_thanh_lap"; public const string tong_so_nhan_vien = "tong_so_nhan_vien"; public const string lich_su_hinh_thanh = "lich_su_hinh_thanh"; public const string hoat_dong_kinh_doanh = "hoat_dong_kinh_doanh"; public const string so_sanh_trong_nganh = "so_sanh_trong_nganh"; public const string chien_luoc_phat_trien = "chien_luoc_phat_trien"; public const string doi_thu_canh_tranh = "doi_thu_canh_tranh"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string ma_chung_khoan = "ma_chung_khoan";
        public const string noi_giao_dich = "noi_giao_dich";
        public const string nganhid = "nganhid";
       
        public static DataTable Lay_DOANH_NGHIEP_THONG_TIN_CHUNG(int ngon_nguid, int  doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_NIEM_YET_THONG_TIN_CHUNG", param_ngon_nguid,param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Lay_DOANH_NGHIEP_THONG_TIN_GIAO_DICH_NOI_BO(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_GIAO_DICH_NOI_BO", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable Lay_DOANH_NGHIEP_BAO_CAO_BACH(int loai_file, int thong_tinid)
        {
            SqlParameter param_loai_file = new SqlParameter("@loai_file", loai_file);
            SqlParameter param_thong_tinid = new SqlParameter("@thong_tinid", thong_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_FILE_BAO_CAO_BACH", param_loai_file, param_thong_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}