
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_BAO_CAO_TAI_CHINH_DAY_DU
    {
        public const string bao_cao_doanh_nghiepid = "bao_cao_doanh_nghiepid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string the_loai_bao_cao = "the_loai_bao_cao"; public const string ky_bao_cao = "ky_bao_cao"; public const string thang_bao_cao = "thang_bao_cao"; public const string quy_bao_cao = "quy_bao_cao"; public const string nam_bao_cao = "nam_bao_cao"; public const string ten_bao_cao = "ten_bao_cao"; public const string noi_dung_bao_cao = "noi_dung_bao_cao"; public const string file_dinh_kem = "file_dinh_kem"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";


        public static DataTable BAO_CAO_TAI_CHINH_DAY_DU(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
           
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_BAO_CAO_TAI_CHINH_DAY_DU", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable BAO_CAO_TAI_CHINH_DAY_DU_XEM_TUY_CHON(int ngon_nguid, int doanh_nghiepid, int thang_bao_cao, int quy_bao_cao, int nam_bao_cao, int the_loai_bao_cao)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            SqlParameter param_thang_bao_cao = new SqlParameter("@thang_bao_cao", thang_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            SqlParameter param_the_loai_bao_cao = new SqlParameter("@the_loai_bao_cao", the_loai_bao_cao);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_BAO_CAO_TAI_CHINH_DAY_DU_XEM_TUY_CHON", param_ngon_nguid, param_doanh_nghiepid, param_thang_bao_cao, param_quy_bao_cao, param_nam_bao_cao,param_the_loai_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable BAO_CAO_TAI_CHINH_DAY_DU_THEO_NAM_BAO_CAO(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_BAO_CAO_TAI_CHINH_DAY_DU_THEO_NAM_BAO_CAO", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
       
    }
}