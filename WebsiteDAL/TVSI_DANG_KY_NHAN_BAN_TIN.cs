using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
using TVSI.Common;
namespace TVSI.WebsiteDAL
{
    public class TVSI_DANG_KY_NHAN_BAN_TIN
    {
        public const string dia_chi_email = "dia_chi_email";
        public static int  HUY_DAN_KY_NHAN_BAN_TIN(string dia_chi_email)
        {
            try
            {
                SqlParameter param_dia_chi_email = new SqlParameter("@dia_chi_email", dia_chi_email);
                return SqlHelper.ExecuteNonQuery(ConnectionString.ActionQuery_Website, CommandType.StoredProcedure, "TVSI_sREMOVE_ACC_DANG_KY_NHAN_BAN_TIN", param_dia_chi_email);
            }
            catch
            {
                return 0;
            }
             
        }

        public static DataTable DANH_SACH_DANG_KY_NHAN_BAN_TIN()
        {
            using (DataSet dtsDanhsachemail = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sDANH_SACH_EMAIL_DANG_KY_BAN_TIN"))
            {
                if (dtsDanhsachemail.Tables.Count > 0)
                {
                    return dtsDanhsachemail.Tables[0];

                }
                else
                {
                    return new DataTable(); 
                }
            }
        }
    }
}
