
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_Y_KIEN_KHACH_HANG
    {
        public const string ho_ten = "ho_ten";
        public const string dien_thoai = "dien_thoai";
        public const string dia_chi_email = "dia_chi_email";
        public const string giao_dien_website = "giao_dien_website";
        public const string thong_tin_website = "thong_tin_website";
        public const string tim_kiem_thong_tin = "tim_kiem_thong_tin";
        public const string kha_nang_xu_ly_nghiep_vu = "kha_nang_xu_ly_nghiep_vu";
        public const string thai_do_phuc_vu = "thai_do_phuc_vu";
        public const string thoi_gian_phuc_vu = "thoi_gian_phuc_vu";
        public const string kha_nang_cung_cap_tt = "kha_nang_cung_cap_tt";
        public const string su_tuan_thu_quy_dinh = "su_tuan_thu_quy_dinh";
        public const string y_kien_khac = "y_kien_khac";

        public static void TVSI_GOP_Y_KHACH_HANG_TVSI(string ho_ten, string dien_thoai, string dia_chi_email, int giao_dien_website, int thong_tin_website, int tim_kiem_thong_tin, int kha_nang_xu_ly_nghiep_vu, int thai_do_phuc_vu, int thoi_gian_phuc_vu, int kha_nang_cung_cap_tt, int su_tuan_thu_quy_dinh, string y_kien_khac)
        {
            SqlParameter param_ho_ten = new SqlParameter("@ho_ten", ho_ten);
            SqlParameter param_dien_thoai = new SqlParameter("@dien_thoai", dien_thoai);
            SqlParameter param_dia_chi_email = new SqlParameter("@dia_chi_email", dia_chi_email);
            SqlParameter param_giao_dien_website = new SqlParameter("@giao_dien_website", giao_dien_website);
            SqlParameter param_thong_tin_website = new SqlParameter("@thong_tin_website", thong_tin_website);
            SqlParameter param_tim_kiem_thong_tin = new SqlParameter("@tim_kiem_thong_tin", tim_kiem_thong_tin);
            SqlParameter param_kha_nang_xu_ly_nghiep_vu = new SqlParameter("@kha_nang_xu_ly_nghiep_vu", kha_nang_xu_ly_nghiep_vu);
            SqlParameter param_thai_do_phuc_vu = new SqlParameter("@thai_do_phuc_vu", thai_do_phuc_vu);
            SqlParameter param_thoi_gian_phuc_vu = new SqlParameter("@thoi_gian_phuc_vu", thoi_gian_phuc_vu);
            SqlParameter param_kha_nang_cung_cap_tt = new SqlParameter("@kha_nang_cung_cap_tt", kha_nang_cung_cap_tt);
            SqlParameter param_su_tuan_thu_quy_dinh = new SqlParameter("@su_tuan_thu_quy_dinh", su_tuan_thu_quy_dinh);
            SqlParameter param_y_kien_khac = new SqlParameter("@y_kien_khac", y_kien_khac);

            SqlHelper.ExecuteNonQuery(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sY_KIEN_KHACH_HANG_INSERT", param_ho_ten, param_dien_thoai, param_dia_chi_email, param_giao_dien_website, param_thong_tin_website, param_tim_kiem_thong_tin, param_kha_nang_xu_ly_nghiep_vu, param_thai_do_phuc_vu, param_thoi_gian_phuc_vu, param_kha_nang_cung_cap_tt, param_su_tuan_thu_quy_dinh, param_y_kien_khac);
            
        }

    }
}
