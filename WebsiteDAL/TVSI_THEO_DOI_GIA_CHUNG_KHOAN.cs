
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THEO_DOI_GIA_CHUNG_KHOAN
    {
        public const string theo_doi_gia_chung_khoanid = "theo_doi_gia_chung_khoanid";
        public const string so_tai_khoan = "so_tai_khoan";
        public const string ngay_bat_dau_theo_doi = "ngay_bat_dau_theo_doi";
        public const string ma_chung_khoan = "ma_chung_khoan";
        public const string doanh_nghiepid = "doanh_nghiepid";

        public static DataTable LayTheoSoTaiKhoan(string so_tai_khoan)
        {
            SqlParameter param_so_tai_khoan = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_THEO_DOI_GIA_CHUNG_KHOAN_LayTheo_so_tai_khoan", param_so_tai_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTinLienQuanTheoSoTaiKhoan(int ngon_nguid, string so_tai_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_so_tai_khoan = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_THEO_DOI_GIA_CHUNG_KHOAN_LayTopTinTucLienQuan_Theo_so_tai_khoan", param_ngon_nguid, param_so_tai_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static int Them(string so_tai_khoan, DateTime ngay_bat_dau_theo_doi, string ma_chung_khoan)
        {
            SqlParameter param_theo_doi_gia_chung_khoanid = new SqlParameter("@theo_doi_gia_chung_khoanid", -1);
            param_theo_doi_gia_chung_khoanid.Direction = ParameterDirection.Output;
            SqlParameter param_so_tai_khoan = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            SqlParameter param_ngay_bat_dau_theo_doi = new SqlParameter("@ngay_bat_dau_theo_doi", ngay_bat_dau_theo_doi);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);

            SqlHelper.ExecuteNonQuery(ConnectionString.ActionQuery_Website, CommandType.StoredProcedure,
                                                        "TVSI_THEO_DOI_GIA_CHUNG_KHOAN_Them",
                                                        param_so_tai_khoan,
                                                        param_ngay_bat_dau_theo_doi,
                                                        param_ma_chung_khoan,
                                                        param_theo_doi_gia_chung_khoanid);
            return Convert.ToInt32(param_theo_doi_gia_chung_khoanid.Value);
        }

        public static bool Xoa(int theo_doi_gia_chung_khoanid)
        {
            SqlParameter param_theo_doi_gia_chung_khoanid = new SqlParameter("@theo_doi_gia_chung_khoanid", theo_doi_gia_chung_khoanid);

            return (SqlHelper.ExecuteNonQuery(ConnectionString.ActionQuery_Website, CommandType.StoredProcedure,
                                                        "TVSI_THEO_DOI_GIA_CHUNG_KHOAN_Xoa",
                                                        param_theo_doi_gia_chung_khoanid) > 0);
        }

        public static bool XoaTheoSoTaiKhoan_MaChungKhoan(string so_tai_khoan, string ma_chung_khoan)
        {
            SqlParameter param_so_tai_khoan = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);

            return (SqlHelper.ExecuteNonQuery(ConnectionString.ActionQuery_Website, CommandType.StoredProcedure,
                                                        "TVSI_THEO_DOI_GIA_CHUNG_KHOAN_XoaTheo_So_tai_khoan_Ma_chung_khoan",
                                                        param_so_tai_khoan,
                                                        param_ma_chung_khoan) > 0);
        }
    }
}
