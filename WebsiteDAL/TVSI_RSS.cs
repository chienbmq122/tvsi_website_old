using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;

using TVSI.Common;

namespace TVSI.WebsiteDAL
{
    public class TVSI_RSS
    {
        public enum LoaiRSS
        {
            THONG_TIN_CONG_BO = 1,
            THONG_TIN_SU_KIEN = 2,
            THONG_TIN_TVSI = 3
        }

        public static DataTable RSSSelect_THONG_TIN_CONG_BO(int chuyen_muc_tin_tucid)
        {
            SqlParameter param_chuyen_muc_tin_tucid = new SqlParameter("@chuyen_muc_tin_tucid", chuyen_muc_tin_tucid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_THONG_TIN_CONG_BO", param_chuyen_muc_tin_tucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable RSSSelect_THONG_TIN_SU_KIEN(int chuyen_muc_tin_tucid)
        {
            SqlParameter param_chuyen_muc_tin_tucid = new SqlParameter("@chuyen_muc_tin_tucid", chuyen_muc_tin_tucid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_THONG_TIN_SU_KIEN", param_chuyen_muc_tin_tucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable RSSSelect_THONG_TIN_TVSI(int chuyen_muc_tin_tucid)
        {
            SqlParameter param_chuyen_muc_tin_tucid = new SqlParameter("@chuyen_muc_tin_tucid", chuyen_muc_tin_tucid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_THONG_TIN_TVSI", param_chuyen_muc_tin_tucid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable RSSSelect_CHUYEN_MUC_THONG_TIN_CONG_BO(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_CHUYEN_MUC_THONG_TIN_CONG_BO", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable RSSSelect_CHUYEN_MUC_THONG_TIN_SU_KIEN(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_CHUYEN_MUC_THONG_TIN_SU_KIEN", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable RSSSelect_CHUYEN_MUC_THONG_TIN_TVSI(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectRSS_CHUYEN_MUC_THONG_TIN_TVSI", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
