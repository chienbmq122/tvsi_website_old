﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TVSI.Common;
using TVSI.Common.DAL;

namespace TVSI.WebsiteDAL
{
    public class TVSI_STOX_MESSAGE
    {
        public static DataTable LayTintucTheoChuyenMuc(int chuyenmuc_id)
        {

            SqlParameter param_categoryId = new SqlParameter("@CategoryID", chuyenmuc_id);
            string connect_String = ConfigurationManager.ConnectionStrings["SelectQuery_Tvsi_S"].ConnectionString;
            using (DataSet dsData = SqlHelper.ExecuteDataset(connect_String, CommandType.StoredProcedure,
                                                            "TVSI_STOX_sSelect_Messages_By_CategoryID", param_categoryId))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable LayTintucTheoMessageId(int messageID)
        {
            SqlParameter param_messageID = new SqlParameter("@messageID", messageID);
            string connect_String = ConfigurationManager.ConnectionStrings["SelectQuery_Tvsi_Stox"].ConnectionString;
            using (DataSet dsData = SqlHelper.ExecuteDataset(connect_String, CommandType.StoredProcedure,
                                                            "TVSI_STOX_sSelect_Messages_By_messageID", param_messageID))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }

        }
    }
}
