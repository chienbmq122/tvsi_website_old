
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_CO_DONG
    {
        public const string doanh_nghiep_co_dongid = "doanh_nghiep_co_dongid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ten_co_dong = "ten_co_dong"; public const string loai_co_dong = "loai_co_dong"; public const string so_luong_nam_giu = "so_luong_nam_giu"; public const string ti_le_nam_giu = "ti_le_nam_giu"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";


        public static DataTable LayTheo_doanh_nghiepid(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_NIEM_YET_CO_DONG", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}