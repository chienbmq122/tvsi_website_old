
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_NGANH
    {
        public const string nganhid = "nganhid"; public const string ma_nganh = "ma_nganh"; public const string ten_nganh = "ten_nganh"; public const string ma_nganh_phu_thuoc = "ma_nganh_phu_thuoc"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_NGANH"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGANH_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTEN_NGANH(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGANH", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayDANH_SACH_CONG_TY_THUOC_NGANH(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_NGANH_VA_DOANH_NGHIEP_THUOC_NGANH", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheoId(int nganhid)
        {
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGANH_LayTheoId", param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        // thanhtn
        public static DataTable LayDanhsach_NganhMucCha(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGANH_muc_cha", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        // thanhtn
        public static DataTable LayTheo_ma_nganh_phu_thuoc(int ma_nganh_phu_thuoc)
        {
            SqlParameter param_ma_nganh_phu_thuoc = new SqlParameter("@ma_nganh_phu_thuoc", ma_nganh_phu_thuoc);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGANH_By_ma_nganh_phu_thuoc", param_ma_nganh_phu_thuoc))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        // thanhtn

        public static DataTable LayDANH_SACH_CONG_TY_THUOC_NGANH(int ngon_nguid, int nganhid, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_DOANH_NGHIEP_THUOC_NGANH_theo_ma_chung_khoan", param_ngon_nguid, param_nganhid, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTheo_Nam_bao_cao_nganh(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_BAO_CAO_NGANH_THEO_NAM_BAO_CAO", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }



        public static DataTable Xem_Nganh_Theo_Tuy_Chon(int ngon_nguid, int nganhid, int thang_bao_cao, int quy_bao_cao, int nam_bao_cao)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_thang_bao_cao = new SqlParameter("@thang_bao_cao", thang_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_BAO_CAO_NGANH_XEM_TUY_CHON", param_ngon_nguid, param_nganhid, param_thang_bao_cao, param_quy_bao_cao, param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable DoanhNghiep_Nganh(int ngon_nguid, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_NGANH_theo_ma_chung_khoan",param_ngon_nguid, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }

        }
    }
}