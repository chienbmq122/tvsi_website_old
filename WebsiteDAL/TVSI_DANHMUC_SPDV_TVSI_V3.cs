﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
namespace TVSI.WebsiteDAL
{
    public class TVSI_DANHMUC_SPDV_TVSI_V3
    {
        public static DataTable Laytheo_chuyenmuc_phuthuocid(int ngon_nguid, int chuyenmuc_phuthuocid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyenmuc_phuthuocid = new SqlParameter("@danh_muc_phu_thuocid", chuyenmuc_phuthuocid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                "TVSI_sSelect_TVSI_CHUYEN_MUC_SPDV_TVSI_BY_CHUYENMUC_PHUTHUOCID", param_ngon_nguid, param_chuyenmuc_phuthuocid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable LayTheo_danh_muc_spdvid(int ngon_nguid, int danh_muc_spdvid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_spdvid = new SqlParameter("@danh_muc_spdvid", danh_muc_spdvid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_SPDV_TVSI_by_danhmuc_id", param_ngon_nguid, param_danh_muc_spdvid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

    }
}
