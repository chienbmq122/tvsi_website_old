﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;


namespace TVSI.WebsiteDAL
{
    public class TVSI_DANH_MUC_HO_TRO_v3
    {
        public static DataTable LayTheo_danh_muc_ho_troid(int ngon_nguid, int danh_muc_ho_troid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_ho_troid = new SqlParameter("@danh_muc_ho_troid", danh_muc_ho_troid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DANH_MUC_HO_TRO_v3_By_danh_muc_ho_troid", param_ngon_nguid, param_danh_muc_ho_troid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable Laytheo_danh_muc_phu_thuocid(int ngon_nguid, int danh_muc_phu_thuocid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_phu_thuocid = new SqlParameter("@danh_muc_phu_thuocid", danh_muc_phu_thuocid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DANH_MUC_HO_TRO_v3_By_danh_muc_phu_thuocid", param_ngon_nguid, param_danh_muc_phu_thuocid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
