
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_TVSI
    {
        public const string thong_tin_tvsiid = "thong_tin_tvsiid"; public const string chuyen_muc_tin_tvsiid = "chuyen_muc_tin_tvsiid"; public const string nganhid = "nganhid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ngay_dang_tai = "ngay_dang_tai"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string tin_quan_trong = "tin_quan_trong"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string file_dang_tai = "file_dang_tai";
        
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_TVSI"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_chuyen_muc_tin_tvsiid(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_By_chuyen_muc_tin_tvsiid", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTheo_chuyen_muc_tin_tvsi_ontop_id(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_ON_TOP_By_chuyen_muc_tin_tvsiid", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }



        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayThongTinRaTrangChu(int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_TRANGCHU", param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


         






        public static DataTable Lay2ThongTinKhacRaTrangChu(int chuyen_muc_tin_tvsiid, int thong_tin_tvsiid)
        {
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TOP2_TVSI_THONG_TIN_TVSI_TRANGCHU", param_chuyen_muc_tin_tvsiid,param_thong_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }



        public static DataTable LayThongTinRa1tindautien(int chuyen_muc_tin_tvsiid,int ngon_nguid)
        {
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_CHITIET_TIN_DAUTIEN", param_chuyen_muc_tin_tvsiid,param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable ChiTiet_THONG_TIN_TVSI(int ngon_nguid, int thong_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI", param_ngon_nguid, param_thong_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable TVSI_fGetPortfolioNews(string sMaChungKhoan, int iNgonNguID)
        {
            SqlParameter spMaChungKhoan = new SqlParameter("@ma_chung_khoan", sMaChungKhoan);
            SqlParameter spNgonNguID = new SqlParameter("@ngon_nguid", iNgonNguID);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sPORTFOLIO_NEWS", spMaChungKhoan, spNgonNguID))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable TVSI_THONG_TIN_NHAN_SU_CHU_CHOT(int ngon_nguid, int thong_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_THONG_TIN_NHAN_SU_CHU_CHOT", param_ngon_nguid, param_thong_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable TVSI_DANH_SACH_NHAN_SU_CHU_CHOT(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_DANH_SACH_NHAN_SU_CHU_CHOT", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TVSI_DANH_SACH_NHAN_SU_CHU_CHOT_KHAC(int ngon_nguid, int thong_tin_tvsiid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_DANH_SACH_NHAN_SU_CHU_CHOT_KHAC", param_ngon_nguid, param_thong_tin_tvsiid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable TVSI_TOP5_TIN_THONG_TIN_TVSI_TRANG_CHU(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTop5ThongTinTVSI_Trang_chu", param_ngon_nguid,param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }

        }

    }
}