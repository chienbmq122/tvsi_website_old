using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_CHUYEN_MUC_THONG_TIN_TVSI_v2
    {
        public const string chuyen_muc_tin_tvsiid = "chuyen_muc_tin_tvsiid"; public const string ten_chuyen_muc = "ten_chuyen_muc"; public const string ten_chuyen_muc_phu_thuocid = "ten_chuyen_muc_phu_thuocid"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string hien_thi_tren_trang_chu = "hien_thi_tren_trang_chu"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
       
        /// <summary>
        /// ////
        /// 
        ///  
        public static DataTable LayTheo_ngon_ngu_danh_muc_id(int ngon_nguid, int ten_chuyen_muc_phu_thuocid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ten_chuyen_muc_phu_thuocid = new SqlParameter("@ten_chuyen_muc_phu_thuocid", ten_chuyen_muc_phu_thuocid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_CHUYEN_MUC_THONG_TIN_TVSI_By_id_v2", param_ngon_nguid, param_ten_chuyen_muc_phu_thuocid))
            {

                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }




        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="chuyen_muc_tin_tvsiid"></param>
        /// <returns></returns>



        public static DataTable LayTheo_danh_muc_tin_tvsi_id(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_THONG_TIN_TVSI_v2", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable Lay_ten_chuyen_muc_thong_tin_tvsi(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_TVSI_v2", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        
    }
}
