using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_TVSI_v2
    {
        public const string thong_tin_tvsiid = "thong_tin_tvsiid"; public const string chuyen_muc_tin_tvsiid = "chuyen_muc_tin_tvsiid"; public const string nganhid = "nganhid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ngay_dang_tai = "ngay_dang_tai"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string tin_quan_trong = "tin_quan_trong"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string file_dang_tai = "file_dang_tai";

        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_TVSI"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_chuyen_muc_tin_tvsiid(int ngon_nguid, int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_By_chuyen_muc_tin_tvsiid", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_TVSI_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="thong_tin_tvsiid"></param>
        /// <returns></returns>



      
        public static DataTable ChiTiet_THONG_TIN_TVSI(int ngon_nguid, int thong_tin_tvsiid)
        {
           
                SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
                SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                                "TVSI_sSelect_TVSI_THONG_TIN_TVSI", param_ngon_nguid, param_thong_tin_tvsiid))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                } 
        }

   


        public static DataTable THONG_TIN_TVSI_Hotnew(int Count, string Language)
        {
            try
            {
                SqlParameter param_Count = new SqlParameter("@Count", Count);
                SqlParameter param_Language = new SqlParameter("@Language", Language);
                string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
                using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                                "stock_News_GetHotNews", param_Count, param_Language))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }



        public static DataTable THONG_TIN_TVSI_GET_NEWSTVSI(int GroupID, int Count, string Language)
        {

            try
            { 
                SqlParameter param_GroupID = new SqlParameter("@GroupID", GroupID);
                SqlParameter param_Count = new SqlParameter("@Count", Count);
                SqlParameter param_Language = new SqlParameter("@Language", Language);
                string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
                using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                                "stock_News_GetHotNewsinGroup", param_GroupID, param_Count, param_Language))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        // HaiNM create 01-09-2017 lay thong tin tvsi theo thong_tin_tvsiid

        public static DataTable ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(int thong_tin_tvsiid)
        {
            try { 
                    SqlParameter param_thong_tin_tvsiid = new SqlParameter("@thong_tin_tvsiid", thong_tin_tvsiid);
                    using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                                    "TVSI_sSelect_TVSI_THONG_TIN_TVSI_THEO_THONG_TIN_TVSIID", param_thong_tin_tvsiid))
                    {
                        if (dsData.Tables.Count > 0)
                        {
                            return dsData.Tables[0];
                        }
                        else
                        {
                            return new DataTable();
                        }
                    }
            }
            catch
            {
                return null;
            }
        }
        public static DataTable LayTheo_tvsi_map_ngon_ngu_id(int thongtin_ngonnguid, string  name_service)
        {
            SqlParameter param_thongtin_ngonnguid = new SqlParameter("@thongtin_ngonguid", thongtin_ngonnguid);
            SqlParameter param_name_service = new SqlParameter("@name_service", name_service);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_MAP_NGON_NGU_ID_Theo_ThongTin_NgonNguId", param_thongtin_ngonnguid, param_name_service))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
