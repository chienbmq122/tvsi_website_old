
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_CHUYEN_MUC_THONG_TIN_TVSI
    {
        public const string chuyen_muc_tin_tvsiid = "chuyen_muc_tin_tvsiid"; public const string ten_chuyen_muc = "ten_chuyen_muc"; public const string ten_chuyen_muc_phu_thuocid = "ten_chuyen_muc_phu_thuocid"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string hien_thi_tren_trang_chu = "hien_thi_tren_trang_chu"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_CHUYEN_MUC_THONG_TIN_TVSI"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_THONG_TIN_TVSI_By_ngon_nguid", param_ngon_nguid))
            {
               
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable Lay1Chuyenmuc_ra_trang_chu(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_TVSI_CHUYEN_MUC_THONG_TIN_TVSI_TRANGCHU", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else 
                {
                    return new DataTable(); 
                }
            }
        }
        public static DataTable Lay1Chuyenmuc_ra_theo_chuyen_muc_tin_tvsiid(int chuyen_muc_tin_tvsiid)
        {
            SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_TVSI_CHUYEN_MUC_THONG_TIN_TVSI_By_chuyen_muc_tin_tvsiid", param_chuyen_muc_tin_tvsiid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TVSI_CHUYEN_MUC_THONG_TIN_TVSI_THANH_MENU(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_THONG_TIN_TVSI_tren_thanh_tab_menu", param_ngon_nguid))
            {

                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}