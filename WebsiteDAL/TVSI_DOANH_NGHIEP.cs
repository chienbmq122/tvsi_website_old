
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP
    {
        public const string doanh_nghiepid = "doanh_nghiepid"; public const string dia_phuongid = "dia_phuongid"; public const string loai_chung_khoanid = "loai_chung_khoanid"; public const string nganhid = "nganhid"; public const string ma_chung_khoan = "ma_chung_khoan"; public const string von_dieu_le = "von_dieu_le"; public const string von_dieu_le_ban_dau = "von_dieu_le_ban_dau"; public const string ngay_phat_hanh_dau_tien = "ngay_phat_hanh_dau_tien"; public const string tong_so_chung_khoan_phat_hanh = "tong_so_chung_khoan_phat_hanh"; public const string tong_so_chung_khoan_phat_hanh_lan_dau = "tong_so_chung_khoan_phat_hanh_lan_dau"; public const string menh_gia_chung_khoan = "menh_gia_chung_khoan"; public const string niem_yet = "niem_yet"; public const string tong_so_chung_khoan_niem_yet = "tong_so_chung_khoan_niem_yet"; public const string tong_so_chung_khoan_niem_yet_lan_dau = "tong_so_chung_khoan_niem_yet_lan_dau"; public const string ngay_niem_yet = "ngay_niem_yet"; public const string gia_len_san_dau_tien = "gia_len_san_dau_tien"; public const string noi_giao_dich = "noi_giao_dich"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string eps_co_ban = "eps_co_ban";
        public const string ten_doanh_nghiep = "ten_doanh_nghiep";


        public static DataTable LayDanh_sach_cong_ty_niem_yet(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_DOANH_NGHIEP_NIEM_YET", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

       


        public static DataTable Lay_cong_ty_niem_yet_theo_ma_chung_khoan(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_NIEM_YET_THEO_MCK", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayDanh_sach_cong_ty_niem_yet_theo_chu_cai(int ngon_nguid, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_DOANH_NGHIEP_NIEM_YET_BY_KEY_A_Z", param_ngon_nguid, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayDanh_sach_cong_ty_niem_yet_theo_ma_chung_khoan(int ngon_nguid, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sLAY_RA_DOANH_NGHIEP_ID", param_ngon_nguid, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TIN_TUC_THEO_MA_DOANH_NGHIEP(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_TIN_TUC_LIENQUAN_DOANH_NGHIEP_NIEM_YET", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LAY_MA_CHUNG_KHOAN_DOANH_NGHIEP(int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
           
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sLAY_VE_MA_CHUNG_KHOAN_DOANH_NGHIEP_NIEM_YET",param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }    


        public static DataTable LayTheo_noi_giao_dich_VA_ngon_nguid(int ngon_nguid, int noi_giao_dich)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_noi_giao_dich = new SqlParameter("@noi_giao_dich", noi_giao_dich);
             string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
            using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_BY_noi_giao_dich", param_ngon_nguid, param_noi_giao_dich))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Lay_Ma_Chung_Khoan_Theo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
            using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_BY_Ngonnguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        // Doanh nghiep OTC
        public static DataTable LayDanh_sach_cong_ty_otc(int ngon_nguid, int noi_giao_dich)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_noi_giao_dich = new SqlParameter("@noi_giao_dich", noi_giao_dich);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_DOANH_NGHIEP_OTC", param_ngon_nguid,param_noi_giao_dich))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Lay_cong_ty_OTC_theo_ma_chung_khoan(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_OTC_THEO_MCK", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable LayDanh_sach_cong_ty_OTC_theo_chu_cai(int ngon_nguid, string ma_chung_khoan, int noi_giao_dich)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            SqlParameter param_noi_giao_dich = new SqlParameter("@noi_giao_dich", noi_giao_dich);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_DOANH_NGHIEP_OTC_BY_KEY_A_Z", param_ngon_nguid, param_ma_chung_khoan, param_noi_giao_dich))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Lay_Ra_Doanh_Nghiep_ID(int ngon_nguid, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sLAY_RA_DOANH_NGHIEP_ID", param_ngon_nguid, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }



        #region Doanh nghiep cung nganh (thanhtn)
        public static DataTable doanh_nghiep_cung_nganh(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_DOANH_NGHIEP_CUNG_NGANH", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        #endregion

    }
}