using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_HO_TRO_v2
    {

        public const string thong_tin_ho_troid = "thong_tin_ho_troid"; public const string danh_muc_ho_troid = "danh_muc_ho_troid"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string file_dang_tai = "file_dang_tai";
        public const string logo_ho_tro = "logo_ho_tro";
        public const string flash_gioi_thieu_ho_tro = "flash_gioi_thieu_ho_tro";

        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_v2"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_danh_muc_ho_troid(int ngon_nguid, int danh_muc_ho_troid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_ho_troid = new SqlParameter("@danh_muc_ho_troid", danh_muc_ho_troid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_By_danh_muc_hotro_v2", param_ngon_nguid, param_danh_muc_ho_troid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_By_ngon_nguid_v2", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTheo_thong_tin_ho_tro_id(int ngon_nguid, int thong_tin_ho_troid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_ho_troid = new SqlParameter("@thong_tin_ho_troid", thong_tin_ho_troid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_By_thong_tin_ho_tro_v2", param_ngon_nguid, param_thong_tin_ho_troid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable Thong_tin_ho_tro_khac(int ngon_nguid, int thong_tin_spdvid, int danh_muc_spdvid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_ho_troid = new SqlParameter("@thong_tin_ho_troid", thong_tin_ho_troid);
            SqlParameter param_danh_muc_ho_troid = new SqlParameter("@danh_muc_ho_troid", danh_muc_ho_troid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_Khac_v2", param_ngon_nguid, param_thong_tin_ho_troid, param_danh_muc_ho_troid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable LayTheo_thong_tin_ho_tro_id_2017(int thong_tin_ho_troid)
        {
            SqlParameter param_thong_tin_ho_troid = new SqlParameter("@thong_tin_ho_troid", thong_tin_ho_troid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_HO_TRO_By_hotro_id_2017", param_thong_tin_ho_troid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
