﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TVSI.Common;
using TVSI.Common.DAL;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_KIEN_THUC_KINH_NGHIEM
    {
        public static DataTable Search_ky_tu_theo_tieu_de(int ngon_nguid, int danh_muc_kien_thucid, String ky_tu)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_kien_thucid = new SqlParameter("@danh_muc_kien_thucid", danh_muc_kien_thucid);
            SqlParameter param_ky_tu = new SqlParameter("@ky_tu", ky_tu);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_KIEN_THUC_By_ky_tu_theo_tieu_de", param_ngon_nguid, param_danh_muc_kien_thucid, param_ky_tu))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
