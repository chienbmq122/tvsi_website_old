
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_LANH_DAO
    {
        public const string doanh_nghiep_lanh_daoid = "doanh_nghiep_lanh_daoid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ten_lanh_dao = "ten_lanh_dao"; public const string chuc_vu = "chuc_vu"; public const string chuyen_mon_kinh_nghiem = "chuyen_mon_kinh_nghiem"; public const string so_luong_nam_giu = "so_luong_nam_giu"; public const string ti_le_nam_giu = "ti_le_nam_giu"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";


        public static DataTable LayTheo_doanh_nghiepid(int ngon_nguid, int doanh_nghiepid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_NIEM_YET_LANH_DAO", param_ngon_nguid, param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}