using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DANH_MUC_BIEU_PHI_LAI_SUAT
    {
        public const string danh_muc_kien_thucid = "danh_muc_bplsid";
        public const string danh_muc_bplsid = "danh_muc_bplsid";
        public const string ten_dmsp_phu_thuocid = "ten_dmsp_phu_thuocid";
        public const string ten_danh_muc_bpls = "ten_danh_muc_bpls";
        public const string thu_tu_hien_thi = "thu_tu_hien_thi";
        public const string ngon_nguid = "ngon_nguid";
        public const string trang_thai = "trang_thai";
        public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi";
        public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";

        public static DataTable DanhSachDanhMucBieuPhi(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_DanhSachDanhMucBieuPhi", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

      
    }
}
