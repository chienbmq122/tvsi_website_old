
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_FAQ
    {
        public const string thong_tin_faqid = "thong_tin_faqid"; public const string chuyen_muc_faqid = "chuyen_muc_faqid"; public const string tieu_de = "tieu_de"; public const string noi_dung = "noi_dung"; public const string ngay_faq = "ngay_faq"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_FAQ"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTop5_Theo_chuyen_muc_faqid(int ngon_nguid, int chuyen_muc_faqid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_faqid = new SqlParameter("@chuyen_muc_faqid", chuyen_muc_faqid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TOP5_TVSI_THONG_TIN_FAQ_By_chuyen_muc_faqid",param_ngon_nguid , param_chuyen_muc_faqid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ma_chuyen_muc_id(int ngon_nguid, int chuyen_muc_faqid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_faqid = new SqlParameter("@chuyen_muc_faqid", chuyen_muc_faqid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_FAQ_By_ngon_nguid", param_ngon_nguid, param_chuyen_muc_faqid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTin_dau_tien_Theo_ma_chuyen_muc_id(int ngon_nguid, int chuyen_muc_faqid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_faqid = new SqlParameter("@chuyen_muc_faqid", chuyen_muc_faqid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_TOP1_THONG_TIN_FAQ", param_ngon_nguid, param_chuyen_muc_faqid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable Laynoidung_tin_faq(int ngon_nguid, int thong_tin_faqid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_faqid = new SqlParameter("@thong_tin_faqid", thong_tin_faqid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NOI_DUNG_THONG_TIN_FAQ", param_ngon_nguid, param_thong_tin_faqid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}