﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_TRAI_PHIEU
    {
       /// <summary>
       /// Lấy dữ liệu trái phiếu
       /// </summary>
       /// <param name="ngon_nguID">ID ngôn ngữ sử dụng</param>
       /// <param name="key">Chữ cái đầu tiên của mã trái phiếu. Truyền vào "" nếu lấy tất cả mã trái phiếu</param>
       /// <returns></returns>
        public static DataTable LayDanh_sach_trai_phieu_niem_yet(int ngon_nguID, string key)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguID);
            SqlParameter param_ma_trai_phieu = new SqlParameter("@ma_trai_phieu", key);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTRAI_PHIEU_SELECT", param_ngon_nguid,param_ma_trai_phieu))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Lấy ra tên chuyên mục tin tức trái phiếu theo ID
        /// Nếu ID= 0 thì lấy tất cả các chuyên mục tin về trái phiếu
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="chuyen_muc_tinid"></param>
        /// <returns></returns>
        public static DataTable LayDanhSach_ChuyenMuc_TinTraiPhieu(int ngon_nguid, int chuyen_muc_tinid)
        {            
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sCHUYEN_MUC_TRAI_PHIEU_SELECT", param_ngon_nguid, param_chuyen_muc_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Lấy danh sách các tin theo chuyên mụcID, và trang hiện tại
        /// Truyền vào trang hiện tại -1 nếu muốn lấy số row trả về
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="chuyen_muc_tinid"></param>
        /// <returns></returns>
        public static DataTable LayDanhSach_TinTraiPhieuTheoChuyenMucID(int ngon_nguid, int chuyen_muc_tinid, int current_Page, int return_Rows)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            SqlParameter param_current_Page = new SqlParameter("@current_Page", current_Page);
            SqlParameter param_return_Rows = new SqlParameter("@return_Rows", return_Rows);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sDANH_SACH_THONG_TIN_TRAI_PHIEU_BY_PAGE_SELECT", param_ngon_nguid,
                                                                                                                   param_chuyen_muc_tinid,
                                                                                                                   param_current_Page,
                                                                                                                   param_return_Rows))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Lấy chi tiết tin tức trái phiếu
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="chuyen_muc_tinid"></param>
        /// <param name="tin_tucid"></param>
        /// <returns></returns>
        public static DataTable LayChiTiet_TinTuc_TraiPhieutTheoIDTinTuc(int ngon_nguid, int chuyen_muc_tinid, int tin_tucid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            SqlParameter param_tin_tuc_id = new SqlParameter("@tin_tuc_id", tin_tucid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTHONG_TIN_TRAI_PHIEU", param_ngon_nguid,
                                                                                          param_chuyen_muc_tinid,
                                                                                          param_tin_tuc_id
                                                                                          ))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Lấy chi tiết thông tin trái phiếu
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="ma_trai_phieu"></param>
        /// <returns></returns>
        public static DataTable LayChiTiet_TraiPhieu(int ngon_nguid, string ma_trai_phieu)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_trai_phieu = new SqlParameter("@ma_trai_phieu", ma_trai_phieu);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTRAI_PHIEU_CHI_TIET_SELECT", param_ngon_nguid,
                                                                                       param_ma_trai_phieu
                                                                                          ))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Tìm kiếm chi tiết thông tin trái phiếu
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="ma_trai_phieu"></param>
        /// <returns></returns>
        public static DataTable TimKiemThongTin_TraiPhieu(int ngon_nguid, string ma_trai_phieu)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_trai_phieu = new SqlParameter("@ma_trai_phieu", ma_trai_phieu);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTRAI_PHIEU_SELECT", param_ngon_nguid,
                                                                                       param_ma_trai_phieu
                                                                                          ))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Lấy danh sách các tin liên quan cho trái phiếu
        /// </summary>
        /// <param name="ngon_nguid"></param>
        /// <param name="ma_trai_phieu"></param>
        /// <returns></returns>
        public static DataTable LayTinLienQuan_TraiPhieu(int ngon_nguid, string ma_trai_phieu)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_ma_trai_phieu = new SqlParameter("@ma_trai_phieu", ma_trai_phieu);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTHONG_TIN_LIEN_QUAN_TRAI_PHIEU_SELECT", param_ngon_nguid,
                                                                                       param_ma_trai_phieu
                                                                                          ))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        /// <summary>
        /// Map nơi niêm yết trái phiếu
        /// </summary>
        /// <param name="noiNiemYetID"></param>
        /// <returns></returns>
        public static string Map_NoiNiemYet(object noiNiemYetID)
        {
            switch (noiNiemYetID.ToString())
            {
                case "1":
                    return "HSX";
                case "2":
                    return "HNX";
                case "3":
                    return "UPCOM";
                case "4":
                    return "OTC";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Format cho dữ liệu số hiển thị
        /// </summary>
        /// <param name="sKhoiLuong"></param>
        /// <returns></returns>
        public static string Format_Number(object objNumber)
        {
            if(objNumber == null || objNumber.ToString().Equals(""))
                return string.Empty;
            else
                return string.Format("{0:#,0}", TVSI_fGetDecimal(objNumber));
        }

        /// <summary>
        /// Get Decimal number
        /// </summary>
        /// <param name="inObj"></param>
        /// <returns></returns>
        public static decimal TVSI_fGetDecimal(object inObj)
        {
            try
            {
                return Convert.ToDecimal(inObj.ToString().Replace(",", ""));
            }
            catch
            {
                return 0;
            }
        }
    }
}
