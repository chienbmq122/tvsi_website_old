using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;

using TVSI.Common;

namespace TVSI.WebsiteDAL
{
    public class TVSI_TIM_KIEM_THONG_TIN
    {
        public static DataTable TimkiemThongtinCongbo(int ngon_nguid,
                                                int nganhid,
                                                string ma_chung_khoan,
                                                string ten_doanh_nghiep,
                                                string tu_khoa, 
                                                bool tim_kiem_co_ban)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", Lib.FixSQLInjection(ma_chung_khoan));
            SqlParameter param_ten_doanh_nghiep = new SqlParameter("@ten_doanh_nghiep", Lib.FixSQLInjection(ten_doanh_nghiep));
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));
            SqlParameter param_tim_kiem_co_ban = new SqlParameter("@tim_kiem_co_ban", (tim_kiem_co_ban ? 1 : 0));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_CONG_BO", param_ngon_nguid,
                                                                                                param_nganhid,
                                                                                                param_ma_chung_khoan,
                                                                                                param_ten_doanh_nghiep,
                                                                                                param_tu_khoa,
                                                                                                param_tim_kiem_co_ban))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinSukien(int ngon_nguid,
                                                int nganhid,
                                                string ma_chung_khoan,
                                                string ten_doanh_nghiep,
                                                string tu_khoa,
                                                bool tim_kiem_co_ban)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", Lib.FixSQLInjection(ma_chung_khoan));
            SqlParameter param_ten_doanh_nghiep = new SqlParameter("@ten_doanh_nghiep", Lib.FixSQLInjection(ten_doanh_nghiep));
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));
            SqlParameter param_tim_kiem_co_ban = new SqlParameter("@tim_kiem_co_ban", (tim_kiem_co_ban ? 1 : 0));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_SU_KIEN", param_ngon_nguid,
                                                                                                param_nganhid,
                                                                                                param_ma_chung_khoan,
                                                                                                param_ten_doanh_nghiep,
                                                                                                param_tu_khoa,
                                                                                                param_tim_kiem_co_ban))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinTVSI(int ngon_nguid,
                                                int nganhid,
                                                string ma_chung_khoan,
                                                string ten_doanh_nghiep,
                                                string tu_khoa,
                                                bool tim_kiem_co_ban)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", Lib.FixSQLInjection(ma_chung_khoan));
            SqlParameter param_ten_doanh_nghiep = new SqlParameter("@ten_doanh_nghiep", Lib.FixSQLInjection(ten_doanh_nghiep));
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));
            SqlParameter param_tim_kiem_co_ban = new SqlParameter("@tim_kiem_co_ban", (tim_kiem_co_ban ? 1 : 0));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_TVSI", param_ngon_nguid,
                                                                                                param_nganhid,
                                                                                                param_ma_chung_khoan,
                                                                                                param_ten_doanh_nghiep,
                                                                                                param_tu_khoa,
                                                                                                param_tim_kiem_co_ban))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinKienthucCK(int ngon_nguid,
                                                string tu_khoa)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_KIEN_THUC_CHUNG_KHOAN", param_ngon_nguid,
                                                                                                param_tu_khoa))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinKienthucLuat(int ngon_nguid,
                                                string tu_khoa)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_KIEN_THUC_LUAT", param_ngon_nguid,
                                                                                                param_tu_khoa))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinNhandinhThitruong(int ngon_nguid,
                                                string tu_khoa)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_NHAN_DINH_THI_TRUONG", param_ngon_nguid,
                                                                                                param_tu_khoa))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemThongtinThuatnguCK(int ngon_nguid,
                                                string tu_khoa)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa", Lib.FixSQLInjection(tu_khoa));

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemThongtin_THONG_TIN_THUAT_NGU_CHUNG_KHOAN", param_ngon_nguid,
                                                                                                param_tu_khoa))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
