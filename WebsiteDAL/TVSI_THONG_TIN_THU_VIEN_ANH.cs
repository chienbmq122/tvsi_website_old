﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_THU_VIEN_ANH
    {

        public static string ActionQuery_Cms_Admin()
        {
                return ConfigurationManager.ConnectionStrings["ActionQuery_Cms_Admin"].ConnectionString;
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@language_id", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(TVSI_THONG_TIN_THU_VIEN_ANH.ActionQuery_Cms_Admin(), CommandType.StoredProcedure,
                                                            "TVSI_sSelect_THONG_TIN_DANH_MUC_ANH_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
