using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
   	

    public class TVSI_THONG_TIN_BIEU_PHI_LAI_SUAT
    {
        public const string thong_tin_bplsid = "thong_tin_bplsid";
        public const string danh_muc_bplsid = "danh_muc_bplsid";
        public const string tieu_de = "tieu_de";
        public const string tom_tat = "tom_tat";
        public const string logo_bpls = "logo_bpls";
        public const string flash_bpls = "flash_bpls";
        public const string noi_dung = "noi_dung";
        public const string thu_tu_hien_thi = "thu_tu_hien_thi";
        public const string file_dinh_kem = "file_dinh_kem";
        public const string ngon_nguid = "ngon_nguid";
        public const string trang_thai = "trang_thai";
        public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi";
        public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";

        public static DataTable DanhSachThongTinBieuPhi(int ngon_nguid, int danh_muc_bplsid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_bplsid = new SqlParameter("@danh_muc_bplsid", danh_muc_bplsid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_DanhSachThongTinBieuPhi", param_ngon_nguid, param_danh_muc_bplsid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable ChiTietThongTinBieuPhi(int ngon_nguid, int danh_muc_bplsid, int thong_tin_bplsid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_bplsid = new SqlParameter("@danh_muc_bplsid", danh_muc_bplsid);
            SqlParameter param_thong_tin_bplsid = new SqlParameter("@thong_tin_bplsid", thong_tin_bplsid);


            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_ChiTietThongTinBieuPhi", param_ngon_nguid, param_danh_muc_bplsid, param_thong_tin_bplsid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable DanhSachThongTinBieuPhiKhac(int ngon_nguid, int danh_muc_bplsid, int thong_tin_bplsid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_bplsid = new SqlParameter("@danh_muc_bplsid", danh_muc_bplsid);
            SqlParameter param_thong_tin_bplsid = new SqlParameter("@thong_tin_bplsid", thong_tin_bplsid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_DanhSachThongTinBieuPhiKhac", param_ngon_nguid, param_danh_muc_bplsid, param_thong_tin_bplsid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }



    }
}
