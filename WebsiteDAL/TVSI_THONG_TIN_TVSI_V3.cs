﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;

namespace TVSI.WebsiteDAL
{
   public class TVSI_THONG_TIN_TVSI_V3
    {
       public static DataTable LayTheo_chuyen_muc_tin_tvsiid(int ngon_nguid, int chuyen_muc_tin_tvsiid)
       {
           SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
           SqlParameter param_chuyen_muc_tin_tvsiid = new SqlParameter("@chuyen_muc_tin_tvsiid", chuyen_muc_tin_tvsiid);
           using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                           "TVSI_sSelect_TVSI_THONG_TIN_TVSI_By_chuyen_muc_tin_tvsiid_v3", param_ngon_nguid, param_chuyen_muc_tin_tvsiid))
           {
               if (dsData.Tables.Count > 0)
               {
                   return dsData.Tables[0];
               }
               else
               {
                   return new DataTable();
               }
           }
       }
    
    public static DataTable THONG_TIN_TVSI_GET_NEWSTVSI_BY_PAGING(int GroupID, int PageNumber,int PageSize, int LanguageId)
        {
        SqlParameter param_GroupID = new SqlParameter("@GroupID", GroupID);
        SqlParameter param_PageNumber = new SqlParameter("@PageNumber", PageNumber);
        SqlParameter param_PageSize = new SqlParameter("@PageSize", PageSize);
        SqlParameter param_LanguageId = new SqlParameter("@ngon_nguid", LanguageId);
            string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
            using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                            "stock_News_GetHotNewsinGroupByPaging", param_GroupID, param_PageNumber, param_PageSize, param_LanguageId))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    public static DataTable TVSI_Stock_Markets_GetTodayQuotes(String symbol)
    {
        SqlParameter param_exchange = new SqlParameter("@Symbol", symbol);
        string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
        using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                        "stock_Stocks_GetTodayQuotes", param_exchange))
        {
            if (dsData.Tables.Count > 0)
            {
                return dsData.Tables[0];
            }
            else
            {
                return new DataTable();
            }
        }
    }
    public static DataTable LayMachungkhoan(String symbol)
    {
        SqlParameter param_symbol = new SqlParameter("@Symbol", symbol);
        string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
        using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                        "TVSI_sAutoComplate_ForHomeNews", param_symbol))
        {
            if (dsData.Tables.Count > 0)
            {
                return dsData.Tables[0];
            }
            else
            {
                return new DataTable();
            }
        }
    }
    public static DataTable LayMachungkhoanEn(String symbol)
    {
        SqlParameter param_symbol = new SqlParameter("@Symbol", symbol);
        string connectionString = ConfigurationManager.ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString;
        using (DataSet dsData = SqlHelper.ExecuteDataset(connectionString, CommandType.StoredProcedure,
                                                        "TVSI_sAutoComplate_ForHomeNewsEN", param_symbol))
        {
            if (dsData.Tables.Count > 0)
            {
                return dsData.Tables[0];
            }
            else
            {
                return new DataTable();
            }
        }
    }


    }  
}
