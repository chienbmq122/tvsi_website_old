using System;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using System.Web.UI;

namespace TVSI.WebsiteDAL
{
    public class TVSI_LANGUAGE_KEY
    {
        public static string LayMoTaNgonNguTheoMaster(MasterPage master, string key)
        {
            LanguageManager ngon_ngu = new LanguageManager(master);
            return ngon_ngu.GetString(key);
        }

        public static string LayMoTaNgonNguTheoPage(Page page, string key)
        {
            LanguageManager ngon_ngu = new LanguageManager(page);
            return ngon_ngu.GetString(key);
        }
    }
}
