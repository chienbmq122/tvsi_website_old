
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_CONG_BO
    {
        public const string tin_tuc_id = "tin_tuc_id"; public const string chuyen_muc_tinid = "chuyen_muc_tinid"; public const string nganhid = "nganhid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string ngay_dang_tai = "ngay_dang_tai"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string nguon_dang_tai = "nguon_dang_tai"; public const string tin_quan_trong = "tin_quan_trong"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string file_dang_tai = "file_dang_tai";
        
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_CONG_BO"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_chuyen_muc_tinid(int ngon_nguid, int chuyen_muc_tinid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO_By_chuyen_muc_tinid", param_ngon_nguid, param_chuyen_muc_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_THONG_TIN_CONG_BO_TRANG_CHU(int ngon_nguid, int chuyen_muc_tinid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO_TRANGCHU", param_ngon_nguid, param_chuyen_muc_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTVSI_THONG_TIN_CONG_BO_Theo_tin_tuc_id(int ngon_nguid, int tin_tuc_id)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_tin_tuc_id = new SqlParameter("@tin_tuc_id", tin_tuc_id);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO", param_ngon_nguid, param_tin_tuc_id))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_THONG_TIN_CONG_BO_Lay_ra_1_tin_chuyen_muc_tinid(int ngon_nguid, int chuyen_muc_tinid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_tinid = new SqlParameter("@chuyen_muc_tinid", chuyen_muc_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO_CHITIET_TIN_DAUTIEN", param_ngon_nguid, param_chuyen_muc_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Layra_top10_TIN_THONG_TIN_CONG_BO_TVSI(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_CONG_BO", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTVSI_THONG_TIN_CONG_BO_NGANH(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_CONG_BO_THEO_NGANH", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_THONG_TIN_CONG_BO_NGANH_Theo_nganhid(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP5_THONG_TIN_CONG_BO_NGANH_Theo_nganhid", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_TOP_5_THONG_TIN_CONG_BO_MOI_NHAT(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_TOP_5_THONG_TIN_CONG_BO_MOI_NHAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTVSI_TOP_5_THONG_TIN_CONG_BO_MOI_NHAT_TRANG_CHU(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_TOP_5_THONG_TIN_CONG_BO_MOI_NHAT_TRANG_CHU", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }




    }
}