
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_BAO_CAO_NGANH
    {
        public static DataTable LayTopTheoNganhId(int ngon_nguid, int nganhid, int top)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", 0);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", 0);
            SqlParameter param_thang_bao_cao = new SqlParameter("@thang_bao_cao", 0);
            SqlParameter param_top = new SqlParameter("@top", top);
            
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_BAO_CAO_NGANH_Theo_nganhid", param_ngon_nguid, 
                                                                                                            param_nganhid,
                                                                                                            param_nam_bao_cao,
                                                                                                            param_quy_bao_cao,
                                                                                                            param_thang_bao_cao,
                                                                                                            param_top))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Timkiem(int ngon_nguid, int nganhid, int nam_bao_cao, int quy_bao_cao, int thang_bao_cao)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            SqlParameter param_thang_bao_cao = new SqlParameter("@thang_bao_cao", thang_bao_cao);
            SqlParameter param_top = new SqlParameter("@top", 0);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_BAO_CAO_NGANH_Theo_nganhid", param_ngon_nguid,
                                                                                                            param_nganhid,
                                                                                                            param_nam_bao_cao,
                                                                                                            param_quy_bao_cao,
                                                                                                            param_thang_bao_cao,
                                                                                                            param_top))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayDanhSachFileBaoCao(int bao_cao_nganhid)
        {
            SqlParameter param_bao_cao_nganhid = new SqlParameter("@bao_cao_nganhid", bao_cao_nganhid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_FILES_Theo_bao_cao_nganhid", param_bao_cao_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
