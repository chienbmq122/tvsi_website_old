
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_CHI_SO_TAI_CHINH
    {
        public const string ti_le_tra_co_tuc_gan_nhat = "ti_le_tra_co_tuc_gan_nhat";
  
        public static DataTable CHISOTAICHINH_DOANH_NGHIEP_XEM_THEO_TUY_CHON(int doanh_nghiepid, int thang_bao_cao, int quy_bao_cao, int nam_bao_cao)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            SqlParameter param_thang_bao_cao = new SqlParameter("@thang_bao_cao", thang_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_CHI_SO_TAI_CHINH_XEM_THEO_TUY_CHON", param_doanh_nghiepid, param_thang_bao_cao,param_quy_bao_cao,param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable NAM_BAO_CAO_TAI_CHINH_THEO_MA_DOANH_NGHIEP(int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_CHI_SO_TAI_CHINH_THEO_NAM_BAO_CAO",param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        #region Finance Index Comparison (thanhtn - 2009Jan10)
        public static DataTable GET_BAO_CAO_TAI_CHINH_Gan_nhat(int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_BAO_CAO_TAI_CHINH_GAN_NHAT", param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable GET_BAO_CAO_TAI_CHINH_Theo_Nam(string doanh_nghiepids, int nam_bao_cao)
        {
            SqlParameter param_doanh_nghiepids = new SqlParameter("@doanh_nghiepids", doanh_nghiepids);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_BAO_CAO_TAI_CHINH_THEO_NAM", param_doanh_nghiepids, param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable GET_BAO_CAO_TAI_CHINH_Theo_Quy(string doanh_nghiepids, int quy_bao_cao, int nam_bao_cao)
        {
            SqlParameter param_doanh_nghiepids = new SqlParameter("@doanh_nghiepids", doanh_nghiepids);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_BAO_CAO_TAI_CHINH_THEO_QUY", param_doanh_nghiepids, param_quy_bao_cao, param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable GET_CHI_TIEU_TAI_CHINH_Gan_nhat(int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_CHI_TIEU_TAI_CHINH_GAN_NHAT", param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable GET_CHI_TIEU_TAI_CHINH_Theo_Nam(string doanh_nghiepids, int nam_bao_cao)
        {
            SqlParameter param_doanh_nghiepids = new SqlParameter("@doanh_nghiepids", doanh_nghiepids);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_CHI_TIEU_TAI_CHINH_THEO_NAM", param_doanh_nghiepids, param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable GET_CHI_TIEU_TAI_CHINH_Theo_Quy(string doanh_nghiepids, int quy_bao_cao, int nam_bao_cao)
        {
            SqlParameter param_doanh_nghiepids = new SqlParameter("@doanh_nghiepids", doanh_nghiepids);
            SqlParameter param_nam_bao_cao = new SqlParameter("@nam_bao_cao", nam_bao_cao);
            SqlParameter param_quy_bao_cao = new SqlParameter("@quy_bao_cao", quy_bao_cao);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "sTVSI_sSelect_CHI_TIEU_TAI_CHINH_THEO_QUY", param_doanh_nghiepids, param_quy_bao_cao, param_nam_bao_cao))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        #endregion


    }
}