﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;


namespace TVSI.WebsiteDAL
{
    public class TVSI_DU_LIEU_BIEU_DO_V3
    {
        public static DataTable LayKetQuaGiaoDichThiTruongGanNhat()
        {

            try
            {
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_KET_QUA_GIAO_DICH_THI_TRUONG_v2"))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        public static DataTable LayThongTinThiTruongUpcom()
        {

            try
            {
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sTHONG_TIN_THI_TRUONG_UPCOM_v3"))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        public static DataTable LayKetQuaGiaoDichThiTruongUpcom()
        {

            try
            {
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sKET_QUA_GIAO_DICH_THI_TRUONG_UPCOM_v3"))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
