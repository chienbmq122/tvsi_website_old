
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_KIEN_THUC_PHAP_LUAT
    {
        public const string kien_thuc_phap_luatid = "kien_thuc_phap_luatid"; public const string so_hieu_van_ban = "so_hieu_van_ban"; public const string ngay_ban_hanh = "ngay_ban_hanh"; public const string co_quan_ban_hanh = "co_quan_ban_hanh"; public const string ten_van_ban = "ten_van_ban"; public const string tom_tat = "tom_tat"; public const string duong_dan_file_noi_dung = "duong_dan_file_noi_dung"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_KIEN_THUC_PHAP_LUAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimKiem_Kien_Thuc_Phap_Luat(int ngon_nguid, int loai_van_banid, int co_quan_ban_hanhid, string so_hieu_van_ban, string tu_khoa, int tu_khoa_chinh_xac)
        {
            //        @ngon_nguid int,
            //@loai_van_banid int,
            //@co_quan_ban_hanhid int,
            //@so_hieu_van_ban nvarchar(100),
            //@tu_khoa nvarchar(100),
            //@tu_khoa_chinh_xac int
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_loai_van_banid = new SqlParameter("@loai_van_banid", loai_van_banid);

            SqlParameter param_co_quan_ban_hanhid = new SqlParameter("@co_quan_ban_hanhid", co_quan_ban_hanhid);
            SqlParameter param_so_hieu_van_ban = new SqlParameter("@so_hieu_van_ban", Lib.FixSQLInjection(so_hieu_van_ban));

            SqlParameter param_tu_khoa = new SqlParameter("@tu_khoa",Lib.FixSQLInjection(tu_khoa));
            SqlParameter param_tu_khoa_chinh_xac = new SqlParameter("@tu_khoa_chinh_xac", tu_khoa_chinh_xac);




            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sFind_TVSI_THONG_TIN_KIEN_THUC_PHAP_LUAT", param_ngon_nguid, param_loai_van_banid, param_co_quan_ban_hanhid,param_so_hieu_van_ban,param_tu_khoa, param_tu_khoa_chinh_xac))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

    }
}