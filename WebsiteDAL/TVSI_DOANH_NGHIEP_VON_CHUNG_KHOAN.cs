
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DOANH_NGHIEP_VON_CHUNG_KHOAN
    {
        public const string doanh_nghiep_von_chung_khoanid = "doanh_nghiep_von_chung_khoanid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string von_dieu_le_bo_sung = "von_dieu_le_bo_sung"; public const string so_chung_khoan_phat_hanh_them = "so_chung_khoan_phat_hanh_them"; public const string so_chung_khoan_niem_yet_them = "so_chung_khoan_niem_yet_them"; public const string ngay_bo_sung = "ngay_bo_sung"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        
        public static DataTable LayTheo_doanh_nghiepid(int doanh_nghiepid)
        {
            SqlParameter param_doanh_nghiepid = new SqlParameter("@doanh_nghiepid", doanh_nghiepid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_DOANH_NGHIEP_NIEM_YET_VON_CHUNG_KHOAN", param_doanh_nghiepid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}