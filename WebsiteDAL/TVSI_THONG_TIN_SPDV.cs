using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_SPDV
    {
        public const string thong_tin_spdvid = "thong_tin_spdvid"; public const string danh_muc_spdvid = "danh_muc_spdvid"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public const string file_dang_tai = "file_dang_tai";
        public const string logo_spdv = "logo_spdv";
        public const string flash_gioi_thieu_spdv = "flash_gioi_thieu_spdv";

        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_SPDV"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_danh_muc_spdvid(int ngon_nguid, int danh_muc_spdvid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_spdvid = new SqlParameter("@danh_muc_spdvid", danh_muc_spdvid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SPDV_By_danh_muc_spdvid", param_ngon_nguid, param_danh_muc_spdvid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SPDV_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTheo_thong_tin_spdvid(int ngon_nguid, int thong_tin_spdvid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_spdvid = new SqlParameter("@thong_tin_spdvid", thong_tin_spdvid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SPDV_By_thong_tin_spdvid", param_ngon_nguid, param_thong_tin_spdvid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable Thong_tin_san_pham_dv_khac(int ngon_nguid, int thong_tin_spdvid, int danh_muc_spdvid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_thong_tin_spdvid = new SqlParameter("@thong_tin_spdvid", thong_tin_spdvid);
            SqlParameter param_danh_muc_spdvid = new SqlParameter("@danh_muc_spdvid", danh_muc_spdvid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SPDV_Khac", param_ngon_nguid, param_thong_tin_spdvid, param_danh_muc_spdvid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }




    }
}