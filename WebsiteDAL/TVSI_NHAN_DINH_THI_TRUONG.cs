
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_NHAN_DINH_THI_TRUONG
    {
        public const string nhan_dinh_thi_truongid = "nhan_dinh_thi_truongid"; public const string tieu_de = "tieu_de"; public const string tom_tat = "tom_tat"; public const string noi_dung = "noi_dung"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
       
        public static DataTable LayTheo_ngon_nguid(int ngon_nguid, int danh_muc_san_pham)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_san_pham = new SqlParameter("@danh_muc_san_pham", danh_muc_san_pham);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NHAN_DINH_THI_TRUONG_By_ngon_nguid", param_ngon_nguid,param_danh_muc_san_pham))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable Layra_tin_dau_tien(int ngon_nguid, int danh_muc_san_pham)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_danh_muc_san_pham = new SqlParameter("@danh_muc_san_pham", danh_muc_san_pham);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NHAN_DINH_THI_TRUONG_TIN_DAU_TIEN", param_ngon_nguid,param_danh_muc_san_pham))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Layra_chi_tiet_tin(int ngon_nguid, int nhan_dinh_thi_truongid)
        {
            
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nhan_dinh_thi_truongid = new SqlParameter("@nhan_dinh_thi_truongid", nhan_dinh_thi_truongid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NHAN_DINH_THI_TRUONG_CHI_TIET_TIN", param_ngon_nguid, param_nhan_dinh_thi_truongid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable Lay5tin_moi_nhat(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TOP5_NHAN_DINH_THI_TRUONG_MOI_NHAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTinMoiNhat_TrangChu(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sPhanTichNhanDinh_TrangChu", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }

        }

        public static DataTable Lay2TinMoiNhat_Tiep_Theo_TrangChu(int ngon_nguid, int nhan_dinh_thi_truongid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nhan_dinh_thi_truongid = new SqlParameter("@nhan_dinh_thi_truongid", nhan_dinh_thi_truongid);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sNHAN_DINH_THI_TRUONG_TIN_KHAC", param_ngon_nguid,param_nhan_dinh_thi_truongid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }

        }


    }
}