
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_CHUYEN_MUC_FAQ
    {
        public const string chuyen_muc_faqid = "chuyen_muc_faqid"; public const string ten_chuyen_muc = "ten_chuyen_muc"; public const string ten_chuyen_muc_phu_thuocid = "ten_chuyen_muc_phu_thuocid"; public const string thu_tu_hien_thi = "thu_tu_hien_thi"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable Layra_1_chuyen_muc_theo_ma_chuyen_muc(int ngon_nguid, int chuyen_muc_faqid)
        {
            SqlParameter param_chuyen_muc_faqid = new SqlParameter("@chuyen_muc_faqid", chuyen_muc_faqid);
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_CHUYEN_MUC_FAQ_by_id",param_ngon_nguid, param_chuyen_muc_faqid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHUYEN_MUC_FAQ_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}