
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI.WebControls;

namespace TVSI.WebsiteDAL
{
    public class TVSI_NGON_NGU
    {
        #region Connection Data

        public static SqlConnection SqlConn()
        {
            SqlConnection SqlConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SelectQuery_Website"].ConnectionString.ToString());
            return SqlConn;
        }
        #endregion Connection Date


        public const string ngon_nguid = "ngon_nguid"; public const string ma_ngon_ngu = "ma_ngon_ngu"; public const string ten_ngon_ngu = "ten_ngon_ngu"; public const string anh_ngon_ngu = "anh_ngon_ngu"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_NGON_NGU"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable CreateDataTable(string strSQL)
        {
            SqlDataAdapter Adapter = new SqlDataAdapter(strSQL, SqlConn());
            SqlConn().Open();
            DataTable dt = new DataTable();
            try
            {
                Adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            finally
            {
                SqlConn().Close();
                SqlConn().Dispose();
            }
            return dt;
        }


    }
}