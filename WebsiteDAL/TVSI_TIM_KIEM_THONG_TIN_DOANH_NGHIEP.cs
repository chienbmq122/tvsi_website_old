using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;

using TVSI.Common;

namespace TVSI.WebsiteDAL
{
    public class TVSI_TIM_KIEM_THONG_TIN_DOANH_NGHIEP
    {
        public const string ma_chung_khoan = "ma_chung_khoan";
        public const string ten_doanh_nghiep = "ten_doanh_nghiep";
        public const string eps_co_ban = "eps_co_ban";
        public const string eps_dieu_chinh = "eps_dieu_chinh";
        public const string doanh_nghiepid = "doanh_nghiepid";

        public static DataTable TimkiemCoban(int ngon_nguid, int noi_giao_dich, string ma_chung_khoan)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_noi_giao_dich = new SqlParameter("@noi_giao_dich", noi_giao_dich);
            SqlParameter param_ma_chung_khoan = new SqlParameter("@ma_chung_khoan", ma_chung_khoan);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemCoban_DOANH_NGHIEP", param_ngon_nguid, param_noi_giao_dich, param_ma_chung_khoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TimkiemNangCao(int ngon_nguid,
                                                int nganhid,
                                                string ten_doanh_nghiep,
                                                string hoat_dong_kinh_doanh,
                                                decimal von_dieu_le1,
                                                decimal von_dieu_le2,
                                                decimal von_chu_so_huu1,
                                                decimal von_chu_so_huu2,
                                                decimal doanh_thu1,
                                                decimal doanh_thu2,
                                                decimal loi_nhuan_gop1,
                                                decimal loi_nhuan_gop2,
                                                decimal loi_nhuan_rong1,
                                                decimal loi_nhuan_rong2,
                                                decimal tong_tai_san1,
                                                decimal tong_tai_san2,
                                                decimal eps_co_ban1,
                                                decimal eps_co_ban2,
                                                decimal eps_dieu_chinh1,
                                                decimal eps_dieu_chinh2,
                                                decimal ti_le_tra_co_tuc_gan_nhat1,
                                                decimal ti_le_tra_co_tuc_gan_nhat2,
                                                decimal roa1,
                                                decimal roa2,
                                                decimal roe1,
                                                decimal roe2,
                                                decimal ty_suat_loi_nhuan_gop1,
                                                decimal ty_suat_loi_nhuan_gop2,
                                                decimal ty_suat_loi_nhuan_thuan1,
                                                decimal ty_suat_loi_nhuan_thuan2,
                                                decimal ty_suat_loi_nhuan_truoc_thue_doanh_thu1,
                                                decimal ty_suat_loi_nhuan_truoc_thue_doanh_thu2,
                                                decimal ty_suat_loi_nhuan_sau_thue_doanh_thu1,
                                                decimal ty_suat_loi_nhuan_sau_thue_doanh_thu2,
                                                decimal p_e_co_ban1,
                                                decimal p_e_co_ban2,
                                                decimal p_e_dieu_chinh1,
                                                decimal p_e_dieu_chinh2,
                                                decimal p_b_price_per_book1,
                                                decimal p_b_price_per_book2)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            SqlParameter param_ten_doanh_nghiep = new SqlParameter("@ten_doanh_nghiep", Lib.FixSQLInjection(ten_doanh_nghiep));
            SqlParameter param_hoat_dong_kinh_doanh = new SqlParameter("@hoat_dong_kinh_doanh", Lib.FixSQLInjection(hoat_dong_kinh_doanh));

            SqlParameter param_von_dieu_le1 = new SqlParameter("@von_dieu_le1", von_dieu_le1);
            SqlParameter param_von_dieu_le2 = new SqlParameter("@von_dieu_le2", von_dieu_le2);

            SqlParameter param_von_chu_so_huu1 = new SqlParameter("@von_chu_so_huu1", von_chu_so_huu1);
            SqlParameter param_von_chu_so_huu2 = new SqlParameter("@von_chu_so_huu2", von_chu_so_huu2);

            SqlParameter param_doanh_thu1 = new SqlParameter("@doanh_thu1", doanh_thu1);
            SqlParameter param_doanh_thu2 = new SqlParameter("@doanh_thu2", doanh_thu2);

            SqlParameter param_loi_nhuan_gop1 = new SqlParameter("@loi_nhuan_gop1", loi_nhuan_gop1);
            SqlParameter param_loi_nhuan_gop2 = new SqlParameter("@loi_nhuan_gop2", loi_nhuan_gop2);

            SqlParameter param_loi_nhuan_rong1 = new SqlParameter("@loi_nhuan_rong1", loi_nhuan_rong1);
            SqlParameter param_loi_nhuan_rong2 = new SqlParameter("@loi_nhuan_rong2", loi_nhuan_rong2);

            SqlParameter param_tong_tai_san1 = new SqlParameter("@tong_tai_san1", tong_tai_san1);
            SqlParameter param_tong_tai_san2 = new SqlParameter("@tong_tai_san2", tong_tai_san2);

            SqlParameter param_eps_co_ban1 = new SqlParameter("@eps_co_ban1", eps_co_ban1);
            SqlParameter param_eps_co_ban2 = new SqlParameter("@eps_co_ban2", eps_co_ban2);

            SqlParameter param_eps_dieu_chinh1 = new SqlParameter("@eps_dieu_chinh1", eps_dieu_chinh1);
            SqlParameter param_eps_dieu_chinh2 = new SqlParameter("@eps_dieu_chinh2", eps_dieu_chinh2);

            SqlParameter param_ti_le_tra_co_tuc_gan_nhat1 = new SqlParameter("@ti_le_tra_co_tuc_gan_nhat1", ti_le_tra_co_tuc_gan_nhat1);
            SqlParameter param_ti_le_tra_co_tuc_gan_nhat2 = new SqlParameter("@ti_le_tra_co_tuc_gan_nhat2", ti_le_tra_co_tuc_gan_nhat2);

            SqlParameter param_roa1 = new SqlParameter("@roa1", roa1);
            SqlParameter param_roa2 = new SqlParameter("@roa2", roa2);

            SqlParameter param_roe1 = new SqlParameter("@roe1", roe1);
            SqlParameter param_roe2 = new SqlParameter("@roe2", roe2);

            SqlParameter param_ty_suat_loi_nhuan_gop1 = new SqlParameter("@ty_suat_loi_nhuan_gop1", ty_suat_loi_nhuan_gop1);
            SqlParameter param_ty_suat_loi_nhuan_gop2 = new SqlParameter("@ty_suat_loi_nhuan_gop2", ty_suat_loi_nhuan_gop2);

            SqlParameter param_ty_suat_loi_nhuan_thuan1 = new SqlParameter("@ty_suat_loi_nhuan_thuan1", ty_suat_loi_nhuan_thuan1);
            SqlParameter param_ty_suat_loi_nhuan_thuan2 = new SqlParameter("@ty_suat_loi_nhuan_thuan2", ty_suat_loi_nhuan_thuan2);

            SqlParameter param_ty_suat_loi_nhuan_truoc_thue_doanh_thu1 = new SqlParameter("@ty_suat_loi_nhuan_truoc_thue_doanh_thu1", ty_suat_loi_nhuan_truoc_thue_doanh_thu1);
            SqlParameter param_ty_suat_loi_nhuan_truoc_thue_doanh_thu2 = new SqlParameter("@ty_suat_loi_nhuan_truoc_thue_doanh_thu2", ty_suat_loi_nhuan_truoc_thue_doanh_thu2);

            SqlParameter param_ty_suat_loi_nhuan_sau_thue_doanh_thu1 = new SqlParameter("@ty_suat_loi_nhuan_sau_thue_doanh_thu1", ty_suat_loi_nhuan_sau_thue_doanh_thu1);
            SqlParameter param_ty_suat_loi_nhuan_sau_thue_doanh_thu2 = new SqlParameter("@ty_suat_loi_nhuan_sau_thue_doanh_thu2", ty_suat_loi_nhuan_sau_thue_doanh_thu2);

            SqlParameter param_p_e_co_ban1 = new SqlParameter("@p_e_co_ban1", p_e_co_ban1);
            SqlParameter param_p_e_co_ban2 = new SqlParameter("@p_e_co_ban2", p_e_co_ban2);

            SqlParameter param_p_e_dieu_chinh1 = new SqlParameter("@p_e_dieu_chinh1", p_e_dieu_chinh1);
            SqlParameter param_p_e_dieu_chinh2 = new SqlParameter("@p_e_dieu_chinh2", p_e_dieu_chinh2);

            SqlParameter param_p_b_price_per_book1 = new SqlParameter("@p_b_price_per_book1", p_b_price_per_book1);
            SqlParameter param_p_b_price_per_book2 = new SqlParameter("@p_b_price_per_book2", p_b_price_per_book2);

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTimkiemNangcao_DOANH_NGHIEP2", param_ngon_nguid,
                                                                                                param_nganhid,
                                                                                                param_ten_doanh_nghiep,
                                                                                                param_hoat_dong_kinh_doanh,
                                                                                                param_von_dieu_le1,
                                                                                                param_von_dieu_le2,
                                                                                                param_von_chu_so_huu1,
                                                                                                param_von_chu_so_huu2,
                                                                                                param_doanh_thu1,
                                                                                                param_doanh_thu2,
                                                                                                param_loi_nhuan_gop1,
                                                                                                param_loi_nhuan_gop2,
                                                                                                param_loi_nhuan_rong1,
                                                                                                param_loi_nhuan_rong2,
                                                                                                param_tong_tai_san1,
                                                                                                param_tong_tai_san2,
                                                                                                param_eps_co_ban1,
                                                                                                param_eps_co_ban2,
                                                                                                param_eps_dieu_chinh1,
                                                                                                param_eps_dieu_chinh2,
                                                                                                param_ti_le_tra_co_tuc_gan_nhat1,
                                                                                                param_ti_le_tra_co_tuc_gan_nhat2,
                                                                                                param_roa1,
                                                                                                param_roa2,
                                                                                                param_roe1,
                                                                                                param_roe2,
                                                                                                param_ty_suat_loi_nhuan_gop1,
                                                                                                param_ty_suat_loi_nhuan_gop2,
                                                                                                param_ty_suat_loi_nhuan_thuan1,
                                                                                                param_ty_suat_loi_nhuan_thuan2,
                                                                                                param_ty_suat_loi_nhuan_truoc_thue_doanh_thu1,
                                                                                                param_ty_suat_loi_nhuan_truoc_thue_doanh_thu2,
                                                                                                param_ty_suat_loi_nhuan_sau_thue_doanh_thu1,
                                                                                                param_ty_suat_loi_nhuan_sau_thue_doanh_thu2,
                                                                                                param_p_e_co_ban1,
                                                                                                param_p_e_co_ban2,
                                                                                                param_p_e_dieu_chinh1,
                                                                                                param_p_e_dieu_chinh2,
                                                                                                param_p_b_price_per_book1,
                                                                                                param_p_b_price_per_book2))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
    }
}
