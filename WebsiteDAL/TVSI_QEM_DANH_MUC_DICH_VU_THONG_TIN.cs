using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
using TVSI.Common;

namespace TVSI.WebsiteDAL
{
    public class TVSI_QEM_DANH_MUC_DICH_VU_THONG_TIN
    {


        // Fields
        public const string danh_muc_cha = "danh_muc_cha";
        public const string ngon_nguid = "ngon_nguid";

        // Methods
        public static DataTable DANH_SACH_DANG_KY_NHAN_BAN_TIN(int ngon_nguid)
        {
            SqlParameter para_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet set = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_QEM_DANH_MUC_DICH_VU_THONG_TIN", para_ngon_nguid))
            {
                if (set.Tables.Count > 0)
                {
                    return set.Tables[0];
                }
                return new DataTable();
            }
        }

        public static DataTable DANH_SACH_DANH_MUC_NHAN_BAN_TIN(int danh_muc_cha)
        {
            SqlParameter para_danh_muc_cha = new SqlParameter("@danh_muc_cha", danh_muc_cha);
            using (DataSet set = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_QEM_LAY_DANH_MUC_DICH_VU_SUB_EMAIL", para_danh_muc_cha))
            {
                if (set.Tables.Count > 0)
                {
                    return set.Tables[0];
                }
                return new DataTable();
            }
        }
    }
}
