using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_DU_LIEU_BIEU_DO
    {     
        public static DataTable LayThongTinThiTruong(string matt) //Mess300
        {

            try
            { 
                    SqlParameter param_matt = new SqlParameter("@ma_ttgd", matt);
                    using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo,CommandType.StoredProcedure,
                                                                    "TVSI_sTHONG_TIN_THI_TRUONG", param_matt))                                                                                            
                    {
                        if (dsData.Tables.Count > 0)
                        {
                            return dsData.Tables[0];
                        }
                        else
                        {
                            return new DataTable();
                        }
                    }
            }
            catch
            {
                return null;
            }
        }
         
        public static DataTable LayKetQuaGiaoDichThiTruong(string matt,string tu_ngay,string den_ngay) //Mess240
        {
            try
            {
                 SqlParameter param_matt = new SqlParameter("@ma_ttgd", matt);
                 SqlParameter param_tu_ngay = new SqlParameter("@tu_ngay", tu_ngay);
                 SqlParameter param_den_ngay = new SqlParameter("@den_ngay", den_ngay);
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sKET_QUA_GIAO_DICH_THI_TRUONG", param_matt,param_tu_ngay,param_den_ngay))
                                                                                                             
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }

       
        public static DataTable LayKetQuaGiaoDich(string mack, string tu_ngay,string den_ngay) //Mess230
        {
            try
            { 

                SqlParameter param_mack = new SqlParameter("@ma_chung_khoan", mack);
                 SqlParameter param_tu_ngay = new SqlParameter("@tu_ngay", tu_ngay);
                 SqlParameter param_den_ngay = new SqlParameter("@den_ngay", den_ngay);


                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sKET_QUA_GIAO_DICH", param_mack,param_tu_ngay,param_den_ngay))
                                                                                                             
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }
        public static DataTable LayDuLieuGiaCongTy(string mack) //Mess220
        {
            try
            {

                SqlParameter param_mack = new SqlParameter("@ma_chung_khoan", mack);
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sDU_LIEU_GIA_CHUNG_KHOAN", param_mack))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }

        // UPCOM Kết quả giao dịch 

        // Kết quả giao dịch UPCOM index
        public static DataTable LayKetQuaGiaoDichThiTruongUPCOM(string matt, string tu_ngay, string den_ngay) //Mess240
        {
            try
            {
                SqlParameter param_matt = new SqlParameter("@ma_ttgd", matt);
                SqlParameter param_tu_ngay = new SqlParameter("@tu_ngay", tu_ngay);
                SqlParameter param_den_ngay = new SqlParameter("@den_ngay", den_ngay);
                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sUPCOM_KET_QUA_GIAO_DICH_THI_TRUONG", param_matt, param_tu_ngay, param_den_ngay))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            
            }
            catch
            {
                return null;
            }
        }

        // Kết quả giao dịch Mã Chứng Khoán Upcom
        public static DataTable LayKetQuaGiaoDichUPCOM(string mack, string tu_ngay, string den_ngay) //Mess230
        {
            try
            {
                SqlParameter param_mack = new SqlParameter("@ma_chung_khoan", mack);
                SqlParameter param_tu_ngay = new SqlParameter("@tu_ngay", tu_ngay);
                SqlParameter param_den_ngay = new SqlParameter("@den_ngay", den_ngay);


                using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Priceinfo, CommandType.StoredProcedure,
                                                                "TVSI_sUPCOM_KET_QUA_GIAO_DICH", param_mack, param_tu_ngay, param_den_ngay))
                {
                    if (dsData.Tables.Count > 0)
                    {
                        return dsData.Tables[0];
                    }
                    else
                    {
                        return new DataTable();
                    }
                }
            }
            catch
            {
                return null;
            }
        }


    }
}
