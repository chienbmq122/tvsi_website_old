﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TVSI.Common;
using TVSI.Common.DAL;


namespace TVSI.WebsiteDAL
{
   public class TVSI_THONG_TIN_SPDV_v3
    {

       public static DataTable LayTheo_danh_muc_spdvid(int ngon_nguid, int danh_muc_spdvid)
       {
           SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
           SqlParameter param_danh_muc_spdvid = new SqlParameter("@danh_muc_spdvid", danh_muc_spdvid);
           using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                           "TVSI_sSelect_TVSI_THONG_TIN_SPDV_By_danh_muc_spdvid_v3", param_ngon_nguid, param_danh_muc_spdvid))
           {
               if (dsData.Tables.Count > 0)
               {
                   return dsData.Tables[0];
               }
               else
               {
                   return new DataTable();
               }
           }
       }
    }
}
