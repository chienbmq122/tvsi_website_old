
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_THONG_TIN_SU_KIEN
    {
        public const string su_kienid = "su_kienid"; public const string chuyen_muc_su_kienid = "chuyen_muc_su_kienid"; public const string nganhid = "nganhid"; public const string doanh_nghiepid = "doanh_nghiepid"; public const string tu_ngay = "tu_ngay"; public const string den_ngay = "den_ngay"; public const string tieu_de = "tieu_de"; public const string noi_dung = "noi_dung"; public const string nguon_dang_tai = "nguon_dang_tai"; public const string tin_quan_trong = "tin_quan_trong"; public const string ngon_nguid = "ngon_nguid"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi"; public const string ngay_dang_tai = "ngay_dang_tai";
        public const string file_dang_tai = "file_dang_tai";
        
       
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_THONG_TIN_SU_KIEN"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_chuyen_muc_su_kienid(int ngon_nguid, int su_kienid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_su_kienid = new SqlParameter("@chuyen_muc_su_kienid", su_kienid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SU_KIEN_By_chuyen_muc_su_kienid", param_ngon_nguid, param_su_kienid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_ngon_nguid(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SU_KIEN_By_ngon_nguid", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_THONG_TIN_SU_KIEN_TRANG_CHU(int ngon_nguid, int chuyen_muc_su_kienid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_su_kienid = new SqlParameter("@chuyen_muc_su_kienid", chuyen_muc_su_kienid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SU_KIEN_TRANG_CHU", param_ngon_nguid, param_chuyen_muc_su_kienid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable LayTVSI_THONG_TIN_SU_KIEN_CHITIET_1TIN(int ngon_nguid, int su_kienid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_su_kienid = new SqlParameter("@su_kienid", su_kienid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SU_KIEN_By_su_kienid", param_ngon_nguid, param_su_kienid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTVSI_THONG_TIN_SU_KIEN_Lay_ra_1Tin_dauTien(int ngon_nguid, int chuyen_muc_su_kienid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_chuyen_muc_su_kienid = new SqlParameter("@chuyen_muc_su_kienid", chuyen_muc_su_kienid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_THONG_TIN_SU_KIEN_Lay_ra_1tin_dautien_chuyen_muc_su_kienid", param_ngon_nguid, param_chuyen_muc_su_kienid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


        public static DataTable LayTOP10_THONG_TIN_SU_KIEN_CONG_TY(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_SU_KIEN", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTOP10_THONG_TIN_SU_KIEN_SAP_DIEN_RA(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_SU_KIEN_SAP_TOI", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTOP10_THONG_TIN_SU_KIEN_DANG_DIEN_RA(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_SU_KIEN_DANG_DIEN_RA", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTOP10_THONG_TIN_SU_KIEN_SAP_DIEN_RA_Theo_nganhid(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_SU_KIEN_SAP_TOI_Theo_nganhid", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        // thanhtn
        public static DataTable LayTOP10_THONG_TIN_SU_KIEN_DANG_DIEN_RA_Theo_nganhid(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sTOP10_TVSI_THONG_TIN_SU_KIEN_DANG_DIEN_RA_Theo_nganhid", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTOP5_THONG_TIN_SU_KIEN_MOI_NHAT(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TOP5_THONG_TIN_SU_KIEN_MOI_NHAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}