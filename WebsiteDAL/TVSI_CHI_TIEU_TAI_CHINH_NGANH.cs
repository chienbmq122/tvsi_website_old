
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_CHI_TIEU_TAI_CHINH_NGANH
    {
        public const string chi_tieu_tai_chinh_nganhid = "chi_tieu_tai_chinh_nganhid"; public const string nganhid = "nganhid"; public const string performance_nganh = "performance_nganh"; public const string trang_thai = "trang_thai"; public const string ngay_cap_nhat_cuoi = "ngay_cap_nhat_cuoi"; public const string nguoi_cap_nhat_cuoi = "nguoi_cap_nhat_cuoi";
        public static DataTable LayToanBo()
        {
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelectAll_TVSI_CHI_TIEU_TAI_CHINH_NGANH"))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable LayTheo_nganhid(int nganhid)
        {
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_CHI_TIEU_TAI_CHINH_NGANH_By_nganhid", param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        //public static DataTable TOP5_NGANH_TANG_CAO_NHAT(int ngon_nguid)
        //{
        //    SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
        //    using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
        //                                                    "TVSI_sSelect_TVSI_TOP_5_NGANH_CAO_NHAT", param_ngon_nguid))
        //    {
        //        if (dsData.Tables.Count > 0)
        //        {
        //            return dsData.Tables[0];
        //        }
        //        else
        //        {
        //            return new DataTable();
        //        }
        //    }
        //}

        //public static DataTable TOP5_NGANH_GIAM_THAP_NHAT(int ngon_nguid)
        //{
        //    SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
        //    using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
        //                                                    "TVSI_sSelect_TVSI_TOP_5_NGANH_THAP_NHAT", param_ngon_nguid))
        //    {
        //        if (dsData.Tables.Count > 0)
        //        {
        //            return dsData.Tables[0];
        //        }
        //        else
        //        {
        //            return new DataTable();
        //        }
        //    }
        //}
        public static DataTable TOP5_NGANH_TANG_CAO_NHAT(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_TOP_5_NGANH_CAO_NHAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TOP5_NGANH_GIAM_THAP_NHAT(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_TOP_5_NGANH_THAP_NHAT", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable CHI_SO_NGANH_theo_nganhid(int ngon_nguid, int nganhid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            SqlParameter param_nganhid = new SqlParameter("@nganhid", nganhid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_vCHI_TIEU_TAI_CHINH_NGANH_by_nganhid", param_ngon_nguid, param_nganhid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DateTime NGAY_CAP_NHAT_CUOI(int ngon_nguid)
        {
            SqlParameter param_ngon_nguid = new SqlParameter("@ngon_nguid", ngon_nguid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_NGAY_CAP_NHAT_CUOI_CHI_TIEU_TAI_CHINH", param_ngon_nguid))
            {
                if (dsData.Tables.Count > 0 && dsData.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToDateTime(dsData.Tables[0].Rows[0]["ngay_tinh_chi_so"]);
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }
    }
}