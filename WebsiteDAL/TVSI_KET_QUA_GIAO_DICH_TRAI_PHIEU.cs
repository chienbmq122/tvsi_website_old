using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_KET_QUA_GIAO_DICH_TRAI_PHIEU
    {
        public static DataTable LAY_KET_QUA_GIAO_DICH_TRAI_PHIEU(int currentPage, int pageSize)
        {
            SqlParameter p_currentPage = new SqlParameter("@currentPage", SqlDbType.Int);
            p_currentPage.Value = currentPage;

            SqlParameter p_pageSize = new SqlParameter("@pageSize", SqlDbType.Int);
            p_pageSize.Value = pageSize;

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_TVSI_KET_QUA_GIAO_DICH_TRAI_PHIEU", p_currentPage, p_pageSize))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

        public static DataTable TIM_KIEM_KET_QUA_GIAO_DICH_TRAI_PHIEU(int currentPage, int pageSize, string strKetQuaGDTraiPhieuIds)
        {
            SqlParameter p_currentPage = new SqlParameter("@currentPage", SqlDbType.Int);
            p_currentPage.Value = currentPage;

            SqlParameter p_pageSize = new SqlParameter("@pageSize", SqlDbType.Int);
            p_pageSize.Value = pageSize;

            SqlParameter p_ketQuaGDTraiPhieuIds = new SqlParameter("@strKetQuaGDTraiPhieuIds", SqlDbType.VarChar, 2000);
            p_ketQuaGDTraiPhieuIds.Value = strKetQuaGDTraiPhieuIds;


            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure, "TVSI_sSelect_TVSI_TIM_KIEM_KET_QUA_GIAO_DICH_TRAI_PHIEU", p_currentPage, p_pageSize, p_ketQuaGDTraiPhieuIds))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }


    }
}
