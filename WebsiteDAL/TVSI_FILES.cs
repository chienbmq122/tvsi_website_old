using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationBlocks.Data;

using TVSI.Common;

namespace TVSI.WebsiteDAL
{
    public class TVSI_FILES_DINH_KEM
    {
        public const string noi_luu_file = "noi_luu_file";
        
        public static DataTable TVSI_FILE_DINH_KEM(int loai_file,int thong_tinid)
        {
            SqlParameter param_loai_file = new SqlParameter("@loai_file", loai_file);
            SqlParameter param_thong_tinid = new SqlParameter("@thong_tinid", thong_tinid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_FILE", param_loai_file, param_thong_tinid))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static DataTable TVSI_FILE_DOWNLOAD_DINH_KEM(string file_downloadid)
        {
            SqlParameter param_URLCode = new SqlParameter("@file_downloadid", file_downloadid);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sDOWNLOADI_FILE", param_URLCode))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
       
    }
}
