﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using TVSI.Common;
using Microsoft.ApplicationBlocks.Data;

namespace TVSI.WebsiteDAL
{
    public class TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG
    {
        public static DataTable LayTheo_so_tai_khoan(string so_tai_khoan)
        {
            SqlParameter param_sotaikhoan = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.StoredProcedure,
                                                            "TVSI_sSelect_TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG_By_so_tai_khoan", param_sotaikhoan))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }
        public static bool ExistsSTK(string strSTK, string AStr)
        {
            SqlConnection SqlConn = new SqlConnection(AStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sSelect_TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG_By_so_tai_khoan", SqlConn);
            comm.Parameters.AddWithValue("@so_tai_khoan", strSTK);
            comm.CommandType = CommandType.StoredProcedure;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return bOutPut;
        }


        public static DataTable Lay_thong_tin_email(string so_tai_khoan)
        {
            SqlParameter param_email = new SqlParameter("@so_tai_khoan", so_tai_khoan);
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Common, CommandType.Text,
                                                            "SELECT dia_chi_email FROM TVSI_THONG_TIN_LIEN_HE_KHACH_HANG WHERE so_tai_khoan = " + so_tai_khoan + " "))
            {
                if (dsData.Tables.Count > 0)
                {
                    return dsData.Tables[0];
                }
                else
                {
                    return new DataTable();
                }
            }
        }

    }
}
