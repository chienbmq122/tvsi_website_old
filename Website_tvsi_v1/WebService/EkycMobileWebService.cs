﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TVSI.Utility;
using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.WebService
{
    public class EkycMobileWebService
    {
        readonly string host = ConfigurationManager.AppSettings["CRM_WEBAPI_URL_V2"];

        public  bool ConfirmOTPEkycMobile(string codem)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/EK_CF_ConFirmOPTEkycM";
                    string JsonCheck = "{\"codem\":\"" + codem + "\"}";
                    HttpResponseMessage response = httpClient.PostAsync(uri, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsAsync<EkycMobileConfirm>().Result;
                        return data.RetCode.Equals("000");
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(EkycWebService),"ConfirmOTPEkycMobile");
                throw;
            }
        }
    }
}