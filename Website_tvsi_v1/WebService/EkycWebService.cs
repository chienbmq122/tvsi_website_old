﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TVSI.Utility;
using Website_tvsi_v1.Common;
using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.WebService
{
    public class EkycWebService
    {

        public static string token_id = ConfigurationManager.AppSettings["TokenID"] + "";
        public static string token_key = ConfigurationManager.AppSettings["TokenKey"] + "";
        public static string access_token = ConfigurationManager.AppSettings["AccessToken"] + "";
        
        
        public static string domain_addfile = ConfigurationManager.AppSettings["domain_addfile"] + "";
        public static string domain_checkekyc = ConfigurationManager.AppSettings["domain_checkekyc"] + "";
        public static string domain_checkekycRealFake = ConfigurationManager.AppSettings["domain_checkekycRealFake"] + "";
        public static string domain_getEkycFront = ConfigurationManager.AppSettings["domain_getEkycFront"] + "";
        public static string domain_getEkycBack = ConfigurationManager.AppSettings["domain_getEkycBack"] + "";
        public static string domain_getEkycFrontAndBack = ConfigurationManager.AppSettings["domain_getEkycFrontAndBack"] + "";
        public static string domain_getEkycAndFace = ConfigurationManager.AppSettings["domain_getEkycAndFace"] + "";
        public static string domain_checkFaceLiveness = ConfigurationManager.AppSettings["domain_checkFaceLiveness"] + "";

        public static async Task<int> CheckEKYC(object hashimage)
        {
            try {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string JsonCheck = "{\"img_card\":\"" + hashimage + "\" , \"client_session\":\"client_session\" , \"token\":\"TVSI_Token_id_\" }";

                    Logger.Info(typeof(EkycWebService), "CheckEKYC:domain=" + domain_checkekyc + ";input=" + JsonCheck);

                    HttpResponseMessage response = client.PostAsync(domain_checkekyc, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    var responseString = await response.Content.ReadAsStringAsync();
                    EkycModel.Root responseConvertJson = JsonConvert.DeserializeObject<EkycModel.Root>(responseString);

                    Logger.Info(typeof(EkycWebService), "CheckEKYC:output=" + responseString);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        /*
                        UtilTvsi.InsertLogApi(HttpStatusCode.OK,domain_checkekyc,"",Convert.ToString(responseConvertJson.@object.type));
                        */
                        return responseConvertJson.@object.type;
                    }

                    return -1;

                }

            } catch(Exception e)
            {
                Logger.Debug(typeof(EkycWebService),"CheckEKYC that bai" + e.Message);
                return -1;
            }

        }

        public static async Task<JsonResult> GetEkycFront(object hashimage, int objecttype)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (!string.IsNullOrEmpty(Convert.ToString(objecttype)))
                    {
                        string JsonCheck = "{ \"img_front\":\"" + hashimage +
                                           "\" , \"client_session\":\"client_session\"  , \"type\":\"" + objecttype +
                                           "\" , \"validate_postcode\":\"true\" , \"token\":\"Djhasye_odaw\"}";

                        Logger.Info(typeof(EkycWebService),
                            "GetEkycFront:domain=" + domain_getEkycFront + ";input=" + JsonCheck);

                        HttpResponseMessage response = client.PostAsync(domain_getEkycFront,
                            new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                        
                        var responseString = await response.Content.ReadAsStringAsync();
                        GetEkycFrontModel.Root responseConvertJson =
                            JsonConvert.DeserializeObject<GetEkycFrontModel.Root>(responseString);

                        Logger.Info(typeof(EkycWebService), "GetEkycFront:output=" + responseString);

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            /*
                            UtilTvsi.InsertLogApi(HttpStatusCode.OK, domain_getEkycFront, "", responseString);
                            */
                            return new JsonResult()
                            {
                                Data = responseConvertJson.@object,
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                    }
                    return new JsonResult()
                    {
                        Data = null,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(EkycWebService),"GetEkycFront that bai" + e.Message);
                return null;
            }
        }


        public static async Task<JsonResult> GetEkycBack(object hashimage, int objecttype)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (!string.IsNullOrEmpty(Convert.ToString(objecttype)))
                    {
                        string JsonCheck = "{ \"img_back\":\"" + hashimage +
                                           "\" , \"client_session\":\"client_session\"  , \"type\":\"" + objecttype +
                                           "\" , \"validate_postcode\":\"true\" , \"token\":\"NDSHAQ_DHHAGS_GHA\"}";

                        Logger.Info(typeof(EkycWebService),
                            "GetEkycBack:domain=" + domain_getEkycBack + ";input=" + JsonCheck);

                        HttpResponseMessage response = client.PostAsync(domain_getEkycBack,
                            new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                        var responseString = await response.Content.ReadAsStringAsync();
                        GetEkycBackModel.Root responseConvertJson =
                            JsonConvert.DeserializeObject<GetEkycBackModel.Root>(responseString);

                        Logger.Info(typeof(EkycWebService), "GetEkycBack:output=" + responseString);

                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            /*
                            UtilTvsi.InsertLogApi(HttpStatusCode.OK,domain_getEkycBack,"",responseString);
                            */
                            return new JsonResult()
                            {
                                Data = responseConvertJson.@object,
                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                            };
                        }
                    }

                    return new JsonResult()
                    {
                        Data = null,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(EkycWebService), "GetEkycBack that bai " + e.Message);
                return null;
            }
        }

        public static async Task<CheckFaceToEkyc.Root> CheckFaceToEkyc(object hashImageEkyc, object hashImageFace)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string JsonCheck = "{ \"img_front\":\"" + hashImageEkyc + "\" , \"img_face\":\"" + hashImageFace + "\"  , \"client_session\":\"client_session\"  ,  \"token\":\"NDSHAQ_DHHAGS_GHA\"}";

                    Logger.Info(typeof(EkycWebService), "CheckFaceToEkyc:domain=" + domain_getEkycAndFace + ";input=" + JsonCheck);

                    HttpResponseMessage response = client.PostAsync(domain_getEkycAndFace, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    var responseString = await response.Content.ReadAsStringAsync();

                    Logger.Info(typeof(EkycWebService), "CheckFaceToEkyc:output=" + responseString);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        CheckFaceToEkyc.Root responseConvertJson = JsonConvert.DeserializeObject<CheckFaceToEkyc.Root>(responseString);
                        /*
                        UtilTvsi.InsertLogApi(HttpStatusCode.OK,domain_getEkycAndFace,"",responseString);
                        */
                        return responseConvertJson;

                    }
                    return null;
                }

            }
            catch(Exception e)
            {
                return null;
            }



        }

        public static async Task<JsonResult> GetIDEKYC(object hashimagefront, object hashimageback, int objecttype)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string JsonCheck = "{\"img_front\":\"" + hashimagefront + "\" ,\"img_back\":\"" + hashimageback + "\", \"client_session\":\"client_session\",\"type\":\"" + objecttype + "\",\"crop_param\":\"0.14\",\"validate_postcode\":\"" + true + "\",\"token\":\" Token_TVSI_DSAHDA\" }";

                    Logger.Info(typeof(EkycWebService), "GetIDEKYC:domain=" + domain_getEkycFrontAndBack + ";input=" + JsonCheck);

                    HttpResponseMessage response = await client.GetAsync(domain_getEkycFrontAndBack);
                    var responseString = await response.Content.ReadAsStringAsync();
                    Logger.Info(typeof(EkycWebService), "GetIDEKYC:output=" + responseString);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        GetEkycFrontAndBack.Root responseConvertJson = JsonConvert.DeserializeObject<GetEkycFrontAndBack.Root>(responseString);
                        /*
                        UtilTvsi.InsertLogApi(HttpStatusCode.OK,domain_getEkycFrontAndBack,"",responseString);
                        */
                        return new JsonResult
                        {
                            Data = responseConvertJson.@object.id,
                            JsonRequestBehavior = JsonRequestBehavior.DenyGet
                        };
                    }
                    return new JsonResult
                    {
                        Data = null,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet

                    };
                }
            }
            catch (Exception e)
            {
                var error = new { Data = false, Message = e.Message };
                Logger.Debug(typeof(EkycWebService),"GetIDEKYC that bai" + e.Message);

                return new JsonResult
                {
                    Data = error,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }


        public static async Task<GetEkycFrontAndBack.Root> GetEkycFrontAndBack(object hashimagefront, object hashimageback, int objecttype)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string JsonCheck = "{\"img_front\":\"" + hashimagefront + "\" ,\"img_back\":\"" + hashimageback + "\", \"client_session\":\"client_session\",\"type\":\"" + objecttype + "\",\"crop_param\":\"\",\"validate_postcode\":\"" + true + "\",\"token\":\" Token_TVSI_DSAHDA\" }";

                    Logger.Info(typeof(EkycWebService), "GetEkycFrontAndBack:domain=" + domain_getEkycFrontAndBack + ";input=" + JsonCheck);

                    HttpResponseMessage response = client.PostAsync(domain_getEkycFrontAndBack, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    var responseString = await response.Content.ReadAsStringAsync();

                    Logger.Info(typeof(EkycWebService), "GetEkycFrontAndBack:output=" + responseString);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        GetEkycFrontAndBack.Root responseConvertJson = JsonConvert.DeserializeObject<GetEkycFrontAndBack.Root>(responseString);
                        /*
                        UtilTvsi.InsertLogApi(HttpStatusCode.OK,domain_getEkycFrontAndBack,"",responseString);
                        */
                        return responseConvertJson;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(EkycWebService),"GetEkycFrontAndBack" + ex.Message);
                return null;
            }

        }

        public static async Task<string> CheckRealFake(object hashimage)
        {
            try {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string JsonCheck = "{\"img\":\"" + hashimage + "\" , \"client_session\":\"client_session\" }";
                    Logger.Info(typeof(EkycWebService), "CheckRealFake:domain=" + domain_checkekycRealFake + ";input=" + JsonCheck);


                    HttpResponseMessage response = client.PostAsync(domain_checkekycRealFake, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    var responseString = await response.Content.ReadAsStringAsync();
                    Logger.Info(typeof(EkycWebService), "CheckRealFake:output=" + responseString);
                    var responseConvertJson = JsonConvert.DeserializeObject<CheckEkycRealFakeModel.Root>(responseString);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return responseConvertJson.@object.liveness;
                    }
                    return string.Empty;
                }
            } catch(Exception)
            {
                Logger.Debug(typeof(EkycWebService),"CheckRealFake");
                return null;
            }


        }

        public static async Task<string> CheckFaceLiveness(object hashimage)
        {
            try {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    client.DefaultRequestHeaders.Add("mac-address", "TEST1");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string JsonCheck = "{\"img\":\"" + hashimage + "\" , \"client_session\":\"client_session\", \"token\":\"TVSI_Token\" }";
                    Logger.Info(typeof(EkycWebService), "CheckFaceLiveness:domain=" + domain_checkFaceLiveness + ";input=" + JsonCheck);

                    HttpResponseMessage response = client.PostAsync(domain_checkFaceLiveness, new StringContent(JsonCheck, Encoding.UTF8, "application/json")).Result;
                    var responseString = await response.Content.ReadAsStringAsync();
                    CheckFaceLivenessModel.Root responseConvertJson = JsonConvert.DeserializeObject<CheckFaceLivenessModel.Root>(responseString);
                    Logger.Info(typeof(EkycWebService), "CheckFaceLiveness:output=" + responseString);
                    
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return responseConvertJson.@object.liveness;
                    }
                    return string.Empty;
                }
            }catch(Exception)
            {
                Logger.Debug(typeof(EkycWebService),"CheckFaceLiveness");
                return null;
            }

        }

        public static async Task<string> GetHashFace(string path)
        {
            try
            {
                string title = "TVSI";
                string description = "TVSI";
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    using (MultipartFormDataContent form = new MultipartFormDataContent())
                    {
                        using (var fs = File.OpenRead(path))
                        {
                            using (var streamContent = new StreamContent(fs))
                            {
                                using (var fileContent = new ByteArrayContent(streamContent.ReadAsByteArrayAsync().Result))
                                {
                                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                    form.Add(fileContent, "file", Path.GetFileName(path));
                                    form.Add(new StringContent(title), "title");
                                    form.Add(new StringContent(description), "description");
                                    HttpResponseMessage response = client.PostAsync(domain_addfile, form).Result;
                                    var responseString = await response.Content.ReadAsStringAsync();

                                    Logger.Info(typeof(EkycWebService), "GetHashFace:output=" + responseString);
                                    EkycFrontFaceModel.Root responseConvertJson =
                                        JsonConvert.DeserializeObject<EkycFrontFaceModel.Root>(responseString);
                                    if (response.StatusCode == HttpStatusCode.OK)
                                    {
                                        return responseConvertJson.@object.hash;
                                    }
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(EkycWebService),"GetHashFace" + e.Message);
                throw;
            }
        }

        public static async Task<string> GetHashUploadImageEkyc(string path)
        {
            string title = "TVSI";
            string description = "TVSI";
            try
            {
                Logger.Info(typeof(EkycWebService),"GetHashUploadImageEkyc -> " + path);
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token-id", token_id);
                    client.DefaultRequestHeaders.Add("Token-key", token_key);
                    Logger.Info(typeof(EkycWebService),"GetHashUploadImageEkyc ->  token_id " + token_id);
                    Logger.Info(typeof(EkycWebService),"GetHashUploadImageEkyc ->  token_key " + token_key);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
                    Logger.Info(typeof(EkycWebService),"GetHashUploadImageEkyc ->  access_token " + access_token);

                    using (MultipartFormDataContent form = new MultipartFormDataContent())
                    {
                        using (var fs = File.OpenRead(path))
                        {
                            Logger.Info(typeof(EkycWebService),"GetHashUploadImageEkyc ->  path " + path);

                            using (var streamContent = new StreamContent(fs))
                            {
                                using (var fileContent =
                                    new ByteArrayContent(streamContent.ReadAsByteArrayAsync().Result))
                                {
                                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                    form.Add(fileContent, "file", Path.GetFileName(path));
                                    form.Add(new StringContent(title), "title");
                                    form.Add(new StringContent(description), "description");
                                    
                                    HttpResponseMessage response = client.PostAsync(domain_addfile, form).Result;
                                    
                                    var responseString = await response.Content.ReadAsStringAsync();
                                    Logger.Info(typeof(EkycWebService),
                                        "GetHashUploadImageEkyc:output=" + responseString);

                                    EkycBackModel.Root responseConvertJson =
                                        JsonConvert.DeserializeObject<EkycBackModel.Root>(responseString);

                                    if (response.StatusCode == HttpStatusCode.OK)
                                    {
                                        return responseConvertJson.@object.hash;
                                    }
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(EkycWebService),"GetHashUploadImageEkyc -> Message -> " + e.Message);
                Logger.Debug(typeof(EkycWebService),"GetHashUploadImageEkyc -> InnerException -> " + e.InnerException);
                throw;
            }
        }

    }
}