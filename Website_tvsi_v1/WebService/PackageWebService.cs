﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using TVSI.Utility;
using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.WebService
{
    public class PackageWebService
    {
        readonly string host = ConfigurationManager.AppSettings["CRM_WEBAPI_URL"];


        public string UpdateDangKyGoiDichVu(string goidichvuid, int trangthai)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/UpdateDangKyGoiDichVu?goi_dich_vu_id=" + goidichvuid + "&trang_thai=" + trangthai + " ";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsAsync<string>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }

        /*public DangKyGoiDichVu GetDetailDangKyGoiDichVu(string id)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(host);
                    var request = new HttpRequestMessage(HttpMethod.Post, "api/website/GetDetailGoiDichVu");
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_id", id));
                    request.Content = new FormUrlEncodedContent(keyValues);
                    var response = httpClient.SendAsync(request).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.Content.ReadAsAsync<DangKyGoiDichVu>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("GetDetailDangKyGoiDichVu - " + e.Message));
                return null;
            }
        }   
        public DangKyGoiDichVuUuDai GetDetailDangKyGoiDichVuUuDai(string id)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(host);
                    var request = new HttpRequestMessage(HttpMethod.Post, "api/website/GetDetailGoiDichVuUuDai");
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_uu_daiid", id));
                    request.Content = new FormUrlEncodedContent(keyValues);
                    var response = httpClient.SendAsync(request).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.Content.ReadAsAsync<DangKyGoiDichVuUuDai>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("GetDetailDangKyGoiDichVu - " + e.Message));
                return null;
            }
        }*/
        
         
        public string UpdateDangKyGoiDichVuUuDai(string goidichvuuudaiid, int trangthai)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/UpdateDangKyGoiDichVuUuDai?goi_dich_vu_id=" + goidichvuuudaiid + "&trang_thai=" + trangthai + " ";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return response.Content.ReadAsAsync<string>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }
        
        
        public checkAccount CheckAccountVerification(string username, string pin)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetUser?userName=" + username + "&pin=" + pin + " ";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    return response.Content.ReadAsAsync<checkAccount>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }     
        public checkAccount CheckPassAccountVerification(string username, string pin)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetUserPass?userName=" + username + "&pass=" + pin + " ";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    return response.Content.ReadAsAsync<checkAccount>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }

        public string setGDVUuDai(string sotaikhoan, string hoten, string ngaydangky, string ngayhieuluc,
            string ngayketthuc, string goidichvuhientai, string goidichvumoi, string nguoitao,string note)
        {
            try
            {
                if (!string.IsNullOrEmpty(sotaikhoan))
                {
                    sotaikhoan += "6";
                }
                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(host);
                    var request = new HttpRequestMessage(HttpMethod.Post, "api/website/InsertDangKyGoiDichVuUuDai");
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("so_tai_khoan", sotaikhoan));
                    keyValues.Add(new KeyValuePair<string, string>("ho_ten_khach_hang", hoten));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_dang_ky", ngaydangky));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_hieu_luc", ngayhieuluc));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_ket_thuc", ngayketthuc));
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_hien_tai", goidichvuhientai));
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_moi", goidichvumoi));
                    keyValues.Add(new KeyValuePair<string, string>("nguoi_tao", nguoitao));
                    keyValues.Add(new KeyValuePair<string, string>("note", note));
                    request.Content = new FormUrlEncodedContent(keyValues);
                    var response = httpClient.SendAsync(request).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.Content.ReadAsAsync<string>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), e.Message);
                return null;
            }
        }
        public string setPackageService(string sotaikhoan, string hoten, string ngaydangky, string ngayhieuluc,
            string ngayketthuc, string goidichvuhientai, string goidichvumoi, string nguoitao,string note)
        {
            try
            {
                if (!string.IsNullOrEmpty(sotaikhoan))
                {
                    sotaikhoan += "6";
                }
                using (var httpClient = new HttpClient())
                {
                    var uri = host;
                    httpClient.BaseAddress = new Uri(uri);
                    var request = new HttpRequestMessage(HttpMethod.Post, "api/website/InsertDangKyGoiDichVu");
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("so_tai_khoan", sotaikhoan));
                    keyValues.Add(new KeyValuePair<string, string>("ho_ten_khach_hang", hoten));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_dang_ky", ngaydangky));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_hieu_luc", ngayhieuluc));
                    keyValues.Add(new KeyValuePair<string, string>("ngay_ket_thuc", ngayketthuc));
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_hien_tai", goidichvuhientai));
                    keyValues.Add(new KeyValuePair<string, string>("goi_dich_vu_moi", goidichvumoi));
                    keyValues.Add(new KeyValuePair<string, string>("nguoi_tao", nguoitao));
                    keyValues.Add(new KeyValuePair<string, string>("note", note));
                    request.Content = new FormUrlEncodedContent(keyValues);
                    var response = httpClient.SendAsync(request).Result;
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        return response.Content.ReadAsAsync<string>().Result;
                    }
                    return null;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), e.Message + e.InnerException);
                return null;
            }
        }

        public GetHistoryPackageModel.Root GetHistoryPackage(string id)
        {
            try
            {
                id += "6";
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetMarginPackHistory?so_tai_khoan=" + id + "";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }

                    return response.Content.ReadAsAsync<GetHistoryPackageModel.Root>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        } 
        public GetHistoryPackagePreModel.Root GetGDVUuDai(string id)
        {
            try
            {
                id += "6";
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetGoiDichVuUuDai?so_tai_khoan=" + id + "";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    return response.Content.ReadAsAsync<GetHistoryPackagePreModel.Root>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }

        public string GetPackageServiceAccount(string id)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetGoiDichVuUuDaiDangDung?ma_khach_hang=" + id + "";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    var value = response.Content.ReadAsAsync<GetGDVUudaiModel.GetGDVHienTai>().Result;
                    return value.ma_dich_vu;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        } 

        public GetPackageCurrentModel.Root GetPackageCurrent(string id) {
            GetPackageCurrentModel.Root getPackageCurrent = new GetPackageCurrentModel.Root();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetMarginGroup?so_tai_khoan=" + id + "";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return  response.Content.ReadAsAsync<GetPackageCurrentModel.Root>().Result;
                    }
                    return new GetPackageCurrentModel.Root();
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }

        public GetServicePackage GetServicePackage(string namepackage,string so_tai_khoan)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetGoiDichVu?ten_goi_dich_vu=" + namepackage + "&so_tai_khoan="+ so_tai_khoan+"";
                    var response = httpClient.PostAsync(uri,null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    return response.Content.ReadAsAsync<GetServicePackage>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService -> GetServicePackage " + e.Message));
                return null;
            }
        }

        public GetgoidichvuuudaiModel GetgoidichvuuudaiRoot()
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetGoiDichVuuudai";
                    var response = httpClient.PostAsync(uri,null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    return response.Content.ReadAsAsync<GetgoidichvuuudaiModel>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(PackageWebService),string.Format("PackageWebservice - " + e.Message));
                return null;
            }
        }

        public GetPackageCurrentModel.Root GetPackageCurrentRoot(string id)
        {
            GetPackageCurrentModel.Root getPackageCurrent = new GetPackageCurrentModel.Root();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/website/GetMarginGroup?so_tai_khoan=" + id + "";
                    var response = httpClient.PostAsync(uri, null).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        return null;
                    }
                    return response.Content.ReadAsAsync<GetPackageCurrentModel.Root>().Result;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(BankWebService), string.Format("PackageWebService - " + e.Message));
                return null;
            }
        }
    }
}