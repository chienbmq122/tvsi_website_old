﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using TVSI.Utility;
using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.WebService
{
    public class BankWebService
    {
        readonly string host = ConfigurationManager.AppSettings["CRM_WEBAPI_URL"];

        public List<ProvincesModel> GetProvinceList()
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetProvinceList";
                var response = httpClient.GetAsync(uri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                return response.Content.ReadAsAsync<List<ProvincesModel>>().Result;
            }
        }

        public List<BankModel> GetBankList()
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetBankList";
                var response = httpClient.GetAsync(uri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                return response.Content.ReadAsAsync<List<BankModel>>().Result;
            }
        }

        public List<SubBranchModel> GetSubBranchList(string bankNo)
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetSubBranchList?bankNo=" + bankNo;
                var response = httpClient.GetAsync(uri).Result;

                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                return response.Content.ReadAsAsync<List<SubBranchModel>>().Result;
            }
        }

        public string GetBankName(string bankNo)
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetBankName?bankNo=" + bankNo;
                var response = httpClient.GetAsync(uri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return "";
                }

                return response.Content.ReadAsAsync<string>().Result;
            }
        }

        public string GetSubBranchName(string bankNo, string branchNo)
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetSubBranchName?bankNo=" + bankNo + "&branchNo=" + branchNo;
                var response = httpClient.GetAsync(uri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return "";
                }

                return response.Content.ReadAsAsync<string>().Result;
            }
        }

        public string GetProvinceName(string provinceID)
        {
            using (var httpClient = new HttpClient())
            {
                var uri = host + "api/Bank/GetProvinceName?provinceID=" + provinceID;
                var response = httpClient.GetAsync(uri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return "";
                }

                return response.Content.ReadAsAsync<string>().Result;
            }
        }

        
    }
}