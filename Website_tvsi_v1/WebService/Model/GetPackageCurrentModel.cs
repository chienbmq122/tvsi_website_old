﻿using System.Collections.Generic;

namespace Website_tvsi_v1.WebService.Model
{
    public class GetPackageCurrentModel
    {
        public class StockList    {
            public string SYMBOL { get; set; } 
            public string MRGRATE { get; set; } 
            public string PLEDGERATE { get; set; } 
            public string CANBUY { get; set; } 
            public string CANSELL { get; set; } 
            public string DEFAULT { get; set; } 
            public string LIMIT_PRICE { get; set; } 
        }

        public class Root
        {
            public string SO_TAI_KHOAN { get; set; }
            public string LAI_SUAT { get; set; }
            public string GOI_DICH_VU { get; set; }

        public string SUC_MUA { get; set; } 
            public string TY_LE_CALL { get; set; } 
            public string MA_CHO_VAY { get; set; } 
            public string CAP_NHAT_SUC_MUA { get; set; } 
            public string SO_NGAY_MIEN_LAI { get; set; } 
            public string TY_LE_KY_QUY { get; set; } 
            public string TY_LE_CANH_BAO { get; set; } 
            public string TY_LE_VAY { get; set; } 
            public List<StockList> StockList { get; set; } 
        }
    }
}