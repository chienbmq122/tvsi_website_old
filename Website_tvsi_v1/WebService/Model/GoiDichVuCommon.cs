﻿namespace Website_tvsi_v1.WebService.Model
{
    public class DangKyGoiDichVu
    {
        public string so_tai_khoan { get; set; }
        public string ho_ten_khach_hang { get; set; }
        public string ngay_dang_ky { get; set; }
        public string ngay_hieu_luc { get; set; }

        public string ngay_ket_thuc { get; set; }
        public string goi_dich_vu_hien_tai { get; set; }
        public string goi_dich_vu_moi { get; set; }
        public string nguoi_tao { get; set; }
    }
    
    public class DangKyGoiDichVuUuDai
    {
        public string so_tai_khoan { get; set; }
        public string ngay_dang_ky { get; set; }
        public string ngay_hieu_luc { get; set; }

        public string ngay_ket_thuc { get; set; }
        public string goi_dich_vu_hien_tai { get; set; }
        public string goi_dich_vu_moi { get; set; }
        public string nguoi_tao { get; set; }
        public string ho_ten_khach_hang { get; set; }

    }
}