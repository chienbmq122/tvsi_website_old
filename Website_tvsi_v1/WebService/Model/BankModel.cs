﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.WebService.Model
{
    public class BankModel
    {
        public string BankNo { get; set; }
        public string ShortName { get; set; }

    }

    public class SubBranchModel
    {
        public string BranchNo { get; set; }
        public string ShortBranchName { get; set; }
    }

    public class ProvincesModel
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }
    }
}