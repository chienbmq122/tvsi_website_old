﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.WebService.Model
{
    public class EkycModel
    {
        public class Object
        {
            public string name { get; set; }
            public int type { get; set; }
        }

        public class Root
        {
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public Object @object { get; set; }
            public int statusCode { get; set; }
            public string challengeCode { get; set; }
        }
    }

    public class EkycBackModel
    {
        public class Object
        {
            public string fileName { get; set; }
            public string tokenId { get; set; }
            public string description { get; set; }
            public string storageType { get; set; }
            public string title { get; set; }
            public string uploadedDate { get; set; }
            public string hash { get; set; }
            public string fileType { get; set; }
        }

        public class Root
        {
            public string message { get; set; }
            public Object @object { get; set; }
        }
    }

    public class EkycFrontModel
    {
        public class Object
        {
            public string fileName { get; set; }
            public string tokenId { get; set; }
            public string description { get; set; }
            public string storageType { get; set; }
            public string title { get; set; }
            public string uploadedDate { get; set; }
            public string hash { get; set; }
            public string fileType { get; set; }
        }

        public class Root
        {
            public string message { get; set; }
            public Object @object { get; set; }
        }
    }

    public class GetEkycFrontModel
    {
        public class Imgs
        {
            public string img_front { get; set; }
        }

        public class PostCode
        {
            public List<object> city { get; set; }
            public List<object> district { get; set; }
            public List<object> ward { get; set; }
            public string type { get; set; }
        }

        public class Tampering
        {
            public string is_legal { get; set; }
            public List<object> warning { get; set; }
        }

        public class Object
        {
            public string msg { get; set; }
            public string origin_location { get; set; }
            public string name_prob { get; set; }
            public string birth_day_label { get; set; }
            public string gender { get; set; }
            public string recent_location_label { get; set; }
            public string expire_warning { get; set; }
            public string print_type { get; set; }
            public string nation_slogan { get; set; }
            public string valid_date_prob { get; set; }
            public double origin_location_prob { get; set; }
            public string nation_policy { get; set; }
            public string corner_warning { get; set; }
            public string valid_date { get; set; }
            public string issue_date { get; set; }
            public double id_fake_prob { get; set; }
            public string nationality_prob { get; set; }
            public string id { get; set; }
            public string id_probs { get; set; }
            public string citizen_id_prob { get; set; }
            public string birth_day_prob { get; set; }
            public string issue_place { get; set; }
            public string id_fake_warning { get; set; }
            public string recent_location { get; set; }
            public string type_id { get; set; }
            public string card_type { get; set; }
            public string birth_day { get; set; }
            public string issue_date_prob { get; set; }
            public string citizen_id { get; set; }
            public double recent_location_prob { get; set; }
            public string issue_place_prob { get; set; }
            public string nationality { get; set; }
            public string gender_prob { get; set; }
            public List<PostCode> post_code { get; set; }
            public string name { get; set; }
            public Tampering tampering { get; set; }
            public string origin_location_label { get; set; }
        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public Object @object { get; set; }
            public int statusCode { get; set; }
            public string challengeCode { get; set; }
        }
    }

    public class GetEkycBackModel
    {
        public class Imgs
        {
            public string img_back { get; set; }
        }

        public class Object
        {
            public double issue_date_label_prob { get; set; }
            public string issue_place { get; set; }
            public string expire_warning { get; set; }
            public List<string> warning_msg { get; set; }
            public List<double> issue_date_probs { get; set; }
            public string back_type_id { get; set; }
            public double issue_date_prob { get; set; }
            public double issue_place_prob { get; set; }
            public string issue_date { get; set; }
            public string back_corner_warning { get; set; }
            public List<string> warning { get; set; }
            public string back_expire_warning { get; set; }
            public string msg_back { get; set; }
        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public Object @object { get; set; }
            public int statusCode { get; set; }
            public string challengeCode { get; set; }
        }

    }
    public class EkycFrontFaceModel
    {
        public class Object
        {
            public string fileName { get; set; }
            public string tokenId { get; set; }
            public string description { get; set; }
            public string storageType { get; set; }
            public string title { get; set; }
            public string uploadedDate { get; set; }
            public string hash { get; set; }
            public string fileType { get; set; }
        }

        public class Root
        {
            public string message { get; set; }
            public Object @object { get; set; }
        }
    }
    public class CheckFaceToEkyc
    {
        public class Imgs
        {
            public string img_face { get; set; }
            public string img_front { get; set; }
        }

        public class Object
        {
            public string msg { get; set; }
            public string result { get; set; }
            public double prob { get; set; }
        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public Object @object { get; set; }
            public int statusCode { get; set; }
            public string challengeCode { get; set; }
        }
    }

    public class GetEkycFrontAndBack
    {
        public class Imgs
        {
            public string img_back { get; set; }
            public string img_front { get; set; }
        }

        public class PostCode
        {
            public List<object> city { get; set; }
            public List<object> district { get; set; }
            public List<object> ward { get; set; }
            public string type { get; set; }
        }

        public class Tampering
        {
            public string is_legal { get; set; }
            public List<object> warning { get; set; }
        }

        public class Object
        {
            public string origin_location { get; set; }
            public double name_prob { get; set; }
            public string msg { get; set; }
            public string gender { get; set; }
            public string expire_warning { get; set; }
            public List<string> warning_msg { get; set; }
            public List<double> issue_date_probs { get; set; }
            public int back_type_id { get; set; }
            public string issue_date_warning { get; set; }
            public double valid_date_prob { get; set; }
            public string nation_policy { get; set; }
            public double origin_location_prob { get; set; }
            public string corner_warning { get; set; }
            public string valid_date { get; set; }
            public string issue_date { get; set; }
            public double id_fake_prob { get; set; }
            public string back_corner_warning { get; set; }
            public List<string> warning { get; set; }
            public string id { get; set; }
            public string back_expire_warning { get; set; }
            public string citizen_id_prob { get; set; }
            public string id_probs { get; set; }
            public string msg_back { get; set; }
            public double birth_day_prob { get; set; }
            public string issue_place { get; set; }
            public string id_fake_warning { get; set; }
            public string recent_location { get; set; }
            public int type_id { get; set; }
            public string card_type { get; set; }
            public string birth_day { get; set; }
            public string issue_date_prob { get; set; }
            public string citizen_id { get; set; }
            public string issue_place_prob { get; set; }
            public double recent_location_prob { get; set; }
            public string nationality { get; set; }
            public List<PostCode> post_code { get; set; }
            public string name { get; set; }
            public Tampering tampering { get; set; }
            public long? valuePlace { get; set; }

        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string server_version { get; set; }
            public string message { get; set; }
            public string statusCode { get; set; }
            public Object @object { get; set; }
            public string challengeCode { get; set; }
        }






    }

    public class CheckEkycRealFakeModel
    {
        public class Imgs
        {
            public string img { get; set; }
        }

        public class Object
        {
            public bool face_swapping { get; set; }
            public bool fake_print_photo { get; set; }
            public string liveness { get; set; }
            public bool fake_liveness { get; set; }
            public string liveness_msg { get; set; }
        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public int statusCode { get; set; }
            public Object @object { get; set; }
            public string challengeCode { get; set; }
        }

    }

    public class CheckFaceLivenessModel
    {
        public class Imgs
        {
            public string img { get; set; }
        }

        public class Object
        {
            public string liveness { get; set; }
            public string blur_face { get; set; }
            public string liveness_msg { get; set; }
            public string is_eye_open { get; set; }
        }

        public class Root
        {
            public Imgs imgs { get; set; }
            public string dataSign { get; set; }
            public string dataBase64 { get; set; }
            public string logID { get; set; }
            public string message { get; set; }
            public string server_version { get; set; }
            public Object @object { get; set; }
            public int statusCode { get; set; }
            public string challengeCode { get; set; }
        }
    }
    public class EkycMobileConfirm
    {
        public string RetCode { get; set; }
        public bool RetData { get; set; }
    }
}