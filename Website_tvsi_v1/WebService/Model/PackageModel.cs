﻿using System;
using System.Collections.Generic;

namespace Website_tvsi_v1.WebService.Model
{
    public class checkAccount    {
        public string SoTaiKhoan { get; set; } 
        public string HoTen { get; set; } 
        public int TrangThai { get; set; } 
        public List<MarginList> MarginList { get; set; }

    }
    public class MarginList    {
        public string goi_dich_vuid { get; set; }
        public string ten_goi_dich_vu { get; set; } 
        public string lai_suat { get; set; } 
        public string ty_le_call { get; set; } 
        public object cap_nhat_suc_mua { get; set; } 
        public object so_ngay_mien_lai { get; set; } 
        public object ty_le_ky_quy { get; set; } 
        public object ty_le_ky_quy_xu_ly { get; set; } 
        public object ty_le_vay { get; set; } 
        
    }
}