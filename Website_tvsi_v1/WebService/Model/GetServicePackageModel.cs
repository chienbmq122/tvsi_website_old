﻿using System.Collections.Generic;

namespace Website_tvsi_v1.WebService.Model
{
    public class GetServicePackage
    {
        public string ten_goi_dich_vu { get; set; } 
        public string lai_suat { get; set; } 
        public string suc_mua { get; set; } 
        public string suc_mua_toi_da { get; set; } 
        public string ty_le_call { get; set; } 
        public string cap_nhat_suc_mua { get; set; } 
        public string so_ngay_mien_lai { get; set; } 
        public string ty_le_ky_quy { get; set; } 
        public string ty_le_duy_tri { get; set; } 
        public string ty_le_canh_bao { get; set; } 
        public string ty_le_vay { get; set; } 
        public string so_luong_ma { get; set; } 
        public string du_no_toi_da { get; set; }
        public double du_no { get; set; }
        public List<StockList> StockList { get; set; }
    }
    public class StockList    {
        public string SYMBOL { get; set; } 
        public string MRGRATE { get; set; } 
        public string PLEDGERATE { get; set; } 
        public string CANBUY { get; set; } 
        public string CANSELL { get; set; } 
        public string DEFAULT { get; set; } 
        public object LIMIT_PRICE { get; set; } 
    }
}