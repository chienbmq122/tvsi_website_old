﻿using System.Collections.Generic;

namespace Website_tvsi_v1.WebService.Model
{
    public class GetgoidichvuuudaiModel
    {
        public List<MARGINPACK> MARGINPACK { get; set; }
    }
    public class MARGINPACK    {
        public string goi_dich_vu_uu_daiID { get; set; } 
        public string ten_goi_dich_vu_uu_dai { get; set; } 
        public string lai_suat_toi_thieu { get; set; } 
        public string ly_le_phi_toi_thieu { get; set; } 
        public string phi_giao_dich_toi_thieu { get; set; } 
        public string so_ngay_mien_lai { get; set; } 
        public string vong_quay_du_no_thang { get; set; } 
        public string du_no_toi_da { get; set; } 
        public string phi_dich_vu_goi { get; set; } 
        public string ty_le_vay_toi_da { get; set; } 
        public string so_luong_ma_cho_vay { get; set; } 
        public string doi_tuong { get; set; } 
        public string mo_ta { get; set; } 
    }
    
}