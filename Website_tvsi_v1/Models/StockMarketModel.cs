﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class StockMarketModel
    {
        public Double vnIndex { get; set; }
        public Double volumnIndex { get; set; }
        public Double valueIndex{ get; set; }
        public Double changeIndex{ get; set; }
        public Double percentIndex{ get; set; }
        public String colorIndex { get; set; }
        public String arrowIndex { get; set; }

    }
}