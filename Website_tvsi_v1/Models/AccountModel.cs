﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class AccountModel
    {
        public int count { set; get; }
        public string languae { set; get; }
    }

    public class AccountInfoModel
    {
        public string Custcode { get; set; }
        public string FullName { get; set; }
        public string CardID { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }

    public class SerivceListModel
    {
        public int ServiceID { get; set; }
        public int RegistID { get; set; }
        public string ServiceCode { get; set; }
        public string CurrentMarginCombo { get; set; }
        public string ServiceName { get; set; }
        public string FeeNote { get; set; }
        public string RateNote { get; set; }
        public string OtherNote { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int IsRegistCourse { get; set; }
        public int IsConfirm { get; set; }
        public string Message { get; set; }
    }

    public class RegisterCourseModel
    {
        public int CourseID { get; set; }
        public string CourseName { get; set; }
        public string Note { get; set; }
        public int CourseType { get; set; }
        public string Duration { get; set; }
        public string NumLession { get; set; }
        public decimal Cost { get; set; }
        
        public  string DisplayName{get;set;}
        
        public  string TeacherName{get;set;}
        
        public  string Description{get;set;}
        public  string ImageUrl{get;set;}
    }

    public class InsertTVComboResult
    {
        public string RegistID { get; set; }
        public string Code { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class CourseModel
    {
        public int CourseCustom { get; set; }
        public int RegCourse { get; set; }
        public int RegCourse_2 { get; set; }
        public int BasiCourse { get; set; }
        public int registerID { get; set; }
        public int serviceid { get; set; }

        public int CourseID { get; set; }
        public int CourseID_01 { get; set; }

        public int JoinType { get; set; }
        public int JoinType_01 { get; set; }

        public string AccountNo { get; set; }
        public string AccountNo_01 { get; set; }
        public string FullName { get; set; }
        public string FullName_01 { get; set; }
        

        public string CardID { get; set; }
        public string CardID_01 { get; set; }
        public string MonthCourse { get; set; }
        public string MonthCourse_01 { get; set; }
        public string Phone { get; set; }
        public string Phone_01 { get; set; }
        public string Email { get; set; }
        public string Email_01 { get; set; }
        public string TimeCourse { get; set; }
        public string TimeCourse_01 { get; set; }
        public string CreatedBy { get; set; }
    }

    public class GetListTVCOMBOHistModel
    {
        public int RegistID { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public DateTime RegDate { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string CurrentService { get; set; }
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string CourseBasicName { get; set; }
        public string CourseOptionName { get; set; }
        public string StatusName { get; set; }
        public string Status { get; set; }
    }

    public class GetListCourseHistModel
    {
        public int CourseID { get; set; }
        public string CustCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CourseName { get; set; }
        public string StudentCode { get; set; }
        public string CustName { get; set; }
        public string CardID { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ScheduleOffer { get; set; }
        public string TimeOffer { get; set; }
        public string RealSchedule { get; set; }
        public string RealTime { get; set; }
        public string StatusName { get; set; }
        public string Note { get; set; }
        
    }

    public class GetInfoTVComboModel
    {
        public string CustCode { get; set; }
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string CustName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public int Status { get; set; }
    }

    public class ProvinceModel
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceCode { get; set; }
    }

    public class DistricModel
    {
        public int DistrictID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
    } 
    public class WardModel
    {
        public int WardID { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
    }

    public class ChangeMarginModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Note { get; set; }
        public int IsConfirm { get; set; }
        
    }

    public class SaleInfoModel
    {
        public string Email { get; set; }
        public int DepartmentID { get; set; }
        public string SaleName { get; set; }
    }

    public class SaleData
    {
        public string SaleID { get; set; }
        public string BranchID { get; set; }
    }
    
}