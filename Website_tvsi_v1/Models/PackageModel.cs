﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class PackageModel
    {
        public string sotaikhoan { set; get; }
        public string ngaydangky { set; get; }
        public string goidichvuhientai { get; set; }
        public string goidichvumoi { get; set; }
        public string trangthai { get; set; }
        public string ngayhieuluc { get; set; }
        public string lydotuchoi { get; set; }
    }
}