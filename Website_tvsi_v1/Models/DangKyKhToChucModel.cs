﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class DangKyKhToChucModel
    {
        //to chuc
        public string tentochuc { set; get; }
        public string sodkkd { set; get; }
        public string ngaycap { set; get; }
        public string noicap { set; get; }
        public string diachi { set; get; }
        public string sodienthoai { set; get; }
        public string somayfax { set; get; }
        //nguoi dai dien
        public string hoten { set; get; }
        public string sodidong { set; get; }
        public string email { set; get; }
        public string sale_id { set; get; }
    }
}