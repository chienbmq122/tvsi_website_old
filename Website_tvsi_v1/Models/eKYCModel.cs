﻿using System;

namespace Website_tvsi_v1.Models
{
    public class eKYCModel
    {
        public string CodeOTP { get; set; }
        public int TimeStep { get; set; }
        
    }

    public class UserInfoOTP
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ConfirmCode { get; set; }
    }

    public class UserCRM
    {
        public  string FullName { get; set; }

        public string DateOfBirth
        {
            get
            {
                return DateOfBirthOn.ToString("dd/MM/yyyy");
            }
        }

        public  DateTime DateOfBirthOn { get; set; }
        public  string CardID { get; set; }

        public string IssueDate
        {
            get
            {
                return IssueDateOn.ToString("dd/MM/yyyy");
            }
        }

        public  DateTime IssueDateOn { get; set; }
        public  string CardIssue { get; set; }
        public  string TypeAccount { get; set; }
        public  string Email { get; set; }
    }
}