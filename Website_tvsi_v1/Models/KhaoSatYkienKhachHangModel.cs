﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class KhaoSatYkienKhachHangModel
    {

        
        public String hoten { set; get; }
        public String sotaikhoan { set; get; }
        public String emailortel { set; get; }
        public int thoigiangiaodich { set; get; }
        public bool kenhthongtin { set; get; }
        public bool luachondichvu { set; get; }
        public int quatangtvsi { set; get; }
        public int chuongtrinhtrian { set; get; }
        public int chuongtrinhhoithao { set; get; }
        public String ykiendichvuchamsoc { set; get; }
        public int tkduocnvmoigioicsoc { set; get; }
        public int mongmuontkduocqlyboinvmoigioi { set; get; }
        public int duytrilienhekhachhang { set; get; }
        public int thaidophucvu { set; get; }
        public int khananghotrogiaodich { set; get; }
        public int tuvandautu { set; get; }
        public String ykiendanhgiadvmg { set; get; }
        public bool kenhgiaodichchungkhoan { set; get; }
        public int hinhthucgiaodien { set; get; }
        public int noidungthongtin { set; get; }
        public int bocuc { set; get; }
        public int tinhnang { set; get; }
        public int tocdotruycap { set; get; }
        public int gdtt_tinhnangdatlenh { set; get; }
        public int giaodichtienchungkhoan { set; get; }
        public int thuchienquyentt { set; get; }
        public int banlolett { set; get; }
        public int ttdanhmuctonghoptsan { set; get; }
        public int saokegiaodich { set; get; }
        public int tinhnanglailotaikhoanqkhu { set; get; }
        public String ykienbosungphanmemgdtt { set; get; }
        public int datlenhgiaodichtongdai { set; get; }
        public int hotrogiaidapquatongdai { set; get; }
        public int hotrogiaidapquaemail { set; get; }
        public int thaidophucvunvientdai { set; get; }
        public int tracuuthongtintudong { set; get; }
        public String ykienbosungtongdai { set; get; }
        public int datlenhdichvusmsemail { set; get; }
        public int gdtienbiendongsodusmsemail { set; get; }
        public int tbaothuchienquyensmsemail { set; get; }
        public int tonghopgiaodichsmsemail { set; get; }
        public int saokegiaodichhangthangsmsemail { set; get; }
        public int truyvantttdongsmsemail { set; get; }
        public int thaidonviengdichtaiquay { set; get; }
        public int cnghieptocdoxlygiaodichtaiquay { set; get; }
        public int bmauhdongdkygiaodichtaiquay { set; get; }
        public int sudungdichvugdkyquy { set; get; }
        public bool lydokhongconhucaugdkquy { set; get; }
        public bool dichvuhotrovon { set; get; }
        public bool goidichvuhotrovongdkquy { set; get; }
        public int tieuchidichvugdkyquy { set; get; }
        public int thutucgiaodichkyquy { set; get; }
        public int phidichvugiaodichkyquy { set; get; }
        public int capnhatdanhmucgiaodichkyquy { set; get; }
        public int tylehtrovontkgiaodichkyquy { set; get; }
        public int goisphamgiaodichkyquy { set; get; }
        public int hthonghotrogiaodichkyquy { set; get; }
        public int guithongtintraodoitsdb { set; get; }
        public int chinhsachchovaygdichkquy { set; get; }
        public int hinhthucgiaodienweb { set; get; }
        public int noidungweb { set; get; }
        public int bocucchucnangweb { set; get; }
        public int tocdoweb { set; get; }
        public int tinhcxaccapnhatbgia { set; get; }
        public int tocdobgia { set; get; }
        public int giaodienbgia { set; get; }
        public int ttthitruongttptich { set; get; }
        public int loccophieuttptich { set; get; }
        public int bieudobolocttptich { set; get; }
        public int bantinphantichttruong { set; get; }
        public int thamkhaobantinphantichttruong { set; get; }
        public int noidungbantinphantichttruong { set; get; }
        public int khuyennghidautubantinpttt { set; get; }
        public int hieuquadautubantinpttt { set; get; }
        public String ykienbantinpttt { set; get; }
        public String gopytvsi { set; get; }
        



        //Khach hang vang lai
        
        public bool kenhdaututaichinh { set; get; }
        public bool kenhthongtinchuacotk { set; get; }
        public bool tieuchiluachontaictyck { set; get; }


        //Cau hoi bo sung

        public String trienkhaictht { set; get; }
        public String sdungdvctyngoai { set; get; }
        public bool lydoluachonctykhac { set; get; }
        public String goidvutchinhctyngoai { set; get; }
        public String donggopykienkhac { set; get; }
    }
}