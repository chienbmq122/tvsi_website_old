﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common;

//using static Website_tvsi_v1.Common.UtilTvsi;

namespace Website_tvsi_v1.Models
{
    public class DangKyKhCaNhanModel
    {
        public int dangkyid { get; set; }
        public string hoten { set; get; }
        public string ngaysinh { set; get; }
        public string noisinh { set; get; }
        public int gioitinh { set; get; }
        public string cmnd { set; get; }
        public string ngaycapcmnd { set; get; }
        public string ngaycapmagiaodich { set; get; }
        public string ngayhethancmnd { set; get; }
        public string giamhovietnam { set; get; }
        public string noicap { set; get; }
        public string email { set; get; }
        public string stateemail { set; get; }
        public string diachi { set; get; }
        public string quoctich { set; get; }
        public string diachicutru { set; get; }
        public string nghenghiep { set; get; }
        public string noilamviec { set; get; }
        
        public string trusochinh { set; get; }
        public string tradingcode { set; get; }
        public string yeucau { set; get; }
        public string sale_id { set; get; }

        public string accNoBIDV {get;set;}
        
        public string fileuploadfront { get; set; }

        public string fileuploadback { get; set; }

        public string fileuploadface { get; set; }
        public string kytubaomatcn { set; get; }

        
        public bool TradeMethodPhone { get; set; }
        public bool TradeMethodInternet { get; set; }
        public bool TradeResultSMS { get; set; } = false;
        public bool TradeResultEmail { get; set; }

        public bool MonthlyReportMail { get; set; }
        public bool MonthlyReportEMail { get; set; }
        public string Province { get; set; }
        public string ProvinceValue { get; set; }
        public string Distric { get; set; }
        public string Ward { get; set; }
        public string AddressDetail { get; set; }
        public int QA1txt { get; set; }

        public bool BankTransfer { get; set; } // BankTransfer or AccountTransfer


        public int ManagerAccount { get; set; } // mô hình quản lý tài khoản
        public int CustomerRelaUsa { get; set; } = 0; // doi tuong cu tru tai hoa ky.

        public int CustomerUSA { get; set; } = 0;  // KH là công dân hoa kỳ hoặc đối tượng cư trú
        public int PlaceOfBirthUSA { get; set; } = 0; // nơi sinh tại hoa kỳ
        public int DepositoryUSA { get; set; } = 0;  // lưu ký tại hoa kỳ
        public int CustomerPhoneNumberUSA { get; set; }  = 0;  // KH có sđt tại hoa kỳ
        public int CustomerTransferBankUSA { get; set; } = 0;   // KH có lệnh định kỳ chuyển khoản
        public int CustomerAuthorizationUSA { get; set; } = 0; // KH có giấy ủy quyên từ Hoa Kỳ
        public int CustomerReceiveMailUSA { get; set; } = 0; // KH Có sử dụng địa chỉ nhận thư hộ hoặc giữ thư tại hoa kỳ
        public int BankReg { get; set; }
        
        public string Note { get; set; } // yêu cầu thêm

        public string Branch_Transaction { get; set; }
        public bool TradetoBank { get; set; }
        public string Phone_01 { get; set; }
        public string Phone_02 { get; set; }
        
        
        public string AccountNo_1 { get; set; }
        public string AccountNo_2 { get; set; }

        public string PhoneResultTrading { get; set; }
        public string TradingPhone_01 { get; set; }
        public string TradingPhone_02 { get; set; }


        public string Bank { get; set; }
        public string BankAccName_01 { get; set; }
        public string BankAccNo_01 { get; set; }
        public string BankNo_01 { get; set; }
        public string BankName_01 { get; set; }
        public string SubBranchNo_01 { get; set; }
        public string SubBranchName_01 { get; set; }
        public string City_01 { get; set; }
        public string City_02 { get; set; }

        public string BankAccName_02 { get; set; }
        public string BankAccNo_02 { get; set; }
        public string BankNo_02 { get; set; }
        public string BankName_02 { get; set; }
        public string SubBranchNo_02 { get; set; }
        public string SubBranchName_02 { get; set; }

        public bool RegisterMargin { get; set; }


        public bool RegistChannelInfo { get; set; }
        public bool RegistChannelDirect { get; set; }
        public bool RegistItradeHome { get; set; }

        public int? type { get; set; } = null;
        
        public string noicapkhac { get; set; }
        
        public string ChannelInfoMethod
        {
            get
            {
                var ret = "";

                if (RegistChannelDirect)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Direct.ToString();

                if (RegistItradeHome)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.ITradeHome.ToString();

                return ret;
            }
        }

        public string MarginAccount
        {
            get
            {
                var ret = string.Empty;
                if (RegisterMargin)
                {
                    ret += UtilTvsi.RegisterConst.Margin;
                }

                return ret;
            }
        }

        public string BankMethod
        {
            get
            {
                var ret = "";
                if (Bank == "remittance")
                {
                    ret = "1";
                }else
                    ret = "2";

                return ret;
            }
        }
        public string TradeMethod
        {
            get
            {
                var ret = "";
                if (TradeMethodPhone)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Phone.ToString();

                if (TradeMethodInternet)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Internet.ToString();
                return ret;
            }
        }

        public string TradeResult
        {
            get
            {
                var ret = "";
                if (TradeResultSMS)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.SMS.ToString();

                if (TradeResultEmail)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Email.ToString();

                return ret;
            }
        }

        public string MonthlyReport
        {
            get
            {
                var ret = "";
                if (MonthlyReportMail)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.RequisteredLetter.ToString();

                if (MonthlyReportEMail)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Email.ToString();
                return ret;
            }
        }

        public string TransferType
        {
            get
            {
                var ret = "";

                if (BankTransfer)
                    ret += Website_tvsi_v1.Common.UtilTvsi.RegisterConst.Bank.ToString();
                return ret;
            }
        }
    }

    public class ConfirmEkycModel
    {
        public string Code { get; set; }
        public string CodeM { get; set; }
        public string Otp { get; set; }
        public string Sms { get; set; }
    }

    public class UserInfoContact
    {
        public string FullName { get; set; }
        public string BirthDay { get; set; }
        public string CardID { get; set; }
        public string IssueDate { get; set; }
        public string Issuer { get; set; }
        
    }

    public class GenerateSaleIDModel
    {
        public int WorkTimeID { get; set; }
        public string SaleID { get; set; }
        public int Count { get; set; }
    }
    
}