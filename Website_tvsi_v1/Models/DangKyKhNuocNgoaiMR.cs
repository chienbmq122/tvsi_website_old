﻿namespace Website_tvsi_v1.Models
{
    public class DangKyKhNuocNgoaiMR
    {
        public string ngayhethancmnd { set; get; }
        public string ngaycapmagiaodich { set; get; }
        public string giamhovietnam { set; get; }
        public string stateemail { set; get; }
        public string noisinh { set; get; }
        public string quoctich { set; get; }
        public string diachicutru { set; get; }
        public string nghenghiep { set; get; }
        public string noilamviec { set; get; }
        public string trusochinh { set; get; }
        public string tradingcode { set; get; }
        public string accNoBIDV {get;set;}
        public string TradingPhone_01 { get; set; }
        public string TradingPhone_02 { get; set; }
        public string PhoneResultTrading { get; set; }
    }
}