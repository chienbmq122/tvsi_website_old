﻿// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

using System;
using System.Collections.Generic;

namespace Website_tvsi_v1.Models
{
    public class RetData
    {
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public List<Value> Values { get; set; }
    }
    
    public class Value
    {
        public double FeeValue { get; set; }
        public double TradingValue { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public int IsConfirm { get; set; }
        public string Message { get; set; }
        public string Unit { get; set; }
        public int FeeID { get; set; }
        public int? RegistID { get; set; }
    }

    public class ApiResponse
    {
        public string RetCode { get; set; }
        public List<RetData> RetData { get; set; }
    }
    
    public class EcoTvApiReponse
    {
        public string RetCode { get; set; }
        public EcoTvRetData RetData { get; set; }
        
        public string RetMsg { get; set; }
    }

    public class EcoTvCancelResponse
    {
        public string RetCode { get; set; }
        public object RetData { get; set; }
    }
    
    
    public class EcoTvRetData
    {
        public int RegistID { get; set; }
        public string Code { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class EcoTvApiResponse<T>
    {
        public string RetCode { get; set; }
        public List<T> RetData { get; set; }
    }
    
    public class SearchTvEcoFeeResponse
    {
        public string Title { get; set; }
        public string Value { get; set; }
    }
    
    public class EcoTvDataHistResponse
    {
        public int RegistID { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public DateTime RegDate { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string CurrentService { get; set; }
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public double FeeValue { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }
    
    public class SuccessModel<T>
    {
        public string RetCode { get; set; }
        public T RetData { get; set; }
    }
    
    public class RetDataCashBalance
    {
        public string AccountNo { get; set; }
        public List<object> BankInfos { get; set; }
        public CashBalanceInfo CashBalanceInfo { get; set; }
        public List<object> InternalAccountInfos { get; set; }
        public List<BlockFeeInfo> BlockFeeInfos { get; set; }
    }
    
    public class BlockFeeInfo
    {
        public string FeeType { get; set; }
        public string FeeName { get; set; }
        public double Amount { get; set; }
    }

    public class CashBalanceInfo
    {
        public string AccountNo { get; set; }
        public double Balance { get; set; }
        public double Withdrawal { get; set; }
        public double BlockFee { get; set; }
        public double MinMarginRate { get; set; }
        public double MaxWithdrawal { get; set; }
        public double FastWithdrawal { get; set; }
    }

}