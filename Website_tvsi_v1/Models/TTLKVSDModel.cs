﻿using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.Models
{
    public class TTLKVSDModel
    {
        public string So_dang_ky_tv { get; set; }
        public string Ten_Tieng_Viet { get; set; }
        public string Ten_tieng_anh { get; set; }
        public string loai_thanh_vien { get; set; }
        public string ten_viet_tat { get; set; }
        public string status { get; set; }
    }
}