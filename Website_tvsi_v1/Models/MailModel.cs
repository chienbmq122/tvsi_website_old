﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Models
{
    public class MailModel
    {
        [Required(ErrorMessage="Họ và Tên không được trống")]
        public String HovaTen { set; get; }
        [Required(ErrorMessage = "Vị trí tuyển dụng không được trống")]
        public String Vitrituyendung { set; get; }
        [Required(ErrorMessage = "Email không được trống")]
        public String Email { set; get; }
        [Required(ErrorMessage = "Số điện thoại Không được trống ")]
        [Range(0,Int32.MaxValue,ErrorMessage="Bạn phải nhập số")]
        public int Sodienthoai { set; get; }

    }
}