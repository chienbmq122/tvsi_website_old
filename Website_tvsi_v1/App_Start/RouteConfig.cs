﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Website_tvsi_v1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
                routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
                //san phan dich vu
                routes.MapRoute(
                    name: "dich vu tvsi khach hang doanh nghiep",
                    url: "product-services/tvsi-customer-business/{id}",
                    defaults: new { controller = "spdv", action = "DichvuTVSIKhdn", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "khach hang ca nhan",
                    url: "product-services/customer-personal/{id}",
                    defaults: new { controller = "spdv", action = "khachhangcanhan", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "khach hang doanh nghiep",
                    url: "product-services/customer-business/{id}",
                    defaults: new { controller = "spdv", action = "khachhangdoanhnghiep", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "giao dich truc tuyen",
                    url: "product-services/trading-online/{id}",
                    defaults: new { controller = "spdv", action = "giaodichtructuyen", id = UrlParameter.Optional }
                );
                //routes.MapRoute(
                //    name: "Cam ket chat luong dich vu sla",
                //    url: "product-services/ChatLuongDichVuSLA/{id}",
                //    defaults: new { controller = "spdv", action = "ChatLuongDichVuSLA", id = UrlParameter.Optional }
                //);

                //end 

                // ho tro khach hang customer-support
                routes.MapRoute(
                    name: "cac bieu mau",
                    url: "customer-support/form-tvsi/{id}",
                    defaults: new { controller = "hotro", action = "Cacbieumau", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "chi tiet kien thuc",
                    url: "customer-support/detail-answer-knowledge/{id}",
                    defaults: new { controller = "hotro", action = "ChitietKienthuc", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "chi tiet kinh nghiem",
                    url: "customer-support/detail-answer-experience/{id}",
                    defaults: new { controller = "hotro", action = "ChitietKinhnghiem", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "ho tro",
                    url: "customer-support/guide-tvsi/{id}",
                    defaults: new { controller = "hotro", action = "HuongdanTvsi", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "huong dan nop tien",
                    url: "customer-support/guide-payment/{id}",
                    defaults: new { controller = "hotro", action = "HDnoptien", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "huong dan giao dich co phieu",
                    url: "customer-support/guide-trading/{id}",
                    defaults: new { controller = "hotro", action = "HDgiaodichcophieu", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "huong dan tra cuu",
                    url: "customer-support/guide-search/{id}",
                    defaults: new { controller = "hotro", action = "HDtracuu", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "hoi dap faq",
                    url: "customer-support/answer-faq/{id}",
                    defaults: new { controller = "hotro", action = "hoidapfaq", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "hoi dap kien thuc",
                    url: "customer-support/answer-knowledge/{id}",
                    defaults: new { controller = "hotro", action = "hoidapkienthuc", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "hoi dap kinh nghiem",
                    url: "customer-support/answer-experience/{id}",
                    defaults: new { controller = "hotro", action = "hoidapkinhnghiem", id = UrlParameter.Optional }
                );
                //end 

                // home tvsi
                routes.MapRoute(
                    name: "bao cao tai chinh",
                    url: "introduction/report-finance/{id}",
                    defaults: new { controller = "Qhcodong", action = "Baocaotaichinh", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "bao cao thuong nien",
                    url: "introduction/report-annual/{id}",
                    defaults: new { controller = "Qhcodong", action = "Baocaothuongnien", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "quan tri doanh nghiep",
                    url: "introduction/admin-business/{id}",
                    defaults: new { controller = "Qhcodong", action = "Quantridoanhnghiep", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "thong tin co dong",
                    url: "introduction/information-shareholder/{id}",
                    defaults: new { controller = "Qhcodong", action = "Thongtincodong", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "thong tin cong bo",
                    url: "introduction/information-publish/{id}",
                    defaults: new { controller = "Qhcodong", action = "Thongtincongbo", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "co hoi nghe nghiep",
                    url: "introduction/opportunity-job/{id}",
                    defaults: new { controller = "home", action = "Cohoinghenghiep", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "dang ky khach hang ca nhan",
                    url: "home/register-customer-personal/{id}",
                    defaults: new { controller = "home", action = "DangKyKhCaNhan", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "Dang ky dich vu",
                    url: "home/register-service/{id}",
                    defaults: new { controller = "home", action = "Dangkydichvu", id = UrlParameter.Optional}
                );
                routes.MapRoute(
                    name: "Dang ky dich vu khach hang",
                    url: "service/register-service-customer/{id}",
                    defaults: new { controller = "service", action = "RegisterService", id = UrlParameter.Optional}
                );  
                routes.MapRoute(
                    name: "Dang ky dung thu xtrade",
                    url: "home/register-trial-xtrade/{id}",
                    defaults: new { controller = "home", action = "RegisterExperienceXtrade", id = UrlParameter.Optional}
                );
                routes.MapRoute(
                    name: "Dang ky khach hang ca nhan quoc te",
                    url: "home/register-customer-personal-international/{id}",
                    defaults: new { controller = "home", action = "DangkyKhCanhanNuocNgoai", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "dang ky khach hang ca nhan ekyc",
                    url: "home/register-customer-personal-ekyc/{id}",
                    defaults: new { controller = "home", action = "DangKyKhCaNhanEkyc", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                   name: "Xac nhan mo tai khoan",
                   url: "user/confirm-ekyc-open-account/{id}",
                   defaults: new { controller = "user", action = "ConfirmEkycPage", id = UrlParameter.Optional }
               );
                routes.MapRoute(
                    name: "dang ky khac hang to chuc",
                    url: "home/register-customer-organizations/{id}",
                    defaults: new { controller = "home", action = "DangkyKhTochuc", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "dang ky mo tai khoan",
                    url: "home/open-account/{id}",
                    defaults: new { controller = "home", action = "DangkyMoTaikhoan", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "gioi thieu tvsi",
                    url: "introduction/about-tvsi/{id}",
                    defaults: new { controller = "home", action = "GioithieuTVSI", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "hoat dong tvsi",
                    url: "introduction/activity-tvsi/{id}",
                    defaults: new { controller = "home", action = "Hoatdongtvsi", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "chi tiet hoat dong tvsi",
                    url: "introduction/detail-activity-tvsi/{id}",
                    defaults: new { controller = "home", action = "Imagehoatdongtvsi", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "chi tiet tvsi voi cong dong",
                    url: "introduction/detail-tvsi-community/{id}",
                    defaults: new { controller = "home", action = "ImageTvsicongdong", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                    name: "khuyen cao",
                    url: "home/terms-use/{id}",
                    defaults: new { controller = "home", action = "Khuyencao", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "site map",
                    url: "home/site-map/{id}",
                    defaults: new { controller = "home", action = "SiteMap", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "tvsi voi bao chi",
                    url: "introduction/tvsi-newspapers/{id}",
                    defaults: new { controller = "home", action = "Tvsibaochi", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                  name: "tvsi voi cong dong",
                  url: "introduction/tvsi-community/{id}",
                  defaults: new { controller = "home", action = "Tvsicongdong", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                  name: "vi tri tuyen dung",
                  url: "introduction/detail-recruitment/{id}",
                  defaults: new { controller = "home", action = "VitriTuyendungChitiet", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                 name: "Khao sat y kien khach hang",
                 url: "khao-sat/lay-y-kien-khach-hang/{id}",
                 defaults: new { controller = "home", action = "khaosatykienkhachhang", id = UrlParameter.Optional }
               );
                routes.MapRoute(
                 name: "Khao sat Trai phieu khach hang",
                 url: "khao-sat/khao-sat-trai-phieu-khach-hang/{id}",
                 defaults: new { controller = "khaosat", action = "KSTraiPhieuKhachHang", id = UrlParameter.Optional }
               );
                routes.MapRoute(
                 name: "Khao sat Trai phieu can bo nhan vien",
                 url: "khao-sat/khao-sat-trai-phieu-can-bo-nhan-vien/{id}",
                 defaults: new { controller = "khaosat", action = "KSTraiPhieuCBNV", id = UrlParameter.Optional }
               );

                routes.MapRoute(
                    name: "Khao sat y kien khach hang-thong tin chung",
                    url: "khao-sat/thong-tin-chung/{id}",
                    defaults: new { controller = "khaosat", action = "thongtinchung", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                    name: "Khao sat y kien khach hang-Dich vu moi gioi",
                    url: "khao-sat/dich-vu-moi-gioi/{id}",
                    defaults: new { controller = "khaosat", action = "dichvumoigioi", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                   name: "Khao sat y kien khach hang-Giao dich chung khoan",
                   url: "khao-sat/giao-dich-chung-khoan/{id}",
                   defaults: new { controller = "khaosat", action = "giaodichchungkhoan", id = UrlParameter.Optional }
               );
                routes.MapRoute(
                      name: "Khao sat y kien khach hang-Dich vu ho tro von",
                      url: "khao-sat/dich-vu-ho-tro-von/{id}",
                      defaults: new { controller = "khaosat", action = "dichvuhotrovon", id = UrlParameter.Optional }
                 );
                routes.MapRoute(
                         name: "Khao sat y kien khach hang-Giao-dich-truc-tuyen",
                         url: "khao-sat/giao-dich-truc-tuyen/{id}",
                         defaults: new { controller = "khaosat", action = "giaodichtructuyen", id = UrlParameter.Optional }
                 );
                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Y kien khach hang",
                             url: "khao-sat/y-kien-khach-hang/{id}",
                             defaults: new { controller = "khaosat", action = "ykienkhachhang", id = UrlParameter.Optional }
                 );

                // NEW
                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Khach hang dau tu co phieu",
                             url: "khao-sat/khach-hang-dau-tu-co-phieu-tai-tvsi/{id}",
                             defaults: new { controller = "khaosat", action = "KhachHangDauTuCoPhieu", id = UrlParameter.Optional }
                 );

                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Khach hang dau tu trai phieu",
                             url: "khao-sat/khach-hang-dau-tu-trai-phieu-tai-tvsi/{id}",
                             defaults: new { controller = "khaosat", action = "KhachHangDauTuTraiPhieu", id = UrlParameter.Optional }
                 );

                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Khach hang dau tu co phieu va trai phieu",
                             url: "khao-sat/khach-hang-dau-tu-co-phieu-va-trai-phieu-tai-tvsi/{id}",
                             defaults: new { controller = "khaosat", action = "KhachHangDauTuCoPhieuVaTraiPhieu", id = UrlParameter.Optional }
                 );

                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Khach hang chua co tai khoan tai TVSI",
                             url: "khao-sat/khach-hang-chua-co-tai-khoan-tai-tvsi/{id}",
                             defaults: new { controller = "khaosat", action = "KhachHangChuaCoTaiKhoan", id = UrlParameter.Optional }
                 );
                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Lua-chon-thong-tin",
                             url: "khao-sat/{id}",
                             defaults: new { controller = "khaosat", action = "Luachonthongtin", id = UrlParameter.Optional }
                 );
                routes.MapRoute(
                            name: "Khao sat y kien khach hang ket qua",
                            url: "ket-qua-khao-sat/tong-quan/{id}",
                            defaults: new { controller = "khaosat", action = "Ketquakhaosat", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Redirect",
                             url: "khao-sat/submit/{id}",
                             defaults: new { controller = "khaosat", action = "Redirect", id = UrlParameter.Optional }
                 );

                routes.MapRoute(
                             name: "Khao sat y kien khach hang-Ket qua khao sat khach hang chua co tai khoan tai tvsi",
                             url: "khao-sat/kqkskhachhangchuacotkhaontvsi/{id}",
                             defaults: new { controller = "khaosat", action = "kqkskhachhangchuacotkhaontvsi", id = UrlParameter.Optional }
                 );
                routes.MapRoute(
                   name: "Chi tiet TVSI voi bao tri",
                   url: "introduction/tvsi-newspapers-details/{id}",
                   defaults: new { controller = "Home", action = "TvsiVoiBaoChiDetails", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                name: "Tin TVSI voi cong dong",
                url: "introduction/tvsi-community-news/{id}",
                defaults: new { controller = "home", action = "TinTvsicongdong", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                 name: "Chi tiet tin TVSI voi cong dong",
                 url: "introduction/tvsi-community-new-details/{id}",
                 defaults: new { controller = "Home", action = "TinTvsicongdongDetails", id = UrlParameter.Optional }
                );
                routes.MapRoute(
                 name: "Thong tin dang ky goi margin",
                 url: "margin-mf/{id}",
                 defaults: new { controller = "Margin", action = "index", id = UrlParameter.Optional }
                );

                routes.MapRoute(
                name: "Thong tin dang ky goi margin TV10.9",
                url: "margin-tv10-9/{id}",
                defaults: new { controller = "Margin", action = "margin_tv10_9", id = UrlParameter.Optional }
               );
                routes.MapRoute(
                name: "Thong tin dang ky goi margin TV",
                url: "Margin-TVSI",
                defaults: new { controller = "Margin", action = "margin_tv", id = UrlParameter.Optional }
               );


            //end home

            routes.MapRoute(
                name: "appinstall",
                url: "appinstall/{action}/{id}",
                defaults: new { controller = "AppInstall", action = "Index", id = UrlParameter.Optional }
            );  
            //end home

            routes.MapRoute(
                name: "InstallIWork",
                url: "installiwork/{action}/{id}",
                defaults: new { controller = "AppInstall", action = "InstallIWork", id = UrlParameter.Optional }
            ); 
            
            //end home

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}