/**
 * Created by phuongthanh92 on 10/13/2015.
 */
$(document).ready(function () {
    setTimeout(function () {
        $('body').addClass('loaded');
    }, 2000);
    /*if($(window).width() > 640){
     $('.dropdown-toggle', $("#main-nav")).removeAttr('data-toggle');
     }*/

    var slider = $('.item_wrapper', $('#main-slider')).bxSlider({
        auto: true,
        slideSelector: '.item',
        minSlides: 1,
        maxSlides: 1,
        pager: false,
        speed: 1000,
        easing: 'ease-out'

    });
    if ($(window).width() > 960) {
        /*$('.dropdown-toggle', $("#main-nav")).removeAttr('data-toggle');*/
        $(".tab-news", $("#wrapper-top-content")).hover(function () {
            $(this).toggleClass("open");
        })
        $('#main-nav .dropdown').each(function () {
            var partMenu = $(this).find('.dropdown-menu.customize-1 .part-menu');
            if (partMenu.length > 4) {
                partMenu.css({
                    paddingRight: "10px",
                    paddingLeft: '10px'
                });
                partMenu.find('.wrapper').css({
                    paddingLeft: '0 '
                })
                partMenu.find('.title-part >span').css({
                    fontSize: "15px"
                })
                partMenu.find('.wrapper li > a> span.thumb').css({
                    'width': '30px'
                })
            }
        })
    }
    $(".title-part", $("#transaction-network")).on("click", function () {
        $("#transaction-network").toggleClass("open");
    })
    $("#quick-link-status").on('click', function () {
        $("#quick-link").toggleClass('open');
    })
    if ($(window).width() < 960) {
        $(".tab-news", $("#wrapper-top-content")).addClass("open");
    }
    $(".language-box a").on('click', function () {
        $(".language-box a").not('.active').addClass('active');
        $(this).removeClass('active');

    })

    /*timelineSlider*/
    var timelineSlider = $(".timelineSlider");
    if (timelineSlider.length > 0) {
        $("#timeline-slider .slider .item").on('click', function () {
            $("#timeline-slider .slider .item").removeClass('active');
            $(this).addClass('active');

            var indexItemActive = $("#timeline-slider .slider .item").index($(this));
            if ((indexItemActive > 0) && (indexItemActive < $("#timeline-slider .slider .item").length - 1)) {
                $("#timeline-slider .slider").css({
                    "height": 80 * 5 + 30 + 80 * 2
                })
                $("#timeline-slider .slider .wrapper").css({
                    'marginTop': (indexItemActive - 1) * (-80) + 80
                })
            }
            else {
                $("#timeline-slider .slider").css({
                    "height": 80 * 5 + 80 * 2
                })
                $("#timeline-slider .slider .wrapper").css({
                    'marginTop': 0
                })
            }

            var $contentItemActive = $(this).find('a').attr('href');
            $('.content .item-content').removeClass('active');

            $($contentItemActive).addClass('active')
            $('.slider .wrapper .item.active').find('a').css({
                'width': '100px',
                'height': '70px',
                'lineHeight': '70px'
            });
        })
    }

    /*tab*/
    var tab_customize = $(".tab-customize");
    if (tab_customize.length > 0) {
        $('.tab-customize li').on('click', function () {
            $('.tab-customize li').removeClass('active');
            $(this).addClass('active');
            var contentActive = $(this).find('a').attr('href');
            $('.content').removeClass('active');
            $(contentActive).addClass('active');
        })

    }
    /*$.changeActive('#accordion');*/
    $('#accordion').changeActive($('#accordion'));
    $('.tab-list.action-click').changeActive($('.tab-list.action-click'));

    $("#uploadBtn").change(function () {
        $("#uploadFile").value = this.value;
    })
    $('.bottom-links a').first().click(function (event) {
        event.preventDefault();
        $("#transaction-network").toggleClass("open");
    })

   /* var quickbox = $('#quick-link.inner');

    if (quickbox.length > 0) {
        $(window).on('scroll', function () {
            if ($(window).scrollTop() > 100) {
                quickbox.addClass('open');
            } else {
                quickbox.removeClass('open');
            }
        });
    }
*/
    $(".my-customize-menu .nav>li.dropdown .dropdown-menu")
        .mouseover(function () {
            $('body').css({
                'position': 'fixed',
                'width': '100%',
                'left': '0',
                'top': '0',
                'overflow-y': 'scroll'
            })
        })
        .mouseout(function () {
            $('body').css({
                'position': 'static',
                'overflow-y': 'inherit'
            })
        });

    $('.sitemap-page .drop-list > span > i').click(function(){
        $(this).parents('.drop-list').first().toggleClass('open');
        console.log('a');
    })
})
$.fn.changeActive = function (id) {
    id.find('.item').on('click', function () {
        id.find('.item').removeClass('active');
        $(this).toggleClass('active');
    })
};


