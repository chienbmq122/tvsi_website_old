﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for Constants
/// </summary>
public class ConstantsFAQ
{
    public const string CST_MAX_RECORD = "50000";
    public const int CST_DEFAULT_RECORD_PER_PAGE = 10;
    public const string PATH_ANH_HOAT_DONG_TVSI = "~/thu vien anh/hoat dong tvsi/";
    public const string PATH_ANH_TVSI_CONG_DONG = "~/thu vien anh/tvsi cong dong/";
    public const string PATH_ANH_TVSI_BAO_CHI = "~/thu vien anh/tvsi bao chi/";
    public const string PATH_ANH_HOAT_DONG_TVSI_E = "~/thu vien anh/TVSI activities/";
    public const string PATH_ANH_TVSI_CONG_DONG_E = "~/thu vien anh/TVSI with the community/";
    public const string PATH_ANH_TVSI_BAO_CHI_E = "~/thu vien anh/TVSI with press/";
    public const string KY_TU_BAO_MAT_CA_NHAN = "tvsi_canhan";
    public const string KY_TU_BAO_MAT_TO_CHUC = "tvsi_tochuc";
}

public class UtilsConstant
{
    public const string Phone = "Phone";
}

public class UtilsBranchID
{
    public const string DVGDHS = "12";
    public const string DVGDHCM = "02";
    public const string DVKH = "54";
    public const string DVDTHS = "53-002";
    public const string DVDTHCM = "53-001";
}


