﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net.Mail;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Web.Services.Protocols;
using System.Net;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Website_tvsi_v1.App_Data
{
    /// <summary>
    /// Summary description for Utilities
    /// </summary>
    public class Utilities
    {
        private static readonly HttpClient _Client = new HttpClient();
        private static JavaScriptSerializer _Serializer = new JavaScriptSerializer();
        public Utilities()
        {


            //
            // TODO: Add constructor logic here
            //
        }

        public static String ReadFileToString(string fileName)
        {
            System.Web.Services.WebService s = new System.Web.Services.WebService();
            fileName = s.Server.MapPath(fileName);
            System.IO.StreamReader reader = new System.IO.StreamReader(fileName);
            string result = reader.ReadToEnd();
            reader.Close();
            return result;
        }

        /// <summary>
        /// Hiển thị dữ liệu FAQ theo trang
        /// </summary>
        /// <param name="xsltFile">File xslt template</param>
        /// <param name="resultsXml">Chuỗi XML chứa tất cả dữ liệu FAQ</param>
        /// <returns></returns>
        public static string DisplayResults(string xsltFile, string resultsXml)
        {
            if (resultsXml.Trim() == "Aplication error.")
                return "";
            string xslt;
            // Get the xslt to transform the results with
            using (FileStream fileStream = File.OpenRead(xsltFile))
            {
                xslt = ININ.Xml.FileHelper.ConvertStreamToString(fileStream);
            }
            // Transform the results via xslt and display them
            StringBuilder sBuilder = new StringBuilder();
            //Loại bỏ các ký tự đặc biệt trong HTML
            //System.Security.SecurityElement.Escape(resultsXml);
            string sXML = resultsXml.Replace("<BR/>", "&lt;br/&gt; ");
            return ININ.Xml.XsltTransformer.Transform(sXML, xslt);
        }

        public static void SendEFAQAnswer(String mailTo,String subject, String message)
        {
            MailMessage mailMsg = new MailMessage();
            mailMsg.Subject = subject;
            mailMsg.Body = message;
            mailMsg.BodyEncoding = System.Text.Encoding.GetEncoding("UTF-8"); // Turkish Character Encoding
            mailMsg.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
            mailMsg.To.Add(new MailAddress(mailTo));
            mailMsg.IsBodyHtml = true;
            System.Net.Mail.SmtpClient Smtp = new SmtpClient();
            Smtp.Host = ConfigurationManager.AppSettings["SmtpServer"]; // for example gmail smtp server
            Smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPassWord"]);
            Smtp.Send(mailMsg);
        }

        public static SoapHttpClientProtocol GetService(Type type)
        {
            string url = ConfigurationSettings.AppSettings["WEB.SERVICE.URL"];
            string encode = ConfigurationSettings.AppSettings["WEB.SERVICE.ENCODE"];
            string timeOut = ConfigurationSettings.AppSettings["WEB.SERVICE.TIMEOUT"];
            string className = ConfigurationSettings.AppSettings["WEB.SERVICE.CLASS_NAME"];
            return GetService(url, encode, timeOut, type, className);
        }
        public static SoapHttpClientProtocol GetService(
            string url, string encode, string timeOut, Type type, string className)
        {
            Type classType = type.Assembly.GetType(className);
            SoapHttpClientProtocol service = (SoapHttpClientProtocol)
                    Activator.CreateInstance(type);           
            String strUrl = url.Trim('/', '\\', ' ');
            service.Url = strUrl;
            service.Proxy = WebProxy.GetDefaultProxy();
            service.Timeout = int.Parse(timeOut);
            service.RequestEncoding = Encoding.GetEncoding(encode);
            return service;
        }

        public static string GenerateOTP()
        {
            
            string numbers = "1234567890";

            string characters = numbers;

            int length = 6;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp.ToString();
        }
        public static string MD5EncodePassword(string originalPassword)
        {
            //Declarations
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            

            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);

            //Convert encoded bytes back to a 'readable' string
            return BitConverter.ToString(encodedBytes);
        }

        public static void clearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                clearFolder(di.FullName);
                di.Delete();
            }
        }
        
        
            /// <summary>
            /// Makes an async HTTP Request
            /// </summary>
            /// <param name="pMethod">Those methods you know: GET, POST, HEAD, etc...</param>
            /// <param name="pUrl">Very predictable...</param>
            /// <param name="pJsonContent">String data to POST on the server</param>
            /// <param name="pHeaders">If you use some kind of Authorization you should use this</param>
            /// <returns></returns>
            public static async Task<HttpResponseMessage> Request(HttpMethod pMethod, string pUrl, string pJsonContent,
                Dictionary<string, string> pHeaders)
            {
                var httpRequestMessage = new HttpRequestMessage();
                httpRequestMessage.Method = pMethod;
                httpRequestMessage.RequestUri = new Uri(pUrl);
                foreach (var head in pHeaders)
                {
                    httpRequestMessage.Headers.Add(head.Key, head.Value);
                }

                switch (pMethod.Method)
                {
                    case "POST":
                        HttpContent httpContent = new StringContent(pJsonContent, Encoding.UTF8, "application/json");
                        httpRequestMessage.Content = httpContent;
                        break;
                }

                return await _Client.SendAsync(httpRequestMessage);
            }




    }

    #region Get DCom
    /// <summary>
    /// Define RPC_C_AUTHN_LEVEL_ constants
    /// </summary>
    public enum RpcAuthnLevel
    {
        Default = 0,
        None,
        Connect,
        Call,
        Pkt,
        PktIntegrity,
        PktPrivacy
    }

    /// <summary>
    /// Define RPC_C_IMP_LEVEL_ constants
    /// </summary>
    public enum RpcImpLevel
    {
        Default = 0,
        Anonymous,
        Identify,
        Impersonate,
        Delegate
    }

    /// <summary>
    /// Define EOAC_ constants
    /// </summary>
    public enum EoAuthnCap
    {
        None = 0x0000,
        MutualAuth = 0x0001,
        StaticCloaking = 0x0020,
        DynamicCloaking = 0x0040,
        AnyAuthority = 0x0080,
        MakeFullSIC = 0x0100,
        Default = 0x0800,
        SecureRefs = 0x0002,
        AccessControl = 0x0004,
        AppID = 0x0008,
        Dynamic = 0x0010,
        RequireFullSIC = 0x0200,
        AutoImpersonate = 0x0400,
        NoCustomMarshal = 0x2000,
        DisableAAA = 0x1000
    }

    /// <summary>
    /// InitVS handles the special COM/DCOM startup code required by
    /// the Virtual Server security model.
    /// </summary>
    public class DComConnect
    {
        // Create the call with PreserveSig:=FALSE so the COM InterOp
        // layer will perform the error checking and throw an 
        // exception instead of returning an HRESULT.
        //
        [DllImport("Ole32.dll",
               ExactSpelling = true,
               EntryPoint = "CoInitializeSecurity",
               CallingConvention = CallingConvention.StdCall,
               SetLastError = false,
               PreserveSig = false)]

        private static extern void CoInitializeSecurity(
            IntPtr pVoid,
            int cAuthSvc,
            IntPtr asAuthSvc,
            IntPtr pReserved1,
            uint dwAuthnLevel,
            uint dwImpLevel,
            IntPtr pAuthList,
            uint dwCapabilities,
            IntPtr pReserved3);

        /// <summary>
        /// Call CoInitializeSecurity with dwImpLevel set to 
        /// Impersonate. Required by the Virtual Server COM Interface.  
        /// </summary>
        public DComConnect()
        {
            Connect();
        }

        public static void Connect()
        {
            CoInitializeSecurity(IntPtr.Zero,
                    -1,
                    IntPtr.Zero,
                    IntPtr.Zero,
                    (uint)RpcAuthnLevel.PktPrivacy,
                    (uint)RpcImpLevel.Impersonate,
                    IntPtr.Zero,
                    (uint)EoAuthnCap.DynamicCloaking,
                    IntPtr.Zero);
        }



    }
    #endregion

}
        