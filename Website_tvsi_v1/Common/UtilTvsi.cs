﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Script.Serialization;
using System.Xml;
using Dapper;
using DocumentFormat.OpenXml.EMMA;
using Newtonsoft.Json;
using TVSI.Utility;
using Website_tvsi_v1.App_Data;
using Website_tvsi_v1.Models;
using Website_tvsi_v1.WebService;
using Website_tvsi_v1.WebService.Model;
using OtpNet;
using ProvinceModel = Website_tvsi_v1.Models.ProvinceModel;


namespace Website_tvsi_v1.Common
{
    public class UtilTvsi
    {
        private static readonly HttpClient _client = new HttpClient();
        private static JavaScriptSerializer _Serializer = new JavaScriptSerializer();

        public static object EncryptHelper { get; private set; }

        public static string MauVNINDEX(double giatri)
        {
            string color = "";
            if (giatri > 0)
            {
                color = "#00FF00";
            }
            else if (giatri == 0)
            {
                color = "#FF9900";
            }
            else
            {
                color = "#FF0000";
            }
            return color;
        }
        public static bool isValidDateTime(string value, string formatTime = "dd/MM/yyyy")
        {
            try
            {
                DateTime timeValue;
                return DateTime.TryParseExact(value, formatTime,CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out timeValue);
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public static bool checkDateYear(DateTime date)
        {
            var yearnow = DateTime.Now.Year;
            var totalold = yearnow - date.Year;
            if (totalold > 18 && totalold < 150)
            {
                return true;
            }
            return false;
        }

        public static bool checkDatePassport(DateTime date)
        {
            var yearnow = DateTime.Now.Year;
            var total = yearnow - date.Year;
            if (total < 100)
            {
                return true;
            }
            return false;
        }

        public static bool checkExpiredDatePassport(DateTime date)
        {
            Logger.Info(typeof(UtilTvsi),string.Format("checkExpiredDatePassport - " + date));
            var datetime = DateTime.Now.ToString("dd/MM/yyyy");
            DateTime pdatetime = DateTime.ParseExact(datetime, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dateexpired = date.ToString("dd/MM/yyyy");
            DateTime pdateexpired = DateTime.ParseExact(dateexpired, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var yearnow = DateTime.Now.Year;
            var total = date.Year - yearnow;
            Logger.Info(typeof(UtilTvsi),"Total - " + total);
            Logger.Info(typeof(UtilTvsi),"dateexpired - " + pdateexpired);
            Logger.Info(typeof(UtilTvsi),"datetime - " + pdatetime);
            if (pdateexpired > pdatetime)
            {
                if (total < 100)
                {
                    return true;
                }
            }
            return false;
        }
        
        // Ham check cmnd con hieu luc > 15 nam
        public static bool IsValidCardId15Year(string dateCardIdClient)
        {
            var dateclient = DateTime.ParseExact(dateCardIdClient, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ddatenow = DateTime.Now.ToString("dd/MM/yyyy");
            var datenow = DateTime.ParseExact(ddatenow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var clientfifteenyear = dateclient.AddYears(15);
            var nDatenow = datenow - dateclient;
            var totaldate = clientfifteenyear - dateclient;
            return totaldate > nDatenow;
        }

        public static bool checkDateTradingCode(DateTime dateTime)
        {
            var datetimenow = DateTime.Now.ToString("dd/MM/yyyy");
            DateTime pdatetimenow = DateTime.ParseExact(datetimenow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dateissue = dateTime.ToString("dd/MM/yyyy");
            DateTime pdateissue = DateTime.ParseExact(dateissue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dateset = "28/07/2000";
            DateTime pdateset = DateTime.ParseExact(dateset, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            Logger.Info(typeof(UtilTvsi),"checkDateTradingCode - " + datetimenow);
            Logger.Info(typeof(UtilTvsi),"checkDateTradingCode - " + dateissue);
            Logger.Info(typeof(UtilTvsi),"checkDateTradingCode - " + dateset);
            if (pdateissue <= pdatetimenow)
            {
                if (pdateissue > pdateset)
                {
                    Logger.Info(typeof(UtilTvsi), " - checkDateTradingCode  return TRUE");
                    return true;
                }
            }
            Logger.Info(typeof(UtilTvsi), " - checkDateTradingCode  return false");
            return false;
        }
        public static List<string> getPathImageDeleteAll(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                List<string> listDel = new List<string>();
                listDel.Add(path);
                return listDel;   
            }
            return null;
        }
        // ham check tren 18 tuoi
        public static bool IsLegalAge18(string oldDate) 
        {
            var bday = Convert.ToDateTime(oldDate).ToString("dd/MM/yyyy");
            DateTime pbday = DateTime.ParseExact(bday, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var datenow = DateTime.Now.ToString("dd/MM/yyyy");
            DateTime pdDateTime = DateTime.ParseExact(bday, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var ts = pdDateTime - pbday;
            var year = DateTime.MinValue.Add(ts).Year - 1;
            return year >= 18;
        }

        public static bool CalculateAge(string ddateOfBirth) 
        { 
            DateTime dateOfBirth = DateTime.ParseExact(ddateOfBirth, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear) 
                age = age - 1;

            return age >= 18;
        }
        public static bool CheckUnderAge18(string dateOfBirthh)
        {
            try
            {
                var dateOfBirth =  DateTime.ParseExact(dateOfBirthh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var today = DateTime.Today;
                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                return (a - b) / 10000 >= 18;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("CheckUnderAge18 - " + ex.Message));
                return false;
            }
        }

        public static bool CheckValidDate(string validdate)
        {
            try
            {
                DateTime validateid = DateTime.ParseExact(validdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var now = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime datenow = DateTime.ParseExact(now, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                return  validateid > datenow;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("CheckValidDate - " + ex.Message));
                return false;
            }
        }
        
        public static Saleinfo GetMailSaleById(string saleid)
        {
            try
            {
                SqlConnection SqlConn = new SqlConnection(ConfigurationManager
                    .ConnectionStrings["EMS"].ConnectionString.ToString());
                SqlCommand comm = null;
                SqlDataReader drd;
                comm = new SqlCommand("TVSI_sGET_EMAIL_SALE", SqlConn);
                comm.Parameters.Add("@saleid", saleid);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                var saleinfo = new Saleinfo();
                while (drd.Read())
                {
                    saleinfo.email = drd["dia_chi_email"].ToString();
                    saleinfo.fullname = drd["ho_va_ten"].ToString();
                    saleinfo.gender = Convert.ToInt32(drd["gioi_tinh"]);
                }

                drd.Close();
                comm.Connection.Close();
                comm.Connection.Dispose();
                return saleinfo;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("Get Email By Sale - " + ex.Message));
                return null;
            }
        }
        
        
        public static string GetIDSale(string saleID)
        {
            try
            {
                SqlConnection SqlConn = new SqlConnection(ConfigurationManager
                    .ConnectionStrings["EMS"].ConnectionString.ToString());
                SqlCommand comm = null;
                SqlDataReader drd;
                comm = new SqlCommand("TVSI_sGET_ID_SALE", SqlConn);
                comm.Parameters.Add("@id_he_thong", saleID.Substring(0,4));
                comm.CommandType = CommandType.StoredProcedure;
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                var saleinfo = String.Empty;
                while (drd.Read())
                {
                    saleinfo = drd["ma_nhan_vien_quan_ly"].ToString();
                }
                drd.Close();
                comm.Connection.Close();
                comm.Connection.Dispose();
                return saleinfo;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetIDSale - " + ex.Message));
                return null;
            } 
        }

        public static String ArrowIndex(double giatri)
        {
            String arrowIndex = "fa";
            if (giatri > 0)
            {
                arrowIndex = "fa fa-caret-up";
            }
            else if (giatri == 0)
            {
                arrowIndex = "fa";
            }
            else
            {
                arrowIndex = "fa fa-caret-down";
            }
            return arrowIndex;
        }
        public static double TVSI_fGetDouble(object inObj)
        {
            try
            {
                return Convert.ToDouble(inObj.ToString().Replace(",", ""));
            }
            catch
            {
                return 0;
            }
        }
        public static StockMarketModel Thongtinthitruong(DataRow thitruong_today, DataRow thitruong_lastday)
        {

            StockMarketModel stockMarket = new StockMarketModel();
            stockMarket.vnIndex = double.Parse(TVSI.Common.Lib.FormatDouble(TVSI_fGetDouble(thitruong_today["Chi_So_Index"])));
            stockMarket.volumnIndex = double.Parse(TVSI.Common.Lib.FormatDouble(TVSI_fGetDouble(thitruong_today["tong_khoi_luong"]) / 1000000));
            stockMarket.valueIndex = double.Parse(TVSI.Common.Lib.FormatDouble(TVSI_fGetDouble(thitruong_today["tong_gia_tri"]) / 1000000000));
            stockMarket.changeIndex = double.Parse(TVSI.Common.Lib.FormatDouble(TVSI_fGetDouble(thitruong_today["Chi_So_Index"]) - TVSI_fGetDouble(thitruong_lastday["gia_dong_cua"])));
            if (stockMarket.changeIndex != 0)
            {
                stockMarket.percentIndex = double.Parse(TVSI.Common.Lib.FormatDouble(stockMarket.changeIndex * 100 / TVSI_fGetDouble(thitruong_lastday["gia_dong_cua"])));
            }
            else
            {
                stockMarket.percentIndex = 0;
            }
            stockMarket.colorIndex = MauVNINDEX(stockMarket.percentIndex);
            stockMarket.arrowIndex = ArrowIndex(stockMarket.percentIndex);

            return stockMarket;

        }
        public static string ReturnNoiDungMail(DangKyKhCaNhanModel dangkycanhan)
        {
            string strReturn = string.Empty;
            strReturn += "<table width=\"760px\">";
            strReturn += "<tr>";
            strReturn += "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn += "<p style=\"color:white; font-size:18px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "THÔNG BÁO <br />";
            strReturn += "ĐĂNG KÝ THÔNG TIN THÀNH CÔNG";
            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px; font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b> Kính gửi: Mr/Ms " + dangkycanhan.hoten + "</b><br />";
            strReturn += "Quý khách đã đăng ký mở tài khoản giao dịch chứng khoán tại Công ty Cổ phần Chứng khoán Tân Việt (TVSI) thành công với các thông tin cụ thể như sau:<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";
            strReturn += "Họ tên: " + dangkycanhan.hoten + "<br />";
            strReturn += "Ngày sinh: " + dangkycanhan.ngaysinh + " <br />";
            strReturn += "Số CMND: " + dangkycanhan.cmnd + "<br />";
            strReturn += "Ngày cấp: " + dangkycanhan.ngaycapcmnd + "<br />";
            strReturn += "Số điện thoại: " + dangkycanhan.Phone_01 + "<br />";
            strReturn += "Email: " + dangkycanhan.email + "<br />";
            strReturn += "Địa chỉ: " + dangkycanhan.diachi + "<br />";
            strReturn += "Yêu cầu : " + dangkycanhan.yeucau + "<br />";
            strReturn += "</p>";
            strReturn += "Cảm ơn Quý khách đã quan tâm và sử dụng sản phẩm – dịch vụ của TVSI<br />";
            strReturn += "<b>Chúng tôi sẽ liên hệ trong vòng 01 ngày làm việc để hoàn tất việc mở tài khoản.</b><br />";
            strReturn += "Để được hỗ trợ về mở tài khoản, vui lòng liên hệ tổng đài chăm sóc khách hàng 1900 1885 của TVSI<br /><br />";
            strReturn += "Trân trọng cảm ơn.<br />";
            strReturn += "<b>Chứng khoán Tân Việt</b><br />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"height:10px;\">&nbsp;</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn += "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn += "<p style=\"color:white; font-size:20px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "SUCCESFUL REGISTRATION";

            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px;font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b>Dear Mr/Ms " + dangkycanhan.hoten + "</b><br />";
            strReturn += "You have finished the registration open account at Tan Viet Securities Incorporation (TVSI):<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";

            strReturn += "Name: " + dangkycanhan.hoten + "<br />";
            strReturn += "Date of birth: " + dangkycanhan.ngaysinh + "<br />";
            strReturn += "ID card: " + dangkycanhan.cmnd + "<br />";
            strReturn += "Date Issue: " + dangkycanhan.ngaycapcmnd + "<br />";
            strReturn += "Mobile phone: " + dangkycanhan.Phone_01 + "<br />";
            strReturn += "Email: " + dangkycanhan.email + "<br />";
            strReturn += "Address: " + dangkycanhan.diachi + "<br />";
            strReturn += "Notice: " + dangkycanhan.yeucau + "<br />";
            strReturn += "</p>";
            strReturn += "TVSI customer service representative will contact you within 24 hours working day.<br />";
            strReturn += "Please kindly contact to TVSI Contact Center 1900 1885 if you have any questions.<br />";
            strReturn += "Thank you & Best Regard<br />";
            strReturn += "TVSI<br />";

            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "</table>";
            return strReturn;
        }

        public static string ReturnNoiDungMailTochuc(DangKyKhToChucModel dangkytochuc)
        {
            string strReturn = string.Empty;
            strReturn += "<table width=\"760px\">";
            strReturn += "<tr>";
            strReturn += "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn += "<p style=\"color:white; font-size:18px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "THÔNG BÁO <br />";
            strReturn += "ĐĂNG KÝ THÔNG TIN THÀNH CÔNG";
            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px; font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b> Kính gửi: Mr/Ms " + dangkytochuc.hoten + "</b><br />";
            strReturn += "Quý khách đã đăng ký mở tài khoản giao dịch chứng khoán tại Công ty Cổ phần Chứng khoán Tân Việt (TVSI) thành công với các thông tin cụ thể như sau:<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";
            strReturn += "Họ tên: " + dangkytochuc.hoten + "<br />";
            strReturn += "Tên tổ chức : " + dangkytochuc.tentochuc + " <br />";
            strReturn += "Số đăng ký kinh doanh: " + dangkytochuc.sodkkd + "<br />";
            strReturn += "Ngày cấp: " + dangkytochuc.ngaycap + "<br />";
            strReturn += "Số di động: " + dangkytochuc.sodidong + "<br />";
            strReturn += "Email: " + dangkytochuc.email + "<br />";
            strReturn += "Địa chỉ: " + dangkytochuc.diachi + "<br />";
            strReturn += "Số điện thoại cơ quan: " + dangkytochuc.sodienthoai + "<br />";
            strReturn += "Số máy fax: " + dangkytochuc.somayfax + "<br />";
            strReturn += "</p>";
            strReturn += "Cảm ơn Quý khách đã quan tâm và sử dụng sản phẩm – dịch vụ của TVSI<br />";
            strReturn += "<b>Chúng tôi sẽ liên hệ trong vòng 01 ngày làm việc để hoàn tất việc mở tài khoản.</b><br />";
            strReturn += "Để được hỗ trợ về mở tài khoản, vui lòng liên hệ tổng đài chăm sóc khách hàng 1900 1885 của TVSI<br /><br />";

            strReturn += "Trân trọng cảm ơn.<br />";
            strReturn += "<b>Chứng khoán Tân Việt</b><br />";

            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"height:10px;\">&nbsp;</td>";
            strReturn += "</tr>";

            strReturn += "<tr>";
            strReturn += "<td style=\"width:680px; height:60px; background-color:#ff0000;border: 0px solid #ff0000;border-top-left-radius: 50px; border-bottom-right-radius:50px; \">";
            strReturn += "<p style=\"color:white; font-size:20px; font-weight:bold; padding-left:40px; font-family:Tahoma;\">";
            strReturn += "SUCCESFUL REGISTRATION";

            strReturn += "</p>";
            strReturn += "</td>";
            strReturn += "<td style=\"padding-left:20px;\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/saoke/logo-tvsi-saokecuoithang.jpg\" height=\"60\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\" style=\"text-align:justify; width:700px; padding-left:30px; padding-right:30px; padding-top:20px; padding-bottom:20px;font-family:Tahoma; font-size:10pt;\">";
            strReturn += "<b>Dear Mr/Ms " + dangkytochuc.hoten + "</b><br />";
            strReturn += "You have finished the registration open account at Tan Viet Securities Incorporation (TVSI):<br />";
            strReturn += "<p style=\"padding-left:30px; font-weight:bold;\">";

            strReturn += "Name: " + dangkytochuc.hoten + "<br />";
            strReturn += "Organization name: " + dangkytochuc.tentochuc + "<br />";
            strReturn += "ID Business : " + dangkytochuc.sodkkd + "<br />";
            strReturn += "Date Issue: " + dangkytochuc.ngaycap + "<br />";
            strReturn += "Mobile phone: " + dangkytochuc.sodidong + "<br />";
            strReturn += "Email: " + dangkytochuc.email + "<br />";
            strReturn += "Address: " + dangkytochuc.diachi + "<br />";
            strReturn += "Office telephone: " + dangkytochuc.sodienthoai + "<br />";
            strReturn += "Fax number: " + dangkytochuc.somayfax + "<br />";
            strReturn += "</p>";
            strReturn += "TVSI customer service representative will contact you within 24 hours working day.<br />";
            strReturn += "Please kindly contact to TVSI Contact Center 1900 1885 if you have any questions.<br />";
            strReturn += "Thank you & Best Regard<br />";
            strReturn += "TVSI<br />";

            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "<tr>";
            strReturn += "<td colspan=\"2\">";
            strReturn += "<img src=\"http://www.tvsi.com.vn/images/dktk/footer_dangky_tai_khoan_email_vn.jpg\" />";
            strReturn += "</td>";
            strReturn += "</tr>";
            strReturn += "</table>";
            return strReturn.ToString();
        }

        public static bool ExistsSaleID(string strSales_ID, string CRMStr)
        {
            SqlConnection SqlConn = new SqlConnection(CRMStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlParameter para_saleid = new SqlParameter("@AssignUser", strSales_ID);
            SqlCommand comm = new SqlCommand("select * from CustInfo where SUBSTRING(AssignUser, 0, 5) = @AssignUser", SqlConn);
            comm.Parameters.Add(para_saleid);
            comm.CommandType = CommandType.Text;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        
        public static bool ExistsSaleIDEMS(string strSales_ID, string EMSStr)
        {
           var isNumer =  UtilTvsi.IsNumeric(strSales_ID.Replace("-",""));
           if (isNumer)
           {
               var SaleNumber = UtilTvsi.ExistsSaleIDEMSINT(strSales_ID,EMSStr);
               return SaleNumber;
           }
           else
           {
               var SaleStr = UtilTvsi.ExistsSaleIDEMSSTR(strSales_ID, EMSStr);
               return SaleStr;
           }
           return false;
        }

        public static string GetNumberSaleID(string strSales_ID, string EMSStr)
        {
            var getSaleStr = string.Empty;
            using (SqlConnection sqlConnection = new SqlConnection(EMSStr))
            using (SqlCommand comm = new SqlCommand("TVSI_sGET_ID_SALE", sqlConnection))
            {
                SqlDataReader drd;
                comm.Parameters.Add("@id_he_thong", strSales_ID);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Connection.Open();
                comm.ExecuteNonQuery();
                drd = comm.ExecuteReader();
                while (drd.Read())
                {
                    getSaleStr = drd["id_he_thong"].ToString();
                }

                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return getSaleStr;
        }

        public static bool CheckIssueDateForm(string cardID, DateTime issueDate)
        {
            try
            {
                var currentDate = DateTime.Now;
                var checkNumber = IsNumeric(cardID);
                if (checkNumber)
                {
                    if (cardID.Length == 9)
                    {
                        return currentDate <= issueDate.AddYears(15);
                    }
                    if (cardID.Length == 12)
                    {
                        return true;
                    }
                    if (cardID.Length != 9 && cardID.Length != 12)
                    {
                        return currentDate <= issueDate.AddYears(12);
                    }
                }
                return currentDate <= issueDate.AddYears(12);

            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi), "CheckDateIssue " + e.Message);
                return false;
            }
        }

        
        public static bool CheckIssueDateEKYC(string cardID, int type, DateTime issueDate)
        {
            try
            {
                var currentDate = DateTime.Now;
                if (type == 0 || type == 1)
                {
                    
                    if (cardID.Length == 9)
                    {
                        return currentDate <= issueDate.AddYears(15);
                    }
                    if (cardID.Length == 12)
                    {
                        return true;
                    }
                }
                if (type == 3)
                {
                    return currentDate <= issueDate.AddYears(12);
                }
                if (type == 7)
                {
                    return currentDate <= issueDate.AddYears(12);
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi), "CheckIssueDateEKYC " + e.Message);
                return false;
            }
        }


        public async static Task<SaleData> GetSaleIDByQA(int value,int type)
        {
            try
            {
                if (type == 1)
                    switch (value)
                    {
                        case 1: return await GenerateSaleID(UtilsBranchID.DVGDHS,value);
                        case 2: return await GenerateSaleID(UtilsBranchID.DVDTHS,value);
                        case 3: return await GenerateSaleID(UtilsBranchID.DVKH,value);
                    }

                if (type == 2)
                    switch (value)
                    {
                        case 1: return await GenerateSaleID(UtilsBranchID.DVGDHCM,value);
                        case 2: return await GenerateSaleID(UtilsBranchID.DVDTHCM,value);
                        case 3: return await GenerateSaleID(UtilsBranchID.DVKH,value);
                    }

                return null;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi),
                    string.Format("GetSaleIDByQA ---> " + "{0}" + "GetSaleIDByQA ---> " + "{1}", e.Message,
                        e.InnerException));
                return null;
            }
        }

        public async static Task<SaleData> ProcessGenSaleID(string province, int value)
        {
            try
            {
                var crmStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                using (var conn = new SqlConnection(crmStr))
                {
                    var sql = "SELECT Type FROM Province WHERE ProvinceCode = @ProvinceCode And Status = 1";
                    var typeProvince = await conn.QueryFirstOrDefaultAsync<int>(sql, new
                    {
                        ProvinceCode = province
                    });
                    if (typeProvince > 0)
                    {
                       return await GetSaleIDByQA(value, typeProvince);
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi), 
                    string.Format("GenerateSaleID ---> " + "{0}" + "GenerateSaleID ---> " + "{1}", e.Message,e.InnerException));
                throw;
            }
        }

        public async static Task<SaleData> GenerateSaleID(string branchID, int value)
        {
            try
            {
                var crmStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                var sql = "SELECT * FROM WorkTime WHERE Status = 1 {condition}";
                if (value == 2)
                    sql = sql.Replace("{condition}", "and BranchCode = @BranchCode");
                else
                    sql = sql.Replace("{condition}", "and BranchID = @BranchID");
                
                using (var conn = new SqlConnection(crmStr))
                {
                    var data = await conn.QueryAsync<GenerateSaleIDModel>(sql, new
                    {
                        BranchID = branchID,
                        BranchCode = branchID,
                    });
                    if (data.Any())
                    {
                        var min = data.Min(x => x.Count);
                        var dataSale = data.Select( t => t).Where(x => x.Count == min).FirstOrDefault();
                        if (dataSale != null)
                        {
                            var workTimeID = dataSale.WorkTimeID;
                            var countSet = Convert.ToInt64(dataSale.Count) + 1;

                            var sqlUP = "update WorkTime set Count = @Count where WorkTimeID = @WorkTimeID";
                            if (workTimeID > 0)
                            {
                                using (var connn = new SqlConnection(crmStr))
                                {
                                    await connn.ExecuteAsync(sqlUP, new
                                    {
                                        Count = countSet,
                                        WorkTimeID = workTimeID
                                    });
                                }
                            }
                        }

                        if (dataSale != null && dataSale.SaleID != null && dataSale.SaleID.Length >= 4)
                        {
                            return new SaleData()
                            {
                                BranchID = branchID,
                                SaleID = dataSale.SaleID.Substring(0, 4)
                            };
                        }
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi), 
                    string.Format("GenerateSaleID ---> " + "{0}" + "GenerateSaleID ---> " + "{1}", e.Message,e.InnerException));
                return null;
            }
        }


        public static async Task<bool> CheckCustCodeWhiteListExisted(string custcode)
        {
            try
            {
                var commStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(commStr))
                {
                    var sql = "SELECT Custcode FROM DANH_SACH_XTRADE WHERE Custcode = @CustCode and STATUS = 1";
                    var data = conn.QueryFirstOrDefault<string>(sql, new
                    {
                        CustCode = custcode
                    });
                    return string.IsNullOrEmpty(data);
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi), 
                    string.Format("CheckCustCodeWhiteListExisted {0}", e.Message));
                throw;
            }
        }

        public static async Task<bool> InsertWhitelistXtrade(string custcode)
        {
            try
            {
                var commStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();

                using (var conn = new SqlConnection(commStr))
                {
                    var param = new DynamicParameters();
                    param.Add("@CustCode", custcode, DbType.String, ParameterDirection.Input);
                    var sql =
                        "INSERT INTO DANH_SACH_XTRADE(CustCode,CreatedBy,CreatedDate,Status) VALUES(@CustCode,@CustCode,GETDATE(),1)";
                    return await conn.ExecuteAsync(sql, param, commandType: CommandType.Text) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Error(typeof(UtilTvsi),
                    string.Format("InsertWhitelistXtrad {0}", e.Message));
                throw;
            }
        }
        public static async Task<List<string>> GetBranchList(string branchID)
        {
            try
            {
                var emsStr = ConfigurationManager.ConnectionStrings["EMS"].ToString();
                using (var conn = new SqlConnection(emsStr))
                {
                    conn.Open();
                    var listChildBranch = new List<string>();
                    var sql = "SELECT B.ma_quan_ly as SaleID FROM TVSI_THONG_TIN_TRUY_CAP A  INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id  LEFT JOIN TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id  WHERE c.ma_chi_nhanh = @branchID AND a.trang_thai = 1";
                    var listSubBranch = await conn.QueryAsync<string>(sql, new
                    {
                        branchID = branchID
                    });
                    if (listSubBranch.Count() > 0)
                    {
                        foreach (var data in listSubBranch)
                        {
                            listChildBranch.Add(data);
                        }
                    }
                    return listChildBranch;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), ex.Message);
                throw;
            }
        }

        public static bool IsNumericCardID(string value)
        {
            bool isNumeric = true;
            char[] digits = "0123456789".ToCharArray();
            char[] letters = value.ToCharArray();
            for (int k = 0; k < letters.Length; k++)
            {
                for (int i = 0; i < digits.Length; i++)
                {
                    if (letters[k] != digits[i])
                    {
                        isNumeric = false;
                        break;
                    }
                }
            }

            return isNumeric;
        }

        
        public static bool ExistsSaleIDEMSSTR(string strSales_ID, string EMSStr)
        {
            SqlConnection SqlConn = new SqlConnection(EMSStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sGet_String_Sale_ID", SqlConn);
            comm.Parameters.Add("@id_he_thong",strSales_ID);
            comm.CommandType = CommandType.StoredProcedure;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        public static bool ExistsSaleIDEMSINT(string strSales_ID, string EMSStr)
        {
            SqlConnection SqlConn = new SqlConnection(EMSStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sGet_Sale_ID", SqlConn);
            comm.Parameters.Add("@id_he_thong",strSales_ID);
            comm.CommandType = CommandType.StoredProcedure;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        public static bool IsNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }

        public static IList<TTLKVSDModel> ReadFileXmlVSD(string path)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/data/records/record");
            IList<TTLKVSDModel> ttlkvsdModels = new List<TTLKVSDModel>();
            foreach (XmlNode node in nodeList)
            {
                var ttlkvsd = new TTLKVSDModel();
                ttlkvsd.So_dang_ky_tv = node.SelectSingleNode("So_dang_ky_tv").InnerText;
                ttlkvsd.Ten_Tieng_Viet = node.SelectSingleNode("Ten_Tieng_Viet").InnerText;
                ttlkvsd.Ten_tieng_anh = node.SelectSingleNode("Ten_tieng_anh").InnerText;
                ttlkvsd.loai_thanh_vien = node.SelectSingleNode("loai_thanh_vien").InnerText;
                ttlkvsd.ten_viet_tat = node.SelectSingleNode("ten_viet_tat").InnerText;
                ttlkvsd.status = node.SelectSingleNode("status").InnerText;
                ttlkvsdModels.Add(ttlkvsd);
            }
            return ttlkvsdModels;
        }     
        public static IList<CountryCode> ReadFileCountryCode(string path)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(path);
            XmlNodeList itemList = Doc.DocumentElement.SelectNodes("Code");
            IList<CountryCode> cdilist = new List<CountryCode>();
            foreach (XmlNode currNode in itemList)
            {
                var countrycode = new CountryCode();
                countrycode.countrycode = currNode.Attributes["Value"].Value;
                countrycode.namecountry = currNode.Attributes["Text"].Value;
                cdilist.Add(countrycode);
            }
            return cdilist;
        }
        public static checkAccount CheckPinServicePackage(string pin,string id)
        {
            var webservice = new PackageWebService();
            try
            {
                var idUser = id;
                var pinUser = pin.Trim();
                var passwordMd5 = Utilities.MD5EncodePassword(pinUser);
                var checkAccount = webservice.CheckAccountVerification(Convert.ToString(idUser), passwordMd5);
                if (checkAccount == null)
                {
                    return null;
                }
                return checkAccount;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "UtilTvsi-CheckPinServicePackage-" + ex.Message.ToString());
                return null;
            }
        } 
        public static checkAccount CheckPassServicePackage(string pass,string id)
        {
            var webservice = new PackageWebService();
            try
            {
                var idUser = id;
                var pinUser = pass.Trim();
                var passwordMd5 = Utilities.MD5EncodePassword(pinUser);
                var checkAccount = webservice.CheckPassAccountVerification(idUser, passwordMd5);
                return checkAccount;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "UtilTvsi-CheckPassServicePackage-" + ex.Message.ToString());
                return null;
            }
        }
        
        

        public class RegisterConst
        {
            public const string Direct = "1";
            public const string ITradeHome = "2";
            public const string Phone = "1";
            public const string Internet = "2";
            public const string SMS = "1";
            public const string Email = "2";
            public const string RequisteredLetter = "1";
            public const string Monthly = "1";
            public const string Quarterly = "2";
            public const string Bank = "1";
            public const string Margin = "1";
        }
        


        public enum ServiceStatusConst
        {
            [Description("Không đăng ký")]
            KO_DANG_KY = -1,
            [Description("Chờ xử lý")]
            CHO_XU_LY = 0,
            [Description("Chờ HO xác nhận")]
            CHO_HO_XAC_NHAN = 1,
            [Description("Chờ KH xác nhận")]
            CHO_KH_XAC_NHAN = 2,
            [Description("Từ chối")]
            BUMAN_TU_CHOI = 99,
            [Description("Từ chối")]
            KSV_TU_CHOI = 98
        }

        //Decode
        public static string TVSI_DecodeBase64(string pstrBase)
        {
            try
            {
                string base64Decoded;
                byte[] data = System.Convert.FromBase64String(pstrBase);
                base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
                return base64Decoded;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "TVSI_DecodeBase64--" + ex.ToString());
                return "";
            }
        }

        //Check stk đang chờ xử lý trên EMS 

        public static List<string> GetProcessingCustCodeList()
        {
            string CommonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
            List<string> trOutPut = new List<string>();
            SqlConnection SqlConn = new SqlConnection(CommonStr);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("SELECT ma_khach_hang FROM TVSI_DANG_KY_MO_TAI_KHOAN where trang_thai_tk NOT IN('199','299','200')", SqlConn);
            comm.CommandType = CommandType.Text;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                while (drd.Read())
                {
                    trOutPut.Add(drd[0].ToString());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return trOutPut;
        }

        public static bool GetStatusContractEkyc(string custCode)
        {
            try
            {
                bool blockCust = false;
                string commonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(commonStr))
                {
                    SqlDataReader reader = null;
                    using (var comm = new SqlCommand("select ma_khach_hang from TVSI_DANG_KY_MO_TAI_KHOAN where trang_thai_ho_so = 200 and ma_khach_hang = @CustCode and (nguon_du_lieu = 4 or nguon_du_lieu = 6)",conn))
                    {
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@CustCode", custCode);
                        comm.Connection.Open();
                        reader = comm.ExecuteReader();
                        if (reader.Read())
                        {
                            blockCust = true;
                        }
                    }
                    return blockCust;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetNoContractEKYC - " + ex.Message));
                return false;
            }   
        }
        
        public static bool GetSourceEkyc(string custCode)
        {
            try
            {
                bool blockCust = false;
                string commonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(commonStr))
                {
                    SqlDataReader reader = null;
                    using (var comm = new SqlCommand("select ma_khach_hang from TVSI_DANG_KY_MO_TAI_KHOAN where ma_khach_hang = @CustCode and (nguon_du_lieu = 4 or nguon_du_lieu = 6)",conn))
                    {
                        comm.CommandType = CommandType.Text;
                        comm.Parameters.AddWithValue("@CustCode", custCode);
                        comm.Connection.Open();
                        reader = comm.ExecuteReader();
                        if (reader.Read())
                        {
                            blockCust = true;
                        }
                    }
                    return blockCust;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetNoContractEKYC - " + ex.Message));
                return false;
            }   
        }

        public static string RandomNumber(int length = 6)
        {
            //Initiate objects & vars  
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random random = new Random(BitConverter.ToInt32(seed, 0));
            int randNumber = 0;
            //Loop ‘length’ times to generate a random number or character
            String randomNumber = "";
            for (int i = 0; i < length; i++)
            {
                randNumber = random.Next(48, 58);
                randomNumber = randomNumber + (char)randNumber;
                //append random char or digit to randomNumber string

            }
            return randomNumber;
        }

        public static AccountInfoModel GetCustomerInfo(string custcode)
        {
            try
            {
                
                string conn = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                SqlConnection sqlconn = new SqlConnection(conn);
                SqlDataReader drd = null;
                SqlCommand comm = new SqlCommand(
                    "select ma_khach_hang CustCode,so_cmnd_passport CardID,ten_khach_hang FullName,so_dien_thoai_mb as Phone, dia_chi_email Email from TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @custcode and trang_thai = 1", sqlconn);
                comm.CommandType = CommandType.Text;
                comm.Parameters.AddWithValue("@custcode", custcode);
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    return new AccountInfoModel()
                    {
                        Custcode = drd["CustCode"].ToString(),
                        CardID = drd["CardID"].ToString(),
                        Email = drd["Email"].ToString(),
                        FullName = drd["FullName"].ToString(),
                        Phone = drd["Phone"].ToString()
                    };
                }

                return null;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public static string EncriptMd5(string content)
        {
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(content);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }

        
        public static int SendEmailOpenEKYC(string user, string so_dien_thoai,  string email,  string confirmCode, string  confirmSms, string url)
        {
            try
            {
                string smsMessage = ConfigurationManager.AppSettings["SMSMessage"];
                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đăng ký mở tài khoản của Khách hàng " + user;
                
                
                var templateFile = "";
                templateFile = AppDomain.CurrentDomain.BaseDirectory + @"Template\Mail_EKYCTemplateConfirm.html";
                
                
                var content = "";
                

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Khong load duoc noi dung email template");
                }
                var contentData = new EmailData
                {
                    FullName = user,
                    url = url,
                    ConfirmCode = confirmCode,
                    ConfirmSms = confirmSms
                };
                content = ReplaceEmailContent(content, contentData);
                Logger.Info(typeof(UtilTvsi), string.Format("User {0} gui mail cho {1}", user, email));
                // Ghi log vao DB

                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
                /*
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_SMS_INSERT(smsMessage + ConfirmSms + "Vui long khong cung cap ma OTP cho ai vi bat ki ly do nao. Tran Trong!", so_dien_thoai, 0);
                */
                return 1;
            }
            catch (Exception e)
            {
                Logger.Info(typeof(UtilTvsi), string.Format(" gui mail cho {0} -- {1}",email, e.Message ));
                return 0;
            }

        }       
        public static int SendEmailConfirmOpenEKYC(string user, string email,  string  ConfirmCode, string url)
        {
            try
            {
                string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
                var subject = "Chứng Khoán Tân Việt(TVSI): Xác thực đăng ký mở tài khoản của Ông(Bà): " + user;

                var templateFile = "";
                templateFile = HttpContext.Current.Server.MapPath("~/Template/Mail_EKYCTemplateConfirm.html");
                var content = "";

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Khong load duoc noi dung email template");
                }
                var contentData = new EmailData
                {
                    FullName = user,
                    url = url,
                    ConfirmCode = ConfirmCode,
                };
                content = ReplaceEmailContent(content, contentData);
                Logger.Info(typeof(UtilTvsi), string.Format("User {0} gui mail cho {1}", user, email));
                // Ghi log vao DB

                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
                return 1;
            }
            catch (Exception ex)
            {
                
                throw new Exception();
               
            }

        }
        public static int SendEmailContactEKYC(UserCRM model)
        {
            try
            {
                var urlCrm = ConfigurationManager.AppSettings["CRM_URL"].ToString();
                string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
                var subject = "CHÚC MỪNG QUÝ KHÁCH MỞ TÀI KHOẢN GIAO DỊCH CHỨNG KHOÁN THÀNH CÔNG TẠI TVSI";

                var templateFile = "";
                templateFile = HttpContext.Current.Server.MapPath("~/Template/Mail_EKYCTemplateContact.html");
                var content = "";

                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                }
                if (string.IsNullOrEmpty(content))
                {
                    throw new Exception("Khong load duoc noi dung email template");
                }
                var contentData = new EmailData
                {
                    FullName = model.FullName,
                    DateOfBirth = model.DateOfBirth,
                    CardID = model.CardID,
                    IssueDate = model.IssueDate,
                    CardIssue = model.CardIssue,
                    TypeAccount = model.TypeAccount,
                    url = urlCrm
                };
                content = ReplaceEmailContact(content, contentData);
                Logger.Info(typeof(UtilTvsi), string.Format("User {0} gui mail cho {1}", model.FullName, model.Email));
                // Ghi log vao DB

                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, model.Email, 0);
                return 1;
            }
            catch (Exception ex)
            {
                throw new Exception();
               
            }

        }

        public static bool SendMailToSaleEKYC(string saleName, string fullName, string birthDay, string cardID,
            string issueDate, string phone
            , string email, string address,int package ,string note,string channelOpen,string department,string subject,string emailSend)
        {
            var templateFile = string.Empty;
                templateFile = AppDomain.CurrentDomain.BaseDirectory + @"Template\Mail_SendSale_EKYC.html";
                var content = "";
                using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                {
                    using (var readFileStr = new StreamReader(readFile))
                    {
                        content = readFileStr.ReadToEnd();
                    }
                    if (string.IsNullOrEmpty(content))
                    {
                        Logger.Error(typeof(UtilTvsi),"Not content mail");
                    }

                    var packName = "";

                    if (package == 1)
                        packName = "Dịch vụ Tư vấn Giao Dịch";
                    if (package == 2)
                        packName = "Dịch vụ Đầu Tư";
                    if (package == 3)
                        packName = "Dịch vụ chủ động giao dịch hoàn toàn";

                    content = content.Replace("{Channel_Open}", channelOpen ?? "");
                    content = content.Replace("{Department}", department ?? "");
                    content = content.Replace("{SaleName}", saleName ?? "");
                    content = content.Replace("{FullName}", fullName ?? "");
                    content = content.Replace("{BirthDay}", birthDay ?? "");
                    content = content.Replace("{CardID}", cardID ?? "");
                    content = content.Replace("{IssueDate}", issueDate ?? "");
                    content = content.Replace("{Phone}", phone ?? "");
                    content = content.Replace("{Emai}", email ?? "");
                    content = content.Replace("{Address}", address ?? "");
                    content = content.Replace("{Package}", packName);
                    content = content.Replace("{Note}", note ?? "");
                    content = content.Replace("{CreateDate}", DateTime.Now.ToString("dd/MM/yyyy"));
                    TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, emailSend, 0); 
                    return true;
                }  
        } 
        public static bool SendMailToSaleForm(string saleName, string fullName, string birthDay, string cardID,
            string issueDate, string phone
            , string email, string address,string note,string channelOpen,string area,string subject,string emailSend)
        {
            var templateFile = string.Empty;
            templateFile = AppDomain.CurrentDomain.BaseDirectory + @"Template\Mail_SendSale_Form.html";
            var content = "";
            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
                if (string.IsNullOrEmpty(content))
                {
                    Logger.Error(typeof(UtilTvsi),"Not content mail");
                }
                    
                content = content.Replace("{Channel_Open}", channelOpen ?? "");
                content = content.Replace("{Area}", area ?? "");
                content = content.Replace("{SaleName}", saleName ?? "");
                content = content.Replace("{FullName}", fullName ?? "");
                content = content.Replace("{BirthDay}", birthDay ?? "");
                content = content.Replace("{CardID}", cardID ?? "");
                content = content.Replace("{IssueDate}", issueDate ?? "");
                content = content.Replace("{Phone}", phone ?? "");
                content = content.Replace("{Emai}", email ?? "");
                content = content.Replace("{Address}", address ?? "");
                content = content.Replace("{Note}", note ?? "");
                content = content.Replace("{CreateDate}", DateTime.Now.ToString("dd/MM/yyyy"));
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, emailSend, 0); 
                return true;
            }
        }
        
        public static int SendEmailOpenInternational(string user,int gioitinh, string so_dien_thoai,  string email,  string ConfirmCode)
        {

            string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
            var subject = "(TVSI) Verify your email address : " + email ;

            var templateFile = "";
            templateFile = HttpContext.Current.Server.MapPath("~/Template/Mail_InternationalTemplateNotification.html");
            var content = "";

            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }
            if (string.IsNullOrEmpty(content))
            {
                throw new Exception("Khong load duoc noi dung email template");
            }

            var dr = gioitinh == 1 ? "Mr" : "Ms";
            var contentData = new EmailData
            {
                FullName = user,
                dr = dr
            };
            content = ReplaceEmailInternational(content, contentData);
            Logger.Info(typeof(UtilTvsi), string.Format("User {0} gui mail cho {1}", user, email));
            // Ghi log vao DB
            TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, email, 0);
            return 1;
        }     
        public static int SendEmailForSale(DangKyKhCaNhanModel dangKyKhCaNhanModel,Saleinfo saleinfo)
        {

            string smsMessage = ConfigurationManager.AppSettings["SMSMessage"].ToString();
            var subject = "(TVSI) Verify your email address : " + dangKyKhCaNhanModel.email ;

            var templateFile = "";
            templateFile = HttpContext.Current.Server.MapPath("~/Template/Mail_SendSale.html");
            var content = "";

            using (var readFile = new FileStream(templateFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                using (var readFileStr = new StreamReader(readFile))
                {
                    content = readFileStr.ReadToEnd();
                }
            }
            if (string.IsNullOrEmpty(content))
            {
                throw new Exception("Khong load duoc noi dung email template");
            }
            content = ReplaceEmailContentForSale(content, saleinfo,dangKyKhCaNhanModel);
            Logger.Info(typeof(UtilTvsi), string.Format("User {0} gui mail cho {1}", saleinfo.fullname, saleinfo.email));
            // Ghi log vao DB
            TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, content, saleinfo.email, 0);
            return 1;
        }



        private static string ReplaceEmailContentForSale(string content,Saleinfo saleinfo,DangKyKhCaNhanModel dkkhcn)
        {
            var gender = dkkhcn.gioitinh == 1 ? "Nam/Male" : "Nữ/Female";
            var remittance = dkkhcn.BankTransfer ? "Có/Yes" : "Không/No";
            var typebank = dkkhcn.BankMethod == "2" ? "Tài khoản đầu tư gián tiếp nước ngoài/ Foreign Portfolio Investment (FPI)" : "Tài khoản ngân hàng đăng ký chuyển tiền/ Bank accounts registered for remittance";
            var bankNo = string.Empty;
            if (!string.IsNullOrEmpty(dkkhcn.accNoBIDV) && dkkhcn.accNoBIDV != "No_Number")
            {
                bankNo = dkkhcn.accNoBIDV;
            }
            else
            {
                bankNo = dkkhcn.BankAccNo_01;
            }

            if (!dkkhcn.BankTransfer)
            {
                typebank = "";
            }

            var notefpi = dkkhcn.accNoBIDV == "No_Number"
                ? "*Ghi chú/Note:   Mở mới tài khoản đầu tư gián tiếp nước ngoài BIDV<br>Register for a new BIDV FPI account"
                : " ";

            content = content.Replace("{fullnamesale}", saleinfo.fullname);
            content = content.Replace("{fullname}", dkkhcn.hoten);
            content = content.Replace("{Dateofbirth}", dkkhcn.ngaysinh);
            content = content.Replace("{gender}", gender);
            content = content.Replace("{passportnumber}", dkkhcn.cmnd);
            content = content.Replace("{Nationality}", dkkhcn.quoctich);
            content = content.Replace("{contactaddress}", dkkhcn.diachi);
            content = content.Replace("{email}", dkkhcn.email);
            content = content.Replace("{phonenumber}", dkkhcn.Phone_01);
            content = content.Replace("{tradingcode}", dkkhcn.tradingcode);
            content = content.Replace("{remittance}", remittance);
            content = content.Replace("{typebank}", typebank);
            content = content.Replace("{bankaccountnumber}", bankNo);
            content = content.Replace("{notefpi}", notefpi);
            return content;

        }

        private static string ReplaceEmailContent(string content, EmailData data)
        {
            
            content = content.Replace("{FullName}", data.FullName);
            content = content.Replace("{url}", data.url);
            content = content.Replace("{confirmCode}", data.ConfirmCode);
            content = content.Replace("{confirmSms}", data.ConfirmSms);
            return content;
        }  
        private static string ReplaceEmailContact(string content, EmailData data)
        {
            
            content = content.Replace("{Full_Name}", data.FullName);
            content = content.Replace("{Date_of_birth}", data.DateOfBirth);
            content = content.Replace("{Card_ID}", data.CardID);
            content = content.Replace("{Issue_date}", data.IssueDate);
            content = content.Replace("{Issuer}", data.CardIssue);
            content = content.Replace("{Text_Margin}", string.IsNullOrEmpty(data.TypeAccount) ? "Tài khoản giao dịch thông thường" : "Tài khoản giao dịch thông thường, Tài khoản giao dịch ký quỹ " );
            content = content.Replace("{Url}", data.url);
            content = content.Replace("{CustId}", Base64Encode(data.CardID));
            /*
            content = content.Replace("{&type=1}", Base64Encode("&type=1"));
            */
            
            return content;
        }   
        
        private static string ReplaceEmailInternational(string content, EmailData data)
        {
            
            content = content.Replace("{Fullname}", data.FullName);
            content = content.Replace("{Mr/Ms}", data.dr);
            return content;
        }



        public static string GetEmailByAccNo(string accNo)
        {
            try
            {
                var emailStr = string.Empty;
                string CommonDB = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                SqlConnection SqlConn = new SqlConnection(CommonDB);
                SqlDataReader drd = null;
                SqlCommand comm = new SqlCommand("TVSI_sGET_EMAIL_BY_ACCNO", SqlConn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("@so_tai_khoan", accNo);
                try
                {
                    comm.Connection.Open();
                    drd = comm.ExecuteReader();
                    if (drd.Read())
                    {
                        emailStr = drd["dia_chi_email"].ToString();
                    }
                    
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
                finally
                {
                    comm.Connection.Close();
                    comm.Connection.Dispose();
                }

                return emailStr;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public static string  ConfirmEkyc(string code)
        {
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sCONFIRM_EKYC", SqlConn);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    if (int.Parse(drd["ReturnCode"].ToString()) == 1)
                    {
                        // 20220922 CuongLM Add Xu ly Image tap trung
                        // BEGIN
                        try
                        {
                            var custCode = drd["CustCode"] + "";
                            var openAccId = int.Parse(drd["OpenAccID"] + "");
                            var registID = long.Parse(drd["RegistID"] + "");

                            if (!string.IsNullOrEmpty(custCode))
                            {
                                var connStr = ConfigurationManager.ConnectionStrings["SignatureDB_Connection"] + "";
                                using (var signaturConn = new SqlConnection(connStr))
                                {
                                    var sql = @"Insert into TVSI_OPENACCOUNT_FILES(CustCode, OpenAccID, SourceType, Type, FileName, FileData, Status, CreatedBy, CreatedDate)
                                        values(@CustCode, @OpenAccID, @SourceType, @Type, @FileName, @FileData, @Status, @CreatedBy, @CreatedDate)";

                                    var folderPath = HttpContext.Current.Server.MapPath(CommonConstant.FolderKYC + registID + "/");
                                    // Ảnh Front
                                    var frontFileName = registID + CommonConstant.Front + ".jpg";
                                    var frontFileInfo = new FileInfo(folderPath + frontFileName);
                                    var frontFileData = new byte[frontFileInfo.Length];
                                    using (FileStream fs = frontFileInfo.OpenRead())
                                    {
                                        fs.Read(frontFileData, 0, frontFileData.Length);
                                    }

                                    signaturConn.Execute(sql, new
                                    {
                                        CustCode = custCode,
                                        OpenAccID = openAccId,
                                        SourceType = 4,
                                        Type = 1,
                                        FileName = frontFileName,
                                        FileData = frontFileData,
                                        Status = 1,
                                        CreatedBy = "Winservices",
                                        CreatedDate = DateTime.Now
                                    });

                                    // Ảnh Back
                                    var backFileName = registID + CommonConstant.Back + ".jpg";
                                    var backFileInfo = new FileInfo(folderPath + backFileName);
                                    var backFileData = new byte[backFileInfo.Length];
                                    using (FileStream fs = backFileInfo.OpenRead())
                                    {
                                        fs.Read(backFileData, 0, backFileData.Length);
                                    }

                                    signaturConn.Execute(sql, new
                                    {
                                        CustCode = custCode,
                                        OpenAccID = openAccId,
                                        SourceType = 4,
                                        Type = 2,
                                        FileName = backFileName,
                                        FileData = backFileData,
                                        Status = 1,
                                        CreatedBy = "Winservices",
                                        CreatedDate = DateTime.Now
                                    });

                                    // Ảnh Face
                                    var faceFileName = registID + CommonConstant.Back + ".jpg";
                                    var faceFileInfo = new FileInfo(folderPath + faceFileName);
                                    var faceFileData = new byte[faceFileInfo.Length];
                                    using (FileStream fs = faceFileInfo.OpenRead())
                                    {
                                        fs.Read(faceFileData, 0, faceFileData.Length);
                                    }

                                    signaturConn.Execute(sql, new
                                    {
                                        CustCode = custCode,
                                        OpenAccID = openAccId,
                                        SourceType = 4,
                                        Type = 3,
                                        FileName = faceFileName,
                                        FileData = faceFileData,
                                        Status = 1,
                                        CreatedBy = "Winservices",
                                        CreatedDate = DateTime.Now
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(typeof(UtilTvsi), "CentrerImage - " + ex.Message);
                        }
                        // END Xu ly Image tap trung

                        return "Success";
                    }
                    else
                        return "RejectSuccess";
                }
                    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return "RejectSuccess";
        }

        public static bool QueueSMSOTP(string phone, string message,int status)
        {
            try
            {
                var systemDB = ConfigurationManager.ConnectionStrings["ActionQuery_SystemNotificationDB"].ToString();
                using (var conn = new SqlConnection(systemDB))
                {
                    conn.Open();
                    using (var comm = new SqlCommand("TVSI_sHANG_DOI_SMS_INSERT_SMS_OTP",conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.AddWithValue("@noi_dung",message);
                        comm.Parameters.AddWithValue("@so_dien_thoai",phone);
                        comm.Parameters.AddWithValue("@ma_dich_vu","SMS_OTP");
                        comm.Parameters.AddWithValue("@trang_thai",status);
                        SqlParameter outPutParameter = new SqlParameter();
                        outPutParameter.ParameterName = "@smsid";
                        outPutParameter.SqlDbType = SqlDbType.Int;
                        outPutParameter.Direction = ParameterDirection.Output;
                        comm.Parameters.Add(outPutParameter);
                        comm.ExecuteNonQuery();
                        return Convert.ToInt32(comm.Parameters["@smsid"].Value.ToString()) > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "QueueSMSOTP - " + ex.Message);
                throw;
            }
        }
        
        /*public static string  ConfirmEkycMobile(string codem)
        {
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("TVSI_sCONFIRM_EKYC_API_MOBILE", SqlConn);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@ConfirmCode", codem);
            comm.Parameters.AddWithValue("@ho_so_mo_tk_id", ParameterDirection.Output);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    if(int.Parse(drd["ReturnCode"].ToString()) == 1)
                        return "Success";
                    else
                        return "RejectSuccess";
                }
                    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return "RejectSuccess";
        } */
        public static bool ConfirmEkycMobile(string codem)
        {

            try
            {
                var ekycMobileWs = new EkycMobileWebService();
                return  ekycMobileWs.ConfirmOTPEkycMobile(codem);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static async Task<IEnumerable<RetData>> GetTvEcoList(string custCode)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_GetTVEcoList?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custCode
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<ApiResponse>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult.RetData;
                }
                return new List<RetData>();
                
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }

        public static async Task<bool> CheckInsertTvEco(int feeID, double maxWidthBalance, string custcode)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_GetTVEcoList?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custcode
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<ApiResponse>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                    {
                        var data = serializedResult.RetData;
                        if (data != null && data.Count > 0)
                        {
                            return data.Any(x => x.Values.FirstOrDefault(b => b.FeeID == feeID && maxWidthBalance >= b.FeeValue  ) != null);
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "CheckInsertTvEco - " + ex.Message);
                throw;
            }
        }

        public static async Task<EcoTvRetData> CreateTvEco(string custCode,int id)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_CreateTvEco?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custCode.Substring(0,6),
                    FeeID = id
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<EcoTvApiReponse>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult.RetData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }  
        public static async Task<EcoTvCancelResponse> CancelTvEco(string custCode,int id)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_CancelTvEco?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custCode,
                    RegistID = id
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<EcoTvCancelResponse>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }  
        
        public static async Task<IEnumerable<EcoTvDataHistResponse>> GetHistTvEco(string custCode)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_GetTvEcoHistList?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custCode,
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<EcoTvApiResponse<EcoTvDataHistResponse>>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult.RetData;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }        
        public static async Task<double> GetCashBalance(string custcode)
        {
            try
            {
                var url = /*GetConfigAppSetting("URL_API_BOND");*/ "http://10.0.140.138:8087/";
                var urlAPI = url + "api/CashTransfer/CT_CI_CustCashTransferInfo?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    UserId = custcode,
                    AccountNo = custcode + "1"
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<SuccessModel<RetDataCashBalance>>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult.RetData.CashBalanceInfo.MaxWithdrawal;
                }
                return 0;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }   
        
        public static async Task<EcoTvApiResponse<SearchTvEcoFeeResponse>> GetTvEcoFeeValue(string custCode,int id)
        {
            try
            {
                var url = GetConfigAppSetting("URL_API_BOND");
                var urlAPI = url + "api/MarginPackage/TV_ECO_GetTvEcoFeeValue?src=W&lang=VI";
                var json = _Serializer.Serialize(new
                {
                    CustCode = custCode,
                    RegistID = id
                });
                var response = await Utilities.Request(HttpMethod.Post, urlAPI, json, new Dictionary<string, string>());
                var responseText= await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseText))
                {
                    var serializedResult =  JsonConvert.DeserializeObject<EcoTvApiResponse<SearchTvEcoFeeResponse>>(responseText);
                    if (serializedResult != null && serializedResult.RetCode == "000")
                        return serializedResult;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), "GetTVEcoList - " + ex.Message);
                throw;
            }
        }
        

        public static bool CheckEkycConfirmCode(string code,string sms)
        {
            
            bool bOutPut = false;
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ConfirmCode from TVSI_DANG_KY_MO_TAI_KHOAN where ConfirmCode = @ConfirmCode and ConfirmSms = @ConfirmSms and ConfirmStatus = 0", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            comm.Parameters.AddWithValue("@ConfirmSms", sms);
            
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        public static bool CheckEkycConfirmCodeMobile(string code)
        {
            
            bool bOutPut = false;
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ConfirmCode from TVSI_DANG_KY_MO_TAI_KHOAN where ConfirmCode = @ConfirmCode and ConfirmStatus = 0", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }
        public static string ReturnBrandID(string SaleID)
        {

            string brand_id = "";
            string EMSStrConfig = ConfigurationManager.ConnectionStrings["EMS"].ToString();
            SqlConnection SqlConn = new SqlConnection(EMSStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ma_chi_nhanh from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@SaleID", SaleID);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    brand_id = drd["ma_chi_nhanh"].ToString();
                }
                    
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }
            return brand_id;
        }


        public static UserCRM GetUserCRMConfirm(string code)
        {
            try
            {
                var userResult = new UserCRM();
                string crmSTR = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                using (var sqlconn = new SqlConnection(crmSTR))
                {
                    SqlDataReader drd = null;
                    using (var command = new SqlCommand("  select ho_ten FullName, ngay_sinh DateOfBirthOn, so_cmnd CardID, ngay_cap_cmnd IssueDateOn,noi_cap_cmnd CardIssue, hop_dong_bo_sung TypeAccount,email Email from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where ConfirmCode = @confrimCode and ConfirmStatus = 1",sqlconn))
                    {
                        command.CommandType = CommandType.Text;
                        command.Parameters.AddWithValue("@confrimCode", code);
                        command.Connection.Open();
                        drd = command.ExecuteReader();
                        while (drd.Read())
                        {
                            userResult.FullName = drd["FullName"].ToString();
                            userResult.DateOfBirthOn = Convert.ToDateTime(drd["DateOfBirthOn"].ToString());
                            userResult.CardIssue = drd["CardIssue"].ToString();
                            userResult.IssueDateOn = Convert.ToDateTime(drd["IssueDateOn"].ToString());
                            userResult.TypeAccount = drd["TypeAccount"].ToString();
                            userResult.CardID = drd["CardID"].ToString();
                            userResult.Email = drd["Email"].ToString();
                        }

                        return userResult;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());

            }
        } 
        public static string ReturnBrandName(string SaleID)
        {

            string brand_Name = "";
            string EMSStrConfig = ConfigurationManager.ConnectionStrings["EMS"].ToString();
            SqlConnection SqlConn = new SqlConnection(EMSStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ten_bo_phan from TVSI_DANH_MUC_MA_QUAN_LY where ma_quan_ly = @SaleID", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@SaleID", SaleID);

            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                {
                    brand_Name = drd["ten_bo_phan"].ToString();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return brand_Name;
        }

        public static bool CheckEkycOtp(string code, string otp)
        {

            bool bOutPut = false;
            string CRMStrConfig = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            SqlConnection SqlConn = new SqlConnection(CRMStrConfig);
            SqlDataReader drd = null;
            SqlCommand comm = new SqlCommand("select ConfirmCode from TVSI_DANG_KY_MO_TAI_KHOAN where ConfirmCode = @ConfirmCode and ConfirmSms = @ConfirmOtp and ConfirmStatus = 0", SqlConn);
            comm.CommandType = CommandType.Text;
            comm.Parameters.AddWithValue("@ConfirmCode", code);
            comm.Parameters.AddWithValue("@ConfirmOtp", otp);
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }



        public static bool ExistsCMND(string strCMND, string AStr)
        {
            SqlConnection SqlConn = new SqlConnection(AStr);
            bool bOutPut = false;
            SqlDataReader drd = null;
            SqlParameter para_cmnd = new SqlParameter("@so_cmnd", strCMND);
            SqlCommand comm = new SqlCommand("select so_cmnd_passport from TVSI_TAI_KHOAN_KHACH_HANG where trang_thai = 1 and so_cmnd_passport ='" + strCMND + "'", SqlConn);
            comm.Parameters.Add(para_cmnd);
            comm.CommandType = CommandType.Text;
            try
            {
                comm.Connection.Open();
                drd = comm.ExecuteReader();
                if (drd.Read())
                    bOutPut = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            finally
            {
                comm.Connection.Close();
                comm.Connection.Dispose();
            }

            return bOutPut;
        }

        public static bool ConfirmOTPCRM(string cardId, string phone)
        {
            try
            {
                var crmDb = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                using (var conn = new SqlConnection(crmDb))
                {
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            using (var command = new SqlCommand("[dbo].[TVSI_sCONFIRM_OTP_CRM]", conn,trans))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add("@CardId", cardId);
                                command.Parameters.Add("@Phone", phone);
                                SqlParameter outPutParameter = new SqlParameter();
                                outPutParameter.ParameterName = "@check";
                                outPutParameter.SqlDbType = SqlDbType.Int;
                                outPutParameter.Direction = ParameterDirection.Output;
                                command.Parameters.Add(outPutParameter);
                                command.ExecuteNonQuery();
                                trans.Commit();
                                string check = command.Parameters["@check"].Value.ToString();
                                return Convert.ToInt16(check) > 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            Logger.Error(typeof(UtilTvsi), string.Format("ConfirmOTPCRM - " + ex.Message));
                            throw new Exception();

                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("ConfirmOTPCRM - " + ex.Message));
                return false;
            }
        }
        
        public static bool ExistsCMNDinCRM(string strCMND)
        {
            try
            {
                var crmStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                var commonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(crmStr))
                {
                    var sqlfirst =
                        "select trang_thai_tk from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd=@so_cmnd and trang_thai_tk <> 99";
                    var retDataFirst = conn.QueryFirstOrDefaultAsync<string>(sqlfirst, new
                    {
                        so_cmnd = strCMND
                    }).Result;
                    var connsec = new SqlConnection(commonStr);
                    var sqlsecond =
                        "select so_cmnd_passport from TVSI_TAI_KHOAN_KHACH_HANG where trang_thai = 1 and so_cmnd_passport =@so_cmnd";
                    var retDataSec = connsec.QueryFirstOrDefaultAsync<string>(sqlsecond, new
                    {
                        @so_cmnd = strCMND
                    }).Result;
                    
                    return string.IsNullOrEmpty(retDataFirst) && string.IsNullOrEmpty(retDataSec);
                }
            }
            catch (Exception ex)
            {   
                Logger.Error(typeof(UtilTvsi), ex.Message);
                Logger.Error(typeof(UtilTvsi), ex.InnerException);
                throw;
            }
        }

        public static int CheckCustomerForm(string cardID)
        {
            try
            {
                var crmstr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                var commonstr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(crmstr))
                {
                    var sql = "SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @cardID and nguon_du_lieu = 3";
                    var datacrm = conn.QuerySingleOrDefault<string>(sql, new
                    {
                        cardID = cardID
                    });
                    if (!string.IsNullOrEmpty(datacrm))
                    {
                        using (var concom = new SqlConnection(commonstr))
                        {
                            var comsql = "SELECT ma_khach_hang FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @cardID and nguon_du_lieu = 3";
                            var datacom = concom.QueryFirstOrDefault<string>(sql, new
                            {
                                cardID = cardID
                            });
                            if (!string.IsNullOrEmpty(datacom))
                                return 1;
                            return 2;
                        }
                    }

                    return 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), ex.Message);
                Logger.Error(typeof(UtilTvsi), ex.InnerException);
                throw;
            }
        }  
        public static bool CheckCustomerEKYC(string cardID)
        {
            try
            {
                var crmstr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                using (var conn = new SqlConnection(crmstr))
                {
                    var sql = "SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @cardID and  ( nguon_du_lieu = 4 or nguon_du_lieu = 6 ) and ConfirmStatus = 0";
                    var datacrm = conn.QueryFirstOrDefault<string>(sql, new
                    {
                        cardID = cardID
                    });
                    return string.IsNullOrEmpty(datacrm);

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), ex.Message);
                Logger.Error(typeof(UtilTvsi), ex.InnerException);
                throw;
            }
        }

        public static string GetNumberAccount(string cardID)
        {
            try
            {
                var commonstr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                using (var conn = new SqlConnection(commonstr))
                {
                    var sql = "select so_tai_khoan from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd = @cardID";
                    return conn.QueryFirstOrDefault<string>(sql, new
                    {
                        cardID = cardID
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), ex.Message);
                Logger.Error(typeof(UtilTvsi), ex.InnerException);
                throw;
            }
        }

        public static SaleInfoModel GetEmailSale(string saleID)
        {
            try
            {
                var emsSTR = ConfigurationManager.ConnectionStrings["EMS"].ToString();
                using (var conn = new SqlConnection(emsSTR))
                {
                    conn.Open();
                    var sql = "SELECT dia_chi_email Email, danh_muc_phong_ban_id DepartmentID,ho_va_ten SaleName   FROM TVSI_NHAN_SU WHERE ma_quan_ly = @saleID AND trang_thai = 1";
                    return conn.QueryFirstOrDefault<SaleInfoModel>(sql, new
                    {
                        saleID = saleID.Substring(0,4)
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), ex.Message);
                Logger.Error(typeof(UtilTvsi), ex.InnerException);
                throw;
            }
        }

       

        public static string GetConfigAppSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static bool ResendOTPCRM(string confirmCode,string confirmSms)
        {
            try
            {
                var crmDb = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                using (var conn = new SqlConnection(crmDb))
                {
                    conn.Close();
                    conn.Open();
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            using (var command = new SqlCommand("[dbo].[TVSI_sCHANGE_OTP_CRM]",conn,trans))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add("@ConfirmCode", confirmCode);
                                command.Parameters.Add("@ConfirmSms", confirmSms);
                                SqlParameter outPutParameter = new SqlParameter();
                                outPutParameter.ParameterName = "@check";
                                outPutParameter.SqlDbType = SqlDbType.Int;
                                outPutParameter.Direction = ParameterDirection.Output;
                                command.Parameters.Add(outPutParameter);
                                command.ExecuteNonQuery();
                                trans.Commit();
                                string check = command.Parameters["@check"].Value.ToString();
                                return Convert.ToInt16(check) > 0;
                            }

                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            Logger.Error(typeof(UtilTvsi), string.Format("ResendOTPCRM - " + ex.Message));
                            return false;
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                        }
                    }

                }
                
                
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("ResendOTPCRM - " + ex.Message));
                throw new Exception();
            }
        }

        public static UserInfoOTP GetEmailOTPSucess(string phone, string cardID)
        {
            try
            {
                var crmDb = ConfigurationManager.ConnectionStrings["CRM"].ToString();

                using (var sqlconnect = new SqlConnection(crmDb))
                {
                    var email = new UserInfoOTP();
                    SqlDataReader drd = null;
                    using (var command = new 
                               SqlCommand("SELECT email Email,ho_ten UserName,so_dien_thoai_01 Phone,ConfirmCode ConfirmCode FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] WHERE so_dien_thoai_01 = @Phone and so_cmnd = @CardId and ConfirmStatus = 1",sqlconnect))
                    {
                        command.Parameters.Add("@Phone", phone);
                        command.Parameters.Add("@CardId", cardID);
                        command.CommandType = CommandType.Text;
                        try
                        {
                            command.Connection.Open();
                            drd = command.ExecuteReader();
                            while (drd.Read())
                            {
                                email.Email = drd["Email"].ToString();
                                email.UserName = drd["UserName"].ToString();
                                email.Phone = drd["Phone"].ToString();

                            }

                            return email;

                        }
                        catch (Exception ex)
                        {
                            throw new Exception();

                        }
                        finally
                        {
                            sqlconnect.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetEmailOTPSucess - " + ex.Message));
                return null;
            }
        }

        public static UserInfoOTP GetUserOTPUnconfimred(string confirmCode, string confirmSMS)
        {
            try
            {
                var crmDb = ConfigurationManager.ConnectionStrings["CRM"].ToString();

                using (var sqlconnect = new SqlConnection(crmDb))
                {
                    var email = new UserInfoOTP();
                    SqlDataReader drd = null;
                    using (var command = new 
                               SqlCommand("SELECT email Email,ho_ten UserName,so_dien_thoai_01 Phone FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] WHERE ConfirmCode = @ConfirmCode and ConfirmSms = @ConfirmSms and ConfirmStatus = 0",sqlconnect))
                    {
                        command.Parameters.Add("@ConfirmCode", confirmCode);
                        command.Parameters.Add("@ConfirmSms", confirmSMS);
                        command.CommandType = CommandType.Text;
                        try
                        {
                            command.Connection.Open();
                            drd = command.ExecuteReader();
                            while (drd.Read())
                            {
                                email.Email = drd["Email"].ToString();
                                email.UserName = drd["UserName"].ToString();
                                email.Phone = drd["Phone"].ToString();
                            }

                            return email;

                        }
                        catch (Exception ex)
                        {
                            throw new Exception();

                        }
                        finally
                        {
                            sqlconnect.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetEmailOTPSucess - " + ex.Message));
                return null;
            }
        }
        public static bool CheckSaleIDEMS(string sale)
        {
            try
            {
                var check = false;
                var emsconnection  = ConfigurationManager.ConnectionStrings["EMS"].ToString();
                SqlConnection SqlConn = new SqlConnection(emsconnection);
                bool bOutPut = false;
                SqlDataReader drd = null;
                SqlCommand comm = new SqlCommand("Select ma_nhan_vien_quan_ly from TVSI_NHAN_SU t where t.ma_quan_ly = @id_he_thong and trang_thai = 1", SqlConn);
                comm.Parameters.Add(new SqlParameter("@id_he_thong", sale.Substring(0,4)));
                comm.CommandType = CommandType.Text;
                try
                {
                    comm.Connection.Open();
                    drd = comm.ExecuteReader();
                    if (drd.Read())
                    {
                        return check = true;
                    }
                    comm.Connection.Close();
                    return check;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
                finally
                {
                    comm.Connection.Close();
                    comm.Connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("CheckSaleIDEMS - " + ex.Message));
                throw ;
            }
        }
        
        public static IEnumerable<OtherModel> GetListPlaceOfIssue()
        {
            try
            {
                var Data = new List<OtherModel>();
                var CRMDB = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                SqlConnection SqlConn = new SqlConnection(CRMDB);
                bool bOutPut = false;
                SqlDataReader drd = null;
                SqlCommand comm = new SqlCommand("SELECT A.Category AS Category , A.CategoryName as CategoryName FROM ExtendCust A WHERE A.CategoryType = 13", SqlConn);
                
                comm.CommandType = CommandType.Text;
                try
                {
                    comm.Connection.Open();
                    drd = comm.ExecuteReader();
                    while (drd.Read())
                    {
                        Data.Add(new OtherModel(){CategoryName = drd["CategoryName"].ToString(),Category = Convert.ToInt64(drd["Category"].ToString())});
                        
                    }
                    comm.Connection.Close();
                    return Data;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
                finally
                {
                    comm.Connection.Close();
                    comm.Connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetListPlaceOfIssue - " + ex.Message));
                return null;
            }

        }

        public static async Task<SerivceListModel> GetServiceList(string custcode)
        {
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connstr))
                {
                    conn.Open();
                    var sql = "TVSI_REGSERVICE_GetTVSIComboInfo";
                    var result =  await conn.QueryFirstOrDefaultAsync<SerivceListModel>(sql,
                        new {@CustCode = custcode}, commandType: CommandType.StoredProcedure);
                    
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetServiceList - " + ex.Message));
                throw;
            }
        }

        public static IEnumerable<ProvinceModel> GetProvinceList()
        {
            try
            {
                var strCRM = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                if(!string.IsNullOrEmpty(strCRM))
                    using (var conn = new SqlConnection(strCRM))
                    {
                        conn.Open();
                        
                        var sql = "SELECT * FROM Province WHERE status = 1";
                        

                        return conn.Query<ProvinceModel>(sql);
                    }
                return new List<ProvinceModel>();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetProvinceList - " + ex.Message));
                throw;
            } 
        } 
        public static IEnumerable<DistricModel> GetDistricList(string provinceCode = "")
        {
            try
            {
                var strCRM = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                if(!string.IsNullOrEmpty(strCRM))
                    if(!string.IsNullOrEmpty(provinceCode))
                        using (var conn = new SqlConnection(strCRM))
                        {
                            conn.Open();
                            var sql = "SELECT * FROM District  WHERE status = 1 And ProvinceCode = @ProvinceCode";
                            return conn.Query<DistricModel>(sql, new
                            {
                                ProvinceCode = provinceCode
                            });
                        }
                
                return new List<DistricModel>();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetDistricList - " + ex.Message));
                throw;
            } 
        }
        public static IEnumerable<WardModel> GetWardList(string districtCode = "")
        {
            try
            {
                var strCRM = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                if(!string.IsNullOrEmpty(strCRM))
                    if (!string.IsNullOrEmpty(districtCode))
                        using (var conn = new SqlConnection(strCRM))
                        {
                            conn.Open();
                            var sql = "SELECT * FROM Ward  WHERE status = 1 AND DistrictCode = @DistrictCode";
                            return conn.Query<WardModel>(sql, new
                            {
                                DistrictCode = districtCode
                            });
                        }
                return new List<WardModel>();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetWardList - " + ex.Message));
                throw;
            } 
        }

        public async static Task<string> GetSaleName(string saleID)
        {
            try
            {
                var strEMS = ConfigurationManager.ConnectionStrings["EMS"].ToString();
                if (!string.IsNullOrEmpty(strEMS))
                {
                    using (var conn = new SqlConnection(strEMS))
                    {
                        conn.Open();
                        var sql = "select ho_va_ten from TVSI_NHAN_SU where ma_quan_ly = @saleID and trang_thai = 1";
                        return conn.QueryFirstOrDefault<string>(sql, new
                        {
                            saleID = saleID
                        });
                    }
                    
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetSaleName - " + ex.Message));
                throw;
            }
        }
        
        

        public static async Task<InsertTVComboResult> InsertTVCombo(int serviceid, string custcode)
        {
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connstr))
                {
                    conn.Open();
                    var sql = "TVSI_REGSERVICE_InsertRegServiceComboByHome";
                    var parameters = new DynamicParameters();
                    parameters.Add("@CustCode",custcode);
                    parameters.Add("@ServiceID",serviceid);
                    var result = await conn.QueryFirstOrDefaultAsync<InsertTVComboResult>
                        (sql,parameters, commandType: CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("InsertTVCombo - " + ex.Message));
                throw;
            }
        }

        public static async Task<bool> InsertRegCourse(string regisID, int courseID, int joinType, string custcode,
            string cardID,string phone, string email, string scheduleOffer, string timeOffer,string createby,string custname)
        {
            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();
                    var sql = "TVSI_REGSERVICE_InsertRegCourseByHome";
                    var parameters = new DynamicParameters();
                    parameters.Add("@RegistID",regisID);
                    parameters.Add("@CourseID",courseID);
                    parameters.Add("@JoinType",joinType);
                    parameters.Add("@CustCode",custcode);
                    parameters.Add("@CustName",custname);
                    parameters.Add("@Email",email);
                    parameters.Add("@CardID",cardID);
                    parameters.Add("@Phone",phone);
                    parameters.Add("@ScheduleOffer",scheduleOffer);
                    parameters.Add("@TimeOffer",timeOffer);
                    parameters.Add("@CreatedBy",createby);
                    return conn.Execute(sql,parameters,commandType : CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("InsertRegCourse - " + ex.Message));
                throw;
            }
        } 
        public static async Task<bool> CancelRegisterService(int id)
        {
            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connStr))
                {
                    conn.Open();
                    var sql = "TVSI_sREGSERVICE_CancelRegisterServiceCustomer";
                    var parameters = new DynamicParameters();
                    parameters.Add("@ID",id);
                    return conn.Execute(sql,parameters,commandType : CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("InsertRegCourse - " + ex.Message));
                throw;
            }
        }
        public static async  Task<IEnumerable<RegisterCourseModel>> GetCourseList()
        {
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connstr))
                {
                    conn.Open();
                    var sql = "select A.*,B.DisplayName,B.TeacherName,B.Description,B.ImageUrl  from TVSI_REGSERVICE_COURSE A inner join TVSI_REGSERVICE_TEACHER B on a.TeacherID = b.TeacherID where A.Status = 1 ";
                    var retData =  await conn.QueryAsync<RegisterCourseModel>(sql);
                    return retData;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetListCourse - " + ex.Message));
                throw;
            }
        }

        public static bool FieldValidCourse(int courseID,string cardID, string phone, string email,string fullname)
        {
            return courseID > 0 &&
                   !string.IsNullOrEmpty(cardID) &&
                   !string.IsNullOrEmpty(phone) &&
                   !string.IsNullOrEmpty(email) &&
                   !string.IsNullOrEmpty(fullname);
        }

        public static async Task<IEnumerable<GetListTVCOMBOHistModel>> GetListTVCOMBPHist(string accountNo)
        {
            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connStr))
                {
                    var sql = "TVSI_sREGSERVICE_GETCOMBOBYCUSTCODE";
                    var parameters = new DynamicParameters();
                    parameters.Add("@CustCode",accountNo);
                    var result = await  conn.QueryAsync<GetListTVCOMBOHistModel>(sql, parameters,
                        commandType: CommandType.StoredProcedure);
                    return result;

                }

            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetListTVCOMBPHist - " + ex.Message));
                throw;
            }
        }  
        
        public static async Task<IEnumerable<GetListCourseHistModel>> GetListCourseHist(string accountNo)
        {
            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connStr))
                {
                    var sql = "TVSI_sREGSERVICE_GetRegCourseByCustCode";
                    var parameters = new DynamicParameters();
                    parameters.Add("@CustCode",accountNo);
                    var result = await  conn.QueryAsync<GetListCourseHistModel>(sql, parameters,
                        commandType: CommandType.StoredProcedure);
                    return result;

                }

            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetListTVCOMBPHist - " + ex.Message));
                throw;
            }
        }

        public static async Task<GetInfoTVComboModel> GetInfoCombo(string registID)
        {
            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connStr))
                {
                    var storesql = "TVSI_sREGSERVICE_GETCOMBO";
                    var parameters = new DynamicParameters(); 
                    parameters.Add("@RegistID",registID);
                    return await conn.QueryFirstOrDefaultAsync<GetInfoTVComboModel>(storesql,parameters,
                        commandType: CommandType.StoredProcedure);
                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetInfoCombo - " + ex.Message));
                throw;
            }
        }
        
        
        public static int SendEmailRegister(string serviceName,string custCode, string custName,DateTime createDate, DateTime effDate,DateTime expDate
            ,string email)
        {
            string fileTemplate = AppDomain.CurrentDomain.BaseDirectory + @"Template\TVCombo\Register.html";
            var strEmailBody = ReadFile(fileTemplate);
            var strSubject = string.Format("TVSI: KHÁCH HÀNG ĐĂNG KÝ SỬ DỤNG DỊCH VỤ {0} THÀNH CÔNG", serviceName);
            strEmailBody = strEmailBody.Replace("{CustName}", custName);
            strEmailBody = strEmailBody.Replace("{CustCode}", custCode);
            strEmailBody = strEmailBody.Replace("{CreatedDate}", createDate.ToString("dd/MM/yyyy HH:mm:ss"));
            strEmailBody = strEmailBody.Replace("{EffDate}", effDate.ToString("dd/MM/yyyy"));
            strEmailBody = strEmailBody.Replace("{ExpDate}", expDate.ToString("dd/MM/yyyy"));

            if (!string.IsNullOrWhiteSpace(email) && strEmailBody.Length > 0)
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(strSubject, strEmailBody, email, 0);
            
            return 0;
        }
        public static string ReadFile(string file)
        {
            if (!File.Exists(file))
                return "";
            FileStream readFile = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader reader = new StreamReader(readFile);
            string content = reader.ReadToEnd();
            reader.Close();
            readFile.Close();
            return content;
        }
        
        
        public static int SendEmailCancelRegister(string serviceName,string custCode, string custName,DateTime createDate, DateTime effDate,DateTime expDate
            ,string email)
        {
            string fileTemplate = AppDomain.CurrentDomain.BaseDirectory + @"Template\TVCombo\Cancel.html";
            var strEmailBody = ReadFile(fileTemplate);
            var strSubject = string.Format("TVSI: KHÁCH HÀNG HỦY ĐĂNG KÝ SỬ DỤNG DỊCH VỤ {0} THÀNH CÔNG", serviceName);
            strEmailBody = strEmailBody.Replace("{CustName}", custName);
            strEmailBody = strEmailBody.Replace("{CustCode}", custCode);
            strEmailBody = strEmailBody.Replace("{CreatedDate}", createDate.ToString("dd/MM/yyyy HH:mm:ss"));
            strEmailBody = strEmailBody.Replace("{EffDate}", effDate.ToString("dd/MM/yyyy"));
            strEmailBody = strEmailBody.Replace("{ExpDate}", expDate.ToString("dd/MM/yyyy"));
            if (!string.IsNullOrWhiteSpace(email) && strEmailBody.Length > 0)
                TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(strSubject, strEmailBody, email, 0);

            return 0;
            
        }

        
        
        


        public static string GetPlaceOfIssue(string Category)
        {
            try
            {
                var data = string.Empty;
                var crmdb = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                SqlConnection SqlConn = new SqlConnection(crmdb);
                bool bOutPut = false;
                SqlDataReader drd = null;
                SqlCommand comm = new SqlCommand("SELECT A.Category AS Category , A.CategoryName as CategoryName FROM ExtendCust A WHERE A.Category = @Category", SqlConn);
                comm.Parameters.Add(new SqlParameter("@Category", Category));
                comm.CommandType = CommandType.Text;
                try
                {
                    comm.Connection.Open();
                    drd = comm.ExecuteReader();
                    while (drd.Read())
                    {
                        data = drd["CategoryName"].ToString();
                    }
                    comm.Connection.Close();
                    return data;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
                finally
                {
                    comm.Connection.Close();
                    comm.Connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("GetPlaceOfIssue - " + ex.Message));
                return null;
            }
        }
        
        /// <summary>
        /// Verify OTP.
        /// </summary>
        /// <param name="clientOTP">OTP client (gửi lên từ app hoặc web)</param>
        /// <param name="timeStamp">Thời gian thực</param>
        /// <param name="secretKey">Key dùng để generate OTP</param>
        /// <param name="step">Thời gian tồn tại OTP</param>
        /// <param name="digits">Số lượng number chuỗi OTP</param>
        /// <returns></returns>
        public static bool VerifyTOTP(string clientOTP, DateTime? timeStamp, string secretKey, int step = 60, int digits = 6)
        {
            try
            {
                if (string.IsNullOrEmpty(clientOTP))
                    return false;
                var serverOTP = GenerateTOTP(secretKey, timeStamp, step, digits);
                if (string.IsNullOrEmpty(serverOTP))
                    return false;
                return clientOTP.Equals(serverOTP);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        
        /// <summary>
        /// Generate OTP.
        /// </summary>
        /// <param name="timeStamp">Thời gian thực</param>
        /// <param name="secret">Key dùng để generate OTP</param>
        /// <param name="step">Thời gian tồn tại OTP</param>
        /// <param name="digits">Số lượng number chuỗi OTP</param>
        /// <returns></returns>
        public static string GenerateTOTP(string secret, DateTime? timeStamp, int step = 60, int digits = 6)
        {
            if (string.IsNullOrEmpty(secret))
                return string.Empty;
            try
            {
                byte[] bytes = Encoding.ASCII.GetBytes(secret);
                var totp = new Totp(bytes, step, OtpHashMode.Sha1, digits);
                if (timeStamp == null)
                    timeStamp = DateTime.UtcNow;
                return totp.ComputeTotp(timeStamp.Value);
            }
            catch
            {
                return string.Empty;
            }
        }
        

        public static eKYCModel OTPGenerateKey(string secretkey)
        {
            try
            {
                var result = new eKYCModel();
                byte[] bytes = Encoding.ASCII.GetBytes(secretkey);
                var correction = new TimeCorrection(DateTime.Now.AddSeconds(20));
                var totp = new Totp(bytes, 80,OtpHashMode.Sha512, 6,correction);
                var codeOTP = totp.ComputeTotp();
                var remainingTime = totp.RemainingSeconds();
                var remainingSeconds = totp.RemainingSeconds(DateTime.UtcNow);
                result.CodeOTP = codeOTP;
                result.TimeStep = remainingSeconds;
                long timeStepMatched = 0;
                /*
                var test = UtilTvsi.OtpCompare(secretkey, totp.ComputeTotp());
                */
                return result;

            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UtilTvsi), string.Format("OtpGenerateKey - " + ex.Message));
                return null;
            }
        }
        public static bool IsValidDateFormat(string dateFormat)
        {
            try {
                string s = DateTime.Now.ToString(dateFormat, CultureInfo.InvariantCulture);
                DateTime.ParseExact(s, "dd/MM/yyyy",null);
                return true;
            } catch(Exception ex) {
                return false;
            }
        }

        public static string EmailHideCharacters(string email)
        {
            string pattern = @"(?<=[\w]{3})[\w-\._\+%]*(?=[\w]{1}@)";
            return Regex.Replace(email, pattern, m => new string('*', m.Length));
        }


        public static string GetCardIDExpiredDate(string so_cmnd, int loai_hinh_tk, DateTime ngay_cap,
            DateTime ngay_sinh)
        {
            // Luật quy định
            // 1. CMT cũ 9 số => Ngày hết hạn = Ngày cấp + 15 nắm
            // 2. Thẻ căn cước 12 số: Dựa vào ngày sinh + ngày cấp
            // Công dân đủ 25 tuổi, 40 tuổi, 60 tuổi thì phải được đổi lại
            // Truong hợp đổi, cấp lại trong thời hạn 2 năm trước tuổi thì vẫn có giá trị đến tuổi tiếp theo
            // 2.1. Cac giay to khac (CMT quan doi) ngay het han = 31/12/9999
            // 3. Cá nhân nước ngoài dùng Passport => Ngày hết hạn = ngày cấp + 10 năm


            var formatDate = "yyyyMMdd";
            var maxExpireDate = new DateTime(9999, 12, 31).ToString(formatDate);

            // Ca nhan trong nuoc
            if (loai_hinh_tk == 1)
            {
                // Truong hop dung the can cuoc
                if (so_cmnd.Length == 12)
                {
                    var tuoi25 = ngay_sinh.AddYears(25);
                    var tuoi40 = ngay_sinh.AddYears(40);
                    var tuoi60 = ngay_sinh.AddYears(60);

                    var curDate = DateTime.Today;
                    if (curDate < tuoi25)
                    {
                        return tuoi25.AddYears(-2) < ngay_cap
                            ? tuoi40.ToString(formatDate)
                            : tuoi25.ToString(formatDate);
                    }

                    if (tuoi25 <= curDate && curDate < tuoi40)
                    {
                        return tuoi40.AddYears(-2) < ngay_cap
                            ? tuoi60.ToString(formatDate)
                            : tuoi40.ToString(formatDate);
                    }

                    if (tuoi40 <= curDate && curDate < tuoi60)
                    {
                        return tuoi60.AddYears(-2) < ngay_cap ? maxExpireDate : tuoi60.ToString(formatDate);
                    }

                    return maxExpireDate;
                }

                // Truong hop dung CMT cu
                if (so_cmnd.Length == 9)
                {
                    return ngay_cap.AddYears(15).ToString(formatDate);
                }

                // cac truong hop con lai
                return maxExpireDate;
            }

            // Ca nhan nuoc ngoai
            if (loai_hinh_tk == 3)
            {
                return ngay_cap.AddYears(10).ToString(formatDate);
            }

            // Truong hop la to chuc
            return maxExpireDate;
        }

        public static bool CheckDateCardID(string cardID, DateTime issueDate, DateTime birthDay)
        {
            var age25 =  birthDay.AddYears(25);
            var age40 = birthDay.AddYears(40);
            var age60 = birthDay.AddYears(60);
            
            var currentDate = DateTime.Now;

            if (cardID.Length == 12)
            {
                if (issueDate < age25.AddYears(-2))
                {
                    return currentDate <= age25;
                }
                if (age25.AddYears(-2) <= issueDate && issueDate < age40.AddYears(-2))
                {
                    return currentDate <= age40;
                }
                if (age40.AddYears(-2) <= issueDate && issueDate < age60.AddYears(-2))
                {
                    return currentDate <= age60;
                }
                return age60.AddYears(-2) <= issueDate;
            }
            if (cardID.Length == 9)
            {
                /*return issueDate < currentDate.AddYears(-15);*/
                return currentDate <= issueDate.AddYears(15);
            }

            return false;
            
        }

        public static async Task<ChangeMarginModel> ChangeMarginGroup(string custcode, string marginCurrent,string marginNew, string marginPreferNew ,int type)
        {
            try
            {
                var connstr = ConfigurationManager.ConnectionStrings["ActionQuery_IPGDB"].ToString();
                using (var conn = new SqlConnection(connstr))
                {
                    conn.Open();
                    var stored = "TVSI_sREGSERVICE_CheckChangeMargin";
                    return await conn.QueryFirstAsync<ChangeMarginModel>(stored, new
                    {
                        @CustCode = custcode,
                        @MarginGroupCurrent = marginCurrent,
                        @MarginGroupNew = marginNew,
                        @Combo = marginPreferNew,
                        @Type = type,
                        @Source = "W"
                    }, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(UtilTvsi),"ChangeMarginGroup " + e.Message);
                return null;
            }
        }
        

        public static long? GetValueIssuePlace(string cardID, string nameIssuePlace)
        {
            try
            {
                var listIssuePlace = GetListPlaceOfIssue();
                if (cardID.Length == 9)
                {
                    return listIssuePlace.Where(x => x.CategoryName.Contains(nameIssuePlace)).Select(x => x.Category).First();
                }

                if (cardID.Length == 12)
                {
                    var cutIssuePlace = CutPlaceIssueCardID(nameIssuePlace);
                    return listIssuePlace.Where(x => x.CategoryName.Contains(cutIssuePlace)).Select(x => x.Category).First();
                }
                return 0;

            }
            catch (Exception e)
            {
                Logger.Debug(typeof(UtilTvsi),"GetValueIssuePlace " + e.Message);
                return null;
            }
        }

        public static string CutPlaceIssueCardID(string nameIssuePlace)
        {
            try
            {
                var checknoicap = false;
                var result = string.Empty;
                var listnoicap = new List<string>();
                listnoicap.Add("trật tự");
                listnoicap.Add("xã hội"); ;
                listnoicap.Add("trật tự xã hội");
                listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");

                var listnoicap1 = new List<string>();
                listnoicap1.Add("ĐKQL");
                listnoicap1.Add("DLQG");
                listnoicap1.Add("về dân cư");
                listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
                if (nameIssuePlace != null)
                {
                    for (int i = 0; i < listnoicap.Count; i++)
                    {
                        if (nameIssuePlace.ToLower().Contains(listnoicap[i]))
                        {
                            result = "CCS QLHC về TTXH";
                            checknoicap = true;
                            break;
                        }
                    }
                    if (checknoicap == false)
                    {
                        for (int i = 0; i < listnoicap1.Count; i++)
                        {
                            if (nameIssuePlace.ToLower().Contains(listnoicap1[i]))
                            {
                                result = "CCS ĐKQL Cư trú và DLQG về DC";
                                break;
                            }
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(UtilTvsi),"CutPlaceIssueCardID " + e.Message);
                return null;
            }
        }
        
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {   return false;  }
            try
            {
                Regex _regex = new Regex("^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])" +
                                         "+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)" +
                                         "((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|" +
                                         "[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\u" +
                                         "FDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|" +
                                         "(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|" +
                                         "[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900" +
                                         "-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFF" +
                                         "EF])))\\.?$", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.Compiled);
                return _regex.IsMatch(email);
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static string ValidDateOfBirth(DateTime dateOfBirth)
        {
            try
            {
                
                var today = DateTime.Today;
                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                var value = (a - b) / 10000;
                
                if (value > 150)
                    return "Ngày sinh không hợp lệ";
                
                if (value < 18)
                    return "Nhà Đầu Tư phải trên 18 tuổi";
                
                return null;
            }
            catch (Exception e)
            {                
                Logger.Debug(typeof(UtilTvsi),"ValidDateOfBirth  " + e.Message);
                throw;
            }
        }
        
        /// <summary>
        /// This method is used to encode base64
        /// </summary>
        /// <param name="plainText">string to encode</param>
        public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        
        
                
        /// <summary>
        /// This method is used to Decode base64
        /// </summary>
        /// <param name="base64EncodedData">encode to decode</param>
        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


    }
    


    public class EmailData
    {
        public string FullName { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string ConfirmCode { get; set; }
        public string ConfirmSms { get; set; }
        public string dr { get; set; }
        
        public  string DateOfBirth { get; set; }
        public  string CardID { get; set; }
        public  string IssueDate { get; set; }
        public  string CardIssue { get; set; }
        public  string TypeAccount { get; set; }
    }

    public class EmailSendSaleEKYC
    {
        public string SaleName { get; set; }
        public string FullName { get; set; }
        public string BirhtDay { get; set; }
        public string CardID { get; set; }
        public string IssueDate { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Package { get; set; }
        public string Note { get; set; }

    }

    public class Saleinfo
    {
        public string email { get; set; }
        public string fullname { get; set; }
        public int gender { get; set; }
    }
    
    
    


    
}