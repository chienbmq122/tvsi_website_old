﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;


namespace Website_tvsi_v1.Common
{
    public class MailHelper
    {
        public static void SendMail(String ToEmail ,String subject, String content,String fileattach)
        {
            var fromemailaddress = ConfigurationManager.AppSettings["fromemailaddress"].ToString();
            var fromemaildisplayname = ConfigurationManager.AppSettings["fromemaildisplayname"].ToString();
            var fromemailpassword = "tuananhvn1510";// ConfigurationManager.AppSettings["fromemailpassword"].ToString();
            var smtphost = ConfigurationManager.AppSettings["smtphost"].ToString();
            var smtpport = ConfigurationManager.AppSettings["smtpport"].ToString();
            var toemailaddress = ConfigurationManager.AppSettings["toemailaddress"].ToString();
            bool enabledssl = bool.Parse(ConfigurationManager.AppSettings["enabledssl"].ToString());
            String body = content;
            MailMessage message = new MailMessage(new MailAddress(fromemailaddress, fromemaildisplayname), new MailAddress(toemailaddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;
            if (fileattach != null)
                message.Attachments.Add(new Attachment(fileattach));
            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromemailaddress,fromemailpassword);
            client.Host = smtphost;
            client.EnableSsl = enabledssl;
            client.Port = !string.IsNullOrEmpty(smtpport) ? Convert.ToInt32(smtpport) : 0;
            client.Timeout = (60 * 5 * 1000);
            client.Send(message);
        }

    }
}