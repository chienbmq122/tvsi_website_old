﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common
{
    public static class CommonConstant
    {
        public static string name_lang_tvsi_wb = "lang_tvsi_wb";
        public static string name_lang_tvsi_fp = "fp_tvsi_lang";
        public static string FormatDate = "dd/MM/yyyy";
        public static string EmailSubject = "TVSI:";
        public static string Path = "/user/confirm-ekyc-open-account";
        public static string FolderKYC = "~/EkycCustomer/";
        public static string Front = "_Front";
        public static string Back = "_Back";
        public static string Face = "_Face";

    }

    public static class TVComboConst
    {
        public static string Success = "000";
    }
    public enum SourceAccount
    {
        OpenAccountCRM = 2, // mở tài khoản trên CRM
        OpeneKYCMobile = 6, // mở tài khoản eKYC trên Mobile
        OpeneKYCWeb = 4, // mở tài khoản eKYC trên Website
        OpenAccountEMS = 1, // mở tài khoản trên EMS
        OpenWebsite = 3 // mở tài khoản thường trên website
    }
}