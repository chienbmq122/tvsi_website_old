﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common.stock.mapper
{
    public abstract class GenericMapper<T>
    {
        protected abstract T MapOne(IDataRecord record);

        public List<T> Map(IDataReader reader)
        {
            List<T> list = new List<T>();

            while (reader.Read())
            {
                list.Add(MapOne(reader));
            }

            // Close the reader after mapping
            reader.Close();

            return list;
        }
    }
}