﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.stock.market;

namespace Website_tvsi_v1.Common.stock.mapper
{
    public class QuoteMapper : GenericMapper<Quote>
    {
        protected override Quote MapOne(System.Data.IDataRecord record)
        {
            string symbol = (string)record["Symbol"];
            DateTime date = (DateTime)record["PermDate"];
            double volume = Convert.ToDouble(record["Volume"]);
            double totalTrade = record["TotalTrade"] == DBNull.Value ? 0 : Convert.ToDouble(record["TotalTrade"]);
            double totalValue = record["TotalValue"] == DBNull.Value ? 0 : Convert.ToDouble(record["TotalValue"]);
            double open = record["PriceOpen"] == DBNull.Value ? 0 : Convert.ToDouble(record["PriceOpen"]);
            double close = record["PriceClose"] == DBNull.Value ? 0 : Convert.ToDouble(record["PriceClose"]);
            double high = record["PriceHigh"] == DBNull.Value ? 0 : Convert.ToDouble(record["PriceHigh"]);
            double low = record["PriceLow"] == DBNull.Value ? 0 : Convert.ToDouble(record["PriceLow"]);
            double average = record["PriceAverage"] == DBNull.Value ? 0 : Convert.ToDouble(record["PriceAverage"]);
            double adjRatio = record["AdjRatio"] == DBNull.Value ? 1 : Convert.ToDouble(record["AdjRatio"]);
            double previousClose = record["PricePreviousClose"] == DBNull.Value ? 0 : Convert.ToDouble(record["PricePreviousClose"]);

            double buyForeignVolume = record["BuyForeignQuantity"] == DBNull.Value ? 0 : Convert.ToDouble(record["BuyForeignQuantity"]);
            double buyForeignValue = record["BuyForeignValue"] == DBNull.Value ? 0 : Convert.ToDouble(record["BuyForeignValue"]);
            double sellForeignVolume = record["SellForeignQuantity"] == DBNull.Value ? 0 : Convert.ToDouble(record["SellForeignQuantity"]);
            double sellForeignValue = record["SellForeignValue"] == DBNull.Value ? 0 : Convert.ToDouble(record["SellForeignValue"]);

            double putVolume = record["PutVolume"] == DBNull.Value ? 0 : Convert.ToDouble(record["PutVolume"]);
            double putValue = record["PutValue"] == DBNull.Value ? 0 : Convert.ToDouble(record["PutValue"]);
            double dealVolume = record["DealVolume"] == DBNull.Value ? 0 : Convert.ToDouble(record["DealVolume"]);
            double dealValue = record["DealValue"] == DBNull.Value ? 0 : Convert.ToDouble(record["DealValue"]);
            Quote quote = new Quote(symbol, date, volume, totalTrade, totalValue, open, close, high, low, average, previousClose, buyForeignVolume, buyForeignValue, sellForeignVolume, sellForeignValue, putVolume, putValue, dealVolume, dealValue, adjRatio);

            return quote;
        }
    }
}
