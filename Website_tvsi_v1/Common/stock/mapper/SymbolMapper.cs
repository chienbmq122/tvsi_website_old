﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.stock.market;

namespace Website_tvsi_v1.Common.stock.mapper
{
    public class SymbolMapper : GenericMapper<SymbolInfo>
    {
        protected override SymbolInfo MapOne(System.Data.IDataRecord record)
        {
            if (record["Symbol"] == DBNull.Value ||
                record["IndustryID"] == DBNull.Value ||
                record["SectorID"] == DBNull.Value ||
                record["MarketID"] == DBNull.Value)
            {
                return null;
            }
            SymbolInfo symbol =
                new SymbolInfo((string)record["Symbol"],
                               record["SymbolName"] == DBNull.Value ? null : (string)record["SymbolName"],
                               record["CompanyType"] == DBNull.Value ? CompanyType.General : (CompanyType)Enum.Parse(typeof(CompanyType), (string)record["CompanyType"]),
                               Convert.ToInt32(record["IndustryID"]),
                               record["IndustryName"] == DBNull.Value ? null : (string)record["IndustryName"],
                               Convert.ToInt32(record["SectorID"]),
                               record["SectorName"] == DBNull.Value ? null : (string)record["SectorName"],
                               Convert.ToInt32(record["MarketID"]),
                               (string)record["Exchange"]);
            return symbol;
        }
    }
}