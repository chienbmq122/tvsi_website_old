﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.Data;

namespace Website_tvsi_v1.Common.stock.market
{
    public class Quote : IComparable
    {
        private string _symbol;
        private DateTime _date;
        private double _volume;
        private double _totalTrade;
        private double _totalValue;
        private double _open;
        private double _close;
        private double _high;
        private double _low;
        private double _average;
        private double _previousClose;
        private double _buyForeignVolume;
        private double _buyForeignValue;
        private double _sellForeignVolume;
        private double _sellForeignValue;
        private double _putVolume;
        private double _putValue;
        private double _dealVolume;
        private double _dealValue;
        private double _adjRatio;
        private string _Exchange;
        /// <summary>
        /// Initializes a new instance of the Quote class
        /// </summary>
        /// <param name="symbol">String with short symbol name connected with this quote</param>
        public Quote(string symbol)
        {
            this._symbol = symbol;
        }
        /// <summary>
        /// Initializes a new instance of the Quote class
        /// </summary>
        /// <param name="symbol">String with short symbol name connected with this quote</param>
        /// <param name="date">DateTime of this event</param>
        /// <param name="volume">Volume of this event</param>
        /// <param name="open">Open price</param>
        /// <param name="close">Close price</param>
        /// <param name="high">High price</param>
        /// <param name="low">Low price</param>
        /// <param name="low">Previous close price</param>
        /// <param name="adjRatio">Adjustment ratio for calculating new prices according old events as split and dividends</param>
        public Quote(string symbol, DateTime date, double volume, double totalTrade, double totalValue, double open, double close, double high, double low, double average, double previousClose, double buyForeignVolume, double buyForeignValue, double sellForeignVolume, double sellForeignValue, double adjRatio)
        {
            this._symbol = symbol;
            this._date = date;
            this._volume = volume;
            this._totalTrade = totalTrade;
            this._totalValue = totalValue;
            this._open = open;
            this._close = close;
            this._high = high;
            this._low = low;
            this._average = average;
            this._previousClose = previousClose;
            this._buyForeignVolume = buyForeignVolume;
            this._buyForeignValue = buyForeignValue;
            this._sellForeignVolume = sellForeignVolume;
            this._sellForeignValue = sellForeignValue;
            this._adjRatio = adjRatio;
            SymbolInfo symbolInfo = SqlSymbolData.GetSymbolInfo(this._symbol, "vi-vn"); ;
            if (symbolInfo != null)
            {
                this._Exchange = symbolInfo.Exchange;
            }

        }
        public Quote(string symbol, DateTime date, double volume, double totalTrade, double totalValue, double open, double close, double high, double low, double average, double previousClose, double buyForeignVolume, double buyForeignValue, double sellForeignVolume, double sellForeignValue, double putVolume, double putValue, double dealVolume, double dealValue, double adjRatio)
        {
            this._symbol = symbol;
            this._date = date;
            this._volume = volume;
            this._totalTrade = totalTrade;
            this._totalValue = totalValue;
            this._open = open;
            this._close = close;
            this._high = high;
            this._low = low;
            this._average = average;
            this._previousClose = previousClose;
            this._dealVolume = dealVolume;
            this._dealValue = dealValue;
            this._buyForeignVolume = buyForeignVolume;
            this._buyForeignValue = buyForeignValue;
            this._sellForeignVolume = sellForeignVolume;
            this._sellForeignValue = sellForeignValue;
            this._putVolume = putVolume;
            this._putValue = putValue;
            this._adjRatio = adjRatio;
            SymbolInfo symbolInfo = SqlSymbolData.GetSymbolInfo(this._symbol, "vi-vn"); ;
            if (symbolInfo != null)
            {
                this._Exchange = symbolInfo.Exchange;
            }

        }
        /// <summary>
        /// Returns string that represents Symbol of current object
        /// </summary>
        public string Symbol { get { return (this._symbol); } set { this._symbol = value; } }
        public string Exchange { get { return (this._Exchange); } set { this._Exchange = value; } }
        /// <summary>
        /// Returns DateTime that represents Date of current object
        /// </summary>
        public DateTime Date { get { return (this._date); } }
        /// <summary>
        /// Returns double value represent volume of current object
        /// </summary>
        public double Volume { get { return (this._volume); } }
        /// <summary>
        ///  Returns double value that represents open price of current object
        /// </summary>
        public double Open { get { return (this._open); } }
        /// <summary>
        /// Returns double value that represents close price of current object
        /// </summary>
        public double Close { get { return (this._close); } }
        /// <summary>
        /// Returns double value that represents high price of current object
        /// </summary>
        public double High { get { return (this._high); } }
        /// <summary>
        /// Returns double value that represents low price of current object
        /// </summary>
        public double Low { get { return (this._low); } }
        /// <summary>
        /// Returns double value that represents previous close price of current object
        /// </summary>
        public double PreviousClose { get { return (this._previousClose); } }

        public double Average { get { return (this._average); } }
        public double TotalTrade { get { return (this._totalTrade); } }
        public double TotalValue { get { return (this._totalValue); } }

        public double BuyForeignVolume { get { return (this._buyForeignVolume); } }
        public double BuyForeignValue { get { return (this._buyForeignValue); } }
        public double SellForeignVolume { get { return (this._sellForeignVolume); } }
        public double SellForeignValue { get { return (this._sellForeignValue); } }

        public double PutVolume { get { return (this._putVolume); } }
        public double PutValue { get { return (this._putValue); } }
        public double DealVolume { get { return (this._dealVolume); } }
        public double DealValue { get { return (this._dealValue); } }

        public double Change
        {
            get
            {

                // 2015-04-10 HaiNM --- sua thay doi gia

                //if (this._Exchange != "HOSTC" && this._Exchange != null)
                //{
                //    if (this._average == 0)
                //    { return 0; }
                //    else
                //    {
                //        return (this._average - this._previousClose);
                //    }
                //}
                //else
                //{
                return (this._close - this._previousClose);
                //}

            }


        }

        public double PercentChange
        {
            get
            {
                //if (this._Exchange != "HOSTC" && this._Exchange != null)
                //{
                //    if (this._average == 0)
                //    { return 0; }
                //    else
                //    {
                //        return (this._average - this._previousClose) * 100 / this._previousClose;
                //    }
                //}
                //else
                //{
                if (this._previousClose != 0)
                {
                    return (this._close - this._previousClose) * 100 / this._previousClose;
                }

                //}
                return 0;
            }
        }

        /// <summary>
        /// Returns double value that represents adjustment ratio of current object
        /// </summary>
        public double AdjRatio { get { return (this._adjRatio); } }
        /// <summary>
        /// Returns double value that represents adjusted open price of current object
        /// </summary>
        public double AdjOpen
        {
            get
            {
                if (this._open == 0)
                {
                    return this.AdjClose;
                }
                else
                {
                    return (Math.Round(this._open / this._adjRatio, 2));
                }

            }
        }
        /// <summary>
        /// Returns double value that represents adjusted close price of current object
        /// </summary>
        public double AdjClose { get { return (Math.Round(this._close / this._adjRatio, 2)); } }
        /// <summary>
        /// Returns double value that represents adjusted high price of current object
        /// </summary>
        public double AdjHigh
        {
            get
            {
                if (this._high == 0)
                {
                    return this.AdjClose;
                }
                else
                {
                    return
                        (Math.Round(this._high / this._adjRatio, 2));
                }
            }
        }
        /// <summary>
        /// Returns double value that represents adjusted low price of current object
        /// </summary>
        public double AdjLow
        {
            get
            {
                if (this._low == 0)
                {
                    return this.AdjClose;
                }
                else
                {
                    return (Math.Round(this._low / this._adjRatio, 2));
                }

            }
        }


        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is Quote)
            {
                Quote q = (Quote)obj;
                return this.Date.CompareTo(q.Date);
            }
            else
            {
                throw new ArgumentException("object is not a Quote");
            }
        }

        #endregion
    }
}