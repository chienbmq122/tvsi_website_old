﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Website_tvsi_v1.Common.stock.market
{
    public class TimeFrame
    {
        private int _duration;
        private TermTypes _termType;
        /// <summary>
        /// Initializes a new instance of the TimeFrame class
        /// </summary>
        /// <param name="duration">Duration of the time period</param>
        /// <param name="termType">Period type</param>

        public TimeFrame(int duration, TermTypes termType)
        {
            this._duration = duration;
            this._termType = termType;
        }
        /// <summary>
        /// Return duration of time period of this instance
        /// </summary>
        public int Duration { get { return (this._duration); } }

        /// <summary>
        /// Return TermType type of this instance
        /// </summary>
        public TermTypes TermType { get { return (this._termType); } }

        /// <summary>
        /// Return calculated DateTime decremented by this period.
        /// </summary>
        /// <param name="baseDate">DateTime for calculation</param>
        /// <returns>Calculated DateTime</returns>
        public DateTime GetBaseDate(DateTime baseDate)
        {
            DateTime result = baseDate.Date;
            switch (this._termType)
            {
                case TermTypes.Years:
                    result = baseDate.AddYears(-this._duration);
                    break;
                case TermTypes.Months:
                    result = baseDate.AddMonths(-this._duration);
                    break;
                case TermTypes.Days:
                    result = baseDate.AddDays(-this._duration);
                    break;
                case TermTypes.YTD:
                    result = baseDate.AddDays(-baseDate.DayOfYear);
                    break;

            }
            return (result);
        }
        /// <summary>
        /// Returns default TimeInterval object for this instance
        /// </summary>
        /// <returns>Default TimeInterval object</returns>
        public TimeInterval GetTimeInterval()
        {
            TimeInterval result = null;

            switch (_termType)
            {
                case TermTypes.YTD:
                    result = new TimeInterval(1, IntervalType.Days);
                    break;

                case TermTypes.Years:

                    if (_duration > 5)
                        result = new TimeInterval(2, IntervalType.Weeks);
                    else if (_duration > 1)
                        result = new TimeInterval(1, IntervalType.Weeks);
                    else
                        result = new TimeInterval(1, IntervalType.Days);

                    break;

                case TermTypes.Months:
                    if (_duration > 3)
                        result = new TimeInterval(1, IntervalType.Days);
                    else if (_duration > 1)
                        result = new TimeInterval(1, IntervalType.Days);
                    else
                        result = new TimeInterval(1, IntervalType.Days);

                    break;

                case TermTypes.Days:
                    if (_duration > 1)
                        result = new TimeInterval(1, IntervalType.Days);
                    else
                        result = new TimeInterval(5, IntervalType.Minutes);
                    break;

            }
            return (result);
        }

        /// <summary>
        /// Converts the string representation of a TimeFrame object.  
        /// </summary>
        /// <param name="s">A string representing the TimeFrame object to convert.</param>
        /// <returns>TimeFrame object</returns>
        /// <exception cref="FormatException">The s parameter is not of the correct format.</exception>
        /// <remarks>
        /// The s parameter contains a number of the form:
        /// [ws]digits[ws]word
        ///  Items in square brackets ([ and ]) are optional; and the values of the other items are as follows. 
        ///		ws - An optional white space. 
        ///     digits - A sequence of digits ranging from 0 to 9. 
        ///     word - One of the following : "d","day",days", "m","month","months", "y","year","years", "ytd"
        /// </remarks>
        public static TimeFrame Parse(string s)
        {

            // if s is YTD ( year to date ) do not check, just return
            if (s.Trim().ToLower() == "ytd")
                return (new TimeFrame(1, TermTypes.YTD));

            if (s.Trim().ToLower() == "today")
                return (new TimeFrame(0, TermTypes.Days));

            // if s is not YTD
            int count;
            string type;

            Regex r = new Regex(@"^\s*(?<count>\d+)\s*(?<type>\w+)\s*$", RegexOptions.IgnoreCase);
            Match m = r.Match(s);

            if (m.Groups["count"].Success && m.Groups["type"].Success)
            {
                type = m.Groups["type"].Value.ToLower();
                count = Int32.Parse(m.Groups["count"].Value);

                if ("y,year".IndexOf(type) != -1)
                    return (new TimeFrame(count, TermTypes.Years));

                else if ("m,month".IndexOf(type) != -1)
                    return (new TimeFrame(count, TermTypes.Months));

                else if ("d,day".IndexOf(type) != -1)
                    return (new TimeFrame(count, TermTypes.Days));
                else
                    throw (new FormatException("The s parameter is not of the correct format"));
            }
            else
                throw (new FormatException("The s parameter is not of the correct format"));

        }

        /// <summary>
        /// Returns long string representation of this TimeFrame instance 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string s = this._termType.ToString();

            if (this._termType == TermTypes.YTD)
                /// no needs duration
                return (s);
            else
            {
                if (this.TermType == TermTypes.Days && _duration == 0)
                    return ("Today");
                else if (_duration < 2)
                    /// remove the 's' for non plurals
                    s = s.Remove(s.Length - 1, 1);
            }
            return (_duration.ToString() + " " + s);
        }

        /// <summary>
        /// Returns string representation of a TimeInterval object 
        /// </summary>
        /// <param name="shortString">True for short string representation as "10y","2m", false for long - "10 years", "2 months"</param>
        /// <returns></returns>
        public string ToString(bool shortString)
        {
            if (shortString)
            {
                string s = this._termType.ToString();

                if (this._termType == TermTypes.YTD)
                    /// no needs duration
                    return (s);

                return (_duration.ToString() + s.Substring(0, 1).ToLower());
            }
            else
                return (ToString());
        }
    }
}