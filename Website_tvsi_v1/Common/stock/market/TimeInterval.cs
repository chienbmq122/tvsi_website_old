﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Website_tvsi_v1.Common.stock.market
{
    public class TimeInterval
    {
        private int count;
        private IntervalType interval;
        /// <summary>
        ///Initializes a new instance of the TimeInterval Class
        /// </summary>
        /// <param name="count"></param>
        /// <param name="interval"></param>
        public TimeInterval(int count, IntervalType interval)
        {
            this.count = count;
            this.interval = interval;
        }
        /// <summary>
        /// Returns int that represents counts of time slices of current object
        /// </summary>
        public int Count { get { return (this.count); } }
        /// <summary>
        /// Returns IntervalType that represents interval of current object
        /// </summary>
        public IntervalType Interval { get { return (this.interval); } }
        /// <summary>
        /// Returns  DateTime incremented by interval
        /// </summary>
        /// <param name="date">DateTime for calculation</param>
        /// <returns></returns>
        public DateTime AddToDateTime(DateTime date)
        {
            DateTime result;
            switch (interval)
            {
                case IntervalType.Minutes:
                    result = date.AddMinutes(count);
                    break;
                case IntervalType.Weeks:
                    result = date.AddDays(count * 7);
                    break;
                case IntervalType.Days:
                    result = date.AddDays(count);
                    break;
                case IntervalType.Months:
                    result = date.AddMonths(count);
                    break;
                default:
                    throw (new ApplicationException("The s parameter is not of the correct format"));
            }
            return (result);
        }
        /// <summary>
        /// Converts the string representation of a TimeInterval object.  
        /// </summary>
        /// <param name="s">A string representing the TimeInterval object to convert.</param>
        /// <returns>TimeFrame object</returns>
        /// <exception cref="FormatException">The s parameter is not of the correct format.</exception>
        /// <remarks>
        /// The s parameter contains a number of the form:
        /// [ws]digits[ws]word
        ///  Items in square brackets ([ and ]) are optional; and the values of the other items are as follows. 
        ///		ws - An optional white space. 
        ///     digits - A sequence of digits ranging from 0 to 9. 
        ///     word - One of the following : "min, "d","day",days", "w","week","weeks", "m","month","months"
        /// </remarks>

        public static TimeInterval Parse(string s)
        {
            // if s is not YTD
            int count;
            string type;

            Regex r = new Regex(@"^\s*(?<count>\d+)\s*(?<type>\w+)\s*$", RegexOptions.IgnoreCase);
            Match m = r.Match(s);

            if (m.Groups["count"].Success && m.Groups["type"].Success)
            {
                type = m.Groups["type"].Value.ToLower();
                count = Int32.Parse(m.Groups["count"].Value);


                if ("m,month".IndexOf(type) != -1)
                    return (new TimeInterval(count, IntervalType.Months));
                else if ("min".IndexOf(type) != -1)
                    return (new TimeInterval(count, IntervalType.Minutes));

                else if ("d,day".IndexOf(type) != -1)
                    return (new TimeInterval(count, IntervalType.Days));

                else if ("w,week".IndexOf(type) != -1)
                    return (new TimeInterval(count, IntervalType.Weeks));

                else
                    throw (new FormatException("The s parameter is not of the correct format"));
            }
            else
                throw (new FormatException("The s parameter is not of the correct format"));

        }
        /// <summary>
        /// Returns long string representation of a TimeInterval object 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string s = this.interval.ToString();

            /// remove the 's' for non plurals
            if (count < 2)
                s = s.Remove(s.Length - 1, 1);

            return (count.ToString() + " " + s);
        }

        /// <summary>
        /// Returns string representation of a TimeInterval object 
        /// </summary>
        /// <param name="shortString">True for short string representation as "10min","2w", false for long - "10 minutes", "2 weeks"</param>
        /// <returns></returns>
        public string ToString(bool shortString)
        {
            if (shortString)
            {
                if (interval == IntervalType.Minutes)
                    return (count.ToString() + "min");
                else
                    return (count.ToString() + interval.ToString().Substring(0, 1).ToLower());
            }
            else
                return (ToString());
        }

    }
}
