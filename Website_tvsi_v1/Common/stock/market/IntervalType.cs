﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common.stock.market
{
    // Summary:
    //     Specifies an interval type.
    public enum IntervalType
    {
        // Summary:
        //     A numerical interval.
        Number = 0,
        //
        // Summary:
        //     An interval of years.
        Years = 1,
        //
        // Summary:
        //     An interval of months.
        Months = 2,
        //
        // Summary:
        //     An interval of weeks.
        Weeks = 3,
        //
        // Summary:
        //     An interval of days.
        Days = 4,
        //
        // Summary:
        //     An interval of hours.
        Hours = 5,
        //
        // Summary:
        //     An interval of minutes.
        Minutes = 6,
        //
        // Summary:
        //     An interval of seconds.
        Seconds = 7,
        //
        // Summary:
        //     An interval of milliseconds.
        Milliseconds = 8,
    }
}