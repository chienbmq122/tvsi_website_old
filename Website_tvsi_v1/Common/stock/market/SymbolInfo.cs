﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common.stock.market
{
    public enum CompanyType { General, Bank, Securities, Fund, Insurance };
    public enum SecType { Stock = 1, Bond, CertificateOfFund }
    public class SymbolInfo
    {
        private string _symbol;

        public string Symbol
        {
            get { return _symbol; }
        }

        private string _name;

        public string Name
        {
            get { return _name; }
        }

        private string _industry;

        public string Industry
        {
            get { return _industry; }
            set { _industry = value; }
        }

        private string _sector;

        public string Sector
        {
            get { return _sector; }
        }

        private int _sectorId;

        public int SectorId
        {
            get { return _sectorId; }
        }

        private int _industryId;

        public int IndustryId
        {
            get { return _industryId; }
        }

        private string _exchange;

        public string Exchange
        {
            get { return _exchange; }
        }

        private int _marketID;

        public int MarketID
        {
            get { return _marketID; }
        }

        private CompanyType _companyType;
        public CompanyType CompanyType
        {
            get { return _companyType; }

        }
        public SymbolInfo(string symbol, string name, CompanyType companyType, int industryId, string industry, int sectorId, string sector, int marketID, string exchange)
        {
            this._symbol = symbol;
            this._name = name;
            this._companyType = companyType;
            this._industry = industry;
            this._sector = sector;
            this._exchange = exchange;
            this._industryId = industryId;
            this._sectorId = sectorId;
            this._marketID = marketID;
        }
    }
}