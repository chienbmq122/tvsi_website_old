﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.newswebsite;
using Website_tvsi_v1.Common.newswebsite.mapper;
using Website_tvsi_v1.Common.stock.mapper;
using Website_tvsi_v1.Common.stock.market;

namespace Website_tvsi_v1.Common.Data
{
    public class SqlSymbolData
    {
        private static DataHelper _dataHelper = new DataHelper("Data");
        public static List<Quote> GetTodayQuotes(string symbol = "HOSTC")
        {
            QuoteMapper mapper = new QuoteMapper();

            IDataReader reader =
                _dataHelper.ExecuteReader("stock_Stocks_GetTodayQuotes", symbol);

            return mapper.Map(reader);
        }
        public static SymbolInfo GetSymbolInfo(string symbol, string language)
        {
            SymbolMapper mapper = new SymbolMapper();

            IDataReader reader = _dataHelper.ExecuteReader("stock_Symbols_GetSymbolInfo", symbol, language);

            List<SymbolInfo> symbols = mapper.Map(reader);

            if (symbols.Count > 0)
            {
                return symbols[0];
            }

            return null;
        }
        public static Quote GetQuote(string symbol)
        {
            QuoteMapper mapper = new QuoteMapper();

            IDataReader reader =
                _dataHelper.ExecuteReader("stock_Stocks_GetQuote", symbol);

            List<Quote> quotes = mapper.Map(reader);

            if (quotes.Count > 0)
            {
                return quotes[0];
            }

            return null;
        }
        public static News GetNews(int newsID)
        {
            NewsMapper mapper = new NewsMapper();

            IDataReader reader = _dataHelper.ExecuteReader("stock_News_GetNews", newsID);

            List<News> news = mapper.Map(reader);

            if (news.Count > 0)
            {
                return news[0];
            }
            return null;
        }

        public static string GetNewsContent(int newsID)
        {
            object content = _dataHelper.ExecuteScalar("stock_News_GetNewsContent", newsID);

            return content != DBNull.Value ? (string)content : null;
        }

        public static int GetNewsInGroupCount(int groupID, DateTime startDate, DateTime endDate, string language)
        {
            object count = _dataHelper.ExecuteScalar("stock_News_GetNewsInGroupCount", groupID, startDate, endDate, language);

            return (int)count;
        }
        public static List<News> GetNewsInGroup(int groupID, DateTime startDate, DateTime endDate, int startIndex, int count, string language)
        {
            NewsMapper mapper = new NewsMapper();

            IDataReader reader =
                _dataHelper.ExecuteReader("stock_News_GetNewsInGroup", groupID, startDate, endDate, startIndex, count, language);

            return mapper.Map(reader);
        }
    }
}