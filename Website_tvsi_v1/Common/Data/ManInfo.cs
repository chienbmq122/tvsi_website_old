﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common.Data
{
    public class ManInfo : IComparable
    {
        private string _gia_tri;
        private string _ma_chi_nhanh;
        private DateTime _ngay_tao;
        public ManInfo(string gia_tri, string ma_chi_nhanh)
        {
            this._gia_tri = gia_tri;
            this._ma_chi_nhanh = ma_chi_nhanh;
        }

        public string gia_tri { get 
            { return (this._gia_tri); } set { this._gia_tri = value; } }
        public string ma_chi_nhanh { get { return (this._ma_chi_nhanh); } set { this._ma_chi_nhanh = value; } }
        public DateTime ngay_tao { get { return (this._ngay_tao); } }
        public int CompareTo(object obj)
        {
            if (obj is ManInfo)
            {
                ManInfo q = (ManInfo)obj;
                return this.ngay_tao.CompareTo(q.ngay_tao);
            }
            else
            {
                throw new ArgumentException("object is not a ManInfo");
            }
        }
    }
}