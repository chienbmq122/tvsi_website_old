﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.stock.mapper;

namespace Website_tvsi_v1.Common.Data
{
    public class ManIngoMapper : GenericMapper<ManInfo>
    {
        protected override ManInfo MapOne(System.Data.IDataRecord record)
        {
            string gia_tri = (string)record["gia_tri"];
            string ma_chi_nhanh = (string)record["ma_chi_nhanh"];
            ManInfo maninfo = new ManInfo(gia_tri, ma_chi_nhanh);
            return maninfo;
        }
    }
}