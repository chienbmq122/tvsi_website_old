﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.stock.mapper;
using Website_tvsi_v1.Common.Data;

namespace Website_tvsi_v1.Common.Data
{
   
    public class SqlManInfoData
    {
        private static DataHelper _dataHelper = new DataHelper("CommonDB");
        public static List<ManInfo> GetManInfos()
        {
            ManIngoMapper mapper = new ManIngoMapper();

            IDataReader reader =_dataHelper.ExecuteReader("TVSI_sLAY_THONG_TIN_NGUOI_DAI_DIEN");

            return mapper.Map(reader);
        }
    }
}