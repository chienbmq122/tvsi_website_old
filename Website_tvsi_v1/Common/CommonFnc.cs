﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common
{
    public class CommonFnc
    {
        private static readonly string[] VietNamChar = new string[] 
    { 
        "aAeEoOuUiIdDyY", 
        "áàạảãâấầậẩẫăắằặẳẵ", 
        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ", 
        "éèẹẻẽêếềệểễ", 
        "ÉÈẸẺẼÊẾỀỆỂỄ", 
        "óòọỏõôốồộổỗơớờợởỡ", 
        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ", 
        "úùụủũưứừựửữ", 
        "ÚÙỤỦŨƯỨỪỰỬỮ", 
        "íìịỉĩ", 
        "ÍÌỊỈĨ", 
        "đ", 
        "Đ", 
        "ýỳỵỷỹ", 
        "ÝỲỴỶỸ" 
    };

        public static string SubString(string str, int length)
        {
            if (str.Length < length)
            {
                return str;
            }
            else
            {
                string leftStr = str.Substring(0, length);
                int index = leftStr.LastIndexOf(" ");
                if (index < 0)
                    return leftStr + " ...";
                else
                    return leftStr.Substring(0, index) + " ...";
            }            
        }


        public static string FormatDate(string date)
        {
            DateTime dtmDate = DateTime.Today;
            DateTime.TryParse(date, out dtmDate);
            return dtmDate.ToString("dd/MM/yyyy");  
        }

        public static string LocDau(string str)
        {
            //Thay thế và lọc dấu từng char      
            for (int i = 1; i < VietNamChar.Length; i++)
            {
                for (int j = 0; j < VietNamChar[i].Length; j++)
                    str = str.Replace(VietNamChar[i][j], VietNamChar[0][i - 1]);
            }
            return str;
        }
        public static string GetstringfirstIndex(string str, string str_index)
        {

            string str_temp = str;
            int index = str.IndexOf(str_index);
            int int_length = str.Length;
            if (index > 0)
            {
                str_temp = str.Substring(index + 1);
            }
            return str_temp;
        }
        public static string GetUrlImg(string str_img)
        {

            string str_temp = str_img, str_index = "\"";
            int index = str_img.IndexOf(str_index);
            int length = str_img.Length;
            if (index > 0)
            {
                str_temp = str_img.Substring(index + 1);
            }
            index = str_temp.IndexOf(str_index);
            if (index > 0)
            {
                str_temp = str_temp.Substring(0, index);
            }
            else
            {
                str_temp = "";
            }


            return str_temp;
        }       
        public static string FormatDate2(string date)
        {
            DateTime dtmDate = DateTime.Today;
            DateTime.TryParse(date, out dtmDate);
            return dtmDate.ToString("dd/MM/yyyy hh:mm:ss tt ");
        }
        public static int getLanguage(HttpCookie cookie)
        {
            string currentCulture = "en-GB";
            int ngonngu_id = 1;
            if (cookie != null)
            {
                currentCulture = cookie.Value;
            }
            if (currentCulture == "en-US")
            {
                ngonngu_id = 2;
            }
            return ngonngu_id;
        }

        public static string getLanguageStr(HttpCookie cookie)
        {
            string currentCulture = "en-GB", language = "en-US";
            if (cookie != null)
            {
                currentCulture = cookie.Value;
            }
            if (currentCulture == "vi-VN")
            {
                language = "vi-VN";
            }
            return language;
        }

        public static string twoNumber(int number)
        {

            int tmp_number = number % 100;
            string str_number = tmp_number.ToString();
            string result = "";
            if (str_number.Length < 2)
            {
                result = "0" + str_number;
            }
            else
            {
                result = str_number;
            }
            return result;
        }
    }
}