﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Website_tvsi_v1.Common.stock.mapper;

namespace Website_tvsi_v1.Common.newswebsite.mapper
{
    public class NewsMapper : GenericMapper<News>
    {
        protected override News MapOne(IDataRecord record)
        {
            News news = new News();
            // NewsCategory group = new NewsCategory();
            news.NewsID = (int)record["NewsID"];
            news.Title = (string)record["Title"];
            news.Description = (DBNull.Value == record["Description"]) ? null : (string)record["Description"];
            news.HasAttachedFile = (bool)record["HasAttachedFile"];
            news.AttachedFileName = (DBNull.Value == record["AttachedFileName"]) ? null : (string)record["AttachedFileName"];
            news.AttachedFileExtension = (DBNull.Value == record["AttachedFileExtension"]) ? null : (string)record["AttachedFileExtension"];
            news.Date = ((DateTime)record["Date"]).ToLocalTime();
            news.Source = (DBNull.Value == record["Source"]) ? null : (string)record["Source"];
            news.Author = (DBNull.Value == record["Author"]) ? null : (string)record["Author"];
            news.NewsUrl = (DBNull.Value == record["NewsUrl"]) ? null : (string)record["NewsUrl"];
            news.ImageUrl = (DBNull.Value == record["ImageUrl"]) ? null : (string)record["ImageUrl"];
            news.GroupID = (int)record["GroupID"];
            news.StoryID = (DBNull.Value == record["StoryID"]) ? null : (int?)record["StoryID"];
            news.IsTopStory = (bool)record["IsTopStory"];
            news.IsHotNews = (bool)record["IsHotNews"];
            news.IsMarketUpdate = (bool)record["IsMarketUpdate"];
            news.IsExpertIdea = (bool)record["IsExpertIdea"];
            news.Symbol = (DBNull.Value == record["Symbol"]) ? null : (string)record["Symbol"];
            news.Approved = (bool)record["Approved"];
            news.CreatedBy = (string)record["CreatedBy"];
            news.CreatedDate = ((DateTime)record["CreatedDate"]).ToLocalTime();
            news.LastModifiedBy = (string)record["LastModifiedBy"];
            news.LastModifiedDate = ((DateTime)record["LastModifiedDate"]).ToLocalTime();
            news.Status = (DBNull.Value == record["Status"]) ? "Inserted" : (string)record["Status"];
            //news.GroupName = (string)record["GroupName"];
            return news;
        }
    }
}