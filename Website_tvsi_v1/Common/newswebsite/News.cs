﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website_tvsi_v1.Common.newswebsite
{
    public class News
    {
        private int _newsId;

        public int NewsID
        {
            get { return _newsId; }
            set { _newsId = value; }
        }

        private int? _storyID;
        public int? StoryID
        {
            get { return _storyID; }
            set { _storyID = value; }
        }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private DateTime _date;

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _source;

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        private string _author;



        public string Author
        {
            get { return _author; }
            set { _author = value; }
        }

        private string _newsUrl;

        public string NewsUrl
        {
            get { return _newsUrl; }
            set { _newsUrl = value; }
        }

        private string _imageUrl;

        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }

        private bool _isTopStory;

        public bool IsTopStory
        {
            get { return _isTopStory; }
            set { _isTopStory = value; }
        }

        private bool _isHotNews;

        public bool IsHotNews
        {
            get { return _isHotNews; }
            set { _isHotNews = value; }
        }

        private bool _isMarketUpdate;

        public bool IsMarketUpdate
        {
            get { return _isMarketUpdate; }
            set { _isMarketUpdate = value; }
        }

        private bool _isExpertIdea;

        public bool IsExpertIdea
        {
            get { return _isExpertIdea; }
            set { _isExpertIdea = value; }
        }

        private int _groupID;

        public int GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        private string _symbol;
        public string Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        private bool _approved;

        public bool Approved
        {
            get { return _approved; }
            set { _approved = value; }
        }

        private string _content;
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }


        private bool _hasAttachedFile;

        public bool HasAttachedFile
        {
            get { return _hasAttachedFile; }
            set { _hasAttachedFile = value; }
        }

        private string _attachedFileName;
        public string AttachedFileName
        {
            get { return _attachedFileName; }
            set { _attachedFileName = value; }
        }

        private string _attachedFileExtension;
        public string AttachedFileExtension
        {
            get { return _attachedFileExtension; }
            set { _attachedFileExtension = value; }
        }

        private string _createdBy;
        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private DateTime _createdDate;
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { _createdDate = value; }
        }

        private string _lastModifiedBy;
        public string LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }

        private DateTime _lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return _lastModifiedDate; }
            set { _lastModifiedDate = value; }
        }


        private string _status;

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

    }
}