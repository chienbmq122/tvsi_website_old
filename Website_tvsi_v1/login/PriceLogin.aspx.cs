﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TVSI.Webservice.Connections;
using log4net;
using TVSI.Utility;

namespace Website_tvsi_v1.login
{
    public partial class PriceLogin : System.Web.UI.Page
    {
        public string str = "";
        public string str2 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            // Response.Redirect(strConfig + "er");
            string strConfig = string.Empty;
            strConfig = System.Configuration.ConfigurationManager.AppSettings["PriceUrl"];
            string strConfigRef = string.Empty;
            strConfigRef = System.Configuration.ConfigurationManager.AppSettings["PriceUrlRef"];

            string strPriced = string.Empty;
            string strPricedRef = string.Empty;
            strPriced = System.Configuration.ConfigurationManager.AppSettings["PricedUrl"];
            strPricedRef = System.Configuration.ConfigurationManager.AppSettings["PricedUrlRef"];

            string strReferrer = "";
            string strAPIINNO = string.Empty;
            strAPIINNO = System.Configuration.ConfigurationManager.AppSettings["strAPIINNO"];
            try
            {
                // Response.Write("<script language=\"javascript\">alert('1');</script>");
                strReferrer = Request.UrlReferrer.Host.ToString();
                Logger.Info(typeof(PriceLogin), "=====================URL:"+strReferrer);
                //strReferrer = "price.tvsi.com.vn";
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(PriceLogin), string.Format(" PriceLogin() ") + ex.Message);
                Response.Redirect(strConfig + "/check.aspx?token=false");
            }
            if (strReferrer == strConfigRef || strReferrer == strPricedRef)
            {
                Logger.Info(typeof(PriceLogin), "=====================GOING URL RIGHT=============");
                // Response.Write("<script language=\"javascript\">alert('2');</script>");
                try
                {
                    str = Request["u"].ToString().Trim().ToUpper().Replace("044C", "").Replace("044F", "");
                }
                catch (Exception ex)
                {
                    Logger.Error(typeof(PriceLogin), string.Format(" PriceLogin() ") + ex.Message);
                    Response.Redirect(strConfig + "/check.aspx?token=false");
                }
                if (str.Length == 6)
                {
                    str = str + "1";
                }
                
                if (str.Length != 7)
                {
                    Response.Redirect(strConfig + "/check.aspx?token=false");
                }
                else
                {
                    if (str.Length == 7)
                    {
                        str = str.Substring(0, 6) + "1";
                    }
                    try
                    {
                        str2 = Request["p"].ToString().Trim();
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                        //Logger.Error(typeof(PriceLogin), string.Format(" PriceLogin() ") + ex.Message);
                        Response.Redirect(strConfig + "/check.aspx?token=false");
                    }
                    string strJson = ReturnStringJson();
                    //Logger.Info(typeof(PriceLogin), "==============:"+ strJson);
                    if ((strJson.IndexOf("WRONG_PASSWORD") != -1) || (strJson.IndexOf("ERROR_ACCOUNT") != -1) || (strJson.IndexOf("ERROR_LOCKEDACCOUNT") != -1) || (strJson.IndexOf("ERROR_NOT_ACCEPT_AGREEMENT_YET") != -1) || (strJson.IndexOf("ERROR_NOT_ACCESS_PERMISSION") != -1) || (strJson.IndexOf("SYSTEM_ERROR") != -1))
                    {
                        Response.Redirect(strConfig + "/check.aspx?token=false");
                    }
                    else
                    {

                        //Response.Redirect(strJson);
                        Logger.Info(typeof(PriceLogin), "==============login thanh cong tro toi====" + strConfig);
                        //Kết nối đến webservice 4080 ( WS RF đang trỏ đến 136.7 -> chuyen server add file host)
                        string ticket = TVSIWSConnection.TVSI_fEncryptPassword("tvsi", "tvsi", "tvsi", "tvsi", DateTime.Today.ToString("yyyy-MM-dd") + Request["s"].ToString().Trim(), 0);
                        Response.Redirect(strConfig + "/check.aspx?token=" + ticket + "&u=" + str);
                    }
                }
            }
            else
            {
                Response.Write("<script language=\"javascript\">alert('Bạn không có quyền truy cập trang này.');location.href='https://www.tvsi.com.vn';</script>");

            }
        }


        public string ReturnStringJson()
        {
            //Logger.Info(typeof(PriceLogin), "=====================CALL API USERNAME:"+ Request["u"].ToString().Trim().Substring(0, 6)+"======P:"+ Request["p"].ToString().Trim());
            string responseFromServer = string.Empty;
            try
            {
                // Response.Write("<script language=\"javascript\">alert('json 1');</script>");
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //WebRequest request = WebRequest.Create("" + System.Configuration.ConfigurationManager.AppSettings["strAPIINNO"].ToString() + "/api/FO_AM_Logon?CustomerId=000846&password=12345678&authType=0&ipAddress=111&generateSession=false&format=json");
                WebRequest request = WebRequest.Create("" + System.Configuration.ConfigurationManager.AppSettings["strAPIINNO"].ToString() + "/api/FO_AM_Logon?CustomerId=" + Request["u"].ToString().Trim().Substring(0, 6) + "&password=" + Request["p"].ToString().Trim() + "&authType=0&ipAddress=111&generateSession=false&format=json");
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();
                //  Response.Write("<script language=\"javascript\">alert('json 2');</script>");
                // Display the status.
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                responseFromServer = reader.ReadToEnd();
                // Display the content.
                // Response.Write("<script language=\"javascript\">alert('json 3');</script>");
                // Clean up the streams and the response.
                reader.Close();
                response.Close();
                // this.lblReturn.Text = responseFromServer.ToString();
                //  Response.Write("<script language=\"javascript\">alert('" + responseFromServer.ToString() + "');</script>");
                return responseFromServer.ToString();
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(PriceLogin), string.Format("ReturnStringJson()") + ex.Message);
                return "WRONG_PASSWORD";
            }

        }


    }

}