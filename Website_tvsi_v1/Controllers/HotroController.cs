﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.App_Data;
using Website_tvsi_v1.EFAQWS;
using Website_tvsi_v1.Models;
using PagedList;
using System.Data;
using Website_tvsi_v1.Common;

namespace Website_tvsi_v1.Controllers
{
    public class HotroController : Controller
    {
        //
        // GET: /Hotro/
        private string name_service = "customer support";

        public HotroController()
        {
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown active";
            ViewBag.gttvsi_menu = "dropdown";
            ViewBag.title = "Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)- Thông tin hỗ trợ!";
        }

        public ActionResult Index()
        {
            return View();
        }

        // lay danh sach thong tin cua chuyen muc ho tro
       //chuyen muc ho tro id=7
        public void danhmucmennuhotro()
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]); 
            //id danh muc cha ho tro
            int danh_muc_phu_thuocid = 7;
            if (ngonngu_id == 2)
            {
                danh_muc_phu_thuocid = 17;
            }
            var menu_hotro = TVSI_DANH_MUC_HO_TRO_v3.Laytheo_danh_muc_phu_thuocid(ngonngu_id, danh_muc_phu_thuocid);
            // menu ho tro tvsi 
            int danhmuc_id1 = int.Parse(menu_hotro.Rows[0]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_giao_dich = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[1]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_giao_dich_tien = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[2]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_tra_cuu = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[3]["danh_muc_ho_troid"].ToString());
            ViewBag.cac_bieu_mau = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[4]["danh_muc_ho_troid"].ToString());
            ViewBag.kien_thuc_va_hoi_dap = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
        }
        //dieu huong cua menu huong dan giao dich co phieu
        public PartialViewResult NavigationHDgiaodich()
        {

            danhmucmennuhotro();
            return PartialView();
        }
        //dieu huong cua menu huong dan giao dich tien
        public PartialViewResult navigationHDgiaodichtien()
        {
            danhmucmennuhotro();
            return PartialView();
        }
        //dieu huong cua menu huong dan chuyen tien
        public PartialViewResult NavigationHDchuyentien()
        {
            danhmucmennuhotro();
            return PartialView();
        }
        //dieu huong cua menu huong dan tra cuu va quan ly tai san
        public PartialViewResult NavigationHDtracuu()
        {
            danhmucmennuhotro();
            return PartialView();
        }
        public PartialViewResult NavigationHoidap()
        {
            danhmucmennuhotro();
            return PartialView();
        }
        public PartialViewResult NavigationKienthuc()
        {
            danhmucmennuhotro();
            return PartialView();
        }
        public PartialViewResult NavigationKinhnghiem()
        {
            danhmucmennuhotro();
            return PartialView();

        }
        public PartialViewResult NavigationBieumau()
        {
            danhmucmennuhotro();
            return PartialView();
        }


        // trang chi tiet cua menu ho tro
        public ActionResult HuongdanTvsi(int id = 18)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            var thong_tin_ho_tro_selected = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            ViewBag.thong_tin_ho_tro_selected = thong_tin_ho_tro_selected;
            int danhmuc_cha_id = 7;
            if (ngonngu_id == 2)
            {
                danhmuc_cha_id = 17;
            }
            var menu_hotro = TVSI_DANH_MUC_HO_TRO_v3.Laytheo_danh_muc_phu_thuocid(ngonngu_id, danhmuc_cha_id);
            // menu khach hang doanh nghiep
            int danhmuc_id1 = int.Parse(menu_hotro.Rows[0]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_giao_dich = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[1]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_giao_dich_tien = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[2]["danh_muc_ho_troid"].ToString());
            ViewBag.huong_dan_tra_cuu = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[3]["danh_muc_ho_troid"].ToString());
            ViewBag.cac_bieu_mau = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            danhmuc_id1 = int.Parse(menu_hotro.Rows[4]["danh_muc_ho_troid"].ToString());
            ViewBag.kien_thuc_va_hoi_dap = TVSI_THONG_TIN_HOTRO_v3.LayTheo_danh_muc_ho_troid(ngonngu_id, danhmuc_id1);
            return View();
        }
        // chuyen muc hd_giao dich co phieu id=9
        public ActionResult HDgiaodichcophieu(int id=21)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            ViewBag.thong_tin_ho_tro = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            return View();
        }
        //chuyen muc hd_nop tien chuyen muc id=11
        public ActionResult HDnoptien(int id = 28)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            ViewBag.thong_tin_ho_tro = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            return View();
        }
        // trang cua menu huong dan chuyen tien khong dung
        public ActionResult HDchuyentien(int id = 100)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            ViewBag.thong_tin_ho_tro = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            return View();
        }
        // chuyen muc hd_tra cuu id =13
        public ActionResult HDtracuu(int id = 33)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            ViewBag.thong_tin_ho_tro = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            return View();
        }
        // chuyen muc hotro_cac bieu mau id=15
        public ActionResult Cacbieumau(int id = 37)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            ViewBag.thong_tin_ho_tro = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            return View();
        }
        // tim kiem hoi dap FAQ theo webservice
        public ActionResult HoidapFAQ(String search, int page = 1, int pagesize = 5)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);

            int id = 62;
            if (ngonngu_id == 2)
            {
                id = 63;
            }
            ViewBag.cauHoiDap = TVSI_THONG_TIN_HO_TRO_v2.LayTheo_thong_tin_ho_tro_id_2017(id);
            if (string.IsNullOrEmpty(search))
            {
                return View();
            }
            EFAQWS.EfaqWS service = (EFAQWS.EfaqWS)Utilities.GetService(typeof(EFAQWS.EfaqWS));
            int numberResult = 0;
            //Lấy trang hiện tại trong Session
            int currentPage = 1;
            //Thay thế tham số currentPage cho số 1
            XmlDocument xml = new XmlDocument();

            string result = service.Search(search, 1, currentPage, ConstantsFAQ.CST_DEFAULT_RECORD_PER_PAGE, Request.Url.ToString(), ref numberResult);
            xml.LoadXml(result);
            var listHoidap = new List<HoidapModel>();

            XmlNodeList xnList = xml.SelectNodes("/Results/Result");

            var stt = 0;
            foreach (XmlNode item in xnList)
            {
                stt++;
                string question = item.SelectSingleNode("QUESTION").InnerText;
                string answer = item.SelectSingleNode("ANSWER").InnerText;
                listHoidap.Add(new HoidapModel { stt = stt, question = question, answer = answer });
            }
            var model = listHoidap.ToPagedList(page, pagesize);
            ViewBag.record = listHoidap.Count;
            ViewBag.search = search;
            return View(model);
        }
        // lay danh sach kinh thuc by ky_tu theo tieu_de
        public ActionResult HoidapKienthuc(String search,int page=1,int pagesize=10)
        {
            int ngon_nguid = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]); 
            int danh_muc_kien_thucid = 1;

            DataTable datatable = new DataTable();
            if (string.IsNullOrEmpty(search))
            {
                datatable = TVSI_THONG_TIN_KIEN_THUC.LayTheo_danh_muc_kien_thucid(ngon_nguid, danh_muc_kien_thucid);
            }
            else 
            {
                datatable = TVSI_THONG_TIN_KIEN_THUC_KINH_NGHIEM.Search_ky_tu_theo_tieu_de(ngon_nguid, danh_muc_kien_thucid, search);
            }

            List<DataRow> danh_sach_kien_thuc = new List<DataRow>();
            foreach (DataRow dr in datatable.Rows)
            {
                danh_sach_kien_thuc.Add(dr);
            }
            var model = danh_sach_kien_thuc.ToPagedList(page, pagesize);
            ViewBag.search = search;
            ViewBag.total_record = danh_sach_kien_thuc.Count;
            return View(model);
        }
        // lay danh sach kinh nghiem by ky_tu theo tieu_de
        public ActionResult HoidapKinhnghiem(String search, int page = 1, int pagesize = 5)
        {
            int ngon_nguid = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]); 
            int danh_muc_kien_thucid = 2;

            DataTable datatable = new DataTable();
            if (string.IsNullOrEmpty(search))
            {
                datatable = TVSI_THONG_TIN_KIEN_THUC.LayTheo_danh_muc_kien_thucid(ngon_nguid, danh_muc_kien_thucid);
            }
            else
            {
                datatable = TVSI_THONG_TIN_KIEN_THUC_KINH_NGHIEM.Search_ky_tu_theo_tieu_de(ngon_nguid, danh_muc_kien_thucid, search);
            }

            List<DataRow> danh_sach_kien_thuc = new List<DataRow>();
            foreach (DataRow dr in datatable.Rows)
            {
                danh_sach_kien_thuc.Add(dr);
            }
            var model = danh_sach_kien_thuc.ToPagedList(page, pagesize);
            ViewBag.search = search;
            ViewBag.total_record = danh_sach_kien_thuc.Count;
            return View(model);
        }
        public ActionResult ChitietKinhnghiem(int id)
        {
            int ngon_nguid = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]); 
            ViewBag.chi_tiet_kinh_nghiem = TVSI_THONG_TIN_KIEN_THUC.Layra_chi_tiet_tin(ngon_nguid, id);
            return View();
        }
        public ActionResult ChitietKienthuc(int id)
        {
            int ngon_nguid = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]); 
            ViewBag.chi_tiet_kien_thuc = TVSI_THONG_TIN_KIEN_THUC.Layra_chi_tiet_tin(ngon_nguid, id);
            return View();
        }
        public ActionResult testnoptien()
        {
            return View();
        }
    }
}
