﻿using SRVTextToImage;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Website_tvsi_v1.Common.Data;
using Website_tvsi_v1.Common.stock.market;

namespace Website_tvsi_v1.Controllers
{
    public class CaptChaController : Controller
    {
        //
        // GET: /CaptCha/

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public FileResult GetCaptChaImageCanhan()
        {
            CaptchaRandomImage ci = new CaptchaRandomImage();
            this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] = ci.GetRandomString(5);
            ci.GenerateImage(this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString(), 132, 35, Color.Black, Color.White);
            MemoryStream stream = new MemoryStream();
            ci.Image.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }

        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public FileResult GetCaptChaImageKhaoSat()
        {
            CaptchaRandomImage ci = new CaptchaRandomImage();
            this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] = GetRandomNumber(3);
            ci.GenerateImage(this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString(), 132, 35, Color.Black, Color.White);
            MemoryStream stream = new MemoryStream();
            ci.Image.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }

        public FileResult GetCaptChaImageTochuc()
        {
            CaptchaRandomImage ci = new CaptchaRandomImage();
            this.Session[ConstantsFAQ.KY_TU_BAO_MAT_TO_CHUC] = ci.GetRandomString(5);
            ci.GenerateImage(this.Session[ConstantsFAQ.KY_TU_BAO_MAT_TO_CHUC].ToString(), 132, 35, Color.Black, Color.White);
            MemoryStream stream = new MemoryStream();
            ci.Image.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }
        [HttpPost]
        public JsonResult chartMarket(string exchange)
        {
            TimeFrame timeframe = TimeFrame.Parse("1d");
            DateTime bottomDate = timeframe.GetBaseDate(DateTime.Today);

            List<Quote> data = new List<Quote>();
            data = SqlSymbolData.GetTodayQuotes(exchange);
            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        private string GetRandomNumber(int length)
        {

            string randomStr = "";
            int[] myIntArray = new int[length];
            int x;

            //That is to create the random # and add it to our string
            Random autoRand = new Random();

            for (x = 0; x < length; x++)
            {
                myIntArray[x] = System.Convert.ToInt32(autoRand.Next(0, 9));
                randomStr += (myIntArray[x].ToString());
            }
            return randomStr;
        }
    }
}
