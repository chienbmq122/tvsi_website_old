﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using PagedList;
using Website_tvsi_v1.Common;

namespace Website_tvsi_v1.Controllers
{
    public class QhcodongController : Controller
    {
        //
        // GET: /Qhcodong/
        public QhcodongController()
        {
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown active";
            ViewBag.title = "Giao dịch và đầu tư cổ phiếu trực tuyến - Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)-";
        }
        public ActionResult Baocaotaichinh(int page = 1, int pagesize = 8)
        {

            //int ngonngu_id = 1;
            int chuyen_muc_tvsiid = 58;
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            //66	Financial Statements
            if (ngonngu_id == 2)
            {
                chuyen_muc_tvsiid = 66;
            }
            DataTable data = new DataTable();
            data = TVSI_THONG_TIN_TVSI_v2.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id, chuyen_muc_tvsiid);
            List<DataRow> ds_bctaichinh = new List<DataRow>();
            foreach (DataRow dr in data.Rows)
            {
                ds_bctaichinh.Add(dr);
            }
            ViewBag.account = ds_bctaichinh.Count;
            var model = ds_bctaichinh.ToPagedList(page, pagesize);

            return View(model);
        }
        public ActionResult Thongtincongbo(int page = 1, int pagesize = 8)
        {
            int ngonngu_id = 1;
            int chuyen_muc_tvsiid = 59; //67	Published information
            DataTable data = new DataTable();
            data = TVSI_THONG_TIN_TVSI_v2.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id, chuyen_muc_tvsiid);
            List<DataRow> ds_ttcongbo = new List<DataRow>();
            foreach (DataRow dr in data.Rows)
            {
                ds_ttcongbo.Add(dr);
            }
            ViewBag.account = ds_ttcongbo.Count;
            var model = ds_ttcongbo.ToPagedList(page, pagesize);
            return View(model);

        }
        public ActionResult Baocaothuongnien(int page = 1, int pagesize = 6)
        {
           
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            int chuyen_muc_tvsiid = 62;//70	Annual Reports may test 72
            if (ngonngu_id == 2)
            {
                chuyen_muc_tvsiid = 70;
            }
           
            DataTable data = new DataTable();
            data = TVSI_THONG_TIN_TVSI_v2.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id, chuyen_muc_tvsiid);
            List<DataRow> ds_bcthuongnien = new List<DataRow>();
            foreach (DataRow dr in data.Rows)
            {
                ds_bcthuongnien.Add(dr);
            }
            ViewBag.account = ds_bcthuongnien.Count;
            var model = ds_bcthuongnien.ToPagedList(page, pagesize);
            return View(model);
        }
        public ActionResult Thongtincodong()
        {
            int id = 1140;
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            if (ngonngu_id == 2)
            {
                id = 1174;
            }
            ViewBag.thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI(ngonngu_id, id);
            return View();
        }
        public ActionResult Quantridoanhnghiep()
        {
            int id = 1141;
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            if (ngonngu_id == 2)
            {
                id = 1175;
            }
            ViewBag.thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI(ngonngu_id, id);
            return View();
        }
    }
}
