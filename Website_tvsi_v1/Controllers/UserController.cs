﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.Utility;
using Website_tvsi_v1.Common;
using Website_tvsi_v1.Models;

namespace Website_tvsi_v1.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ConfirmEkycPage(string code,string codem,string sms = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(code) && !string.IsNullOrEmpty(sms))
                {
                    ViewBag.IsConfirmed = !UtilTvsi.CheckEkycConfirmCode(code,sms);
                }
                if (!string.IsNullOrEmpty(codem))
                {
                    ViewBag.IsConfirmed = !UtilTvsi.CheckEkycConfirmCodeMobile(codem);
                }
                //Logger.Error(typeof(UserController), "ViewBag:" + ViewBag.IsConfirmed);
                return View(new ConfirmEkycModel()
                {
                    Code = code,
                    CodeM = codem
                });
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UserController), "ConfirmEkycPage():" + ex.Message);
                throw;
            }
        }
        
        [HttpPost]
        [AllowAnonymous]
        public JsonResult ConfirmEkyc(ConfirmEkycModel model)
        {
            try
            {
                if (!string.IsNullOrEmpty(model.Code))
                {
                    var isOtpValid = UtilTvsi.CheckEkycOtp(model.Code, model.Sms);
                    var message = "";
                    if (!isOtpValid)
                    {
                        message = "Mã OTP không hợp lệ!";
                        return Json(new {status = false, message = message}, JsonRequestBehavior.DenyGet);
                    }

                    UtilTvsi.ConfirmEkyc(model.Code);

                    return Json(new {status = true});
                }

                if (!string.IsNullOrEmpty(model.CodeM))
                {
                    var isOtpValid = UtilTvsi.CheckEkycOtp(model.CodeM, model.Sms);
                    var message = "";
                    if (!isOtpValid)
                    {
                        message = "Mã OTP không hợp lệ!";
                        return Json(new {status = false, message = message}, JsonRequestBehavior.DenyGet);
                    }

                    Logger.Info(typeof(UserController),
                        "ConfirmEkyc()--Codem->" + model.CodeM + "-- OTP" + model.Otp + "");
                    var retStr = UtilTvsi.ConfirmEkycMobile(model.CodeM);

                    if (retStr)
                    {
                        return Json(new {status = true});
                    }
                    else
                    {
                        return Json(new {status = false, message = "Xác thực không thành công."},
                            JsonRequestBehavior.DenyGet);
                    }
                }

                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(UserController), "ConfirmEkyc():" + ex.Message);
                return Json(new
                {
                    status = false,
                    message = "Lỗi hệ thống!"
                }, JsonRequestBehavior.DenyGet);
            }
        }
    }
}
