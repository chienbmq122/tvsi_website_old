﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.EFAQWS;
using TVSI.Common;
using System.Text;
using Website_tvsi_v1.App_Data;
using System.Xml;
using Website_tvsi_v1.Models;
using PagedList;
using System.Data;
using Website_tvsi_v1.Common;
using System.Web.Mail;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using TVSI.Security.Cryptography;
using TVSI.Common.DAL;
using System.Globalization;
using System.Threading;
using Website_tvsi_v1.Common.newswebsite;
using Website_tvsi_v1.Common.Data;
using Newtonsoft.Json;
using System.Net;
using TVSI.Utility;
using Website_tvsi_v1.WebService;
using Website_tvsi_v1.WebService.Model;
using System.Data.SqlTypes;
using System.Diagnostics.Eventing.Reader;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Web.ModelBinding;
using System.Web.WebPages;
using Dapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Website_tvsi_v1.App_Resources;
using DataTable = System.Data.DataTable;

namespace Website_tvsi_v1.Controllers
{
    //Đổi tên controller nhé
    public class HomeController : Controller
    {
        private string name_service = "about tvsi";

        public HomeController()
        {
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown active";
            ViewBag.title = "Giao dịch và đầu tư cổ phiếu trực tuyến - Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)-";
        }

        public ActionResult Index()
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            DataTable vnindex_Today = TVSI_DU_LIEU_BIEU_DO.LayThongTinThiTruong("HOSE");
            var temp = vnindex_Today.Rows[0];
            var hnxIndex_Today = TVSI_DU_LIEU_BIEU_DO.LayThongTinThiTruong("HASTC");
            var thi_truong_Last_day = TVSI_DU_LIEU_BIEU_DO_V3.LayKetQuaGiaoDichThiTruongGanNhat();
            DataTable upcom_today = TVSI_DU_LIEU_BIEU_DO_V3.LayThongTinThiTruongUpcom();
            DataTable upcom_lastday = TVSI_DU_LIEU_BIEU_DO_V3.LayKetQuaGiaoDichThiTruongUpcom();

            // du lieu vn index
            StockMarketModel stock_vnindex = new StockMarketModel();
            stock_vnindex = UtilTvsi.Thongtinthitruong(vnindex_Today.Rows[0], thi_truong_Last_day.Rows[0]);
            ViewBag.vnIndex = stock_vnindex;
            // du lieu hnx index
            StockMarketModel stock_hnxIndex = new StockMarketModel();
            stock_hnxIndex = UtilTvsi.Thongtinthitruong(hnxIndex_Today.Rows[0], thi_truong_Last_day.Rows[1]);
            ViewBag.hnxIndex = stock_hnxIndex;
            // du lieu upcom index
            StockMarketModel upcom_index = new StockMarketModel();
            upcom_index = UtilTvsi.Thongtinthitruong(upcom_today.Rows[0], upcom_lastday.Rows[0]);
            if (upcom_today.Rows[0].ToString() == "")
            {
                TVSI.Utility.Logger.Error(typeof(HomeController),
                    "Loi khong lay duoc thong tin thi truong UPCOM check store : TVSI_KET_QUA_GIAO_DICH_THI_TRUONG_v2, TVSI_sTHONG_TIN_THI_TRUONG_UPCOM_v3");
            }

            ViewBag.upcomIndex = upcom_index;
            string s = Request.ApplicationPath;
            int count = 4;
            String ngon_ngu = "vi-vn";
            if (ngonngu_id == 2)
            {
                ngon_ngu = "en-us";
            }

            int groupid = 29;

            //News tin_first = SqlSymbolData.GetNews(392680); // 378909 chuong trinh uu dai id 392680
            try
            {
                ViewBag.tin_noi_bat = TVSI_THONG_TIN_TVSI_v2.THONG_TIN_TVSI_Hotnew(count, ngon_ngu);
                ViewBag.tin_tvsi = TVSI_THONG_TIN_TVSI_v2.THONG_TIN_TVSI_GET_NEWSTVSI(groupid, count, ngon_ngu);
            }
            catch
            {
                ViewBag.tin_noi_bat = new DataTable();
                ViewBag.tin_tvsi = new DataTable();
            }

            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown";
            //ViewBag.tin_first = tin_first;
            return View();
        }

        public ActionResult GioithieuTVSI(int id = 1049)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }

            var thong_tin_ve_tvsi = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(id);
            int chuyen_muc_ve_tvsiid = 55;
            if (ngonngu_id == 2)
            {
                chuyen_muc_ve_tvsiid = 65;
            }

            ViewBag.thong_tin_ve_tvsi = thong_tin_ve_tvsi;
            ViewBag.chuyen_muc_ve_tvsi =
                TVSI_THONG_TIN_TVSI_V3.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id, chuyen_muc_ve_tvsiid);
            return View();
        }

        // chuyen muc id=7
        public ActionResult Cohoinghenghiep(int id = 1176)
        {
            //id la danh muc id cua chuyen muc co hoi nghe nghiep
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }

            var thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(id);
            int danh_muc_id = 71;
            if (ngonngu_id == 2)
            {
                danh_muc_id = 72; // 71 72 tren may that 70 72 tren may test
            }

            ViewBag.thong_tin_selected = thong_tin_selected;
            ViewBag.co_hoi_nghe_nghiep = TVSI_THONG_TIN_TVSI_V3.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id, danh_muc_id);
            return View();
        }

        // vi tri tuyen dung chi tiet
        public ActionResult VitriTuyendungChitiet(int id = 1056)
        {
            int ngonngu_id = 1;
            int ngonngu_id2 = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            int danh_muc_id = 71;
            if (ngonngu_id2 == 2)
            {
                danh_muc_id = 72; // 71 72 tren may that 70 72 tren may test
            }

            ViewBag.thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI(ngonngu_id, id);
            ViewBag.co_hoi_nghe_nghiep = TVSI_THONG_TIN_TVSI_V3.LayTheo_chuyen_muc_tin_tvsiid(ngonngu_id2, danh_muc_id);
            return View();
        }

        // hoat dong tvsi
        public ActionResult Hoatdongtvsi(int page = 1, int pagesize = 6)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            List<ImageModel> thu_muc_anh_hoat_dong = GetFirstImagefolders(ngonngu_id == 1
                ? ConstantsFAQ.PATH_ANH_HOAT_DONG_TVSI
                : ConstantsFAQ.PATH_ANH_HOAT_DONG_TVSI_E);

            ViewBag.ngonngu = ngonngu_id;
            ViewBag.record = thu_muc_anh_hoat_dong.Count;
            var model = thu_muc_anh_hoat_dong.ToPagedList(page, pagesize);
            return View(model);
        }

        public ActionResult Imagehoatdongtvsi(String folder, int page = 1, int pagesize = 6)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var pathImages = ngonngu_id == 1
                ? ConstantsFAQ.PATH_ANH_HOAT_DONG_TVSI
                : ConstantsFAQ.PATH_ANH_HOAT_DONG_TVSI_E;
            var anh_hoat_dong = new MyViewModel()
            {
                Images = Directory.EnumerateFiles(Server.MapPath(pathImages + folder))
                    .Select(fn => pathImages + folder + "/" + Path.GetFileName(fn))
            };
            ViewBag.record = anh_hoat_dong.Images.Count();
            var model = anh_hoat_dong.Images.ToPagedList(page, pagesize);
            ViewBag.folder = folder;
            return View(model);
        }

        public ActionResult Tvsicongdong(int page = 1, int pagesize = 6)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            List<ImageModel> thu_muc_anh_cong_dong = GetFirstImagefolders(ngonngu_id == 1
                ? ConstantsFAQ.PATH_ANH_TVSI_CONG_DONG
                : ConstantsFAQ.PATH_ANH_TVSI_CONG_DONG_E);
            ViewBag.ngonngu = ngonngu_id;
            ViewBag.record = thu_muc_anh_cong_dong.Count;
            var model = thu_muc_anh_cong_dong.ToPagedList(page, pagesize);
            return View(model);
        }

        public List<ImageModel> GetFirstImagefolders(String path_image)
        {
            string path = Server.MapPath(path_image);
            string[] dirs = Directory.GetDirectories(path, "*", SearchOption.AllDirectories);
            List<String> thu_muc = new List<string>();
            foreach (string dir in dirs)
            {
                int index = dir.LastIndexOf("\\");
                var ten_thu_muc = dir.Substring(index + 1, dir.Length - index - 1);
                thu_muc.Add(ten_thu_muc);
            }

            var myviewmodle = new MyViewModel();
            List<ImageModel> images_list = new List<ImageModel>();
            for (int i = 0; i < thu_muc.Count; i++)
            {
                myviewmodle.Images = Directory.EnumerateFiles(Server.MapPath(path_image + thu_muc[i]))
                    .Select(fn => path_image + thu_muc[i] + "/" + Path.GetFileName(fn));
                int count = 0;
                foreach (String image in myviewmodle.Images)
                {
                    count++;
                }

                if (count > 0)
                {
                    images_list.Add(new ImageModel()
                    {
                        path_image = myviewmodle.Images.First().ToString(),
                        folder_image = thu_muc[i]
                    });
                }
            }

            return images_list;
        }

        public ActionResult ImageTvsicongdong(String folder, int page = 1, int pagesize = 6)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var pathImages = ngonngu_id == 1
                ? ConstantsFAQ.PATH_ANH_TVSI_CONG_DONG
                : ConstantsFAQ.PATH_ANH_TVSI_CONG_DONG_E;
            var anh_hoat_dong = new MyViewModel()
            {
                Images = Directory.EnumerateFiles(Server.MapPath(pathImages + folder))
                    .Select(fn => pathImages + folder + "/" + Path.GetFileName(fn))
            };
            ViewBag.record = anh_hoat_dong.Images.Count();
            var model = anh_hoat_dong.Images.ToPagedList(page, pagesize);
            ViewBag.folder = folder;
            return View(model);
        }

        public ActionResult Tvsibaochi()
        {
            return View();
        }


        public PartialViewResult _TvsiVoiBaoChiPart(int currentPage = 1)
        {
            string language = string.Empty;
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            int startIndex = 0, count = 0, totalPage = 1, pageSize = 5, groupId = 35;
            DateTime startDate = SqlDateTime.MinValue.Value, endDate = SqlDateTime.MaxValue.Value;
            if (ngonngu_id == 1)
            {
                language = "vi-VN";
            }
            else
            {
                language = "en-US";
            }

            count = SqlSymbolData.GetNewsInGroupCount(groupId, startDate, endDate, language);
            if (count > 1000)
            {
                count = 1000;
            }

            totalPage = (int) Math.Ceiling((double) count / pageSize);
            if (totalPage > 1)
            {
                startIndex = (currentPage - 1) * pageSize;
            }

            List<News> bantintvsi =
                SqlSymbolData.GetNewsInGroup(groupId, startDate, endDate, startIndex, pageSize, language);
            ViewBag.bantintvsi = bantintvsi;
            ViewBag.domain = ConfigurationManager.AppSettings["domain"];
            setPagination(totalPage, currentPage);
            return PartialView();
        }

        public ActionResult TvsiVoiBaoChiDetails(int Id = 321449)
        {
            News tintuc = SqlSymbolData.GetNews(Id);
            if (tintuc != null)
            {
                string language = CommonFnc.getLanguageStr(Request.Cookies[CommonConstant.name_lang_tvsi_fp]),
                    noidung = SqlSymbolData.GetNewsContent(Id);
                ViewBag.tintuc = tintuc;
                ViewBag.noidung = noidung;
                ViewBag.Title = tintuc.Title;
                ViewBag.domain = ConfigurationManager.AppSettings["domain"];
                return View();
            }
            else
            {
                //return View("Index");
                return RedirectToAction("Index", "Home");
            }
        }

        //Tin TVSI với cộng đồng

        public ActionResult TinTvsicongdong()
        {
            return View();
        }

        public PartialViewResult _TinTvsiCongdongPart(int currentPage = 1)
        {
            string language = string.Empty;
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            int startIndex = 0, count = 0, totalPage = 1, pageSize = 5, groupId = 36;
            DateTime startDate = SqlDateTime.MinValue.Value, endDate = SqlDateTime.MaxValue.Value;
            language = ngonngu_id == 1 ? "vi-VN" : "en-US";

            count = SqlSymbolData.GetNewsInGroupCount(groupId, startDate, endDate, language);
            if (count > 1000)
            {
                count = 1000;
            }

            totalPage = (int) Math.Ceiling((double) count / pageSize);
            if (totalPage > 1)
            {
                startIndex = (currentPage - 1) * pageSize;
            }

            List<News> bantintvsi =
                SqlSymbolData.GetNewsInGroup(groupId, startDate, endDate, startIndex, pageSize, language);
            ViewBag.bantintvsi = bantintvsi;
            ViewBag.domain = ConfigurationManager.AppSettings["domain"];
            setPagination(totalPage, currentPage);
            return PartialView();
        }

        public ActionResult TinTvsicongdongDetails(int Id = 321449)
        {
            News tintuc = SqlSymbolData.GetNews(Id);
            if (tintuc != null)
            {
                string language = CommonFnc.getLanguageStr(Request.Cookies[CommonConstant.name_lang_tvsi_fp]),
                    noidung = SqlSymbolData.GetNewsContent(Id);
                ViewBag.tintuc = tintuc;
                ViewBag.noidung = noidung;
                ViewBag.Title = tintuc.Title;
                ViewBag.domain = ConfigurationManager.AppSettings["domain"];
                return View();
            }
            else
            {
                //return View("Index");
                return RedirectToAction("Index", "Home");
            }
        }


        public void setPagination(int totalPage, int currentPage)
        {
            if (totalPage > 1)
            {
                ViewBag.showPrevious = true;
                ViewBag.showNext = true;
                ViewBag.showPageNumbers = true;
            }
            else
            {
                ViewBag.showPrevious = false;
                ViewBag.showNext = false;
                ViewBag.showPageNumbers = false;
            }

            ViewBag.totalPage = totalPage;
            ViewBag.currentPage = currentPage;

            int pageRange = 2;
            int rangeStart = currentPage - pageRange;
            int rangeEnd = currentPage + pageRange;
            if (rangeEnd > totalPage)
            {
                rangeEnd = totalPage;
                rangeStart = totalPage - pageRange * 2;
                rangeStart = rangeStart < 1 ? 1 : rangeStart;
            }

            if (rangeStart <= 1)
            {
                rangeStart = 1;
                rangeEnd = Math.Min(pageRange * 2 + 1, totalPage);
            }

            ViewBag.pageRange = pageRange;
            ViewBag.rangeStart = rangeStart;
            ViewBag.rangeEnd = rangeEnd;
        }

        //dang ky
        public ActionResult DangkyMoTaikhoan()
        {
            ViewBag.spdv_menu = "dropdown active";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown ";
            return View();
        }

        // dang ky khach hang to chuc
        [HttpGet]
        public ActionResult DangKyKhTochuc()
        {
            if (this.Session["check_tochuc"] != null)
            {
                ViewBag.success = this.Session["check_tochuc"];
                this.Session["check_tochuc"] = null;
            }
            else
            {
                ViewBag.success = false;
            }

            ViewBag.spdv_menu = "dropdown active";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown ";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DangKyKhTochuc(DangKyKhToChucModel dangkytochuc, String kytubaomattc)
        {
            int paraDefault = 0;
            string yeucau = string.Empty;
            yeucau += "Tên tổ chức: " + dangkytochuc.tentochuc + "<br>";
            yeucau += "Số điện thoại cơ quan: " + dangkytochuc.sodienthoai + "<br>";
            yeucau += "Số máy fax cơ quan: " + dangkytochuc.somayfax + "<br>";
            ViewBag.spdv_menu = "dropdown active";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown ";
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_TO_CHUC] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View(dangkytochuc);
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_TO_CHUC].ToString() != kytubaomattc)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View(dangkytochuc);
                }
            }


            DateTime ngaycap = DateTime.ParseExact(dangkytochuc.ngaycap, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string CRMStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            string SaleIDCF = ConfigurationManager.AppSettings["SaleID"].ToString();

            string saleid = String.Empty;

            if (!string.IsNullOrEmpty(dangkytochuc.sale_id))
                saleid = UtilTvsi.GetIDSale(dangkytochuc.sale_id);
            else
                saleid = SaleIDCF.ToString();


            //insert dang ky mo tai khoan
            SqlParameter para_hoten = new SqlParameter("@hoten", dangkytochuc.hoten);
            SqlParameter para_ngaysinh = new SqlParameter("@ngaysinh", DateTime.Now);
            SqlParameter para_gioitinh = new SqlParameter("@gioitinh", 1);
            SqlParameter para_cmnd = new SqlParameter("@cmnd", dangkytochuc.sodkkd);
            SqlParameter para_ngaycapcmnd = new SqlParameter("@ngaycapcmnd", ngaycap);
            SqlParameter para_noicap = new SqlParameter("@noicap", dangkytochuc.noicap);
            SqlParameter para_sdt = new SqlParameter("@sdt", dangkytochuc.sodidong);
            SqlParameter para_email = new SqlParameter("@email", dangkytochuc.email);
            SqlParameter para_diachi = new SqlParameter("@diachi", dangkytochuc.diachi);
            SqlParameter para_saleid = new SqlParameter("@saleid", saleid);
            //SqlParameter para_chinhanh = new SqlParameter("@chinhanh", "");
            //SqlParameter para_tknh = new SqlParameter("@tknh", "");
            //SqlParameter para_nganhang = new SqlParameter("@nganhang", "");
            SqlParameter para_yeucau = new SqlParameter("@yeucau", yeucau);
            //insert noi dung gui email dang ky mo tai khoan
            SqlParameter para_Subject = new SqlParameter("@tieu_de",
                "TVSI:" + Website_tvsi_v1.App_Resources.Website.Txt_RegOpenAccount + "");
            SqlParameter para_Content = new SqlParameter("@noi_dung", UtilTvsi.ReturnNoiDungMailTochuc(dangkytochuc));
            SqlParameter para_danhmucdichvu = new SqlParameter("@danh_muc_dich_vu", paraDefault);
            SqlParameter para_trangthai = new SqlParameter("@trang_thai", paraDefault);
            SqlParameter para_nguoi_tao = new SqlParameter("@nguoi_tao", "system");
            SqlParameter para_ngay_tao = new SqlParameter("@ngay_tao", DateTime.Now);
            SqlParameter para_nguoi_cap_nhat = new SqlParameter("@nguoi_cap_nhat", "system");
            SqlParameter para_ngay_cap_nhat = new SqlParameter("@ngay_cap_nhat", DateTime.Now);
            SqlParameter para_thoi_gian_nhan = new SqlParameter("@thoi_gian_nhan", DateTime.Now);
            SqlParameter para_thoi_gian_gui = new SqlParameter("@thoi_gian_gui", DateTime.Now);
            SqlParameter para_NhomEmail = new SqlParameter("@nhom_email", paraDefault);
            SqlParameter para_dia_chi_email = new SqlParameter("@dia_chi_email", dangkytochuc.email);
            SqlParameter para_trangthai1 = new SqlParameter("trang_thai1", paraDefault);
            SqlParameter para_thoi_gian_gui1 = new SqlParameter("@thoi_gian_gui1", DateTime.Now);
            SqlParameter para_thoi_gian_nhan1 = new SqlParameter("@thoi_gian_nhan1", DateTime.Now);
            SqlParameter para_codeEncryp1 = new SqlParameter("@code",
                TVSIEncryption.SimpleEncryptPassword("", dangkytochuc.email.Trim()));
            try
            {
                SqlHelper.ExecuteNonQuery(CRMStr, "TVSI_sDANG_KY_MO_TAI_KHOAN_TO_CHUC",
                    para_hoten,
                    para_ngaysinh,
                    para_gioitinh,
                    para_cmnd,
                    para_ngaycapcmnd,
                    para_noicap,
                    para_sdt,
                    para_email,
                    para_diachi,
                    para_saleid,
                    para_yeucau
                );
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sQEM_NOI_DUNG_EMAIL_INSERT_2TABLE11", para_Subject,
                    para_Content, para_danhmucdichvu, para_trangthai, para_nguoi_tao, para_ngay_tao,
                    para_nguoi_cap_nhat, para_ngay_cap_nhat, para_thoi_gian_nhan, para_thoi_gian_gui, para_NhomEmail,
                    para_dia_chi_email, para_trangthai1, para_thoi_gian_gui1, para_thoi_gian_nhan1, para_codeEncryp1);
                if (string.IsNullOrEmpty(dangkytochuc.sale_id))
                {
                    string strEmail = ConfigurationManager.AppSettings["SendEmail"].ToString();
                    string[] arrListStr = strEmail.Split(',');
                    foreach (var item in arrListStr)
                    {
                        SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sQEM_NOI_DUNG_EMAIL_INSERT_2TABLE11", para_Subject,
                            para_Content, para_danhmucdichvu, para_trangthai, para_nguoi_tao, para_ngay_tao,
                            para_nguoi_cap_nhat, para_ngay_cap_nhat, para_thoi_gian_nhan, para_thoi_gian_gui,
                            para_NhomEmail, item, para_trangthai1, para_thoi_gian_gui1, para_thoi_gian_nhan1,
                            para_codeEncryp1);
                    }

                    Logger.Info(typeof(HomeController),
                        string.Format("DangKyKhTochuc - ") + "Send Email to : " + strEmail);
                }

                this.Session["check_tochuc"] = true;
                return RedirectToAction("DangKyKhTochuc");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), "DangKyKhTochuc", ex);
                this.Session["check_tochuc"] = false;
                ViewBag.Message = "Quý khách đăng ký mở tài khoản không thành công";
                return View(dangkytochuc);
            }

            //}
            //return View(dangkytochuc);
        }


        // dang ky khach hang ca nhan
        [HttpGet]
        public ActionResult DangkyKhCanhan()
        {
            try
            {
                IdTransactionDropCustom();
                string id = Request.QueryString["SaleID"];
                var viewModel = new DangKyKhCaNhanModel
                {
                };
                if (string.IsNullOrEmpty(id))
                {
                    viewModel.sale_id = "";
                    ViewBag.SaleID = id;
                }
                else
                {
                    viewModel.sale_id = id;
                    ViewBag.SaleID = id;
                }

                InitDropdownList();
                if (this.Session["check_canhan"] != null)
                {
                    ViewBag.success = this.Session["check_canhan"];
                    this.Session["check_canhan"] = null;
                }
                else
                {
                    ViewBag.success = false;
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";
                return View(viewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format(" DangkyKhCanhan() ") + ex.Message);
                return null;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DangkyKhCanhan(DangKyKhCaNhanModel dangkycanhan, String kytubaomatcn)
        {
            try
            {
                InitDropdownList(dangkycanhan.BankNo_01, dangkycanhan.BankNo_02);
                IdTransactionDropCustom();
                if (dangkycanhan.noicap.Equals("-1"))
                {
                    if (string.IsNullOrEmpty(dangkycanhan.noicapkhac))
                    {
                        ViewBag.Message = "Vui lòng điền nơi cấp  khác.";
                        return View(dangkycanhan);
                    }
                }

                dangkycanhan.noicap = !string.IsNullOrEmpty(UtilTvsi.CutPlaceIssueCardID(dangkycanhan.noicap))
                    ? UtilTvsi.CutPlaceIssueCardID(dangkycanhan.noicap)
                    : dangkycanhan.noicap;

                var webService = new BankWebService();
                var isValid = true;
                if (!string.IsNullOrEmpty(dangkycanhan.BankAccNo_01) || !string.IsNullOrEmpty(dangkycanhan.BankNo_01) ||
                    !string.IsNullOrEmpty(dangkycanhan.City_01) ||
                    !string.IsNullOrEmpty(dangkycanhan.BankAccNo_02) ||
                    !string.IsNullOrEmpty(dangkycanhan.BankNo_02) ||
                    !string.IsNullOrEmpty(dangkycanhan.City_02))
                {
                    var errorList = ValidateOpenAcc(dangkycanhan);
                    if (errorList.Count > 0)
                    {
                        foreach (string errMsg in errorList)
                        {
                            ModelState.AddModelError("", errMsg);
                        }

                        isValid = false;
                    }
                }

                if (!isValid)
                {
                    InitDropdownList(dangkycanhan.BankNo_01, dangkycanhan.BankNo_02);
                    //TempData["ErrorType"] = 1;
                    return View(dangkycanhan);
                }

                if (dangkycanhan.yeucau == null)
                {
                    dangkycanhan.yeucau = "";
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";

                if (!UtilTvsi.isValidDateTime(dangkycanhan.ngaysinh))
                {
                    ViewBag.Message = Website.Txt_InvalidDateOfBirth;
                    return View(dangkycanhan);
                }


                if (!UtilTvsi.isValidDateTime(dangkycanhan.ngaycapcmnd))
                {
                    ViewBag.Message = Website.Txt_PassportValid;
                    return View(dangkycanhan);
                }

                DateTime ngaysinh =
                    DateTime.ParseExact(dangkycanhan.ngaysinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DateTime ngaycapcmnd =
                    DateTime.ParseExact(dangkycanhan.ngaycapcmnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (ngaycapcmnd.Date > DateTime.Now.Date)
                {
                    ViewBag.Message = "Ngày cấp không hợp lệ";
                    return View(dangkycanhan);
                }

                var validDateOfBirth = UtilTvsi.ValidDateOfBirth(ngaysinh);
                if (!string.IsNullOrEmpty(validDateOfBirth))
                {
                    ViewBag.Message = validDateOfBirth;
                    return View(dangkycanhan);
                }

                var checkDateIssue = UtilTvsi.CheckIssueDateForm(dangkycanhan.cmnd, ngaycapcmnd);
                if (!checkDateIssue)
                {
                    ViewBag.Message = "Ngày cấp hết hiệu lực";
                    return View(dangkycanhan);
                }

                if (!string.IsNullOrEmpty(dangkycanhan.email))
                {
                    if (!UtilTvsi.IsValidEmail(dangkycanhan.email))
                    {
                        ViewBag.Message = "Địa chỉ Email không đúng, Qúy khách vui lòng nhập lại.";
                        return View(dangkycanhan);
                    }
                }

                string crmStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                string commonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                if (UtilTvsi.ExistsCMND(dangkycanhan.cmnd, commonStr))
                {
                    ViewBag.Message = Website.Txt_PassportRegistered;
                    return View(dangkycanhan);
                }

                if (!string.IsNullOrEmpty(dangkycanhan.sale_id))
                {
                    if (!UtilTvsi.CheckSaleIDEMS(dangkycanhan.sale_id))
                    {
                        ViewBag.Message = "Mã nhân viên tư vấn không đúng, Quý khác vui lòng nhập đúng hoặc để trống";
                        return View(dangkycanhan);
                    }
                }

                string saleIDCF = ConfigurationManager.AppSettings["SaleID"].ToString();
                string saleid = String.Empty;
                string id = Request.QueryString["SaleID"];

                if (!string.IsNullOrEmpty(id))
                {
                    if (!UtilTvsi.CheckSaleIDEMS(id))
                    {
                        ViewBag.Message = "Mã nhân viên tư vấn không đúng, Quý khác vui lòng nhập đúng hoặc để trống";
                        return View(dangkycanhan);
                    }

                    dangkycanhan.sale_id = id;
                    ViewBag.SaleID = id;
                }
                else
                {
                    ViewBag.SaleID = id;
                }

                if (!string.IsNullOrEmpty(dangkycanhan.sale_id))
                    saleid = UtilTvsi.GetIDSale(dangkycanhan.sale_id);
                else
                    saleid = !string.IsNullOrEmpty(dangkycanhan.Branch_Transaction)
                        ? dangkycanhan.Branch_Transaction
                        : saleIDCF;


                if (!string.IsNullOrEmpty(dangkycanhan.BankNo_01))
                {
                    dangkycanhan.BankName_01 = webService.GetBankName(dangkycanhan.BankNo_01);
                    dangkycanhan.SubBranchName_01 = webService.GetSubBranchName(dangkycanhan.BankNo_01,
                        dangkycanhan.SubBranchNo_01);
                }

                if (!string.IsNullOrEmpty(dangkycanhan.City_01))
                {
                    dangkycanhan.City_01 =
                        dangkycanhan.City_01 + "|" + webService.GetProvinceName(dangkycanhan.City_01);
                }

                if (!string.IsNullOrEmpty(dangkycanhan.BankNo_02))
                {
                    dangkycanhan.BankName_02 = webService.GetBankName(dangkycanhan.BankNo_02);
                    dangkycanhan.SubBranchName_02 = webService.GetSubBranchName(dangkycanhan.BankNo_02,
                        dangkycanhan.SubBranchNo_02);
                }

                if (!string.IsNullOrEmpty(dangkycanhan.noicap))
                {
                    if (dangkycanhan.noicap.Equals("-1"))
                    {
                        dangkycanhan.noicap = dangkycanhan.noicapkhac;
                    }
                    else
                    {
                        dangkycanhan.noicap =
                            !string.IsNullOrEmpty(UtilTvsi.GetPlaceOfIssue(dangkycanhan.noicap))
                                ? UtilTvsi.GetPlaceOfIssue(dangkycanhan.noicap)
                                : "";
                    }
                }

                if (!string.IsNullOrEmpty(dangkycanhan.City_02))
                {
                    dangkycanhan.City_02 =
                        dangkycanhan.City_02 + "|" + webService.GetProvinceName(dangkycanhan.City_02);
                }

                var transferType = "";

                if (IsRegistBankTransferService(dangkycanhan))
                {
                    transferType += UtilTvsi.RegisterConst.Bank.ToString();
                }

                if (Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    /*ViewBag.captcha = "Lỗi ký tự bảo mật";*/
                    ViewBag.Message = "Lỗi ký tự bảo mật";
                    return View(dangkycanhan);
                }
                else
                {
                    if (Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                    {
                        /*
                        ViewBag.captcha = "Ký tự bảo mật không đúng";
                        */
                        ViewBag.Message = "Ký tự bảo mật không đúng";
                        return View(dangkycanhan);
                    }
                }


                //Lay thong tin dang ky dich vu

                SqlParameter para_hoten = new SqlParameter("@hoten", dangkycanhan.hoten);
                SqlParameter para_ngaysinh = new SqlParameter("@ngaysinh", ngaysinh);
                SqlParameter para_gioitinh = new SqlParameter("@gioitinh", dangkycanhan.gioitinh);
                SqlParameter para_cmnd = new SqlParameter("@cmnd", dangkycanhan.cmnd);
                SqlParameter para_ngaycapcmnd = new SqlParameter("@ngaycapcmnd", ngaycapcmnd);
                SqlParameter para_noicap = new SqlParameter("@noicap", dangkycanhan.noicap);
                SqlParameter para_sdt = new SqlParameter("@sdt", dangkycanhan.Phone_01);
                SqlParameter para_email = new SqlParameter("@email", dangkycanhan.email);
                SqlParameter para_diachi = new SqlParameter("@diachi", dangkycanhan.diachi);
                SqlParameter para_saleid = new SqlParameter("@saleid", saleid);
                SqlParameter para_yeucau = new SqlParameter("@yeucau", dangkycanhan.yeucau);

                SqlParameter para_phuongthucgd = new SqlParameter("@phuong_thuc_gd", dangkycanhan.TradeMethod);
                SqlParameter para_phuong_thuc_nhan_kqgd =
                    new SqlParameter("@phuong_thuc_nhan_kqgd", dangkycanhan.TradeResult);
                SqlParameter para_phuong_thuc_sao_ke =
                    new SqlParameter("@phuong_thuc_sao_ke", dangkycanhan.MonthlyReport);
                SqlParameter para_dv_chuyen_tien = new SqlParameter("@dv_chuyen_tien", transferType);
                SqlParameter para_so_dien_thoai_02 = new SqlParameter("@so_dien_thoai_02", dangkycanhan.Phone_02);

                SqlParameter para_chu_tai_khoan_nh_01 =
                    new SqlParameter("@chu_tai_khoan_nh_01", dangkycanhan.BankAccName_01);
                SqlParameter para_so_tai_khoan_nh_01 =
                    new SqlParameter("@so_tai_khoan_nh_01", dangkycanhan.BankAccNo_01);
                SqlParameter para_ma_ngan_hang_01 = new SqlParameter("@ma_ngan_hang_01", dangkycanhan.BankNo_01);
                SqlParameter para_ngan_hang_01 = new SqlParameter("@ngan_hang_01", dangkycanhan.BankName_01);
                SqlParameter para_chi_nhanh_nh_01 = new SqlParameter("@chi_nhanh_nh_01", dangkycanhan.SubBranchName_01);
                SqlParameter para_ma_chi_nhanh_01 = new SqlParameter("@ma_chi_nhanh_01", dangkycanhan.SubBranchNo_01);
                SqlParameter para_tinh_tp_nh_01 = new SqlParameter("@tinh_tp_nh_01", dangkycanhan.City_01);

                SqlParameter para_chu_tai_khoan_nh_02 =
                    new SqlParameter("@chu_tai_khoan_nh_02", dangkycanhan.BankAccName_02);
                SqlParameter para_so_tai_khoan_nh_02 =
                    new SqlParameter("@so_tai_khoan_nh_02", dangkycanhan.BankAccNo_02);
                SqlParameter para_ngan_hang_02 = new SqlParameter("@ngan_hang_02", dangkycanhan.BankName_02);
                SqlParameter para_ma_ngan_hang_02 = new SqlParameter("@ma_ngan_hang_02", dangkycanhan.BankNo_02);
                SqlParameter para_ma_chi_nhanh_02 = new SqlParameter("@ma_chi_nhanh_02", dangkycanhan.SubBranchNo_02);
                SqlParameter para_chi_nhanh_nh_02 = new SqlParameter("@chi_nhanh_nh_02", dangkycanhan.SubBranchName_02);
                SqlParameter para_tinh_tp_nh_02 = new SqlParameter("@tinh_tp_nh_02", dangkycanhan.City_02);

                SqlParameter para_kenh_trao_doi = new SqlParameter("@kenh_trao_doi", dangkycanhan.RegistChannelInfo);
                SqlParameter para_phuong_thuc_kenh_td =
                    new SqlParameter("@phuong_thuc_kenh_td", dangkycanhan.ChannelInfoMethod);

                SqlParameter para_fatca_kh_usa = new SqlParameter("@fatca_kh_usa", dangkycanhan.CustomerUSA);
                SqlParameter para_fatca_noi_sinh_usa =
                    new SqlParameter("@fatca_noi_sinh_usa", dangkycanhan.PlaceOfBirthUSA);
                SqlParameter para_fatca_noi_cu_tru_usa =
                    new SqlParameter("@fatca_noi_cu_tru_usa", dangkycanhan.DepositoryUSA);
                SqlParameter para_fatca_sdt_usa =
                    new SqlParameter("@fatca_sdt_usa", dangkycanhan.CustomerPhoneNumberUSA);
                SqlParameter para_fatca_lenh_ck_usa =
                    new SqlParameter("@fatca_lenh_ck_usa", dangkycanhan.CustomerTransferBankUSA);
                SqlParameter para_fatca_giay_uy_quyen_usa =
                    new SqlParameter("@fatca_giay_uy_quyen_usa", dangkycanhan.CustomerAuthorizationUSA);
                SqlParameter para_fatca_dia_chi_nhan_thu_usa = new SqlParameter("@fatca_dia_chi_nhan_thu_usa",
                    dangkycanhan.CustomerReceiveMailUSA);    
                SqlParameter para_branchId = new SqlParameter("@branch_id",
                    UtilTvsi.ReturnBrandID(saleid.Substring(0, 4)));  
                SqlParameter para_branchName = new SqlParameter("@branch_name",
                    UtilTvsi.ReturnBrandName(saleid.Substring(0, 4)));
                

                SqlHelper.ExecuteNonQuery(crmStr, "TVSI_sDANG_KY_MO_TAI_KHOAN",
                    para_hoten,
                    para_ngaysinh,
                    para_gioitinh,
                    para_cmnd,
                    para_ngaycapcmnd,
                    para_noicap,
                    para_sdt,
                    para_email,
                    para_diachi,
                    para_saleid,
                    para_yeucau,
                    para_phuongthucgd,
                    para_phuong_thuc_nhan_kqgd,
                    para_phuong_thuc_sao_ke,
                    para_dv_chuyen_tien,
                    para_so_dien_thoai_02,
                    para_chu_tai_khoan_nh_01,
                    para_so_tai_khoan_nh_01,
                    para_ma_ngan_hang_01,
                    para_ngan_hang_01,
                    para_ma_chi_nhanh_01,
                    para_chi_nhanh_nh_01,
                    para_tinh_tp_nh_01,
                    para_chu_tai_khoan_nh_02,
                    para_so_tai_khoan_nh_02,
                    para_ngan_hang_02,
                    para_ma_ngan_hang_02,
                    para_ma_chi_nhanh_02,
                    para_chi_nhanh_nh_02,
                    para_tinh_tp_nh_02,
                    para_kenh_trao_doi,
                    para_phuong_thuc_kenh_td,
                    para_fatca_kh_usa,
                    para_fatca_noi_sinh_usa,
                    para_fatca_noi_cu_tru_usa,
                    para_fatca_sdt_usa,
                    para_fatca_lenh_ck_usa,
                    para_fatca_giay_uy_quyen_usa,
                    para_fatca_dia_chi_nhan_thu_usa,
                    para_branchId,
                    para_branchName
                );
                
                var subject = "TVSI:" + Website.Txt_RegOpenAccount + "";
                var contentMailToCustomer = UtilTvsi.ReturnNoiDungMail(dangkycanhan);
                if (!string.IsNullOrEmpty(dangkycanhan.email))
                {
                    TVSI.DAL.SystemNotificationDB.ActionQuery.TVSI_sHANG_DOI_EMAIL_INSERT(subject, contentMailToCustomer,
                        dangkycanhan.email, 0);
                }

                //Gửi mail cho khách hàng liên quan
                if (string.IsNullOrEmpty(dangkycanhan.sale_id))
                {
                    string emailList = ConfigurationManager.AppSettings["SendEmailForm"];
                    string[] email = emailList.Split(',');
                    var area = "";
                    if (dangkycanhan.Branch_Transaction.Equals("1806-001"))
                        area = "Hà Nội";
                    if (dangkycanhan.Branch_Transaction.Equals("1813-001"))
                        area = "Đà Nẵng"; 
                    if (dangkycanhan.Branch_Transaction.Equals("0054-001"))
                        area = "Hồ Chí Minh";
                    foreach (var item in email)
                    {
                        if (item != null)
                        {
                            var sendMail = UtilTvsi.SendMailToSaleForm("TVSI-CĐGD", dangkycanhan.hoten, dangkycanhan.ngaysinh,
                                dangkycanhan.cmnd
                                , dangkycanhan.ngaycapcmnd, dangkycanhan.Phone_01, dangkycanhan.email, dangkycanhan.diachi,
                                dangkycanhan.yeucau, "Điền form",area, subject,item);
                        }
                    }
                }
                else
                {
                    if (saleid != null && saleid.Length >= 4)
                    {

                        var saleInfo = UtilTvsi.GetEmailSale(saleid.Substring(0, 4));
                        var area = "";
                        if (dangkycanhan.Branch_Transaction.Equals("1806-001"))
                            area = "Hà Nội";
                        if (dangkycanhan.Branch_Transaction.Equals("1813-001"))
                            area = "Đà Nẵng";
                        if (dangkycanhan.Branch_Transaction.Equals("0054-001"))
                            area = "Hồ Chí Minh";
                        if (saleInfo != null)
                        {
                            var sendMail = UtilTvsi.SendMailToSaleForm(saleInfo.SaleName, dangkycanhan.hoten,
                                dangkycanhan.ngaysinh,
                                dangkycanhan.cmnd
                                , dangkycanhan.ngaycapcmnd, dangkycanhan.Phone_01, dangkycanhan.email,
                                dangkycanhan.diachi,
                                dangkycanhan.yeucau, "Điền form", area, subject, saleInfo.Email);
                        }
                    }

                }
                Session["check_canhan"] = true;
                return RedirectToAction("DangKyKhCaNhan");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("Mo tai khoan form errors -  {0}" , ex.Message));
                Session["check_canhan"] = false;
                ViewBag.Message = "Đăng ký tài khoản không thành công";
                return View(dangkycanhan);
            }

            //}
            //return View(dangkycanhan);
        }

        [HttpGet]
        public ActionResult DangkyKhCanhanNuocNgoai()
        {
            if (Request.Cookies[CommonConstant.name_lang_tvsi_wb] == null)
            {
                changeLanguage("en-US");
            }

            try
            {
                ExportDataCountryCode();
                IdTransactionDrop();
                ExportDataVSDVN();
                var dangkykhcn = new DangKyKhCaNhanModel();
                if (dangkykhcn.dangkyid == 0 || dangkykhcn.dangkyid != null)
                {
                    Session.Remove("successAccount");
                    Session.Remove("emailNN");
                }

                string id = Request.QueryString["SaleID"];
                var viewModel = new DangKyKhCaNhanModel
                {
                    TradeResultEmail = true,
                    TradeMethodPhone = true,
                    TradeMethodInternet = true,
                    TradeResultSMS = true,
                    RegistChannelInfo = true,
                    BankTransfer = true,
                    RegistChannelDirect = true,
                    RegistItradeHome = true,
                    MonthlyReportEMail = true,
                    TradetoBank = true
                };
                if (id == "")
                {
                    viewModel.sale_id = "";
                    ViewBag.SaleID = id;
                }
                else
                {
                    viewModel.sale_id = id;
                    ViewBag.SaleID = id;
                }

                InitDropdownList();
                if (this.Session["check_canhannuocngoai"] != null)
                {
                    ViewBag.success = this.Session["check_canhannuocngoai"];
                    this.Session["check_canhannuocngoai"] = null;
                }
                else
                {
                    ViewBag.success = false;
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";
                return View(viewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format(" DangkyKhCanhannuocngoai() ") + ex.Message);
                return null;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DangkyKhCanhanNuocNgoai(DangKyKhCaNhanModel dangkycanhannn, string kytubaomatcn)
        {
            var checknoicap = false;
            var listnoicap = new List<string>();
            listnoicap.Add("trật tự");
            listnoicap.Add("xã hội");
            ;
            listnoicap.Add("trật tự xã hội");
            listnoicap.Add("cục trưởng cục cảnh sát quản lý hành chính về trật tự xã hội");

            var listnoicap1 = new List<string>();
            listnoicap1.Add("ĐKQL");
            listnoicap1.Add("DLQG");
            listnoicap1.Add("về dân cư");
            listnoicap1.Add("cục trưởng cục cảnh sát đkql cư trú và dlqg về dân cư");
            try
            {
                ExportDataVSDVN();
                ExportDataCountryCode();
                IdTransactionDrop();
                if (dangkycanhannn.noicap != null)
                {
                    for (int i = 0; i < listnoicap.Count; i++)
                    {
                        if (dangkycanhannn.noicap.ToLower().Contains(listnoicap[i]))
                        {
                            dangkycanhannn.noicap = "Cục CSQLHC về TTXH";
                            checknoicap = true;
                            break;
                        }
                    }

                    if (checknoicap == false)
                    {
                        for (int i = 0; i < listnoicap1.Count; i++)
                        {
                            if (dangkycanhannn.noicap.ToLower().Contains(listnoicap1[i]))
                            {
                                dangkycanhannn.noicap = "Cục CSĐKQLCT và DLQG về dân cư";
                                break;
                            }
                        }
                    }
                }

                var listIsValid = new List<string>();
                InitDropdownList(dangkycanhannn.BankNo_01, dangkycanhannn.BankNo_02);
                var webService = new BankWebService();
                var isValid = true;
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    /*ViewBag.Message = Website.Txt_CaptchaNull;
                    ViewBag.captcha = Website.Txt_CaptchaNull;*/
                    listIsValid.Add(Website.Txt_CaptchaNull);

                    /*
                    return View(dangkycanhannn);
                */
                }
                else
                {
                    if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                    {
                        /*
                                                ViewBag.Message = Website.Txt_CaptchaRegister;
                                                ViewBag.captcha = Website.Txt_CaptchaRegister;
                                                return View(dangkycanhannn);*/
                        listIsValid.Add(Website.Txt_CaptchaRegister);
                    }
                }

                if (dangkycanhannn.BankTransfer)
                {
                    if (dangkycanhannn.BankMethod == "1")
                    {
                        var errorList = ValidateOpenAccForeign(dangkycanhannn);
                        if (errorList.Count > 0)
                        {
                            foreach (string errMsg in errorList)
                            {
                                ModelState.AddModelError("", errMsg);
                            }

                            isValid = false;
                        }

                        if (!isValid)
                        {
                            InitDropdownList(dangkycanhannn.BankNo_01, dangkycanhannn.BankNo_02);
                            return View(dangkycanhannn);
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(dangkycanhannn.accNoBIDV))
                        {
                            /*
                            ViewBag.Message = Website.Txt_SecuritiesNumber;
                            */
                            listIsValid.Add(Website.Txt_SecuritiesNumber);
                            /*return View(dangkycanhannn);*/
                        }

                        if (!string.IsNullOrEmpty(dangkycanhannn.accNoBIDV) && dangkycanhannn.accNoBIDV != "No_Number")
                        {
                            var isAccNoBIDV = UtilTvsi.IsNumeric(dangkycanhannn.accNoBIDV.Trim());
                            if (!isAccNoBIDV)
                            {
                                listIsValid.Add(Website.Txt_NumberOfSecurities);
                            }

                            if (dangkycanhannn.accNoBIDV.Trim().Length > 14 ||
                                dangkycanhannn.accNoBIDV.Trim().Length < 14)
                            {
/**/ /*ViewBag.Message = Website.Txt_NumberLengBIDV;
                                return View(dangkycanhannn);*/
                                listIsValid.Add(Website.Txt_NumberLengBIDV);
                            }
                        }
                    }
                }

                if (dangkycanhannn.yeucau == null)
                {
                    dangkycanhannn.yeucau = "";
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";

                var listErrors = new List<string>();
                var isErrors = true;
                if (!string.IsNullOrEmpty(dangkycanhannn.Phone_01))
                {
                    var isPhoneNumber = UtilTvsi.IsNumeric(dangkycanhannn.Phone_01.Trim());
                    if (!isPhoneNumber)
                    {
                        listErrors.Add(Website.Txt_PhoneIsNum);
                    }
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.TradingPhone_01))
                {
                    var isTradingPhone_01 = UtilTvsi.IsNumeric(dangkycanhannn.TradingPhone_01.Trim());
                    if (!isTradingPhone_01)
                    {
                        listErrors.Add(Website.Txt_TradingPhone_01);
                    }
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.TradingPhone_02))
                {
                    var isTradingPhone_02 = UtilTvsi.IsNumeric(dangkycanhannn.TradingPhone_02.Trim());
                    if (!isTradingPhone_02)
                    {
                        listErrors.Add(Website.Txt_TradingPhone_01);
                    }
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.PhoneResultTrading))
                {
                    var isPhoneResultTrading = UtilTvsi.IsNumeric(dangkycanhannn.PhoneResultTrading.Trim());
                    if (!isPhoneResultTrading)
                    {
                        listErrors.Add(Website.Txt_PhoneResultTrading);
                    }
                }

                if (listErrors.Count > 0)
                {
                    foreach (var errors in listErrors)
                    {
                        ModelState.AddModelError("", errors);
                    }

                    isErrors = false;
                }

                var isDateValid = false;

                if (!UtilTvsi.isValidDateTime(dangkycanhannn.ngaysinh))
                {
                    /*ViewBag.Message = Website.Txt_InvalidDateOfBirth;
                    return View(dangkycanhannn);*/
                    listIsValid.Add(Website.Txt_InvalidDateOfBirth);
                    isDateValid = true;
                }

                if (!UtilTvsi.isValidDateTime(dangkycanhannn.ngaycapcmnd))
                {
                    listIsValid.Add(Website.Txt_PassportValid);
                    isDateValid = true;
                    /*ViewBag.Message = Website.Txt_PassportValid;
                    return View(dangkycanhannn);*/
                }

                if (!UtilTvsi.isValidDateTime(dangkycanhannn.ngayhethancmnd))
                {
                    listIsValid.Add(Website.Txt_ExpiresInvalidIDCard);
                    isDateValid = true;
                    /*ViewBag.Message = Website.Txt_ExpiresInvalidIDCard;
                    return View(dangkycanhannn);*/
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.tradingcode))
                {
                    if (dangkycanhannn.tradingcode.Length < 6)
                    {
                        /*ViewBag.Message = Website.Txt_TradingCodeLength_v1;
                        return View(dangkycanhannn);*/
                        listIsValid.Add(Website.Txt_TradingCodeLength_v1);
                    }

                    if (string.IsNullOrEmpty(dangkycanhannn.ngaycapmagiaodich))
                    {
                        /*ViewBag.Message = Website.Txt_TradingcodeIsValue;
                        return View(dangkycanhannn);*/
                        listIsValid.Add(Website.Txt_TradingcodeIsValue);
                    }

                    if (string.IsNullOrEmpty(dangkycanhannn.giamhovietnam))
                    {
                        /*ViewBag.Message = Website.Txt_VNCustodian;
                        return View(dangkycanhannn);*/
                        listIsValid.Add(Website.Txt_VNCustodian);
                    }

                    if (!string.IsNullOrEmpty(dangkycanhannn.ngaycapmagiaodich))
                    {
                        if (!UtilTvsi.isValidDateTime(dangkycanhannn.ngaycapmagiaodich))
                        {
                            /*ViewBag.Message = Website.Txt_TradeCode;
                            return View(dangkycanhannn);*/
                            listIsValid.Add(Website.Txt_TradeCode);
                        }
                    }

                    if (dangkycanhannn.ngaycapmagiaodich != null)
                    {
                        if (UtilTvsi.isValidDateTime(dangkycanhannn.ngaycapmagiaodich))
                        {
                            DateTime ngaycaptradingcode = DateTime.ParseExact(dangkycanhannn.ngaycapmagiaodich,
                                "dd/MM/yyyy",
                                CultureInfo.InvariantCulture);
                            if (!UtilTvsi.checkDateTradingCode(ngaycaptradingcode))
                            {
                                /*ViewBag.Message = Website.Txt_TradingCodeInvalid;
                                return View(dangkycanhannn);*/
                                listIsValid.Add(Website.Txt_TradingCodeInvalid);
                                isDateValid = true;
                            }
                        }
                    }
                }

                if (isDateValid)
                {
                    foreach (var listErr in listIsValid)
                    {
                        ModelState.AddModelError("", listErr);
                    }

                    return View(dangkycanhannn);
                }


                /*DateTime dDate;
                var isDateValid = false;
                if (!DateTime.TryParse(dangkycanhannn.ngaysinh,out dDate))
                {
                    listIsValid.Add(Website.Txt_CheckYearOld);
                    isDateValid = true;
                } 
                if (!DateTime.TryParse(dangkycanhannn.ngaycapcmnd, out dDate))
                {
                    listIsValid.Add(Website.Txt_DateID_1);
                    isDateValid = true;
                }
                if (!DateTime.TryParse(dangkycanhannn.ngayhethancmnd, out dDate))
                {
                    listIsValid.Add(Website.Txt_CheckExpiredPassport);
                    isDateValid = true;
                }

                if (isDateValid)
                {
                    foreach (var listErr in listIsValid)
                    {
                        ModelState.AddModelError("",listErr);
                    }
                    return View(dangkycanhannn);
                }*/

                var isValidDatePassport = false;
                DateTime ngaysinh =
                    DateTime.ParseExact(dangkycanhannn.ngaysinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime ngaycapcmnd = DateTime.ParseExact(dangkycanhannn.ngaycapcmnd, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture);
                DateTime ngayhethancmnd = DateTime.ParseExact(dangkycanhannn.ngayhethancmnd, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture);
                var datetimenow = DateTime.Now.ToString("dd/MM/yyyy");
                DateTime pdatetimenow = DateTime.ParseExact(datetimenow, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (ngayhethancmnd <= ngaycapcmnd)
                {
                    /*ViewBag.Message = Website.Txt_DateID;
                    return View(dangkycanhannn);*/
                    isValidDatePassport = true;
                    listIsValid.Add(Website.Txt_DateID);
                }

                if (ngaycapcmnd > pdatetimenow)
                {
                    /*ViewBag.Message = Website.Txt_DateID_1;
                    return View(dangkycanhannn);*/
                    listIsValid.Add(Website.Txt_DateID_1);
                }

                if (ngayhethancmnd <= pdatetimenow)
                {
                    /*ViewBag.Message = Website.Txt_PassportExpiration;
                    return View(dangkycanhannn);*/
                    if (!isValidDatePassport)
                    {
                        listIsValid.Add(Website.Txt_PassportExpiration);
                        isValidDatePassport = true;
                    }
                }

                if (!UtilTvsi.checkDateYear(ngaysinh))
                {
                    /*ViewBag.Message = Website.Txt_CheckYearOld;
                    return View(dangkycanhannn);*/
                    listIsValid.Add(Website.Txt_CheckYearOld);
                }

                if (!UtilTvsi.checkDatePassport(ngaycapcmnd))
                {
                    /*ViewBag.Message = Website.Txt_CheckYearPassport;
                    return View(dangkycanhannn);*/
                    listIsValid.Add(Website.Txt_CheckYearPassport);
                }

                if (!UtilTvsi.checkExpiredDatePassport(ngayhethancmnd))
                {
                    if (!isValidDatePassport)
                    {
                        listIsValid.Add(Website.Txt_CheckExpiredPassport);
                    }

                    /*ViewBag.Message = Website.Txt_CheckExpiredPassport;
                    return View(dangkycanhannn);*/
                }


                string CRMStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
                string CommonStr = ConfigurationManager.ConnectionStrings["CommonDB"].ToString();
                string EMSStr = ConfigurationManager.ConnectionStrings["EMS"].ToString();
                if (UtilTvsi.ExistsCMND(dangkycanhannn.cmnd.ToString(), CommonStr) == true)
                {
                    /*ViewBag.Message = Website.Txt_RegisterPassportTvsi;
                    return View(dangkycanhannn);*/
                    listIsValid.Add(Website.Txt_RegisterPassportTvsi);
                }

                var getSaleID = UtilTvsi.GetNumberSaleID(dangkycanhannn.sale_id.Substring(0, 4), EMSStr);

                if (!string.IsNullOrEmpty(dangkycanhannn.sale_id))
                {
                    var isNumber = UtilTvsi.IsNumeric(dangkycanhannn.sale_id.Replace("-", ""));
                    if (isNumber)
                    {
                        var sale_id = dangkycanhannn.sale_id.Substring(0, 4);
                        if (UtilTvsi.ExistsSaleIDEMS(sale_id, EMSStr) == false)
                        {
                            /*ViewBag.Message = Website.Txt_SaleCode;
                            return View(dangkycanhannn);*/
                            listIsValid.Add(Website.Txt_SaleCode);
                        }
                    }
                    else
                    {
                        if (UtilTvsi.ExistsSaleIDEMS(dangkycanhannn.sale_id.ToString(), EMSStr) == false)
                        {
                            /*ViewBag.Message = Website.Txt_SaleCode;
                            return View(dangkycanhannn);*/
                            listIsValid.Add(Website.Txt_SaleCode);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.TradingPhone_01) &&
                    !string.IsNullOrEmpty(dangkycanhannn.TradingPhone_02))
                {
                    if (dangkycanhannn.TradingPhone_01.Trim().Equals(dangkycanhannn.TradingPhone_02.Trim()))
                    {
                        /*ViewBag.Message = Website.Txt_TradingViaPhone;
                        return View(dangkycanhannn);*/
                        listIsValid.Add(Website.Txt_TradingViaPhone);
                    }
                }

                if (listIsValid.Count > 0)
                {
                    foreach (var listvalid in listIsValid)
                    {
                        ModelState.AddModelError("", listvalid);
                    }

                    return View(dangkycanhannn);
                }

                var manName = string.Empty;
                var postionName = string.Empty;
                var manNo = string.Empty;
                var manDate = string.Empty;

                ManInfo infoMan = null;
                List<ManInfo> maninfo = SqlManInfoData.GetManInfos();
                foreach (var item in maninfo)
                {
                    if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                        item.ma_chi_nhanh.Contains(UtilTvsi.ReturnBrandID(dangkycanhannn.sale_id.Substring(0, 4))))
                    {
                        infoMan = item;
                        break;
                    }
                }

                if (infoMan == null)
                {
                    infoMan = maninfo.FirstOrDefault(x => x.ma_chi_nhanh == "All");
                }

                if (infoMan != null)
                {
                    var arrPara = infoMan.gia_tri.Split('|');
                    manName = arrPara[0].ToString();
                    postionName = arrPara[1].ToString();
                    manNo = arrPara[2].ToString();
                    manDate = arrPara[3].ToString();
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.BankNo_01))
                {
                    dangkycanhannn.BankName_01 = webService.GetBankName(dangkycanhannn.BankNo_01);
                    dangkycanhannn.SubBranchName_01 = webService.GetSubBranchName(dangkycanhannn.BankNo_01,
                        dangkycanhannn.SubBranchNo_01);
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.City_01))
                {
                    dangkycanhannn.City_01 =
                        dangkycanhannn.City_01 + "|" + webService.GetProvinceName(dangkycanhannn.City_01);
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.BankNo_02))
                {
                    dangkycanhannn.BankName_02 = webService.GetBankName(dangkycanhannn.BankNo_02);
                    dangkycanhannn.SubBranchName_02 = webService.GetSubBranchName(dangkycanhannn.BankNo_02,
                        dangkycanhannn.SubBranchNo_02);
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.City_02))
                {
                    dangkycanhannn.City_02 =
                        dangkycanhannn.City_02 + "|" + webService.GetProvinceName(dangkycanhannn.City_02);
                }

                var transferType = "";

                if (IsRegistBankTransferServiceNN(dangkycanhannn))
                {
                    transferType += UtilTvsi.RegisterConst.Bank.ToString();
                }


                // Insert vào quee email
                var para_Subject = "TVSI:" + App_Resources.Website.Txt_RegOpenAccount + "";
                var para_Content = UtilTvsi.ReturnNoiDungMail(dangkycanhannn);
                var para_dia_chi_email = dangkycanhannn.email;

                /*Render ma code*/
                var confirmCode = UtilTvsi.EncriptMd5(dangkycanhannn.email + UtilTvsi.RandomNumber(6));
                if (confirmCode.Length > 50)
                    confirmCode = confirmCode.Substring(0, 50);

                var confirmSms = UtilTvsi.RandomNumber(6);

                try
                {
                    using (SqlConnection sqlConnection = new SqlConnection(CRMStr))
                    using (SqlCommand comm = new SqlCommand("TVSI_sDANG_KY_MO_TAI_KHOAN_NUOC_NGOAI", sqlConnection))
                    {
                        comm.Parameters.Add("@hoten", (object) dangkycanhannn.hoten ?? DBNull.Value);
                        comm.Parameters.Add("@ngaysinh", (object) ngaysinh ?? DBNull.Value);
                        comm.Parameters.Add("@gioitinh", (object) dangkycanhannn.gioitinh ?? DBNull.Value);
                        comm.Parameters.Add("@cmnd", (object) dangkycanhannn.cmnd ?? DBNull.Value);
                        comm.Parameters.Add("@ngaycapcmnd", (object) ngaycapcmnd ?? DBNull.Value);
                        comm.Parameters.Add("@noicap", (object) dangkycanhannn.noicap ?? DBNull.Value);
                        comm.Parameters.Add("@sdt", (object) dangkycanhannn.Phone_01 ?? DBNull.Value);
                        comm.Parameters.Add("@email", (object) dangkycanhannn.email ?? DBNull.Value);
                        comm.Parameters.Add("@diachi", (object) dangkycanhannn.diachi ?? DBNull.Value);
                        comm.Parameters.Add("@saleid", (object) getSaleID ?? DBNull.Value);
                        comm.Parameters.Add("@branch_id",
                            (object) UtilTvsi.ReturnBrandID(getSaleID.Substring(0, 4)) ?? DBNull.Value);
                        comm.Parameters.Add("@branch_name",
                            (object) UtilTvsi.ReturnBrandName(getSaleID.Substring(0, 4)) ?? DBNull.Value);
                        comm.Parameters.Add("@tvsi_nguoi_dai_dien", (object) manName ?? DBNull.Value);
                        comm.Parameters.Add("@tvsi_chuc_vu", (object) postionName ?? DBNull.Value);
                        comm.Parameters.Add("@tvsi_giay_uq", (object) manNo ?? DBNull.Value);
                        comm.Parameters.Add("@tvsi_ngay_uq", (object) manDate ?? DBNull.Value);
                        comm.Parameters.Add("@yeucau", (object) dangkycanhannn.yeucau ?? DBNull.Value);
                        comm.Parameters.Add("@phuong_thuc_gd", (object) dangkycanhannn.TradeMethod ?? DBNull.Value);
                        comm.Parameters.Add("@phuong_thuc_nhan_kqgd",
                            (object) dangkycanhannn.TradeResult ?? DBNull.Value);
                        comm.Parameters.Add("@phuong_thuc_sao_ke",
                            (object) dangkycanhannn.MonthlyReport ?? DBNull.Value);
                        comm.Parameters.Add("@dv_chuyen_tien", (object) transferType ?? DBNull.Value);
                        comm.Parameters.Add("@so_dien_thoai_02", (object) dangkycanhannn.Phone_02 ?? DBNull.Value);
                        comm.Parameters.Add("@chu_tai_khoan_nh_01",
                            (object) dangkycanhannn.BankAccName_01 ?? DBNull.Value);
                        comm.Parameters.Add("@so_tai_khoan_nh_01",
                            (object) dangkycanhannn.BankAccNo_01 ?? DBNull.Value);
                        comm.Parameters.Add("@ma_ngan_hang_01", (object) dangkycanhannn.BankNo_01 ?? DBNull.Value);
                        comm.Parameters.Add("@ngan_hang_01", (object) dangkycanhannn.BankName_01 ?? DBNull.Value);
                        comm.Parameters.Add("@chi_nhanh_nh_01",
                            (object) dangkycanhannn.SubBranchName_01 ?? DBNull.Value);
                        comm.Parameters.Add("@ma_chi_nhanh_01", (object) dangkycanhannn.SubBranchNo_01 ?? DBNull.Value);
                        comm.Parameters.Add("@tinh_tp_nh_01", (object) dangkycanhannn.City_01 ?? DBNull.Value);
                        comm.Parameters.Add("@chu_tai_khoan_nh_02",
                            (object) dangkycanhannn.BankAccName_02 ?? DBNull.Value);
                        comm.Parameters.Add("@so_tai_khoan_nh_02",
                            (object) dangkycanhannn.BankAccNo_02 ?? DBNull.Value);
                        comm.Parameters.Add("@ma_ngan_hang_02", (object) dangkycanhannn.BankNo_02 ?? DBNull.Value);
                        comm.Parameters.Add("@ngan_hang_02", (object) dangkycanhannn.BankName_02 ?? DBNull.Value);
                        comm.Parameters.Add("@ma_chi_nhanh_02", (object) dangkycanhannn.SubBranchNo_02 ?? DBNull.Value);
                        comm.Parameters.Add("@chi_nhanh_nh_02",
                            (object) dangkycanhannn.SubBranchName_02 ?? DBNull.Value);
                        comm.Parameters.Add("@tinh_tp_nh_02", (object) dangkycanhannn.City_02 ?? DBNull.Value);
                        comm.Parameters.Add("@kenh_trao_doi",
                            (object) dangkycanhannn.RegistChannelInfo ?? DBNull.Value);
                        comm.Parameters.Add("@phuong_thuc_kenh_td",
                            (object) dangkycanhannn.ChannelInfoMethod ?? DBNull.Value);
                        comm.Parameters.Add("@confirmCode", confirmCode);
                        comm.Parameters.Add("@confirmSms", confirmSms);
                        comm.Parameters.Add("@dangkyid", SqlDbType.Int).Direction = ParameterDirection.Output;
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Connection.Open();
                        comm.ExecuteNonQuery();
                        dangkycanhannn.dangkyid = Convert.ToInt32(comm.Parameters["@dangkyid"].Value);
                        comm.Connection.Close();
                        comm.Connection.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(typeof(HomeController),
                        string.Format("DangkyKhCanhan - save - Dangkykhcanhannuocngoai") + ex.Message);
                    this.Session["check_canhannuocngoai"] = false;
                    ViewBag.Message = Website.Txt_RegisterFail;
                    return View(dangkycanhannn);
                }

                try
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dangkycanhannn.dangkyid)))
                    {
                        var dangkytkmorong = new DangKyKhNuocNgoaiMR()
                        {
                            noisinh = dangkycanhannn.noisinh,
                            ngayhethancmnd = dangkycanhannn.ngayhethancmnd,
                            ngaycapmagiaodich = dangkycanhannn.ngaycapmagiaodich,
                            giamhovietnam = dangkycanhannn.giamhovietnam,
                            stateemail = dangkycanhannn.stateemail,
                            quoctich = dangkycanhannn.quoctich,
                            diachicutru = dangkycanhannn.diachicutru,
                            nghenghiep = dangkycanhannn.nghenghiep,
                            noilamviec = dangkycanhannn.noilamviec,
                            trusochinh = dangkycanhannn.trusochinh,
                            tradingcode = dangkycanhannn.tradingcode,
                            accNoBIDV = dangkycanhannn.accNoBIDV,
                            TradingPhone_01 = dangkycanhannn.TradingPhone_01,
                            TradingPhone_02 = dangkycanhannn.TradingPhone_02,
                            PhoneResultTrading = dangkycanhannn.PhoneResultTrading
                        };
                        var jsonData = JsonConvert.SerializeObject(dangkytkmorong, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});

                        using (SqlConnection sqlConnection = new SqlConnection(CRMStr))
                        using (SqlCommand comm = new SqlCommand("TVSI_sDANG_KY_MO_TAI_KHOAN_NUOC_NGOAI_MO_RONG",
                            sqlConnection))
                        {
                            comm.Parameters.Add("@ho_so_mo_tk_id", dangkycanhannn.dangkyid);
                            comm.Parameters.Add("@thong_tin_mo_rong", jsonData);
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Connection.Open();
                            comm.ExecuteNonQuery();
                            comm.Connection.Close();
                            comm.Connection.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(typeof(HomeController),
                        string.Format("Homecontroller - dangkytknnmorong- " + ex.Message));
                    this.Session["check_canhannuocngoai"] = false;
                    ViewBag.Message = Website.Txt_RegisterFail;
                    return View(dangkycanhannn);
                }

                if (!string.IsNullOrEmpty(dangkycanhannn.email))
                {
                    //Send email thông báo cho khach hang
                    UtilTvsi.SendEmailOpenInternational(dangkycanhannn.hoten, dangkycanhannn.gioitinh,
                        dangkycanhannn.Phone_01, dangkycanhannn.email, confirmCode);
                    Logger.Info(typeof(HomeController),
                        string.Format("DangKyCaNhanNuocNgoai - ") + "Send Email to Customer : " + dangkycanhannn.email);
                }

                // gui mail cho sale
                if (!string.IsNullOrEmpty(dangkycanhannn.sale_id))
                {
                    var emailSubstr = dangkycanhannn.sale_id.Replace("-", "").Substring(0, 4);
                    var saleinfo = UtilTvsi.GetMailSaleById(emailSubstr);
                    if (!string.IsNullOrEmpty(saleinfo.email))
                    {
                        UtilTvsi.SendEmailForSale(dangkycanhannn, saleinfo);
                        Logger.Info(typeof(HomeController),
                            string.Format("DangKyCaNhanNuocNgoai - ") + "Send Email to Sale : " + saleinfo.email);
                    }
                    else
                    {
                        ViewBag.Message = Website.Txt_CheckSaleEmail;
                        return View(dangkycanhannn);
                    }
                }

                //Gửi mail cho khách hàng liên quan
                if (!string.IsNullOrEmpty(Convert.ToString(dangkycanhannn.dangkyid)))
                {
                    this.Session["check_canhannuocngoai"] = true;
                }

                return RedirectToAction("dangkykhcanhannuocngoai");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("DangkyKhCanhannuocngoai - ") + ex.Message);
                Session["check_canhannuocngoai"] = false;
                ViewBag.Message = Website.Txt_RegisterFail;
                return View(dangkycanhannn);
            }
        }

        // khuyen cao
        public ActionResult Khuyencao()
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]), id = 1186;
            if (ngonngu_id == 2)
            {
                id = 1187;
            }

            var thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(id);
            ViewBag.thong_tin_selected = thong_tin_selected;
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown";
            return View();
        }

        // sơ đồ site
        public ActionResult SiteMap()
        {
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown";

            return View();
        }

        // menu header 
        public PartialViewResult MenuHeader(string spdv, string hotro, string gttvsi, string dkdv)
        {
            ViewBag.ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            ViewBag.spdv_menu_header = spdv;
            ViewBag.hotro_menu_header = hotro;
            ViewBag.gttvsi_menu_header = gttvsi;
            return PartialView();
        }

        // menu footer
        public PartialViewResult MenuFooter()
        {
            return PartialView();
        }


        [HttpPost]
        public JsonResult ProcessRequest()
        {
            SqlConnection SqlConn = new SqlConnection(ConfigurationManager
                .ConnectionStrings["SelectQuery_WebsiteFinance"].ConnectionString.ToString());
            StringBuilder tbl = new StringBuilder();
            SqlCommand comm = null;
            SqlDataReader drd;
            comm = new SqlCommand("TVSI_sAutoComplate_ForHomeNews_v3", SqlConn);
            //comm.Parameters.AddWithValue("@ngonngu_id", Lib.Object2Integer(LanguageManager.CurrentLanguage));
            comm.CommandType = CommandType.StoredProcedure;
            comm.Connection.Open();
            drd = comm.ExecuteReader();
            List<CongtyChungkhoanModel> dscongty = new List<CongtyChungkhoanModel>();
            while (drd.Read())
            {
                dscongty.Add(new CongtyChungkhoanModel()
                {
                    machungkhoan = drd["ma_chung_khoan"].ToString(), tendoanhnghiep = drd["ten_doanh_nghiep"].ToString()
                });
            }

            drd.Close();
            comm.Connection.Close();
            comm.Connection.Dispose();
            return Json(new
            {
                data = dscongty,
                status = true
            });
        }

        [HttpPost]
        public string getIDCMND(string idCMND)
        {
            SqlConnection SqlConn = new SqlConnection(ConfigurationManager
                .ConnectionStrings["CRM"].ConnectionString.ToString());
            StringBuilder tbl = new StringBuilder();
            SqlCommand comm = null;
            SqlDataReader drd;
            comm = new SqlCommand("TVSI_sLAY_Id_Ekyc", SqlConn);
            comm.Parameters.Add("@Id_cmnd", idCMND);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Connection.Open();
            drd = comm.ExecuteReader();
            string dataId = string.Empty;
            var dangkymotkekyc = new DangKyKhCaNhanModel();
            if (drd.Read())
            {
                dangkymotkekyc.cmnd = drd["dang_kyid"].ToString();
            }

            drd.Close();
            comm.Connection.Close();
            comm.Connection.Dispose();
            return dangkymotkekyc.cmnd;
        }

        //Get ma chung khoan
        public JsonResult GetMaChungkhoan(string q)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            List<DoanhnghiepModel> data = new List<DoanhnghiepModel>();
            DataTable listMack;
            if (ngonngu_id == 1)
            {
                listMack = TVSI_THONG_TIN_TVSI_V3.LayMachungkhoan(q);
            }
            else
            {
                listMack = TVSI_THONG_TIN_TVSI_V3.LayMachungkhoanEn(q);
            }


            for (int i = 0; i < listMack.Rows.Count; i++)
            {
                data.Add(new DoanhnghiepModel()
                {
                    machungkhoan = listMack.Rows[i]["ma_chung_khoan"].ToString().Trim(),
                    tendoanhnghiep = listMack.Rows[i]["ten_doanh_nghiep"].ToString().Trim()
                });
            }

            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        public void changeLanguage(string lang_tvsi_wb = "en-GB")
        {
            if (lang_tvsi_wb != null)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang_tvsi_wb);
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang_tvsi_wb);
            }

            HttpCookie cookie = new HttpCookie(CommonConstant.name_lang_tvsi_wb);
            cookie.Value = lang_tvsi_wb;
            Response.Cookies.Add(cookie);
            Response.Cookies[CommonConstant.name_lang_tvsi_wb].Expires = DateTime.Now.AddDays(1);
            //return Redirect("Index");
        }

        public PartialViewResult LinkSlider()
        {
            return PartialView();
        }

        public PartialViewResult slideHome()
        {
            return PartialView();
        }

        public PartialViewResult slideHomeEn()
        {
            return PartialView();
        }

        public ActionResult testCheckNoidungThongbao()
        {
            return View();
        }

        public ActionResult khaosatykienkhachhang()
        {
            ViewBag.title = "Khảo sát ý kiến khách hàng - Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)";
            return View();
        }


        //protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        //{
        //    var hostname = requestContext.HttpContext.Request.Url.Host;

        //    // do something based on 'hostname' value
        //    // ....

        //    base.Initialize(requestContext);
        //}

        //[HttpGet]
        public JsonResult CheckLogin(string u, string p)
        {
            string strReferrer = string.Empty;
            string NotcheckloginInno = string.Empty;
            string strPricedRef = string.Empty;
            NotcheckloginInno = ConfigurationManager.AppSettings["NotCheckLoginInno"];
            //strPricedRef = ConfigurationManager.AppSettings["PricedUrlRef"];

            //Lấy thông tin hosting
            //strReferrer = Request.UrlReferrer.Host.ToString();
            //strReferrer = Request.UrlReferrer.Host;
            //strReferrer = "priced.tvsi.com.vn";
            //Logger.Info(typeof(HomeController), string.Format("CheckLogin u:") + u + " pw:" + p);
            var referNaviSoft = UtilTvsi.TVSI_DecodeBase64(p);
            string pass = referNaviSoft;
            if (!string.IsNullOrEmpty(referNaviSoft))
            {
                if (referNaviSoft.Length > 11)
                {
                    pass = referNaviSoft.Substring(11, referNaviSoft.Length - 11);
                }

                if (pass.Length > 13)
                {
                    pass = pass.Substring(0, pass.Length - 13);
                }
            }
            //Logger.Info(typeof(HomeController), string.Format("pass - ") + pass);

            if (NotcheckloginInno == "false")
            {
                // 1. Mã hóa password = MD5 ()
                // Chọc vào bảng Customer trong DB Inno có cột nếu tồn tại trả về true, ko tồn tại trả về false

                string strUser = u.Substring(0, 6);
                string strPass = Utilities.MD5EncodePassword(pass);
                string innoConnectionString = ConfigurationManager.ConnectionStrings["InnoDB_Connection"].ToString();
                var checkSuccess = string.Empty;
                if (!string.IsNullOrEmpty(strPass))
                {
                    using (SqlConnection sqlConnection = new SqlConnection(innoConnectionString))
                    using (SqlCommand comm =
                        new SqlCommand(
                            "SELECT CUSTOMERID,PASSWORD FROM CUSTOMER WHERE CUSTOMERID =@CustomerId AND PASSWORD = @Password",
                            sqlConnection))
                    {
                        SqlDataReader drd;
                        comm.Parameters.Add(new SqlParameter("@CustomerId", strUser));
                        comm.Parameters.Add(new SqlParameter("@Password", strPass));
                        comm.CommandType = CommandType.Text;
                        comm.Connection.Open();
                        //comm.ExecuteNonQuery();
                        drd = comm.ExecuteReader();
                        while (drd.Read())
                        {
                            checkSuccess = drd["PASSWORD"].ToString();
                        }

                        comm.Connection.Close();
                        comm.Connection.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(checkSuccess))
                {
                    return Json(new
                    {
                        status = true,
                        message = strReferrer
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        message = strReferrer
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new
                {
                    //data = data,
                    status = false,
                    message = "Kiểm tra lại user hoặc mật khẩu."
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public string ReturnStringJson(string u, string p)
        {
            string responseFromServer = string.Empty;
            try
            {
                // Response.Write("<script language=\"javascript\">alert('json 1');</script>");
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //WebRequest request = WebRequest.Create("" + System.Configuration.ConfigurationManager.AppSettings["strAPIINNO"].ToString() + "/api/FO_AM_Logon?CustomerId=000846&password=12345678&authType=0&ipAddress=111&generateSession=false&format=json");
                WebRequest request = WebRequest.Create("" +
                                                       System.Configuration.ConfigurationManager
                                                           .AppSettings["strAPIINNO"].ToString() +
                                                       "/api/FO_AM_Logon?CustomerId=" +
                                                       u.ToString().Trim().Substring(0, 6) + "&password=" +
                                                       p.ToString().Trim() +
                                                       "&authType=0&ipAddress=111&generateSession=false&format=json");
                // If required by the server, set the credentials.
                request.Credentials = CredentialCache.DefaultCredentials;
                // Get the response.
                WebResponse response = request.GetResponse();
                //  Response.Write("<script language=\"javascript\">alert('json 2');</script>");
                // Display the status.
                Console.WriteLine(((HttpWebResponse) response).StatusDescription);
                // Get the stream containing content returned by the server.
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.
                responseFromServer = reader.ReadToEnd();
                // Display the content.
                // Response.Write("<script language=\"javascript\">alert('json 3');</script>");
                // Clean up the streams and the response.
                reader.Close();
                response.Close();
                // this.lblReturn.Text = responseFromServer.ToString();
                //  Response.Write("<script language=\"javascript\">alert('" + responseFromServer.ToString() + "');</script>");
                return responseFromServer.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return "WRONG_PASSWORD";
            }
        }


        //Lấy thông tin ngân hàng

        private void InitDropdownList(string bankNo_01 = "", string bankNo_02 = "", string provinceCode = "",
            string district = "")
        {
            try
            {
                
                Logger.Error(typeof(HomeController),string.Format("start InitDropdownList 0 - {0} - {1} - {2} - {3} "
                    ,bankNo_01,bankNo_02,provinceCode,district));

                var webService = new BankWebService();
                var bankList = webService.GetBankList();
                ViewBag.BankList = bankList != null
                    ? new SelectList(bankList, "BankNo", "ShortName")
                    : new SelectList(new List<BankModel>(), "BankNo", "ShortName");
                Logger.Error(typeof(HomeController),string.Format("Buoc 1 - {0} ",bankList.Count));

                ViewBag.SubBranchList_01 = new SelectList(new List<SubBranchModel>(), "BranchNo", "ShortBranchName");
                ViewBag.SubBranchList_02 = new SelectList(new List<SubBranchModel>(), "BranchNo", "ShortBranchName");
                if (!string.IsNullOrEmpty(bankNo_01))
                {
                    ViewBag.SubBranchList_01 =
                        new SelectList(webService.GetSubBranchList(bankNo_01), "BranchNo", "ShortBranchName");
                }

                if (!string.IsNullOrEmpty(bankNo_02))
                {
                    ViewBag.SubBranchList_02 =
                        new SelectList(webService.GetSubBranchList(bankNo_02), "BranchNo", "ShortBranchName");
                }

                Logger.Error(typeof(HomeController),string.Format("Buoc 2 - {0} ",bankNo_01));
                Logger.Error(typeof(HomeController),string.Format("Buoc 3 - {0} ",bankNo_02));

                var provinceList = webService.GetProvinceList();
                ViewBag.ProvinceList = provinceList != null
                    ? new SelectList(provinceList, "ProvinceID", "ProvinceName")
                    : new SelectList(new List<ProvinceModel>(), "ProvinceID", "ProvinceName");
                Logger.Error(typeof(HomeController),string.Format("Buoc 4 - {0} ",provinceList.Count));

                var placeOfIssueList = UtilTvsi.GetListPlaceOfIssue();
                Logger.Error(typeof(HomeController),string.Format("Buoc 5 - {0} ",placeOfIssueList.Count()));
                
                List<OtherModel> lists = (List<OtherModel>) placeOfIssueList;
                lists.Add(new OtherModel()
                {
                    Category = -1,
                    CategoryName = "Khác"
                });
                IEnumerable<OtherModel> listPlaceOfIssue = lists;

                ViewBag.placeOfIssueList = listPlaceOfIssue.Count() > 0
                    ? new SelectList(listPlaceOfIssue, "Category", "CategoryName")
                    : new SelectList(new List<OtherModel>(), "Category", "CategoryName");

                var listGender = new List<OtherModel>();
                listGender.Add(new OtherModel()
                {
                    Category = 1,
                    CategoryName = "Nam"
                });
                listGender.Add(new OtherModel()
                {
                    Category = 2,
                    CategoryName = "Nữ"
                });
                Logger.Error(typeof(HomeController),string.Format("Buoc 6 - {0} ",listGender.Select(x => x.CategoryName)));

                ViewBag.GenderList = listGender.Count() > 0
                    ? new SelectList(listGender, "Category", "CategoryName")
                    : new SelectList(new List<OtherModel>(), "Category", "CategoryName");

                var provinceLists = UtilTvsi.GetProvinceList();
                ViewBag.GetProvinceList = provinceLists.Count() > 0
                    ? new SelectList(provinceLists, "ProvinceCode", "ProvinceName")
                    : new SelectList(new List<ProvinceModel>(), "ProvinceCode", "ProvinceName");

                var districtList = UtilTvsi.GetDistricList(provinceCode);

                ViewBag.District = districtList.Count() > 0
                    ? new SelectList(districtList, "DistrictCode", "DistrictName")
                    : new SelectList(new List<ProvinceModel>(), "DistrictCode", "DistrictName");

                var wardList = UtilTvsi.GetWardList(district);
                ViewBag.Ward = wardList.Count() > 0
                    ? new SelectList(wardList, "WardCode", "WardName")
                    : new SelectList(new List<ProvinceModel>(), "WardCode", "WardName");
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("{0} - InitDropdownList", e.Message));
                throw;
            }
        }

        public async Task<ActionResult> GetSaleName(string saleID)
        {
            try
            {
                if (!string.IsNullOrEmpty(saleID))
                {
                    var data = await UtilTvsi.GetSaleName(saleID);
                    if (!string.IsNullOrEmpty(data))
                    {
                        return Json(new {status = true, data = data}, JsonRequestBehavior.DenyGet);
                    }
                }

                return Json(new {status = false}, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController),
                    DangkyKhCanhan() + " GetSaleName(): saleID= " + saleID + " " + ex.Message);
                return Json(new {status = false}, JsonRequestBehavior.DenyGet);
            }
        }

        public JsonResult GetBankList()
        {
            var webService = new BankWebService();
            var bankList = webService.GetBankList();

            if (bankList != null)
            {
                var data = bankList.Select(x => new
                {
                    bankNo = x.BankNo,
                    bankName = x.ShortName
                });
                return Json(data.ToList(), JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubBranchList(string bankNo)
        {
            var webService = new BankWebService();
            var branchList = webService.GetSubBranchList(bankNo);

            if (branchList != null)
            {
                var data = branchList.Select(x => new
                {
                    branchNo = x.BranchNo,
                    branchName = x.ShortBranchName
                });
                return Json(data.ToList(), JsonRequestBehavior.AllowGet);
            }

            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBankBranchInfo(string bankNo)
        {
            try
            {
                var webService = new BankWebService();
                var branchList = webService.GetSubBranchList(bankNo);
                return Json(new {status = true, data = branchList}, JsonRequestBehavior.DenyGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController),
                    DangkyKhCanhan() + " GetBankBranchInfo(): bankNo= " + bankNo + " " + ex.Message);
                return Json(new {status = false}, JsonRequestBehavior.DenyGet);
            }
        }


        public List<string> ValidateOpenAcc(DangKyKhCaNhanModel model, bool isAdd = true)
        {
            var errorList = new List<string>();

            if (model.RegistChannelInfo)
            {
                if (string.IsNullOrEmpty(model.ChannelInfoMethod))
                    errorList.Add("Bạn phải chọn ít nhất 1 phương thức kênh thông tin đặt lệnh");
            }


            if (string.IsNullOrEmpty(model.Phone_01) && string.IsNullOrEmpty(model.Phone_02))
                errorList.Add("Bạn phải nhập số điện thoại");

            model.BankAccNo_01 = model.BankAccNo_01 ?? "";
            model.BankAccName_01 = model.BankAccName_01 ?? "";
            model.BankNo_01 = model.BankNo_01 ?? "";
            model.SubBranchNo_01 = model.SubBranchNo_01 ?? "";

            model.BankAccNo_02 = model.BankAccNo_02 ?? "";
            model.BankAccName_02 = model.BankAccName_02 ?? "";
            model.BankNo_02 = model.BankNo_02 ?? "";
            model.SubBranchNo_02 = model.SubBranchNo_02 ?? "";

            if (model.TransferType.Contains(UtilTvsi.RegisterConst.Bank))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_01) || string.IsNullOrEmpty(model.BankAccName_01)
                                                             || string.IsNullOrEmpty(model.BankNo_01) ||
                                                             string.IsNullOrEmpty(model.SubBranchNo_01))
                    errorList.Add("Bạn phải nhập thông tin cho ngân hàng");
            }

            var strBankInfo_01 = model.BankAccNo_01 + model.BankNo_01
                                                    + model.SubBranchNo_01;
            // Truong hop co nhap thong tin ngan hang thì phai nhập đủ thông tin 5 ô Textbox ngân hàng
            if (!string.IsNullOrEmpty(strBankInfo_01))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_01) || string.IsNullOrEmpty(model.BankNo_01) ||
                    string.IsNullOrEmpty(model.SubBranchNo_01)
                )
                    errorList.Add("Bạn phải nhập đầy đủ thông tin cho ngân hàng .");
            }

            var strBankInfo_02 = model.BankAccNo_02 + model.BankNo_02
                                                    + model.SubBranchNo_02;
            if (!string.IsNullOrEmpty(strBankInfo_02))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_02)
                    || string.IsNullOrEmpty(model.BankAccNo_02) || string.IsNullOrEmpty(model.SubBranchNo_02))
                    errorList.Add("Bạn phải nhập đầy đủ thông tin cho ngân hàng 2");
            }

            if (!string.IsNullOrEmpty(model.sale_id) && model.sale_id.Length < 4)
                errorList.Add("Mã nhân viên ít nhất phải có 4 ký tự là chữ số.");

            return errorList;
        }

        public List<string> ValidateBank_02(DangKyKhCaNhanModel model)
        {
            var errorList = new List<string>();

            var strBankInfo_02 = model.BankAccNo_02 + model.BankNo_02
                                                    + model.SubBranchNo_02 + model.City_02.Replace("|", "");
            if (!string.IsNullOrEmpty(strBankInfo_02))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_02) || string.IsNullOrEmpty(model.BankAccName_02)
                                                             || string.IsNullOrEmpty(model.BankAccNo_02) ||
                                                             string.IsNullOrEmpty(model.SubBranchNo_02)
                                                             || string.IsNullOrEmpty(model.City_02.Replace("|", "")))
                    errorList.Add("Bạn phải nhập đầy đủ thông tin cho ngân hàng 2");
            }

            return errorList;
        }

        public List<string> ValidateOpenAccForeign(DangKyKhCaNhanModel model, bool isAdd = true)
        {
            var errorList = new List<string>();

            if (model.RegistChannelInfo)
            {
                if (string.IsNullOrEmpty(model.ChannelInfoMethod))
                    errorList.Add(Website.Txt_InfoMethod);
            }

            if (string.IsNullOrEmpty(model.Phone_01) && string.IsNullOrEmpty(model.Phone_02))
                errorList.Add(Website.Txt_PhoneEmpty);

            model.BankAccNo_01 = model.BankAccNo_01 ?? "";
            model.BankAccName_01 = model.BankAccName_01 ?? "";
            model.BankNo_01 = model.BankNo_01 ?? "";
            model.SubBranchNo_01 = model.SubBranchNo_01 ?? "";
            model.City_01 = model.City_01 ?? "";

            model.BankAccNo_02 = model.BankAccNo_02 ?? "";
            model.BankAccName_02 = model.BankAccName_02 ?? "";
            model.BankNo_02 = model.BankNo_02 ?? "";
            model.SubBranchNo_02 = model.SubBranchNo_02 ?? "";
            model.City_02 = model.City_02 ?? "";

            if (string.IsNullOrEmpty(model.BankAccName_01))
                errorList.Add(Website.Txt_NotificationBank1);
            /*if (string.IsNullOrEmpty(model.BankAccNo_01) || string.IsNullOrEmpty(model.BankAccName_01)
                                                         || string.IsNullOrEmpty(model.BankNo_01) ||
                                                         string.IsNullOrEmpty(model.SubBranchNo_01)
                                                         || string.IsNullOrEmpty(model.City_01.Replace("|", "")))
                errorList.Add(Website.Txt_NotificationBank1);*/

            var strBankInfo_01 = model.BankAccNo_01 + model.BankAccName_01 + model.BankNo_01
                                 + model.SubBranchNo_01 + model.City_01.Replace("|", "");

            // Truong hop co nhap thong tin ngan hang thì phai nhập đủ thông tin 5 ô Textbox ngân hàng
            if (!string.IsNullOrEmpty(strBankInfo_01))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_01) || string.IsNullOrEmpty(model.BankAccName_01)
                                                             || string.IsNullOrEmpty(model.BankNo_01) ||
                                                             string.IsNullOrEmpty(model.SubBranchNo_01)
                                                             || string.IsNullOrEmpty(model.City_01.Replace("|", "")))
                    errorList.Add(Website.Txt_NotificationBank1);
            }

            var strBankInfo_02 = model.BankAccNo_02 + model.BankAccName_02 + model.BankNo_02
                                 + model.SubBranchNo_02 + model.City_02.Replace("|", "");
            // Truong hop co nhap thong tin ngan hang thì phai nhập đủ thông tin 5 ô Textbox ngân hàng
            if (!string.IsNullOrEmpty(strBankInfo_02))
            {
                if (string.IsNullOrEmpty(model.BankAccNo_02) || string.IsNullOrEmpty(model.BankAccName_02)
                                                             || string.IsNullOrEmpty(model.BankNo_02) ||
                                                             string.IsNullOrEmpty(model.SubBranchNo_02)
                                                             || string.IsNullOrEmpty(model.City_02.Replace("|", "")))
                    errorList.Add(Website.Txt_NotificationBank2);
            }


            if (!string.IsNullOrEmpty(model.sale_id) && model.sale_id.Length < 4)
                errorList.Add(Website.Txt_SaleCode);
            return errorList;
        }


        private bool IsRegistBankTransferService(DangKyKhCaNhanModel model)
        {
            //            return View.TransferType.Contains(TransferMoneyConst.Bank.Value());
            return !string.IsNullOrEmpty(model.BankAccName_01) || !string.IsNullOrEmpty(model.BankAccName_02);
        }

        private bool IsRegistBankTransferServiceNN(DangKyKhCaNhanModel model)
        {
            if (!string.IsNullOrEmpty(model.BankAccName_01) || !string.IsNullOrEmpty(model.BankAccName_02) ||
                !string.IsNullOrEmpty(model.accNoBIDV))
            {
                return true;
            }

            return false;
        }
        // Dang ky goi margin 
        [HttpGet]
        public async Task<ActionResult> Dangkydichvu(string account,string xtrade)
        {
            try
            {
                if (Session["accountNumber"] != null)
                {
                    if (!string.IsNullOrEmpty(xtrade))
                        return RedirectToAction("RegisterExperienceXtrade", "Home");
                    
                    if (!string.IsNullOrEmpty(account))
                    {
                        if(!Session["accountNumber"].ToString().Equals(account))
                            return LoguotMargin(account, "");
                    }
                    var accountNo = Session["accountNumber"];
                    var service = await UtilTvsi.GetServiceList(accountNo.ToString());
                    ViewBag.ServiceList = await UtilTvsi.GetServiceList(accountNo.ToString());
                    var textMessage = "";
                    if (service != null)
                    {
                        textMessage = service.Message;
                        ViewBag.IsConfirm = service.IsConfirm;
                    }
                    ViewBag.NamePackage = textMessage;
                    ViewBag.CourseList = await UtilTvsi.GetCourseList();
                    var listTVComboHist = await UtilTvsi.GetListTVCOMBPHist(accountNo.ToString());
                    ViewBag.TVCOMBOHist = listTVComboHist.Reverse().ToList();

                    var listCourseHist = await UtilTvsi.GetListCourseHist(accountNo.ToString());
                    ViewBag.TVCourselist = listCourseHist.Reverse().ToList();
                    if (accountNo != null)
                    {
                        Getgoidichvuid(Convert.ToString(accountNo));
                        /*ViewBag.TVEcoList = await UtilTvsi.GetTvEcoList(accountNo.ToString());
                        ViewBag.TVEcoHistList = await UtilTvsi.GetHistTvEco(accountNo.ToString());
                        ViewBag.CashBalance = await UtilTvsi.GetCashBalance(accountNo.ToString());*/
                    }
                    
                }
                else
                {
                    if (!string.IsNullOrEmpty(account))
                    {
                        if (!string.IsNullOrEmpty(xtrade))
                            return RedirectToAction("RegisterExperienceXtrade", "Home");
                        TempData["CustCode"] = account;
                    }
                }
                if (Session["marginList"] != null)
                {
                    if (!string.IsNullOrEmpty(xtrade))
                    {
                        return RedirectToAction("RegisterExperienceXtrade", "Home");
                    }

                    if (Session["accountNumber"] != null)
                    {
                        var accountNo = Session["accountNumber"];
                        Getgoidichvuid(Convert.ToString(accountNo));
                    }
                    var marginlist = Session["marginList"] as List<MarginList>;

                    ViewBag.getHtmlPackage = marginlist;
                    ViewBag.getIntPackage = GetIntPackage();
                    ViewBag.getPPCurrent = GetPPPackageCurrent();
                    var listNamePackage = new List<string>();

                    if (marginlist != null)
                        for (int i = 0; i < marginlist.Count; i++)
                        {
                            listNamePackage.Add(marginlist[i].ten_goi_dich_vu);
                        }

                    ViewBag.GetGoiDichVuUuDai = Getgoidichvuuudai();
                    ViewBag.GetGoiDichVu =
                        GetListServicePackage(listNamePackage, Convert.ToString(Session["accountNumberCheck"]));
                }
                ViewBag.spdv_menu = "dropdown";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown";
                return View();
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("Loi Method - Dangkydichvu {0}" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }
        
        

        public ActionResult LoguotMargin(string custcode,string xtrade = "")
        {
            Session.Clear();
            return RedirectToAction("Dangkydichvu", "Home", new
            {
                id = "", @account = custcode, xtrade = !string.IsNullOrEmpty(xtrade) ? "register":""
            });
        }

        public List<MARGINPACK> Getgoidichvuuudai()
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var getgoidichvuuuadai = packageWebService.GetgoidichvuuudaiRoot();
                return getgoidichvuuuadai.MARGINPACK;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }

        public async Task<ActionResult> CreateTvEco(string accountNo, int id)
        {
            try
            {

                if (!string.IsNullOrEmpty(accountNo) && id > 0)
                {
                    var custcode = accountNo.Substring(0, 6);

                    var cashBalance = await UtilTvsi.GetCashBalance(custcode);
                    var checkInsertTvEco = await UtilTvsi.CheckInsertTvEco(id,cashBalance,custcode);
                    if (!checkInsertTvEco)
                    {
                        return Json(new
                        {
                            Status = false,
                            Data = "Tài khoản không đủ điều kiện đăng ký"
                        }, JsonRequestBehavior.DenyGet);
                    }
                    var data = await UtilTvsi.CreateTvEco(custcode, id);
                    if (data != null)
                    {
                        if (data.Code == "000")
                        {
                            return Json(new
                            {
                                Status = true,
                                Data = data.ErrorMessage
                            }, JsonRequestBehavior.DenyGet);
                        }

                        return Json(new
                        {
                            Status = false,
                            Data = data.ErrorMessage
                        }, JsonRequestBehavior.DenyGet);
                    }
                    
                }
                return Json(new
                {
                    Status = false,
                    Data = "Đăng ký không thành công."
                });
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CreateTvEco" + e.Message));
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public async Task<ActionResult> CancelTvEco(string accountNo,int id)
        {
            try
            {
                if (!string.IsNullOrEmpty(accountNo) && id > 0)
                {
                    var custcode = accountNo.Substring(0, 6);
                    var data = await UtilTvsi.CancelTvEco(custcode, id);
                    if (data != null)
                    {
                        if (data.RetCode == "000")
                        {
                            return Json(new
                            {
                                Status = true,
                                Data = "Hủy thành công"
                            }, JsonRequestBehavior.DenyGet);  
                        }
                        return Json(new
                        {
                            Status = false,
                            Data = "Hủy không thành công"
                        }, JsonRequestBehavior.DenyGet);
                    }
                }
                return Json(new
                {
                    Status = false,
                    Data = "không thành công."
                });
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CancelTvEco" + e.Message));
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public async Task<ActionResult> GetTvEcoFee(string accountNo,int id)
        {
            try
            {
                if (id > 0 && !string.IsNullOrEmpty(accountNo))
                {
                    var custcode = accountNo.Substring(0, 6);
                    var data = await UtilTvsi.GetTvEcoFeeValue(custcode,id);
                    if (data != null)
                    {
                        if (data.RetCode == "000")
                        {
                            return Json(new
                            {
                                Status = true,
                                Data = data.RetData
                            }, JsonRequestBehavior.DenyGet);     
                        }
                        return Json(new
                        {
                            Status = false,
                            Data = "Không tìm thấy dữ liệu"
                        });
                    }
                   
                }
                return Json(new
                {
                    Status = false,
                    Data = "Không tìm thấy dữ liệu"
                });
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("GetTvEcoFee" + e.Message));
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public List<GetServicePackage> GetListServicePackage(List<string> listnamepackage, string numberaccount)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                List<GetServicePackage> listServicePack = new List<GetServicePackage>();
                for (int i = 0; i < listnamepackage.Count; i++)
                {
                    listServicePack.Add(packageWebService.GetServicePackage(listnamepackage[i], numberaccount));
                }

                return listServicePack;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("GetListServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }

        public ActionResult CheckPinServicePackage(string pin)
        {
            var webservice = new PackageWebService();
            try
            {
                var id = Session["accountNumber"];
                if (id == null)
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var idUser = id;
                var pinUser = pin.Trim();

                // tí mở lại nhé
                if (UtilTvsi.GetSourceEkyc(id.ToString()))
                {
                    if (!UtilTvsi.GetStatusContractEkyc(id.ToString()))
                    {
                        return Json(new
                        {
                            Status = false,
                            Data =
                                "Tài khoản của Quý khách không đủ điều kiện để thực hiện thay đổi gói dịch vụ trực tuyến. Quý khách vui lòng liên hệ nhân viên tư vấn hoặc tổng đài 19001885 để được hỗ trợ hoàn thiện Hồ sơ và thực hiện đổi gói."
                        });
                    }
                }


                var checkAccount = UtilTvsi.CheckPinServicePackage(pinUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã PIN không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                if (checkAccount.TrangThai == 2)
                {
                    return Json(new
                    {
                        Status = false,
                        Data =
                            "Quý khách chưa mở tài khoản Margin, vui lòng liên hệ  nhân viên môi giới hoặc 19001885 để biết thêm chi tiết."
                    });
                }

                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public ActionResult CheckPinServiceTvCombo(string pin)
        {
            var webservice = new PackageWebService();
            try
            {
                var id = Session["accountNumber"];
                if (id == null)
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var idUser = id;
                var pinUser = pin.Trim();


                var checkAccount = UtilTvsi.CheckPinServicePackage(pinUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã PIN không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                /*if (checkAccount.TrangThai == 2)
                {
                    return Json(new
                    {
                        Status = false,
                        Data = "Quý khách chưa mở tài khoản Margin, vui lòng liên hệ  nhân viên môi giới hoặc 19001885 để biết thêm chi tiết."
                    });
                }*/

                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public async Task<ActionResult> CheckPassServicePackage(string pass, string stk)
        {
            try {
                Logger.Info(typeof(HomeController),"CheckPassServicePackage");
                if (string.IsNullOrEmpty(stk) || stk == "undefined" || stk == "null")
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var id = stk;
                var idUser = id;
                var passUser = pass.Trim();
                var checkAccount = UtilTvsi.CheckPassServicePackage(passUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã khách hàng hoặc mật khẩu không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                if (checkAccount.TrangThai == 2)
                {
                    
                }
                else
                {
                    Session["AcocuntMarginCheck"] = checkAccount.TrangThai;
                }

                Session["accountNumber"] = stk;

                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }

            catch (Exception e)
            {
                Logger.Info(typeof(HomeController),$"Password = {pass} - CustCode = - {stk} ");
                var mess = e.Message;
                Logger.Error(typeof(HomeController), string.Format("CheckPassServicePackage-" + mess));
                return Json(new
                {
                    Status = false,
                    Data = "Có lỗi xảy ra, vui lòng liên hệ bộ phận hỗ trợ."
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> InsertTVCombo(int serviceid)
        {
            try
            {
                var accountNo = Session["accountNumber"].ToString();
                if (string.IsNullOrEmpty(accountNo))
                {
                    return Json(new {Status = 99, Data = "Vui lòng đăng nhập lại!!!"}, JsonRequestBehavior.AllowGet);
                }

                var result = await UtilTvsi.InsertTVCombo(serviceid, accountNo);
                if (result.Code.Equals(TVComboConst.Success))
                {
                    return Json(new {Status = 1, Data = result.ErrorMessage}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = 2, Data = result.ErrorMessage}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("InsertTVCombo - 1 ") + ex.Message);
                return Json(new {Status = 99, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CancelTVCombo(int id)
        {
            try
            {
                var accountNo = Session["accountNumber"].ToString();
                if (string.IsNullOrEmpty(accountNo))
                {
                    return Json(new {Status = 99, Data = "Vui lòng đăng nhập lại!!!"}, JsonRequestBehavior.AllowGet);
                }


                if (id > 0)
                {
                    var result = await UtilTvsi.CancelRegisterService(id);
                    if (result)
                    {
                        var user = UtilTvsi.GetCustomerInfo(accountNo);
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            var resultCombo = await UtilTvsi.GetInfoCombo(Convert.ToString(id));
                            if (resultCombo.Status == 99)
                                UtilTvsi.SendEmailCancelRegister(resultCombo.ServiceName, resultCombo.CustCode,
                                    resultCombo.CustName, resultCombo.CreatedDate,
                                    resultCombo.EffDate, resultCombo.ExpDate, user.Email);
                        }

                        return Json(new {Status = 1}, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new {Status = 2, Data = "Không thành công"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("CancelTVCombo - ex.Message - {0} ", ex.Message));
                return Json(new {Status = 99, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetIntPackage()
        {
            try
            {
                var webService = new PackageWebService();
                var getIdSession = Session["accountNumberCheck"];
                var goihientai = webService.GetPackageCurrent(Convert.ToString(getIdSession));
                if (string.IsNullOrEmpty(goihientai.GOI_DICH_VU))
                {
                    goihientai.GOI_DICH_VU = "BASIC";
                }

                return goihientai.GOI_DICH_VU;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("Loi GetIntPackage " + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return null;
            }
        }

        public GetPackageCurrentModel.Root GetPPPackageCurrent()
        {
            try
            {
                var webService = new PackageWebService();
                var getIdSession = Session["accountNumberCheck"];
                var goihientai = webService.GetPackageCurrent(Convert.ToString(getIdSession));
                if (string.IsNullOrEmpty(Convert.ToString(goihientai)))
                {
                    return null;
                }

                if (string.IsNullOrEmpty(goihientai.SUC_MUA))
                {
                    goihientai.SUC_MUA = "0";
                }

                return goihientai;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController),
                    string.Format("HomeController->GetPPPackageCurrent -" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> SetPackageToAccount(string packAge, string gdvuudai, int? status)
        {
            try
            {
                if (string.IsNullOrEmpty(packAge))
                    packAge = "";
                if(packAge.Equals("undefined"))
                    packAge = "";
                if (packAge.Equals("null"))
                    packAge = "";
                
                if (string.IsNullOrEmpty(gdvuudai))
                    gdvuudai = "";
                if(gdvuudai.Equals("undefined"))
                    gdvuudai = "";
                if (gdvuudai.Equals("null"))
                    gdvuudai = "";
                
                var custcode = Session["accountNumber"].ToString();
                var webService = new PackageWebService();
                var so_tai_khoan_HS = Session["accountNumber"];
                var so_tai_khoan = Session["accountNumberCheck"];
                var ho_ten = Session["accountName"];

                var notePackAge = string.Empty;
                var notePackPrefer = string.Empty;
                var marginCurrent = webService.GetPackageCurrent(custcode);
                
                var marginCurrentShort = !string.IsNullOrEmpty(marginCurrent.GOI_DICH_VU)
                    ? marginCurrent.GOI_DICH_VU = "BASIC"
                    : marginCurrent.GOI_DICH_VU ;
                
                var valueFirst = await UtilTvsi.ChangeMarginGroup(custcode,marginCurrentShort,packAge,gdvuudai, 1);
                var valueSecond = await UtilTvsi.ChangeMarginGroup(custcode,  marginCurrentShort,packAge,gdvuudai, 2);
                
                if (!valueFirst.Status.Equals("000"))
                    notePackAge = valueFirst.Note;

                if (!valueSecond.Status.Equals("000"))
                    notePackPrefer = valueSecond.Note;

                var goi_hien_tai = webService.GetPackageCurrent(Convert.ToString(so_tai_khoan));
                var goi_hien_tai_uu_dai = webService.GetPackageServiceAccount(Convert.ToString(so_tai_khoan_HS));

                if (string.IsNullOrEmpty(goi_hien_tai_uu_dai))
                {
                    goi_hien_tai_uu_dai = "Chua dang ky";
                }

                if (string.IsNullOrEmpty(goi_hien_tai.GOI_DICH_VU))
                {
                    goi_hien_tai.GOI_DICH_VU = "BASIC";
                }

                var ngay_dang_ky = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var goi_thay_doi = packAge;
                var setDate = new DateTime();
                if (goi_thay_doi == "BASIC")
                {
                    setDate = (DateTime) SqlDateTime.MaxValue;
                }
                else
                {
                    setDate = (DateTime) SqlDateTime.MaxValue;
                }

                var ngay_ket_thuc = setDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                try
                {
                    var webservice = new PackageWebService();
                    if (status == 1)
                    {
                        var savegdvuudai = webservice.setGDVUuDai(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai_uu_dai, gdvuudai.ToString(),
                            Convert.ToString(so_tai_khoan_HS), notePackPrefer);
                        if (savegdvuudai == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    if (status == 2)
                    {
                        var savePackageMargin = webservice.setPackageService(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai.GOI_DICH_VU, goi_thay_doi,
                            Convert.ToString(so_tai_khoan_HS), notePackAge);
                        if (savePackageMargin == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    if (status == 3)
                    {
                        var savePackageMargin = webservice.setPackageService(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai.GOI_DICH_VU, goi_thay_doi,
                            Convert.ToString(so_tai_khoan_HS), notePackAge);

                        var savegdvuudai = webservice.setGDVUuDai(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai_uu_dai, gdvuudai.ToString(),
                            Convert.ToString(so_tai_khoan_HS), notePackPrefer);
                        if (savePackageMargin == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ không thành công."});
                        }

                        if (savegdvuudai == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ ưu đãi không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ ưu đãi không thành công."});
                }
                catch (Exception ex)
                {
                    Logger.Error(typeof(HomeController),
                        string.Format("SetPackageToAccount {0} {1}", ex.Message, ex.InnerException));
                    return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("SetPackageToAccount - 1 ") + ex.Message);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> ChangeMarginAccount(string packname, string packprefername, int? status)
        {
            try
            {
                if (string.IsNullOrEmpty(packname))
                    packname = "";
                if(packname.Equals("undefined"))
                    packname = "";
                if (packname.Equals("null"))
                    packname = "";
                
                if (string.IsNullOrEmpty(packprefername))
                    packprefername = "";
                if(packprefername.Equals("undefined"))
                    packprefername = "";
                if (packprefername.Equals("null"))
                    packprefername = "";
                

                
                var custcode = Session["accountNumber"].ToString();
                var webService = new PackageWebService();
                if (!string.IsNullOrEmpty(custcode))
                {
                    var marginCurrent = webService.GetPackageCurrent(custcode + "6");
                    var marginCurrentShort = string.IsNullOrEmpty(marginCurrent.GOI_DICH_VU)
                        ? marginCurrent.GOI_DICH_VU = "BASIC"
                        : marginCurrent.GOI_DICH_VU;
                    
                    var checkDebt = webService.GetServicePackage(packname,custcode + "6");
                    if (checkDebt != null && checkDebt.du_no_toi_da != null)
                        checkDebt.du_no_toi_da = checkDebt.du_no_toi_da.Replace(".", ",");
                    
                    if (!string.IsNullOrEmpty(packname) || !string.IsNullOrEmpty(packprefername))
                    {
                        if (status == 1)
                        {
                            var value = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,"",packprefername, 2);

                            if (!value.Status.Equals("000"))
                            {
                                if (value.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = value.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = value.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (status == 2)
                        {
                            if (checkDebt !=null)
                            {
                                if (checkDebt.du_no_toi_da != null)
                                {
                                    var val = double.Parse(checkDebt.du_no_toi_da);
                                    if (checkDebt.du_no > val)
                                    {
                                        return Json(
                                            new
                                            {
                                                Status = false,
                                                Data =
                                                    "Quý khách không được đăng ký gói do dư nợ vượt quá hạn mức cho vay tối đa (500.000.000 đồng)",
                                                IsConfirm = 0
                                            }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                            var value = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname, packprefername,1);
                            if (!value.Status.Equals("000"))
                            {


                                if (value.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = value.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = value.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (status == 3)
                        {
                            var valueFirst = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname,packprefername, 1);
                            var valueSecond = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname,packprefername, 2);
                            if (checkDebt != null)
                            {
                                if (checkDebt.du_no_toi_da != null)
                                {
                                    var val = double.Parse(checkDebt.du_no_toi_da);
                                    if (checkDebt.du_no > val)
                                    {
                                        return Json(new {Status = false, Data = "Quý khách không được đăng ký gói do dư nợ vượt quá hạn mức cho vay tối đa (500.000.000 đồng)", IsConfirm = 0}, JsonRequestBehavior.AllowGet);

                                    }
                                }
                            }
                            
                            if (!valueSecond.Status.Equals("000"))
                            {
                                if (valueSecond.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = valueSecond.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = valueSecond.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                            if (!valueFirst.Status.Equals("000"))
                            {
                                if (valueFirst.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = valueFirst.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = valueFirst.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        return Json(new {Status = true}, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new {Status = false, Data = "Vui lòng đăng nhập lại xin cám ơn!!!."},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController),
                    string.Format("Loi phuong thuc ChangeMarginAccount  - {0}", e.Message));
                return Json(new {Status = false, Data = "Có lỗi xảy ra"}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public void Getgoidichvuid(string custcode)
        {
            try
            {
                var historyPackage = new PackageWebService();
                var accountNumber = Session["accountNumber"];
                var check = historyPackage.GetHistoryPackage(custcode);
                var dateNow = DateTime.Now.ToString("dd/MM/yyyy").AsDateTime();
                var getGDVUudai = historyPackage.GetGDVUuDai(custcode);
                var getPackageServiceAccount = historyPackage.GetPackageServiceAccount(custcode);
                ViewBag.listPackage = check.MARGINList;
                var checkStatus = 1;
                var ttHoanThanh = 1;
                for (int i = 0; i < check.MARGINList.Count; i++)
                {
                    if (check.MARGINList[i].trang_thai == 0 || check.MARGINList[i].trang_thai == 5)
                    {
                        checkStatus = check.MARGINList[i].trang_thai;
                    }

                    if (check.MARGINList[i].trang_thai == 1 &&
                        Convert.ToDateTime(check.MARGINList[i].ngay_dang_ky).ToString("dd/MM/yyyy").AsDateTime() ==
                        dateNow)
                    {
                        ttHoanThanh = 2;
                    }
                }

                var checkStatus2 = 1;
                var ttHoanThanh2 = 1;
                for (int i = 0; i < getGDVUudai.MARGINList.Count; i++)
                {
                    if (getGDVUudai.MARGINList[i].trang_thai == 0 || getGDVUudai.MARGINList[i].trang_thai == 5)
                    {
                        checkStatus2 = getGDVUudai.MARGINList[i].trang_thai;
                    }

                    if (getGDVUudai.MARGINList[i].trang_thai == 1 &&
                        Convert.ToDateTime(getGDVUudai.MARGINList[i].ngay_dang_ky).ToString("dd/MM/yyyy")
                            .AsDateTime() == dateNow)
                    {
                        ttHoanThanh2 = 2;
                    }
                }

                ViewBag.TTHoanthanh = ttHoanThanh;
                ViewBag.TTHoanthanh2 = ttHoanThanh2;
                ViewBag.checkStatus = checkStatus;
                ViewBag.checkStatus2 = checkStatus2;
                ViewBag.listGDVUuDai = getGDVUudai.MARGINList;
                ViewBag.accountNumber = check.so_tai_khoan;
                ViewBag.servicePackgeCurrent = getPackageServiceAccount;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("HomeController - Getgoidichvuid" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateDangKyGoiDichVu(string goidichvuid)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var value = packageWebService.UpdateDangKyGoiDichVu(goidichvuid, 5);
                if (value == "1")
                {
                    return Json(new {Status = true, Data = "Chuyển đổi thành công"}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, Data = "Chuyển đổi không thành công,vui lòng liên hệ bỗ phận hỗ trợ"},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message + e.InnerException);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateDangKyGoiDichVuUuDai(string goidichvuuudaiid)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var value = packageWebService.UpdateDangKyGoiDichVuUuDai(goidichvuuudaiid, 5);
                if (value == "1")
                {
                    return Json(new {Status = true, Data = "Chuyển đổi thành công"}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, Data = "Chuyển đổi không thành công,vui lòng liên hệ bỗ phận hỗ trợ"},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message + e.InnerException);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."},
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Dangkydichvu(CourseModel model)
        {
            try
            {
                ViewBag.tab = "active";
                ViewBag.spdv_menu = "dropdown";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown";

                if (string.IsNullOrEmpty(Session["accountNumber"].ToString()))
                    return LoguotMargin("","");


                var accountNo = Session["accountNumber"].ToString();
                var serviceResult = await UtilTvsi.InsertTVCombo(model.serviceid, accountNo);

                if (serviceResult.Code.Equals(TVComboConst.Success) && Convert.ToInt32(serviceResult.RegistID) > 0)
                {
                    if (UtilTvsi.FieldValidCourse(model.CourseID, model.CardID, model.Phone, model.Email,
                        model.FullName))
                    {
                        await UtilTvsi.InsertRegCourse(serviceResult.RegistID,
                            model.CourseID, 1, model.AccountNo, model.CardID, model.Phone, model.Email,
                            model.MonthCourse, model.TimeCourse, accountNo, model.FullName);
                    }

                    if (UtilTvsi.FieldValidCourse(model.CourseID_01, model.CardID_01, model.Phone_01, model.Email_01,
                        model.FullName_01))
                    {
                        await UtilTvsi.InsertRegCourse(serviceResult.RegistID,
                            model.CourseID_01, 2, model.AccountNo_01, model.CardID_01, model.Phone_01, model.Email_01,
                            model.MonthCourse_01, model.TimeCourse_01, accountNo, model.FullName_01);
                    }

                    //Gửi Mail cho khách hàng
                    var user = UtilTvsi.GetCustomerInfo(accountNo);
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        var resultCombo = await UtilTvsi.GetInfoCombo(serviceResult.RegistID);
                        if (resultCombo.Status == 1)
                            UtilTvsi.SendEmailRegister(resultCombo.ServiceName,
                                resultCombo.CustCode, resultCombo.CustName, resultCombo.CreatedDate,
                                resultCombo.EffDate, resultCombo.ExpDate, user.Email);
                    }

                    TempData["CourseSuccess"] = 1;
                    return RedirectToAction("Dangkydichvu", "Home", new
                    {
                        @account = Session["accountNumber"].ToString()
                    });
                }

                if (serviceResult.Code.Equals(TVComboConst.Success) && serviceResult.RegistID.Contains("-1"))
                {
                    TempData["CourseSuccess"] = 2;
                    return RedirectToAction("Dangkydichvu", "Home", new
                    {
                        @account = Session["accountNumber"].ToString()
                    });
                }

                TempData["CourseSuccess"] = 3;
                return RedirectToAction("Dangkydichvu", "Home", new
                {
                    @account = Session["accountNumber"].ToString()
                });
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), "Dangkydichvu - POST " + ex.Message);
                TempData["CourseSuccess"] = 99;
                return RedirectToAction("Dangkydichvu", "Home", new
                {
                    @account = Session["accountNumber"].ToString()
                });
            }
        }

        //Đăng ký mở tài khoản EKYC

        [HttpGet]
        public ActionResult DangkyKhCanhanEkyc()
        {
            try
            {
                var saleID = Request.QueryString["SaleID"];
                var viewModel = new DangKyKhCaNhanModel
                {
                };
                IdTransactionDrop();
                InitDropdownList();
                if (this.Session["check_canhan_ekyc"] != null && Session["email_ekyc"] != null)
                {
                    ViewBag.success = this.Session["check_canhan_ekyc"];
                    ViewBag.email = Session["email_ekyc"];
                    Session["check_canhan_ekyc"] = null;
                    Session["email_ekyc"] = null;
                }
                else
                {
                    ViewBag.email = null;
                    ViewBag.success = false;
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";
                viewModel.TradeMethodPhone = true;
                viewModel.TradeMethodInternet = true;
                viewModel.TradeResultEmail = true;
                viewModel.TradeResultSMS = true;
                viewModel.RegistChannelInfo = true;
                viewModel.BankTransfer = true;
                viewModel.RegistChannelDirect = true;
                viewModel.RegistItradeHome = true;
                viewModel.MonthlyReportEMail = true;
                return View(viewModel);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("DangkyKhCanhanEkyc - {0}",ex.Message));
                return View(new DangKyKhCaNhanModel());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DangkyKhCanhanEkyc(DangKyKhCaNhanModel ekycmodel, string kytubaomatcn)
        {
            try
            {

                var saleCheck = true;
                var confirmOtp = DateTime.Now;
                var branchID = "";
                if (string.IsNullOrEmpty(ekycmodel.sale_id))
                {
                    saleCheck = false;
                    var provinceCode = !string.IsNullOrEmpty(ekycmodel.ProvinceValue) ? ekycmodel.ProvinceValue : "01";
                    var data = await UtilTvsi.ProcessGenSaleID(provinceCode, ekycmodel.QA1txt);
                    if (data != null && data.SaleID.Length > 3 && !string.IsNullOrEmpty(data.BranchID))
                    {
                        branchID = data.BranchID;
                        ekycmodel.sale_id = data.SaleID;
                    }
                }

                ekycmodel.noicap = !string.IsNullOrEmpty(UtilTvsi.CutPlaceIssueCardID(ekycmodel.noicap))
                    ? UtilTvsi.CutPlaceIssueCardID(ekycmodel.noicap)
                    : ekycmodel.noicap;

                if (!string.IsNullOrEmpty(ekycmodel.AddressDetail))
                    ekycmodel.diachi += ekycmodel.AddressDetail + ", ";
                if (!string.IsNullOrEmpty(ekycmodel.Ward))
                    ekycmodel.diachi += ekycmodel.Ward + ", ";
                if (!string.IsNullOrEmpty(ekycmodel.Distric))
                    ekycmodel.diachi += ekycmodel.Distric + ", ";
                if (!string.IsNullOrEmpty(ekycmodel.Province))
                    ekycmodel.diachi += ekycmodel.Province;

                var manName = string.Empty;
                var postionName = string.Empty;
                var manNo = string.Empty;
                DateTime? manDate = null;
                var manCardID = string.Empty;
                var manIssuePlace = string.Empty;
                DateTime? manIssueDate = null;
                IdTransactionDrop();
                InitDropdownList(ekycmodel.BankNo_01, ekycmodel.BankNo_02);
                var webService = new BankWebService();
                var isValid = true;
                var errorList = ValidateOpenAcc(ekycmodel);
                if (errorList.Count > 0)
                {
                    foreach (string errMsg in errorList)
                    {
                        ModelState.AddModelError("", errMsg);
                    }

                    isValid = false;
                }

                if (!isValid)
                {
                    InitDropdownList(ekycmodel.BankNo_01, ekycmodel.BankNo_02);
                    return View(ekycmodel);
                }

                ViewBag.spdv_menu = "dropdown active";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown ";

                if (!UtilTvsi.isValidDateTime(ekycmodel.ngaysinh))
                {
                    ViewBag.Message = "Ngày sinh không đúng, Quý khác vui lòng nhập lại.";
                    return View(ekycmodel);
                }

                if (!UtilTvsi.IsValidEmail(ekycmodel.email))
                {
                    ViewBag.Message = "Email không đúng,Quý khác vui lòng nhập lại.";
                    return View(ekycmodel);
                }

                if (!UtilTvsi.isValidDateTime(ekycmodel.ngaycapcmnd))
                {
                    ViewBag.Message = "Ngày cấp CMT/CCCD không đúng, Quý khác vui lòng nhập lại.";
                    return View(ekycmodel);
                }

                Logger.Info(typeof(HomeController), "ngay sinh + " + ekycmodel.ngaysinh);
                DateTime ngaysinh = DateTime.ParseExact(ekycmodel.ngaysinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                Logger.Info(typeof(HomeController), "ngay cap cmnd + " + ekycmodel.ngaycapcmnd);
                DateTime ngaycapcmnd =
                    DateTime.ParseExact(ekycmodel.ngaycapcmnd, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                Logger.Info(typeof(HomeController),
                    "qua buoc check date - ngay sinh " + ekycmodel.ngaysinh + "ngay cap cmnd " + ekycmodel.ngaycapcmnd);
                string crmSTR = ConfigurationManager.ConnectionStrings["CRM"].ToString();

                var validDateOfBirth = UtilTvsi.ValidDateOfBirth(ngaysinh);
                if (!string.IsNullOrEmpty(validDateOfBirth))
                {
                    ViewBag.Message = validDateOfBirth;
                    return View(ekycmodel);
                }

                /*if (!string.IsNullOrEmpty(ekycmodel.sale_id))
                {
                    if (!UtilTvsi.CheckSaleIDEMS(ekycmodel.sale_id))
                    {
                        ViewBag.Message = "Mã nhân viên tư vấn không đúng, Quý khác vui lòng nhập đúng hoặc để trống";
                        return View(ekycmodel);
                    }
                }*/

                if (!UtilTvsi.ExistsCMNDinCRM(ekycmodel.cmnd))
                {
                    ViewBag.Message = "CMT/CCCD đã tồn tại.";
                    return View(ekycmodel);
                }

                var saleid = string.Empty;
                string paramSaleID = Request.QueryString["SaleID"];

                if (!string.IsNullOrEmpty(paramSaleID))
                {
                    ekycmodel.sale_id = paramSaleID;
                    saleid = UtilTvsi.GetIDSale(paramSaleID);
                }
                else
                    saleid = !string.IsNullOrEmpty(ekycmodel.sale_id)
                        ? UtilTvsi.GetIDSale(ekycmodel.sale_id)
                        : ekycmodel.Branch_Transaction;

                if (!string.IsNullOrEmpty(ekycmodel.BankNo_01))
                {
                    ekycmodel.BankName_01 = webService.GetBankName(ekycmodel.BankNo_01);
                    ekycmodel.SubBranchName_01 = webService.GetSubBranchName(ekycmodel.BankNo_01,
                        ekycmodel.SubBranchNo_01);
                }

                if (ekycmodel.CustomerRelaUsa == 0)
                {
                    ekycmodel.CustomerUSA = 0;
                    ekycmodel.PlaceOfBirthUSA = 0;
                    ekycmodel.DepositoryUSA = 0;
                    ekycmodel.CustomerPhoneNumberUSA = 0;
                    ekycmodel.CustomerTransferBankUSA = 0;
                    ekycmodel.CustomerAuthorizationUSA = 0;
                    ekycmodel.CustomerReceiveMailUSA = 0;
                }

                ManInfo infoMan = null;
                List<ManInfo> maninfo = SqlManInfoData.GetManInfos();
                foreach (var item in maninfo)
                {
                    if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                        item.ma_chi_nhanh.Contains(UtilTvsi.ReturnBrandID(saleid.Substring(0, 4))))
                    {
                        infoMan = item;
                        break;
                    }
                }

                if (infoMan == null)
                {
                    infoMan = maninfo.FirstOrDefault(x => x.ma_chi_nhanh == "All");
                }

                if (infoMan != null)
                {
                    var arrPara = infoMan.gia_tri.Split('|');
                    if (!string.IsNullOrEmpty(arrPara[0]))
                        manName = arrPara[0];
                    if (!string.IsNullOrEmpty(arrPara[1]))
                        postionName = arrPara[1];
                    if (!string.IsNullOrEmpty(arrPara[2]))
                        manNo = arrPara[2];
                    if (!string.IsNullOrEmpty(arrPara[3]))
                        manDate = DateTime.ParseExact(arrPara[3],
                            CommonConstant.FormatDate, CultureInfo.CurrentCulture);
                    if (!string.IsNullOrEmpty(arrPara[4]))
                        manCardID = arrPara[4];
                    if (!string.IsNullOrEmpty(arrPara[5]))
                        manIssueDate = DateTime.ParseExact(arrPara[5],
                            CommonConstant.FormatDate, CultureInfo.CurrentCulture);
                    if (!string.IsNullOrEmpty(arrPara[6]))
                        manIssuePlace = arrPara[6];
                }

                if (!string.IsNullOrEmpty(ekycmodel.BankNo_02))
                {
                    ekycmodel.BankName_02 = webService.GetBankName(ekycmodel.BankNo_02);
                    ekycmodel.SubBranchName_02 = webService.GetSubBranchName(ekycmodel.BankNo_02,
                        ekycmodel.SubBranchNo_02);
                }

                if (!string.IsNullOrEmpty(ekycmodel.City_02))
                {
                    ekycmodel.City_02 = ekycmodel.City_02 + "|" + webService.GetProvinceName(ekycmodel.City_02);
                }

                var transferType = "";
                if (IsRegistBankTransferService(ekycmodel))
                {
                    transferType += UtilTvsi.RegisterConst.Bank;
                }

                //Ren ma confirm code
                var confirmCode = UtilTvsi.EncriptMd5(ekycmodel.email + UtilTvsi.RandomNumber(6));
                if (confirmCode.Length > 50)
                    confirmCode = confirmCode.Substring(0, 50);

                var confirmSms = UtilTvsi.RandomNumber(6);

                var regisID = 0;

                using (var conn = new SqlConnection(crmSTR))
                {
                    var parameter = new DynamicParameters();
                    parameter.Add("@hoten", ekycmodel.hoten);
                    parameter.Add("@ngaysinh", ngaysinh);
                    parameter.Add("@gioitinh", ekycmodel.gioitinh);
                    parameter.Add("@cmnd", ekycmodel.cmnd);
                    parameter.Add("@ngaycapcmnd", ngaycapcmnd);
                    parameter.Add("@noicap", ekycmodel.noicap);
                    parameter.Add("@sdt", ekycmodel.Phone_01);
                    parameter.Add("@email", ekycmodel.email);
                    parameter.Add("@diachi", ekycmodel.diachi);
                    parameter.Add("@saleid", saleid);
                    parameter.Add("@branch_id", UtilTvsi.ReturnBrandID(saleid.Substring(0, 4)));
                    parameter.Add("@branch_name", UtilTvsi.ReturnBrandName(saleid.Substring(0, 4)));
                    parameter.Add("@tvsi_nguoi_dai_dien", manName);
                    parameter.Add("@tvsi_chuc_vu", postionName);
                    parameter.Add("@tvsi_giay_uq", manNo);
                    parameter.Add("@tvsi_ngay_uq", manDate);
                    parameter.Add("@yeucau", ekycmodel.Note ?? "");
                    parameter.Add("@phuong_thuc_gd", ekycmodel.TradeMethod);
                    parameter.Add("@phuong_thuc_nhan_kqgd", ekycmodel.TradeResult);
                    parameter.Add("@phuong_thuc_sao_ke", ekycmodel.MonthlyReport);
                    parameter.Add("@dv_chuyen_tien", transferType);
                    parameter.Add("@so_dien_thoai_02", ekycmodel.Phone_02);
                    parameter.Add("@chu_tai_khoan_nh_01", ekycmodel.BankAccName_01);
                    parameter.Add("@so_tai_khoan_nh_01", ekycmodel.BankAccNo_01);
                    parameter.Add("@ma_ngan_hang_01", ekycmodel.BankNo_01);
                    parameter.Add("@ngan_hang_01", ekycmodel.BankName_01);
                    parameter.Add("@chi_nhanh_nh_01", ekycmodel.SubBranchName_01);
                    parameter.Add("@ma_chi_nhanh_01", ekycmodel.SubBranchNo_01);
                    parameter.Add("@tinh_tp_nh_01", ekycmodel.City_01);
                    parameter.Add("@chu_tai_khoan_nh_02", ekycmodel.BankAccName_02);
                    parameter.Add("@so_tai_khoan_nh_02", ekycmodel.BankAccNo_02);
                    parameter.Add("@ngan_hang_02", ekycmodel.BankName_02);
                    parameter.Add("@ma_ngan_hang_02", ekycmodel.BankNo_02);
                    parameter.Add("@ma_chi_nhanh_02", ekycmodel.SubBranchNo_02);
                    parameter.Add("@chi_nhanh_nh_02", ekycmodel.SubBranchName_02);
                    parameter.Add("@tinh_tp_nh_02", ekycmodel.City_02);
                    parameter.Add("@kenh_trao_doi", ekycmodel.RegistChannelInfo);
                    parameter.Add("@phuong_thuc_kenh_td", ekycmodel.ChannelInfoMethod);
                    parameter.Add("@confirmCode", confirmCode);
                    parameter.Add("@confirmSms", confirmSms);
                    parameter.Add("@fatca_kh_usa", ekycmodel.CustomerUSA);
                    parameter.Add("@fatca_noi_sinh_usa", ekycmodel.PlaceOfBirthUSA);
                    parameter.Add("@fatca_noi_cu_tru_usa", ekycmodel.DepositoryUSA);
                    parameter.Add("@fatca_sdt_usa", ekycmodel.CustomerPhoneNumberUSA);
                    parameter.Add("@fatca_lenh_ck_usa", ekycmodel.CustomerTransferBankUSA);
                    parameter.Add("@fatca_giay_uy_quyen_usa", ekycmodel.CustomerAuthorizationUSA);
                    parameter.Add("@fatca_dia_chi_nhan_thu_usa", ekycmodel.CustomerReceiveMailUSA);
                    parameter.Add("@marginAccount", ekycmodel.MarginAccount);
                    parameter.Add("@confirmOTP", confirmOtp);
                    parameter.Add("@manCardID", manCardID);
                    parameter.Add("@manIssuePlace", manIssuePlace);
                    parameter.Add("@manIssueDate", manIssueDate);
                    parameter.Add("@dangkyid", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    await conn.ExecuteAsync("TVSI_sDANG_KY_MO_TAI_KHOAN_EKYC", parameter,
                        commandType: CommandType.StoredProcedure);
                    regisID = parameter.Get<int>("@dangkyid");
                }


                if (regisID > 0)
                {
                    var path = Request.Url.GetLeftPart(UriPartial.Path).Replace(Request.Url.LocalPath, CommonConstant.Path);
                    //Send email xac thuc cho khach hang
                    UtilTvsi.SendEmailOpenEKYC(ekycmodel.hoten, ekycmodel.Phone_01, ekycmodel.email, confirmCode,
                        confirmSms, path);   
                }

                //Gui mail cho sale lọc từ luồng
                if (!saleCheck)
                {

                    string emailList = ConfigurationManager.AppSettings["SendEmail"];
                    string emailBranch = ConfigurationManager.AppSettings["SendEmailBranchID"];
                    var emailBranchID = emailBranch.Split(',');
                    var saleName = "";
                    var department = "";

                    if (ekycmodel.QA1txt == 1)
                    {
                        saleName = "Phòng Dịch Vụ Tư Vấn Giao Dịch";
                        department = "DVGD";
                    }

                    if (ekycmodel.QA1txt == 2)
                    {
                        saleName = "Phòng Dịch Vụ Đầu Tư";
                        department = "DVĐT";
                    }

                    if (ekycmodel.QA1txt == 3)
                    {
                        saleName = "TVSI-CĐGD";
                        department = "CĐGD";
                    }

                    var item = emailList.Split(',');
                    var emailSubject = CommonConstant.EmailSubject + Website.Txt_RegOpenAccount + "";
                    if (ekycmodel.QA1txt == 3)
                    {
                        if (item != null && item.Length > 0)
                        {
                            foreach (var email in item)
                            {
                                UtilTvsi.SendMailToSaleEKYC("TVSI-CĐGD",ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                    ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                    ,ekycmodel.yeucau,"eKYC Web","CĐGD",emailSubject,email);   
                            }
                        }  
                    }
                    else
                    {
                        if (emailBranchID.Length > 0)
                        {
                            if (branchID == UtilsBranchID.DVGDHS)
                            {
                                UtilTvsi.SendMailToSaleEKYC(saleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                    ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                    ,ekycmodel.yeucau,"eKYC Web",department,emailSubject,emailBranchID[0]);   
                            }  
                            if (branchID == UtilsBranchID.DVGDHCM)
                            {
                                UtilTvsi.SendMailToSaleEKYC(saleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                    ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                    ,ekycmodel.yeucau,"eKYC Web",department,emailSubject,emailBranchID[1]);   
                            } 
                            if (branchID == UtilsBranchID.DVDTHCM)
                            {
                                UtilTvsi.SendMailToSaleEKYC(saleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                    ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                    ,ekycmodel.yeucau,"eKYC Web",department,emailSubject,emailBranchID[2]);   
                            } 
                            if (branchID == UtilsBranchID.DVDTHS)
                            {
                                UtilTvsi.SendMailToSaleEKYC(saleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                    ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                    ,ekycmodel.yeucau,"eKYC Web",department,emailSubject,emailBranchID[3]);   
                            }


                        }

                        var saleInfo = UtilTvsi.GetEmailSale(ekycmodel.sale_id.Substring(0, 4));
                        if (!string.IsNullOrEmpty(saleInfo.Email) && saleInfo != null)
                        {
                            UtilTvsi.SendMailToSaleEKYC(saleInfo.SaleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                                ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                                ,ekycmodel.yeucau,"eKYC Web",department,emailSubject,saleInfo.Email);
                        }    
                        
                    }
                    
                }
                else
                {
                    var saleInfo = UtilTvsi.GetEmailSale(ekycmodel.sale_id.Substring(0, 4));
                    var departmentName = UtilTvsi.ReturnBrandName(saleid.Substring(0, 4)) ?? "";
                    //gui email cho sale
                    if (!string.IsNullOrEmpty(saleInfo.Email) && saleInfo != null)
                    {
                        var emailSubject = CommonConstant.EmailSubject + Website.Txt_RegOpenAccount + "";
                        UtilTvsi.SendMailToSaleEKYC(saleInfo.SaleName,ekycmodel.hoten,ekycmodel.ngaysinh,ekycmodel.cmnd
                            ,ekycmodel.ngaycapcmnd,ekycmodel.Phone_01,ekycmodel.email,ekycmodel.diachi,ekycmodel.QA1txt
                            ,ekycmodel.yeucau,"eKYC Web",departmentName,emailSubject,saleInfo.Email);
                    }
                }
                

                if (!Directory.Exists(Server.MapPath(CommonConstant.FolderKYC + regisID + "")))
                    Directory.CreateDirectory(Server.MapPath(CommonConstant.FolderKYC + regisID + "/"));
                var fullpath1 = Server.MapPath(CommonConstant.FolderKYC + regisID + "/");
                var sourceImageF = Session["imagefront"];
                var sourceImageB = Session["imageback"];
                var sourceImageFace = Session["path_image_face"];
                System.IO.File.Copy(Convert.ToString(sourceImageF),
                    fullpath1 + regisID + CommonConstant.Front + ".jpg");
                System.IO.File.Copy(Convert.ToString(sourceImageB), fullpath1 + regisID + CommonConstant.Back + ".jpg");
                System.IO.File.Copy(Convert.ToString(sourceImageFace),
                    fullpath1 + regisID + CommonConstant.Face + ".jpg");
                string pathPicFront = regisID + CommonConstant.Front + ".jpg";
                string pathPicBack = regisID + CommonConstant.Back + ".jpg";
                string pathPicFace = regisID + CommonConstant.Face + ".jpg";
                using (var conn = new SqlConnection(crmSTR))
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@dang_kyid", regisID);
                    parameters.Add("@file_front", pathPicFront);
                    parameters.Add("@file_back", pathPicBack);
                    parameters.Add("@file_face", pathPicFace);
                    var result = await conn.ExecuteAsync("TVSI_sThem_Anh_Ekyc", parameters,
                        commandType: CommandType.StoredProcedure);
                    if (result == 0)
                    {
                        ViewBag.Message = "Có lỗi xảy ra vui lòng thao tác lại từ đầu.";
                        return View(ekycmodel);
                    }
                }

                System.IO.File.Delete(Convert.ToString(sourceImageF));
                System.IO.File.Delete(Convert.ToString(sourceImageB));
                System.IO.File.Delete(Convert.ToString(sourceImageFace));
                Session["check_canhan_ekyc"] = true;
                Session["email_ekyc"] = ekycmodel.email;
                return RedirectToAction("DangKyKhCaNhanEkyc", "Home");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("DangkyKhCanhanEkyc - {0}", ex.Message));
                Logger.Error(typeof(HomeController), string.Format("DangkyKhCanhanEkyc - {0}", ex.InnerException));
                this.Session["check_canhan_ekyc"] = false;
                ViewBag.Message = "Đăng ký tài khoản không thành công";
                return View(ekycmodel);
            }
        }

        [HttpPost]
        public ActionResult ResendCodeOTPeKYC(string phone)
        {
            try
            {
                if (phone != null)
                {
                    Session[UtilsConstant.Phone] = UtilTvsi.EncriptMd5(phone + UtilTvsi.RandomNumber(6));
                    var otpCode = UtilTvsi.GenerateTOTP(Convert.ToString(Session[UtilsConstant.Phone]), DateTime.Now,
                        120, 6);

                    if (otpCode != null)
                    {
                        string smsMessage = ConfigurationManager.AppSettings["SMSMessageeKYC"];
                        if (UtilTvsi.QueueSMSOTP(phone, otpCode + smsMessage, 0))
                            return Json(new
                            {
                                data = otpCode,
                                Status = true,
                                type = 0
                            }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new {Status = false, message = "Không thành công vui lòng thử lại", type = 1},
                        JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, message = "Không thành công vui lòng thử lại", type = 1},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("ReSendCodeOTP - " + ex.Message));
                return Json(new {Status = false, message = "Lỗi hệ thống", type = 99}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ConfirmOTPeKYC(string confirmSms, string phone, string cardId)
        {
            try
            {
                var otpvalid = UtilTvsi.VerifyTOTP(confirmSms, DateTime.Now,
                    Convert.ToString(Session[UtilsConstant.Phone]), 120, 6);
                
                Logger.Info(typeof(HomeController), string.Format(" ConfirmOTPeKYC mã OTP {0}", confirmSms));
                if (otpvalid)
                {
                    return Json(new
                    {
                        data = true,
                        Status = true,
                        type = 0
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, message = "Mã OTP không đúng. Vui lòng nhập lại", type = 1},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("ConfirmOTP - " + ex.Message));
                return Json(new {Status = false, message = "Lỗi hệ thống", type = 99}, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CheckCaptCha(string captcha)
        {
            try
            {
                string kytubaomatcn = captcha;
                if (Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    return Json(
                        new {status = false, message = "Vui lòng nhập lại ký tự bảo mật do đã hết phiên làm việc."},
                        JsonRequestBehavior.DenyGet);
                }

                if (Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    return Json(new {status = false, message = "Ký tự bảo mật không đúng, vui lòng nhập lại."},
                        JsonRequestBehavior.DenyGet);
                }

                return Json(new {status = true});
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message
                }, JsonRequestBehavior.DenyGet);
            }
        }

        [HttpPost]
        public ActionResult GenerateOTP(string phone)
        {
            try
            {
                Session[UtilsConstant.Phone] = UtilTvsi.EncriptMd5(phone + UtilTvsi.RandomNumber(6));
                var otp = UtilTvsi.GenerateTOTP(Convert.ToString(Session[UtilsConstant.Phone]), DateTime.Now, 120, 6);
                
                Logger.Info(typeof(HomeController), "GenerateOTP" + otp);
                if (!string.IsNullOrEmpty(otp))
                {
                    string smsMessage = ConfigurationManager.AppSettings["SMSMessageeKYC"];
                    if (UtilTvsi.QueueSMSOTP(phone, otp + smsMessage, 0))
                        return Json(new
                        {
                            data = otp,
                            status = true,
                            type = 0
                        }, JsonRequestBehavior.AllowGet);
                }

                return Json(new {status = false, message = "Không thành công vui lòng thử lại", type = 1},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("ConfirmOTPJQuery - " + ex.Message));
                Logger.Error(typeof(HomeController), ex.Message);
                Logger.Error(typeof(HomeController), ex.InnerException);
                return Json(new {status = false, message = "Không thành công"}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DateTimeIsValidBirthDay(string datetime)
        {
            try
            {
                if (UtilTvsi.isValidDateTime(datetime) && datetime.Length >= 10)
                {
                    DateTime dateOfBirth =
                        DateTime.ParseExact(datetime, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var validDateOfBirth = UtilTvsi.ValidDateOfBirth(dateOfBirth);

                    if (!string.IsNullOrEmpty(validDateOfBirth))
                    {
                        return Json(new {status = false, message = validDateOfBirth, type = 1},
                            JsonRequestBehavior.AllowGet);
                    }

                    if (UtilTvsi.CheckUnderAge18(datetime))
                    {
                        return Json(new
                        {
                            data = true,
                            status = true,
                            type = 0
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new {status = false, message = "Ngày sinh không hợp lệ", type = 1},
                        JsonRequestBehavior.AllowGet);
                }

                return Json(new {status = false, message = "Định dạng ngày sinh vui lòng nhập lại", type = 1},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("DateTimeIsValidBirthDay - " + ex.Message));
                return Json(new {status = false, message = "Có lỗi xảy ra 1"}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DateTimeIsValidIssueDate(string dateIssue, string dateBirth, string cardID, int type)
        {
            try
            {
                if (UtilTvsi.isValidDateTime(dateIssue) && dateIssue.Length >= 10)
                {
                    if (!UtilTvsi.isValidDateTime(dateIssue))
                    {
                        return Json(new {status = false, message = "Giấy tờ không hợp lệ", type = 1},
                            JsonRequestBehavior.AllowGet);
                    }
                    var issueDate = DateTime.ParseExact(dateIssue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    if (issueDate.Date > DateTime.Now.Date)
                    {
                        return Json(new {status = false, message = "Giấy tờ không hợp lệ", type = 1},
                            JsonRequestBehavior.AllowGet);
                    }
                    var checkIssueDate = UtilTvsi.CheckIssueDateEKYC(cardID, type, issueDate);
                    if (!checkIssueDate)
                    {
                        return Json(new {status = false, message = "Giấy tờ hết hiệu lực", type = 1},
                            JsonRequestBehavior.AllowGet);
                    }

                    return Json(new
                    {
                        data = true,
                        status = true,
                        type = 0
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new {status = false, message = "Định dạng giấy tờ vui lòng nhập lại", type = 1},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("DateTimeIsValidIssueDate - " + ex.Message));
                return Json(new {status = false, message = "Có lỗi xảy ra 2"}, JsonRequestBehavior.AllowGet);
            }
        }

        public void IdTransactionDrop()
        {
            string[] IdBranchTransaction = ConfigurationManager.AppSettings["IdBranchTransaction"].Split(',');
            var addBranch = new Dictionary<string, string>();
            var branchMerge_1 = new Dictionary<string, string>();
            for (int i = 0; i < IdBranchTransaction.Length; i++)
            {
                if (IdBranchTransaction[i] == IdBranchTransaction[0])
                {
                    addBranch.Add("Hà Nội", IdBranchTransaction[i]);
                }
                else if (IdBranchTransaction[i] == IdBranchTransaction[1])
                {
                    addBranch.Add("Đà Nẵng", IdBranchTransaction[i]);
                }
                else
                {
                    addBranch.Add("Hồ Chí Minh", IdBranchTransaction[i]);
                }
            }

            var branchMerge = addBranch.Select(item => new SelectListItem
                {Value = item.Value.ToString(), Text = item.Key});

            branchMerge_1.Add("1268", "Hà Nội");
            branchMerge_1.Add("5007", "Đà Nẵng");
            branchMerge_1.Add("2111", "Hồ Chí Minh");
            Session["branchmerge"] = branchMerge;

            ViewBag.Branch = Session["branchmerge"];
            ViewBag.Branch = branchMerge;
            ViewData["Branch"] = branchMerge;
            ViewBag.Branch1 = branchMerge_1;
        }

        public void IdTransactionDropCustom()
        {
            string[] IdBranchTransaction = ConfigurationManager.AppSettings["IdBranchTransaction_CS"].Split(',');
            var addBranch = new Dictionary<string, string>();
            var branchMerge_1 = new Dictionary<string, string>();
            for (int i = 0; i < IdBranchTransaction.Length; i++)
            {
                if (IdBranchTransaction[i] == IdBranchTransaction[0])
                {
                    addBranch.Add("Hà Nội", IdBranchTransaction[i]);
                }
                else if (IdBranchTransaction[i] == IdBranchTransaction[1])
                {
                    addBranch.Add("Đà Nẵng", IdBranchTransaction[i]);
                }
                else
                {
                    addBranch.Add("Hồ Chí Minh", IdBranchTransaction[2]);
                }
            }

            var branchMerge = addBranch.Select(item => new SelectListItem
                {Value = item.Value.ToString(), Text = item.Key});

            branchMerge_1.Add("1268", "Hà Nội");
            branchMerge_1.Add("5007", "Đà Nẵng");
            branchMerge_1.Add("2111", "Hồ Chí Minh");
            Session["branchmerge"] = branchMerge;

            ViewBag.Branch = Session["branchmerge"];
            ViewBag.Branch = branchMerge;
            ViewData["Branch"] = branchMerge;
            ViewBag.Branch1 = branchMerge_1;
        }


        public void ExportDataVSDVN()
        {
            try
            {
                var datavsdvn = new Dictionary<string, string>();
                if (!Directory.Exists(Server.MapPath("~/TTLK/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/TTLK/"));
                }

                var pathxmlvsd = Server.MapPath("~/TTLK/Danh_sach_tv_luu_ky.xml");
                if (System.IO.File.Exists(pathxmlvsd))
                {
                    var ttlkvsd = UtilTvsi.ReadFileXmlVSD(pathxmlvsd);
                    for (int i = 0; i < ttlkvsd.Count; i++)
                    {
                        datavsdvn.Add(ttlkvsd[i].Ten_tieng_anh, ttlkvsd[i].Ten_tieng_anh);
                    }

                    var test = datavsdvn;
                    var datavsdviewbag = datavsdvn.Select(item => new SelectListItem
                        {Value = item.Key.ToString(), Text = item.Value});
                    ViewBag.datavsdvn = datavsdviewbag;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("ExportDataVSDVN - " + ex.Message));
            }
        }

        public void ExportDataCountryCode()
        {
            try
            {
                var datavsdvn = new Dictionary<string, string>();
                if (!Directory.Exists(Server.MapPath("~/TTLK/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/TTLK/"));
                }

                var pathxmlcountrycode = Server.MapPath("~/TTLK/country_code.xml");
                if (System.IO.File.Exists(pathxmlcountrycode))
                {
                    var countrycodelist = UtilTvsi.ReadFileCountryCode(pathxmlcountrycode);
                    for (int i = 0; i < countrycodelist.Count; i++)
                    {
                        datavsdvn.Add(countrycodelist[i].namecountry, countrycodelist[i].namecountry);
                    }

                    var datacountryviewbag = datavsdvn.Select(item => new SelectListItem
                        {Value = item.Key.ToString(), Text = item.Value});
                    ViewBag.datacountrycode = datacountryviewbag;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("ExportDataXMLCountryCode - " + ex.Message));
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(string imageFront, string imageBack)
        {
            try
            {
                //Kiểm tra file upload có tồn tại không
                // Xử lý code
                byte[] byte1 = Convert.FromBase64String(imageFront);
                byte[] byte2 = Convert.FromBase64String(imageBack);

                var randomFile = Guid.NewGuid().ToString().Substring(0, 8) + ".png";
                if (!Directory.Exists(Server.MapPath("~/Ekyc/CMND/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Ekyc/CMND/"));
                }

                var fullpath1 = Server.MapPath("~/Ekyc/CMND/webcam_front_") + randomFile;
                var fullpath2 = Server.MapPath("~/Ekyc/CMND/webcam_back_") + randomFile;
                System.IO.File.WriteAllBytes(fullpath1, byte1);
                System.IO.File.WriteAllBytes(fullpath2, byte2);

                string url_front = Url.Content("~/Ekyc/CMND/webcam_front_" + randomFile);
                string url_back = Url.Content("~/Ekyc/CMND/webcam_back_" + randomFile);
                string path_pic_front = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + url_front;
                string path_pic_back = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + url_back;


                // Upload CMND mat truoc
                var hashImageFront = await EkycWebService.GetHashUploadImageEkyc(fullpath1);
                var hashImageBack = await EkycWebService.GetHashUploadImageEkyc(fullpath2);
                if (!string.IsNullOrEmpty(hashImageFront) && !string.IsNullOrEmpty(hashImageBack))
                {
                    Session["hashimagefront"] = hashImageFront;
                    Logger.Info(typeof(HomeController), "FullPath1=" + fullpath1 + ";HashFront=" + hashImageFront);
                    Logger.Info(typeof(HomeController), "FullPath2=" + fullpath2 + ";HashBack=" + hashImageFront);
                    // Check CMND that gia
                    var checklivenessfront = await EkycWebService.CheckRealFake(hashImageFront);

                    // Check CMND mat trươc => OK => Check CMND mat sau   
                    if (checklivenessfront.Equals("success"))
                    {
                        //var checklivenessback = await EkycWebService.CheckRealFake(hashImageBack);

                        //if (checklivenessback == "success")
                        //{
                        var check_front = await EkycWebService.CheckEKYC(hashImageFront);

                        if (check_front >= 0)
                        {
                            // Task<JsonResult> customerInfo = EkycWebService.GetIDEKYC(hashImageFront, hashImageBack, check_front);
                            var customerInfo =
                                await EkycWebService.GetEkycFrontAndBack(hashImageFront, hashImageBack, check_front);
                            /*if (customerInfo != null)
                            {
                                if (customerInfo.@object.post_code.Count >= 0)
                                    Session["ProvinceCode"] = customerInfo.@object.post_code[0].city[0];
                            }*/
                            // get 2 mặt CMT/CCCD
                            if (customerInfo == null)
                            {
                                System.IO.File.Delete(fullpath1);
                                System.IO.File.Delete(fullpath2);

                                //return Json(new { Status = false, message = "CMT/CCCD không hợp lệ, vui lòng tải lên đúng CMT/CCCD 2 mặt.",type = 0});
                                return Json(new
                                {
                                    Status = false,
                                    message = @Website.Txt_NotShowCardID,
                                        type = 2
                                });
                            }

                            // Get các case Api không cho phép vượt qua 
                            var checkTampering = customerInfo.@object.tampering.warning;
                            List<string> checkTamperingString = checkTampering.Select(s => (string) s).ToList();

                            if (checkTamperingString.Count > 0)
                            {
                                // Các case khai báo để so sánh với checkTamperingString
                                string idInvalidLength = "id_invalid_length";
                                string isSuaXoa = "id_sua_xoa";
                                string idKhongHopLe = "id_ko_hop_le";
                                string idDobKoKhop = "id_dob_ko_khop";
                                string invalidDob = "invalid_dob";
                                string denTrang = "den_trang_ko_hop_le";
                                string fakePrinting = "fake_printing";

                                // List các case add vào list.
                                List<string> casevalid = new List<string>();
                                casevalid.Add(idInvalidLength);
                                casevalid.Add(isSuaXoa);
                                casevalid.Add(idKhongHopLe);
                                casevalid.Add(idDobKoKhop);
                                casevalid.Add(invalidDob);
                                casevalid.Add(denTrang);
                                casevalid.Add(fakePrinting);


                                var compareTamperingToCase = false;
                                //So sánh nếu compareTamperingToCase == true thì trả về Status False
                                for (int i = 0; i < checkTamperingString.Count; i++)
                                {
                                    for (int j = 0; j < casevalid.Count; j++)
                                    {
                                        if (casevalid[j].Equals(checkTamperingString[i]))
                                        {
                                            compareTamperingToCase = true;
                                            break;
                                        }
                                    }
                                }

                                if (compareTamperingToCase)
                                {
                                    System.IO.File.Delete(fullpath1);
                                    System.IO.File.Delete(fullpath2);
                                    return Json(new
                                    {
                                        Status = false,
                                        message = @Website.Txt_NotShowCardID,
                                        type = 2
                                    });
                                }
                            }

                            var cardId = customerInfo.@object.id;
                            if (!UtilTvsi.CheckUnderAge18(customerInfo.@object.birth_day))
                            {
                                return Json(new
                                {
                                    Status = false,
                                    message = @Website.TxtRuleSecurities,
                                    type = 0
                                });
                            }

                            if (!string.IsNullOrEmpty(customerInfo.@object.valid_date) &&
                                customerInfo.@object.valid_date != "-" &&
                                !customerInfo.@object.valid_date.Contains("Không thời hạn"))
                            {
                                if (!UtilTvsi.CheckValidDate(customerInfo.@object.valid_date))
                                {
                                    return Json(new
                                    {
                                        Status = false,
                                        message = @Website.TxtRuleSecurities,
                                        type = 0
                                    });
                                }
                            }

                            if (!UtilTvsi.IsValidCardId15Year(customerInfo.@object.issue_date))
                            {
                                return Json(new
                                {
                                    Status = false,
                                    message = @Website.TxtRuleSecurities,
                                    type = 0
                                });
                            }

                            var checkForm = UtilTvsi.CheckCustomerForm(cardId);
                            if (checkForm != 0)
                            {
                                var accountNumber = UtilTvsi.GetNumberAccount(cardId);
                                System.IO.File.Delete(fullpath1);
                                System.IO.File.Delete(fullpath2);
                                if (checkForm == 1)
                                {
                                    return Json(new {Status = false, message =  @Website.Txt_ShowCardID  +accountNumber, type = 3});
                                }
                                return Json(new {Status = false, message = @Website.Txt_ShowAccountComp, type = 4});
                            }
                            var checkFirm = UtilTvsi.CheckCustomerEKYC(cardId);
                            if (!checkFirm)
                            {
                                var accountNumber = UtilTvsi.GetNumberAccount(cardId);
                                if (!string.IsNullOrEmpty(accountNumber))
                                {
                                    return Json(new {Status = false, message =  @Website.Txt_ShowCardID  +accountNumber, type = 3});
                                }
                                return Json(new {Status = false, message = Website.Txt_TradingNow, type = 5});
                            }
                            var accountNo = UtilTvsi.GetNumberAccount(cardId);
                            if (!string.IsNullOrEmpty(accountNo))
                            {
                                return Json(new {Status = false, message =  @Website.Txt_ShowCardID  + accountNo, type = 3});
                            }


                            /*
                            if (!UtilTvsi.ExistsCMNDinCRM(cardId))
                            {
                                System.IO.File.Delete(fullpath1);
                                System.IO.File.Delete(fullpath2);
                                return Json(new {Status = false, message = "CMT/CCCD đã tồn tại.", type = 0});
                            }*/

                            Session["idfront"] = cardId;
                            Session["imagefront"] = fullpath1;
                            Session["imageback"] = fullpath2;
                            var issuePlace = customerInfo.@object.issue_place;
                            var cardID = customerInfo.@object.id;
                            var valuePlace = UtilTvsi.GetValueIssuePlace(cardID, issuePlace);
                            customerInfo.@object.valuePlace = valuePlace;

                            if (!customerInfo.@object.valid_date.Contains("Không thời hạn"))
                                customerInfo.@object.valid_date = "31/12/9999";


                            var result = new
                            {
                                dataSessionFront = Session["imagefront"],
                                dataSessionBack = Session["imageback"],
                                DataEkyc = customerInfo
                            };
                            return Json(new {Status = true, Data = result}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        System.IO.File.Delete(fullpath1);
                        System.IO.File.Delete(fullpath2);
                        //return Json(new { Status = false, message = "Ảnh CMT/CCCD mặt trước không đúng", type = 1 }, JsonRequestBehavior.AllowGet);
                        return Json(
                            new
                            {
                                Status = false,
                                message = @Website.Txt_NotShowCardID,
                                    type = 2
                            }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new {Status = false, message = "", type = 0}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), ex.Message);
                Logger.Error(typeof(HomeController), ex.InnerException);
                return Json(new {Status = false, message = ""}, JsonRequestBehavior.AllowGet);
            }
        }


        public async Task<JsonResult> UploadImageFace(string imageData)
        {
            try
            {
                string session = DateTime.Now.ToString().Replace("/", "-").Replace(" ", "_").Replace(":", "") + ".jpg";

                if (!Directory.Exists(Server.MapPath("~/Ekyc/Face/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Ekyc/Face/"));
                }

                string filename = Server.MapPath("~/Ekyc/Face/webcam_") + session;
                string url = Url.Content("~/Ekyc/Face/webcam_" + session);
                string path = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority) + url;
                using (FileStream fs = new FileStream(filename, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        byte[] data = Convert.FromBase64String(imageData);
                        bw.Write(data);
                        bw.Close();
                    }
                }

                Session["image"] = session;
                Session["path"] = path;
                Session["path_image_face"] = filename;

                var hashFace = await EkycWebService.GetHashFace(filename);
                var hashImageFront = Session["hashimagefront"];

                // duong dan path image FRONT AND BACK
                var imgfront = Session["imagefront"];
                var imgback = Session["imageback"];

                if (!string.IsNullOrEmpty(Convert.ToString(hashImageFront)))
                {
                    Logger.Info(typeof(HomeController), "url=" + url + ";HashFace=" + hashFace);

                    var checkFaceLiveness = await EkycWebService.CheckFaceLiveness(hashFace);
                    Logger.Info(typeof(HomeController), "checkFaceLiveness=" + checkFaceLiveness);
                    if (checkFaceLiveness == "success")
                    {
                        var eKYCtoFace = await EkycWebService.CheckFaceToEkyc(hashImageFront, hashFace);

                        var result = new
                        {
                            DataImageFace = path,
                            DataEkyc = eKYCtoFace
                        };
                        if (eKYCtoFace.@object.prob < 80)
                        {
                            System.IO.File.Delete(filename);
                            System.IO.File.Delete(Convert.ToString(imgfront));
                            System.IO.File.Delete(Convert.ToString(imgback));
                        }

                        return Json(new {Status = true, Data = result}, JsonRequestBehavior.AllowGet);
                    }

                    System.IO.File.Delete(filename);
                    return Json(new
                        {Status = false, message = "Ảnh chân dung không phải là người thật, vui lòng thử lại."});
                }

                System.IO.File.Delete(filename);
                return Json(new {Status = false, message = "Có lỗi xảy ra vui lòng thử lại"});
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message);
                Logger.Error(typeof(HomeController), e.InnerException);
                return Json(new {Status = false, message = "Lỗi vui lòng liên hệ bộ phận hỗ trợ"},
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public async Task<ActionResult> RegisterExperienceXtrade()
        {
            if (Session["accountNumber"] == null)
            {
                return RedirectToAction("Dangkydichvu", "Home", new
                {
                    id = "", @account = "",@xtrade = "register"
                });
            }
            var custcode = Session["accountNumber"].ToString();
            var check = await UtilTvsi.CheckCustCodeWhiteListExisted(custcode);
            if (!check)
                Session["AccountXtrade"] = true;
            else
                Session.Remove("AccountXtrade");


            return View();
        }    
        
        [HttpPost]
        public async Task<ActionResult> RegisterExperienceXtrade(string val = "")
        {
            try
            {
                if (Session["accountNumber"] == null)
                {
                    return RedirectToAction("Dangkydichvu", "Home", new
                    {
                        id = "", @account = "", @xtrade = "register"
                    });
                }

                var custCode = Session["accountNumber"].ToString();
                var check = await UtilTvsi.CheckCustCodeWhiteListExisted(custCode);
                if (check)
                {
                    var data = await UtilTvsi.InsertWhitelistXtrade(custCode);
                    if (data)
                    {
                        TempData["Success"] = true;
                        Session.Remove("AccountXtrade");
                    }
                }
                else
                {
                    TempData["AccountExisted"] = true;
                    Session.Remove("AccountXtrade");
                }

                return RedirectToAction("RegisterExperienceXtrade", "Home");
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController),"RegisterExperienceXtrade" + e.Message);
                Logger.Error(typeof(HomeController),"RegisterExperienceXtrade" + e.InnerException);
                return RedirectToAction("RegisterExperienceXtrade", "Home");
            }
        }
        

        [HttpPost]
        public ActionResult GetDistrictList(string provinceCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(provinceCode))
                {
                    var data = UtilTvsi.GetDistricList(provinceCode);
                    return Json(new {status = true, data = data}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {status = false}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message);
                Logger.Error(typeof(HomeController), e.InnerException);
                return Json(new {status = false}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetWardList(string districtCode)
        {
            try
            {
                if (!string.IsNullOrEmpty(districtCode))
                {
                    var data = UtilTvsi.GetWardList(districtCode);
                    if (data.Count() > 0)
                    {
                        return Json(new {status = true, data = data}, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new {status = false}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message);
                Logger.Error(typeof(HomeController), e.InnerException);
                return Json(new {status = false}, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult CheckIDSaleEMS(string saleid)
        {
            try
            {
                if (!string.IsNullOrEmpty(saleid))
                {
                    if (UtilTvsi.CheckSaleIDEMS(saleid))
                    {
                        return Json(new
                        {
                            status = true,
                            type = 0
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new {status = false, message = "Sale ID không tồn tại"}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {status = false, message = "Vui lòng nhập sale ID"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckIDSaleEMS - " + ex.Message));
                Logger.Error(typeof(HomeController), ex.Message);
                Logger.Error(typeof(HomeController), ex.InnerException);
                return Json(new {status = false, message = "Mã nhân viên tư vấn phải đủ 4 ký tự."},
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCustomerInfo(string custcode)
        {
            custcode = custcode.Remove(custcode.Length - 1);
            try
            {
                if (!string.IsNullOrEmpty(custcode))
                {
                    var user = UtilTvsi.GetCustomerInfo(custcode);
                    if (user != null)
                    {
                        return Json(new {status = 1, data = user}, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new {status = 2, message = "Không tìm thấy thông tin"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), ex.Message);
                Logger.Error(typeof(HomeController), ex.InnerException);
                return Json(new {status = 99, message = "Lỗi hệ thống !!!"}, JsonRequestBehavior.AllowGet);
            }
        }

        private void clearFolder(string FolderName)
        {
            DirectoryInfo dir = new DirectoryInfo(FolderName);

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.Delete();
            }

            foreach (DirectoryInfo di in dir.GetDirectories())
            {
                clearFolder(di.FullName);
                di.Delete();
            }
        }
    }
}