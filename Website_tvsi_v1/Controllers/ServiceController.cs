﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using TVSI.Utility;
using Website_tvsi_v1.Common;
using Website_tvsi_v1.Models;
using Website_tvsi_v1.WebService;
using Website_tvsi_v1.WebService.Model;

namespace Website_tvsi_v1.Controllers
{
    public class ServiceController : Controller
    {
                // Dang ky goi margin 
        [HttpGet]
        public async Task<ActionResult> RegisterService(string account,string xtrade)
        {
            try
            {
                ViewBag.url_website_v2 = ConfigurationManager.AppSettings["URL_WEBSITE_V2"];
                if (Session["accountNumber"] != null)
                {
                    if (!string.IsNullOrEmpty(xtrade))
                        return RedirectToAction("RegisterExperienceXtrade", "Home");
                    
                    if (!string.IsNullOrEmpty(account))
                    {
                        if(!Session["accountNumber"].ToString().Equals(account))
                            return LoguotMargin(account, "");
                    }
                    var accountNo = Session["accountNumber"];
                    var service = await UtilTvsi.GetServiceList(accountNo.ToString());
                    ViewBag.ServiceList = await UtilTvsi.GetServiceList(accountNo.ToString());
                    var textMessage = "";
                    if (service != null)
                    {
                        textMessage = service.Message;
                        ViewBag.IsConfirm = service.IsConfirm;
                    }
                    ViewBag.NamePackage = textMessage;
                    ViewBag.CourseList = await UtilTvsi.GetCourseList();
                    var listTVComboHist = await UtilTvsi.GetListTVCOMBPHist(accountNo.ToString());
                    ViewBag.TVCOMBOHist = listTVComboHist.Reverse().ToList();

                    var listCourseHist = await UtilTvsi.GetListCourseHist(accountNo.ToString());
                    ViewBag.TVCourselist = listCourseHist.Reverse().ToList();
                    if (accountNo != null)
                    {
                        Getgoidichvuid(Convert.ToString(accountNo));
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(account))
                    {
                        if (!string.IsNullOrEmpty(xtrade))
                            return RedirectToAction("RegisterExperienceXtrade", "Home");
                        TempData["CustCode"] = account;
                    }
                }
                if (Session["marginList"] != null)
                {
                    if (!string.IsNullOrEmpty(xtrade))
                    {
                        return RedirectToAction("RegisterExperienceXtrade", "Home");
                    }

                    if (Session["accountNumber"] != null)
                    {
                        var accountNo = Session["accountNumber"];
                        Getgoidichvuid(Convert.ToString(accountNo));
                    }
                    var marginlist = Session["marginList"] as List<MarginList>;

                    ViewBag.getHtmlPackage = marginlist;
                    ViewBag.getIntPackage = GetIntPackage();
                    ViewBag.getPPCurrent = GetPPPackageCurrent();
                    var listNamePackage = new List<string>();

                    if (marginlist != null)
                        for (int i = 0; i < marginlist.Count; i++)
                        {
                            listNamePackage.Add(marginlist[i].ten_goi_dich_vu);
                        }

                    ViewBag.GetGoiDichVuUuDai = Getgoidichvuuudai();
                    ViewBag.GetGoiDichVu =
                        GetListServicePackage(listNamePackage, Convert.ToString(Session["accountNumberCheck"]));
                }
                ViewBag.spdv_menu = "dropdown";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown";
                return View();
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("HomeControler - Dangkydichvu(get)" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }
        
        public ActionResult LoguotMargin(string custcode,string xtrade = "")
        {
            Session.Clear();
            return RedirectToAction("RegisterService", "Service", new
            {
                id = "", @account = custcode, xtrade = !string.IsNullOrEmpty(xtrade) ? "register":""
            });
        }
                [HttpGet]
        public ActionResult Getgoidichvuid(string id)
        {
            try
            {
                var historyPackage = new PackageWebService();
                var accountNumber = Session["accountNumber"];
                var check = historyPackage.GetHistoryPackage(id);
                var dateNow = DateTime.Now.ToString("dd/MM/yyyy").AsDateTime();
                var getGDVUudai = historyPackage.GetGDVUuDai(id);
                var getPackageServiceAccount = historyPackage.GetPackageServiceAccount(id);
                ViewBag.listPackage = check.MARGINList;
                var checkStatus = 1;
                var ttHoanThanh = 1;
                for (int i = 0; i < check.MARGINList.Count; i++)
                {
                    if (check.MARGINList[i].trang_thai == 0 || check.MARGINList[i].trang_thai == 5)
                    {
                        checkStatus = check.MARGINList[i].trang_thai;
                    }

                    if (check.MARGINList[i].trang_thai == 1 &&
                        Convert.ToDateTime(check.MARGINList[i].ngay_dang_ky).ToString("dd/MM/yyyy").AsDateTime() ==
                        dateNow)
                    {
                        ttHoanThanh = 2;
                    }
                }

                var checkStatus2 = 1;
                var ttHoanThanh2 = 1;
                for (int i = 0; i < getGDVUudai.MARGINList.Count; i++)
                {
                    if (getGDVUudai.MARGINList[i].trang_thai == 0 || getGDVUudai.MARGINList[i].trang_thai == 5)
                    {
                        checkStatus2 = getGDVUudai.MARGINList[i].trang_thai;
                    }

                    if (getGDVUudai.MARGINList[i].trang_thai == 1 &&
                        Convert.ToDateTime(getGDVUudai.MARGINList[i].ngay_dang_ky).ToString("dd/MM/yyyy")
                            .AsDateTime() == dateNow)
                    {
                        ttHoanThanh2 = 2;
                    }
                }

                ViewBag.TTHoanthanh = ttHoanThanh;
                ViewBag.TTHoanthanh2 = ttHoanThanh2;
                ViewBag.checkStatus = checkStatus;
                ViewBag.checkStatus2 = checkStatus2;
                ViewBag.listGDVUuDai = getGDVUudai.MARGINList;
                ViewBag.accountNumber = check.so_tai_khoan;
                ViewBag.servicePackgeCurrent = getPackageServiceAccount;
                return View("RegisterService");
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("HomeController - Getgoidichvuid" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return null;
            }
        }
        
        public string GetIntPackage()
        {
            try
            {
                var webService = new PackageWebService();
                var getIdSession = Session["accountNumberCheck"];
                var goihientai = webService.GetPackageCurrent(Convert.ToString(getIdSession));
                if (string.IsNullOrEmpty(goihientai.GOI_DICH_VU))
                {
                    goihientai.GOI_DICH_VU = "BASIC";
                }

                return goihientai.GOI_DICH_VU;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("Loi GetIntPackage " + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return null;
            }
        }
        public GetPackageCurrentModel.Root GetPPPackageCurrent()
        {
            try
            {
                var webService = new PackageWebService();
                var getIdSession = Session["accountNumberCheck"];
                var goihientai = webService.GetPackageCurrent(Convert.ToString(getIdSession));
                if (string.IsNullOrEmpty(Convert.ToString(goihientai)))
                {
                    return null;
                }

                if (string.IsNullOrEmpty(goihientai.SUC_MUA))
                {
                    goihientai.SUC_MUA = "0";
                }

                return goihientai;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController),
                    string.Format("HomeController->GetPPPackageCurrent -" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return null;
            }
        }
        
        public List<MARGINPACK> Getgoidichvuuudai()
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var getgoidichvuuuadai = packageWebService.GetgoidichvuuudaiRoot();
                return getgoidichvuuuadai.MARGINPACK;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }
        
        public List<GetServicePackage> GetListServicePackage(List<string> listnamepackage, string numberaccount)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                List<GetServicePackage> listServicePack = new List<GetServicePackage>();
                for (int i = 0; i < listnamepackage.Count; i++)
                {
                    listServicePack.Add(packageWebService.GetServicePackage(listnamepackage[i], numberaccount));
                }

                return listServicePack;
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("GetListServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra,vui lòng liên hệ bộ phận hỗ trợ.";
                return null;
            }
        }

        public ActionResult CheckPinServicePackage(string pin)
        {
            try
            {
                var id = Session["accountNumber"];
                if (id == null)
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var idUser = id;
                var pinUser = pin.Trim();

                // tí mở lại nhé
                if (UtilTvsi.GetSourceEkyc(id.ToString()))
                {
                    if (!UtilTvsi.GetStatusContractEkyc(id.ToString()))
                    {
                        return Json(new
                        {
                            Status = false,
                            Data =
                                "Tài khoản của Quý khách không đủ điều kiện để thực hiện thay đổi gói dịch vụ trực tuyến. Quý khách vui lòng liên hệ nhân viên tư vấn hoặc tổng đài 19001885 để được hỗ trợ hoàn thiện Hồ sơ và thực hiện đổi gói."
                        });
                    }
                }


                var checkAccount = UtilTvsi.CheckPinServicePackage(pinUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã PIN không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                if (checkAccount.TrangThai == 2)
                {
                    return Json(new
                    {
                        Status = false,
                        Data =
                            "Quý khách chưa mở tài khoản Margin, vui lòng liên hệ  nhân viên môi giới hoặc 19001885 để biết thêm chi tiết."
                    });
                }

                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public ActionResult CheckPinServiceTvCombo(string pin)
        {
            try
            {
                var id = Session["accountNumber"];
                if (id == null)
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var idUser = id;
                var pinUser = pin.Trim();


                var checkAccount = UtilTvsi.CheckPinServicePackage(pinUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã PIN không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                /*if (checkAccount.TrangThai == 2)
                {
                    return Json(new
                    {
                        Status = false,
                        Data = "Quý khách chưa mở tài khoản Margin, vui lòng liên hệ  nhân viên môi giới hoặc 19001885 để biết thêm chi tiết."
                    });
                }*/

                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), string.Format("CheckPinServicePackage" + e.Message));
                ViewBag.accountError = "Có lỗi xảy ra.";
                return Json(new {Status = false, Data = "Có lỗi xả ra,liên hệ bộ phận hỗ trợ."});
            }
        }

        public ActionResult CheckPassServicePackage(string pass, string stk)
        {
            var webservice = new PackageWebService();
            try

            {
                if (string.IsNullOrEmpty(stk) || stk == "undefined" || stk == "null")
                {
                    return Json(new {Status = false, Data = "Không tìm thấy số tài khoản, vui lòng thao tác lại."});
                }

                var id = stk;
                var idUser = id;
                var passUser = pass.Trim();
                var checkAccount = UtilTvsi.CheckPassServicePackage(passUser, Convert.ToString(idUser));
                if (checkAccount == null)
                {
                    return Json(new {Status = false, Data = "Mã khách hàng hoặc mật khẩu không chính xác."});
                }

                if (checkAccount.TrangThai == 0)
                {
                    return Json(new {Status = false, Data = "Đăng nhập không thành công."});
                }

                if (checkAccount.TrangThai == 2)
                {
                    
                }
                else
                {
                    Session["AcocuntMarginCheck"] = checkAccount.TrangThai;
                }

                Session["accountNumber"] = stk;
                var accountNumber = Convert.ToString(Session["accountNumber"]);

                if (!string.IsNullOrEmpty(accountNumber))
                    accountNumber += "6";

                Session["accountNumberCheck"] = accountNumber;
                Session["marginList"] = checkAccount.MarginList;
                Session["checkAccount"] = checkAccount;
                Session["accountName"] = checkAccount.HoTen;
                Session["accountNumber"] = checkAccount.SoTaiKhoan;
                return Json(new {Status = true, Data = checkAccount});
            }

            catch (Exception e)
            {
                var mess = e.Message;
                var inner = e.InnerException;
                Logger.Error(typeof(HomeController), string.Format("CheckPassServicePackage-" + mess));
                return Json(new
                {
                    Status = false,
                    Data = "Có lỗi xảy ra, vui lòng liên hệ bộ phận hỗ trợ."
                });
            }
        }
        
                [HttpPost]
        public async Task<ActionResult> InsertTVCombo(int serviceid)
        {
            try
            {
                var accountNo = Session["accountNumber"].ToString();
                if (string.IsNullOrEmpty(accountNo))
                {
                    return Json(new {Status = 99, Data = "Vui lòng đăng nhập lại!!!"}, JsonRequestBehavior.AllowGet);
                }

                var result = await UtilTvsi.InsertTVCombo(serviceid, accountNo);
                if (result.Code.Equals(TVComboConst.Success))
                {
                    return Json(new {Status = 1, Data = result.ErrorMessage}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = 2, Data = result.ErrorMessage}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("InsertTVCombo - 1 ") + ex.Message);
                return Json(new {Status = 99, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CancelTVCombo(int id)
        {
            try
            {
                var accountNo = Session["accountNumber"].ToString();
                if (string.IsNullOrEmpty(accountNo))
                {
                    return Json(new {Status = 99, Data = "Vui lòng đăng nhập lại!!!"}, JsonRequestBehavior.AllowGet);
                }


                if (id > 0)
                {
                    var result = await UtilTvsi.CancelRegisterService(id);
                    if (result)
                    {
                        var user = UtilTvsi.GetCustomerInfo(accountNo);
                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            var resultCombo = await UtilTvsi.GetInfoCombo(Convert.ToString(id));
                            if (resultCombo.Status == 99)
                                UtilTvsi.SendEmailCancelRegister(resultCombo.ServiceName, resultCombo.CustCode,
                                    resultCombo.CustName, resultCombo.CreatedDate,
                                    resultCombo.EffDate, resultCombo.ExpDate, user.Email);
                        }

                        return Json(new {Status = 1}, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new {Status = 2, Data = "Không thành công"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("CancelTVCombo - ex.Message - {0} ", ex.Message));
                return Json(new {Status = 99, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public async Task<ActionResult> SetPackageToAccount(string packAge, string gdvuudai, int? status)
        {
            try
            {
                if (string.IsNullOrEmpty(packAge))
                    packAge = "";
                if(packAge.Equals("undefined"))
                    packAge = "";
                if (packAge.Equals("null"))
                    packAge = "";
                
                if (string.IsNullOrEmpty(gdvuudai))
                    gdvuudai = "";
                if(gdvuudai.Equals("undefined"))
                    gdvuudai = "";
                if (gdvuudai.Equals("null"))
                    gdvuudai = "";
                
                var custcode = Session["accountNumber"].ToString();
                var webService = new PackageWebService();
                var so_tai_khoan_HS = Session["accountNumber"];
                var so_tai_khoan = Session["accountNumberCheck"];
                var ho_ten = Session["accountName"];

                var notePackAge = string.Empty;
                var notePackPrefer = string.Empty;
                var marginCurrent = webService.GetPackageCurrent(custcode);
                
                var marginCurrentShort = !string.IsNullOrEmpty(marginCurrent.GOI_DICH_VU)
                    ? marginCurrent.GOI_DICH_VU = "BASIC"
                    : marginCurrent.GOI_DICH_VU ;
                
                var valueFirst = await UtilTvsi.ChangeMarginGroup(custcode,marginCurrentShort,packAge,gdvuudai, 1);
                var valueSecond = await UtilTvsi.ChangeMarginGroup(custcode,  marginCurrentShort,packAge,gdvuudai, 2);
                
                if (!valueFirst.Status.Equals("000"))
                    notePackAge = valueFirst.Note;

                if (!valueSecond.Status.Equals("000"))
                    notePackPrefer = valueSecond.Note;

                var goi_hien_tai = webService.GetPackageCurrent(Convert.ToString(so_tai_khoan));
                var goi_hien_tai_uu_dai = webService.GetPackageServiceAccount(Convert.ToString(so_tai_khoan_HS));

                if (string.IsNullOrEmpty(goi_hien_tai_uu_dai))
                {
                    goi_hien_tai_uu_dai = "Chua dang ky";
                }

                if (string.IsNullOrEmpty(goi_hien_tai.GOI_DICH_VU))
                {
                    goi_hien_tai.GOI_DICH_VU = "BASIC";
                }

                var ngay_dang_ky = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var goi_thay_doi = packAge;
                var setDate = new DateTime();
                if (goi_thay_doi == "BASIC")
                {
                    setDate = (DateTime) SqlDateTime.MaxValue;
                }
                else
                {
                    setDate = (DateTime) SqlDateTime.MaxValue;
                }

                var ngay_ket_thuc = setDate.ToString("yyyy-MM-dd HH:mm:ss.fff");
                try
                {
                    var webservice = new PackageWebService();
                    if (status == 1)
                    {
                        var savegdvuudai = webservice.setGDVUuDai(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai_uu_dai, gdvuudai.ToString(),
                            Convert.ToString(so_tai_khoan_HS), notePackPrefer);
                        if (savegdvuudai == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    if (status == 2)
                    {
                        var savePackageMargin = webservice.setPackageService(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai.GOI_DICH_VU, goi_thay_doi,
                            Convert.ToString(so_tai_khoan_HS), notePackAge);
                        if (savePackageMargin == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    if (status == 3)
                    {
                        var savePackageMargin = webservice.setPackageService(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai.GOI_DICH_VU, goi_thay_doi,
                            Convert.ToString(so_tai_khoan_HS), notePackAge);

                        var savegdvuudai = webservice.setGDVUuDai(Convert.ToString(so_tai_khoan_HS),
                            Convert.ToString(ho_ten),
                            ngay_dang_ky, null, ngay_ket_thuc, goi_hien_tai_uu_dai, gdvuudai.ToString(),
                            Convert.ToString(so_tai_khoan_HS), notePackPrefer);
                        if (savePackageMargin == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ không thành công."});
                        }

                        if (savegdvuudai == null)
                        {
                            return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ ưu đãi không thành công."});
                        }

                        return Json(new
                            {Status = true, Data = "Chuyển đổi gói thành công, thông tin của bạn đã được gửi đi."});
                    }

                    return Json(new {Status = false, Data = "Chuyển đổi gói dịch vụ ưu đãi không thành công."});
                }
                catch (Exception ex)
                {
                    Logger.Error(typeof(HomeController),
                        string.Format("SetPackageToAccount {0} {1}", ex.Message, ex.InnerException));
                    return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), string.Format("SetPackageToAccount - 1 ") + ex.Message);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }
       [HttpPost]
        public async Task<ActionResult> ChangeMarginAccount(string packname, string packprefername, int? status)
        {
            try
            {
                if (string.IsNullOrEmpty(packname))
                    packname = "";
                if(packname.Equals("undefined"))
                    packname = "";
                if (packname.Equals("null"))
                    packname = "";
                
                if (string.IsNullOrEmpty(packprefername))
                    packprefername = "";
                if(packprefername.Equals("undefined"))
                    packprefername = "";
                if (packprefername.Equals("null"))
                    packprefername = "";
                
                


                
                var custcode = Session["accountNumber"].ToString();
                var webService = new PackageWebService();
                if (!string.IsNullOrEmpty(custcode))
                {
                    var marginCurrent = webService.GetPackageCurrent(custcode);
                    var marginCurrentShort = !string.IsNullOrEmpty(marginCurrent.GOI_DICH_VU)
                        ? marginCurrent.GOI_DICH_VU = "BASIC"
                        : marginCurrent.GOI_DICH_VU ;
                    if (!string.IsNullOrEmpty(packname) || !string.IsNullOrEmpty(packprefername))
                    {
                        if (status == 1)
                        {
                            var value = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,"",packprefername, 2);
                            if (!value.Status.Equals("000"))
                            {
                                if (value.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = value.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = value.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (status == 2)
                        {
                            var value = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname, packprefername,1);
                            if (!value.Status.Equals("000"))
                            {
                                if (value.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = value.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = value.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (status == 3)
                        {
                            var valueFirst = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname,packprefername, 1);
                            var valueSecond = await UtilTvsi.ChangeMarginGroup(custcode, marginCurrentShort,packname,packprefername, 2);
                            if (!valueSecond.Status.Equals("000"))
                            {
                                if (valueSecond.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = valueSecond.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = valueSecond.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                            if (!valueFirst.Status.Equals("000"))
                            {
                                if (valueFirst.IsConfirm == 1)
                                {
                                    return Json(new {Status = false, Data = valueFirst.Message, IsConfirm = 1}, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new {Status = false, Data = valueFirst.Message, IsConfirm = 0}, JsonRequestBehavior.AllowGet);
                            }
                        }
                        return Json(new {Status = true}, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new {Status = false, Data = "Vui lòng đăng nhập lại xin cám ơn!!!."},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController),
                    string.Format("Loi phuong thuc ChangeMarginAccount  - {0}", e.Message));
                return Json(new {Status = false, Data = "Có lỗi xảy ra"}, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public ActionResult UpdateDangKyGoiDichVu(string goidichvuid)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var value = packageWebService.UpdateDangKyGoiDichVu(goidichvuid, 5);
                if (value == "1")
                {
                    return Json(new {Status = true, Data = "Chuyển đổi thành công"}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, Data = "Chuyển đổi không thành công,vui lòng liên hệ bỗ phận hỗ trợ"},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message + e.InnerException);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateDangKyGoiDichVuUuDai(string goidichvuuudaiid)
        {
            try
            {
                PackageWebService packageWebService = new PackageWebService();
                var value = packageWebService.UpdateDangKyGoiDichVuUuDai(goidichvuuudaiid, 5);
                if (value == "1")
                {
                    return Json(new {Status = true, Data = "Chuyển đổi thành công"}, JsonRequestBehavior.AllowGet);
                }

                return Json(new {Status = false, Data = "Chuyển đổi không thành công,vui lòng liên hệ bỗ phận hỗ trợ"},
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Error(typeof(HomeController), e.Message + e.InnerException);
                return Json(new {Status = false, Data = "Có lỗi xảy ra."},
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> RegisterService(CourseModel model)
        {
            try
            {
                ViewBag.tab = "active";
                ViewBag.spdv_menu = "dropdown";
                ViewBag.hotro_menu = "dropdown";
                ViewBag.gttvsi_menu = "dropdown";

                if (string.IsNullOrEmpty(Session["accountNumber"].ToString()))
                    return LoguotMargin("","");


                var accountNo = Session["accountNumber"].ToString();
                var serviceResult = await UtilTvsi.InsertTVCombo(model.serviceid, accountNo);

                if (serviceResult.Code.Equals(TVComboConst.Success) && Convert.ToInt32(serviceResult.RegistID) > 0)
                {
                    if (UtilTvsi.FieldValidCourse(model.CourseID, model.CardID, model.Phone, model.Email,
                        model.FullName))
                    {
                        await UtilTvsi.InsertRegCourse(serviceResult.RegistID,
                            model.CourseID, 1, model.AccountNo, model.CardID, model.Phone, model.Email,
                            model.MonthCourse, model.TimeCourse, accountNo, model.FullName);
                    }

                    if (UtilTvsi.FieldValidCourse(model.CourseID_01, model.CardID_01, model.Phone_01, model.Email_01,
                        model.FullName_01))
                    {
                        await UtilTvsi.InsertRegCourse(serviceResult.RegistID,
                            model.CourseID_01, 2, model.AccountNo_01, model.CardID_01, model.Phone_01, model.Email_01,
                            model.MonthCourse_01, model.TimeCourse_01, accountNo, model.FullName_01);
                    }

                    //Gửi Mail cho khách hàng
                    var user = UtilTvsi.GetCustomerInfo(accountNo);
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        var resultCombo = await UtilTvsi.GetInfoCombo(serviceResult.RegistID);
                        if (resultCombo.Status == 1)
                            UtilTvsi.SendEmailRegister(resultCombo.ServiceName,
                                resultCombo.CustCode, resultCombo.CustName, resultCombo.CreatedDate,
                                resultCombo.EffDate, resultCombo.ExpDate, user.Email);
                    }

                    TempData["CourseSuccess"] = 1;
                    return RedirectToAction("RegisterService", "Service", new
                    {
                        @account = Session["accountNumber"].ToString()
                    });
                }

                if (serviceResult.Code.Equals(TVComboConst.Success) && serviceResult.RegistID.Contains("-1"))
                {
                    TempData["CourseSuccess"] = 2;
                    return RedirectToAction("RegisterService", "Service", new
                    {
                        @account = Session["accountNumber"].ToString()
                    });
                }

                TempData["CourseSuccess"] = 3;
                return RedirectToAction("RegisterService", "Service", new
                {
                    @account = Session["accountNumber"].ToString()
                });
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(HomeController), "Dangkydichvu - POST " + ex.Message);
                TempData["CourseSuccess"] = 99;
                return RedirectToAction("RegisterService", "Service", new
                {
                    @account = Session["accountNumber"].ToString()
                });
            }
        }
    }
}