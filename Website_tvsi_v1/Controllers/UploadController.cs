﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.Common;
using Website_tvsi_v1.Common.Data;
using Website_tvsi_v1.Common.stock.market;
using Website_tvsi_v1.Models;

namespace Website_tvsi_v1.Controllers
{
    public class UploadController : Controller
    {
        //
        // GET: /Upload/

        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult tabMarket(string exchange = "HOSTC")
        {

            DataTable index_today = new DataTable();
            DataTable index_lastday = new DataTable();
            StockMarketModel market = new StockMarketModel();
            string name_market = "";
            if (exchange == "HOSTC")
            {
                index_today = TVSI_DU_LIEU_BIEU_DO.LayThongTinThiTruong("HOSE");
                index_lastday = TVSI_DU_LIEU_BIEU_DO_V3.LayKetQuaGiaoDichThiTruongGanNhat();
                market = UtilTvsi.Thongtinthitruong(index_today.Rows[0], index_lastday.Rows[0]);
                name_market = "VN-INDEX";
            }
            else if (exchange == "HASTC")
            {
                index_today = TVSI_DU_LIEU_BIEU_DO.LayThongTinThiTruong("HASTC");
                index_lastday = TVSI_DU_LIEU_BIEU_DO_V3.LayKetQuaGiaoDichThiTruongGanNhat();
                market = UtilTvsi.Thongtinthitruong(index_today.Rows[0], index_lastday.Rows[1]);
                name_market = "HNX-INDEX";
            }
            else
            {
                index_today = TVSI_DU_LIEU_BIEU_DO_V3.LayThongTinThiTruongUpcom();
                index_lastday = TVSI_DU_LIEU_BIEU_DO_V3.LayKetQuaGiaoDichThiTruongUpcom();
                market = UtilTvsi.Thongtinthitruong(index_today.Rows[0], index_lastday.Rows[0]);
                name_market = "UPCOM-INDEX";
            }
            ViewBag.market = market;
            ViewBag.name_market = name_market;
            ViewBag.exchange = exchange;
            return PartialView();
        }
        [HttpPost]
        public JsonResult chartMarket(string exchange)
        {
            TimeFrame timeframe = TimeFrame.Parse("1d");
            DateTime bottomDate = timeframe.GetBaseDate(DateTime.Today);

            List<Quote> data = new List<Quote>();
            data = SqlSymbolData.GetTodayQuotes(exchange);
            return Json(new
            {
                data = data,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
