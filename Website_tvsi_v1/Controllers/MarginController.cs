﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.EFAQWS;
using TVSI.Common;
using System.Text;
using Website_tvsi_v1.App_Data;
using System.Xml;
using Website_tvsi_v1.Models;
using PagedList;
using System.Data;
using Website_tvsi_v1.Common;
using System.Web.Mail;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using TVSI.Security.Cryptography;
using TVSI.Common.DAL;
using System.Globalization;
using System.Threading;
using Website_tvsi_v1.Common.newswebsite;
using Website_tvsi_v1.Common.Data;
using Newtonsoft.Json;
using System.Net;
using TVSI.Utility;
using System.Data.SqlTypes;
using System.Diagnostics.Eventing.Reader;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Web.ModelBinding;
using System.Web.WebPages;
using Dapper;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.EMMA;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Website_tvsi_v1.App_Resources;
using DataTable = System.Data.DataTable;

namespace Website_tvsi_v1.Controllers
{
    public class MarginController : Controller
    {

        [HttpGet]
        public ActionResult index()
        {
            var viewModel = new MarginModel
            {
            };
            if (this.Session["check_margin"] != null)
            {
                ViewBag.success = this.Session["check_margin"];
                this.Session["check_margin"] = null;
            }
            else
            {
                ViewBag.success = false;
            }
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult index(MarginModel model)
        {
            try
            {
                string CRMStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                SqlParameter para_hoten = new SqlParameter("@CustName", model.CustName);
                SqlParameter para_email = new SqlParameter("@Email", model.Email);
                SqlParameter para_sdt = new SqlParameter("@Mobile", model.Mobile);
                SqlParameter para_yeucau = new SqlParameter("@Description", model.Description);

                SqlHelper.ExecuteNonQuery(CRMStr, "TVSI_sDANG_KY_TU_VAN_GOI_DV",
                    para_hoten,
                    para_sdt,
                    para_email,
                    para_yeucau
                );
                Session["check_margin"] = true;
                return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof (MarginController), string.Format("Margin register errors -  {0}", ex.Message));
                Session["check_margin"] = false;
                ViewBag.Message = "Đăng ký Thông tin thành công";
                return View(model);
            }

        }



        [HttpGet]
        public ActionResult margin_tv10_9()
        {
            var viewModel = new MarginModel
            {
            };
            if (this.Session["check_margin"] != null)
            {
                ViewBag.success = this.Session["check_margin"];
                this.Session["check_margin"] = null;
            }
            else
            {
                ViewBag.success = false;
            }
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult margin_tv10_9(MarginModel model)
        {
            try
            {
                string CRMStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                SqlParameter para_hoten = new SqlParameter("@CustName", model.CustName);
                SqlParameter para_email = new SqlParameter("@Email", model.Email);
                SqlParameter para_sdt = new SqlParameter("@Mobile", model.Mobile);
                SqlParameter para_yeucau = new SqlParameter("@Description", model.Description);

                SqlHelper.ExecuteNonQuery(CRMStr, "TVSI_sDANG_KY_TU_VAN_GOI_DV",
                    para_hoten,
                    para_sdt,
                    para_email,
                    para_yeucau
                );
                Session["check_margin"] = true;
                return RedirectToAction("margin_tv10_9");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(MarginController), string.Format("Margin register errors -  {0}", ex.Message));
                Session["check_margin"] = false;
                ViewBag.Message = "Đăng ký Thông tin thành công";
                return View(model);
            }

        }
       
       
        [HttpGet]
        public ActionResult Margin_TV()
        {
            if (TempData["SuccessMessage"] != null)
            {
               var test = TempData["SuccessMessage"];
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Margin_TV(MarginModel model)
        {
            try
            {
                string crmStr = ConfigurationManager.ConnectionStrings["CRM"].ToString();
                if (model != null && !string.IsNullOrEmpty(model.CustName) && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Mobile))
                {
                    using (var conn = new SqlConnection(crmStr))
                    {
                        var result = await conn.ExecuteAsync("TVSI_sDANG_KY_TU_VAN_GOI_DV", new
                        {
                            CustName = model.CustName,
                            Email = model.Email,
                            Mobile = model.Mobile,
                            Description = model.Description
                        },commandType: CommandType.StoredProcedure);
                        var o = result > 0 ? TempData["SuccessMessage"] = 0 : TempData["SuccessMessage"] = -1;
                    }
                }
                else
                {
                    TempData["SuccessMessage"] = -1;
                }
                return RedirectToAction("Margin_TV");
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(MarginController), string.Format("Margin register errors -  {0}", ex.Message));
                TempData["SuccessMessage"] = -1;
                return View(model);
            }

        }
    }
}