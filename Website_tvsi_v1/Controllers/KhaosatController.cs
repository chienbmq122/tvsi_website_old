﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.EFAQWS;
using TVSI.Common;
using System.Text;
using Website_tvsi_v1.App_Data;
using System.Xml;
using Website_tvsi_v1.Models;
using PagedList;
using System.Data;
using Website_tvsi_v1.Common;
using System.Web.Mail;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using TVSI.Security.Cryptography;
using TVSI.Common.DAL;
using System.Globalization;
using System.Threading;
using Website_tvsi_v1.Common.newswebsite;
using Website_tvsi_v1.Common.Data;
using TVSI.DAL.WebsiteDB;
using System.Web.UI.WebControls;
using log4net;
using Newtonsoft.Json;
using System.Net.Http;
using TVSI.Utility;

namespace Website_tvsi_v1.Controllers
{
    public class KhaosatController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //
        // GET: /Khaosat/
        public KhaosatController()
        {
            ViewBag.spdv_menu = "dropdown";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown";
            
            ViewBag.title = "Giao dịch và đầu tư cổ phiếu trực tuyến - Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)-";
        }

        [HttpPost]
        public JsonResult CheckCaptCha(string captcha)
        {
            try
            {
                string kytubaomatcn = captcha;
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    return Json(new { status = false, message = "Vui lòng nhập lại ký tự bảo mật do đã hết phiên làm việc." }, JsonRequestBehavior.DenyGet);
                }
                else
                {
                    if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                    {
                        return Json(new { status = false, message = "Ký tự bảo mật không đúng, vui lòng nhập lại." }, JsonRequestBehavior.DenyGet);
                    }
                    else
                    {
                        return Json(new { status = true });
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = false,
                    message = ex.Message
                }, JsonRequestBehavior.DenyGet);
            }
        }

        public ActionResult KSTraiPhieuKhachHang()
        {
            return View();
        } 
        [HttpPost]
        public ActionResult KSTraiPhieuKhachHang(FormCollection fc)
        {
            try
            {
                var idRegis = 0;
                var kyTuBm = fc["kytubaomatcn"] ?? string.Empty;
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    ViewBag.captcha = "Lỗi ký tự bảo mật";
                    return View();
                }
                else
                {
                    if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kyTuBm)
                    {
                        ViewBag.captcha = "Ký tự bảo mật không đúng";
                        return View();
                    }
                }

                var phuongTienTTKhac = fc["ykienkhacphuongtienthongtin"] ?? string.Empty;
                var mongMuonSDAPPTPkhac = fc["mongmuonsudungapptraiphieutext"] ?? string.Empty;
                var khongChacTG = fc["khongchacthamgia"] ?? string.Empty;
                
                
                var soTkkh = fc["sotaikhoan"] ?? string.Empty;
                var doTuoiKh = fc["DoTuoiKhachHang"] ?? string.Empty;
                var phuongTienTt = fc["PhuongTienThongTin"] ?? string.Empty;
                var thoiGianDauTuUuTien = fc["thoigiandautukhuutien"] ?? string.Empty;
                var tieuChiQt = fc["tieuchiquantrong"] ?? string.Empty;
                var danhMucSptvsicc = fc["danhmucsanphamtvsicc"] ?? string.Empty;
                var yKienKhacTieuChiQt = fc["ykienkhactieuchiquantrong"] ?? string.Empty;
                var kHMongMuonCaiTien = fc["quykhmongmuoncaitien"] ?? string.Empty;
                var kHBietSpmg = fc["bietsanphammoigioi"] ?? string.Empty;

                // table danh gia thai do phuc vụ nhan vien tvsi 
                var thaiDoPhucVuKh = fc["thaidokhiphucvukhtvsi"] ?? string.Empty;
                var hieuQuaTttv = fc["hieuquathongtintuvantvsi"] ?? string.Empty;
                var chamSocKh = fc["chamsockhachhangtvsi"] ?? string.Empty;
                var thoiGianXulyHs = fc["thoigianxulyhoso"] ?? string.Empty;
                var troNgaiKhiGdtvsi = fc["trongaikhigiaodichtvsi"] ?? string.Empty;
                var giaoDichTptx = fc["giaodichthuongxuyen"] ?? string.Empty;
                //End
                
                //cau 4 

                var mucDoYTText = fc["mucdoyeuthichtext"] ?? string.Empty;
                var mucDoYT = fc["mucdoyeuthich"] ?? string.Empty;
                var luaChonTGSP = fc["luachonthamgiasp"] ?? string.Empty;
                var chuaLuaChonTGSP = fc["luachonthamgiasp_text"] ?? string.Empty;

                //Cau 7 
                var traiNghiemAppTp = fc["trainghiemapptraiphieu"] ?? string.Empty;
                var giaoDienAppTp = fc["giaodienapptraiphieu"] ?? string.Empty;
                var tocDoXulyAppTp = fc["tocdoxulyapptraiphieu"] ?? string.Empty;
                var dapUngTtAppTp = fc["dapungthongtinapptraiphieu"] ?? string.Empty;
                var yKienKhacveAppTp = fc["ykienkhacapptraiphieu"] ?? string.Empty;
                var mongMuonSdAppTp = fc["mongmuonsudungapptraiphieu"] ?? string.Empty;

                //cau 8
                var troThanhKhtt = fc["trothanhkhthanthiet"] ?? string.Empty;
                var troThanhKhttText = fc["trothanhkhthanthiettext"] ?? string.Empty;

                //cau 9 
                var gioiThieuNTBB = fc["gioithieunguoithanbb"] ?? string.Empty;
                var chinhSachUDKHM = fc["chinhsachuudaikhmoi"] ?? string.Empty;
                var kThamGiaGioiThieuNTBBText = fc["khongthamgiagioithieubb"] ?? string.Empty;

                //cau 10
                var traiNghiemKH = fc["nangcaotrainghiemkh"] ?? string.Empty;
                var traiNghiemKHText = fc["nangcaotrainghiemkhtext"] ?? string.Empty;

                string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
                using (SqlConnection conn = new SqlConnection(AStrWeb))
                {
                    using (SqlCommand command = new SqlCommand("TVSI_sKHAO_SAT_KET_QUA_TRAI_PHIEU_KHACH_HANG_INSERT", conn))
                    {
                        command.Parameters.Add("@so_tai_khoan", soTkkh);
                        command.Parameters.Add("@cau_1_a", doTuoiKh);
                        command.Parameters.Add("@cau_2_a", phuongTienTt);
                        command.Parameters.Add("@cau_2_a_text", phuongTienTTKhac);
                        command.Parameters.Add("@cau_1_b", thoiGianDauTuUuTien);
                        command.Parameters.Add("@cau_2_b", tieuChiQt);
                        command.Parameters.Add("@cau_2_b_text", yKienKhacTieuChiQt);
                        command.Parameters.Add("@cau_3_b", danhMucSptvsicc);
                        command.Parameters.Add("@cau_3_b_text", kHMongMuonCaiTien);
                        command.Parameters.Add("@cau_4_b", kHBietSpmg);
                        command.Parameters.Add("@cau_4_b_1", mucDoYT);
                        command.Parameters.Add("@cau_4_b_1_text", mucDoYTText);
                        command.Parameters.Add("@cau_4_b_2", luaChonTGSP);
                        command.Parameters.Add("@cau_4_b_2_text", chuaLuaChonTGSP);
                        command.Parameters.Add("@cau_5_b_1", thaiDoPhucVuKh);
                        command.Parameters.Add("@cau_5_b_2", hieuQuaTttv);
                        command.Parameters.Add("@cau_5_b_3", chamSocKh);
                        command.Parameters.Add("@cau_5_b_4", thoiGianXulyHs);
                        command.Parameters.Add("@cau_5_b_text", troNgaiKhiGdtvsi);
                        command.Parameters.Add("@cau_6_b", giaoDichTptx);
                        command.Parameters.Add("@cau_7_b_1", traiNghiemAppTp);
                        command.Parameters.Add("@cau_7_b_1_1", giaoDienAppTp);
                        command.Parameters.Add("@cau_7_b_1_2", tocDoXulyAppTp);
                        command.Parameters.Add("@cau_7_b_1_3", dapUngTtAppTp);
                        command.Parameters.Add("@cau_7_b_1_text", yKienKhacveAppTp);
                        command.Parameters.Add("@cau_7_b_2", mongMuonSdAppTp);
                        command.Parameters.Add("@cau_7_b_2_text", mongMuonSDAPPTPkhac);
                        command.Parameters.Add("@cau_8_b", troThanhKhtt);
                        command.Parameters.Add("@cau_8_b_text", troThanhKhttText);
                        command.Parameters.Add("@cau_9_b_1", gioiThieuNTBB);
                        command.Parameters.Add("@cau_9_b_2", chinhSachUDKHM);
                        command.Parameters.Add("@cau_9_b_2_text", khongChacTG);
                        command.Parameters.Add("@cau_9_b_text", kThamGiaGioiThieuNTBBText);
                        command.Parameters.Add("@cau_10_b", traiNghiemKH);
                        command.Parameters.Add("@cau_10_b_text", traiNghiemKHText);
                        command.Parameters.Add("@nguoi_tao", "Website");
                        command.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.ExecuteNonQuery();
                        idRegis = Convert.ToInt32(command.Parameters["@id"].Value);
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                }

                ViewBag.khachhang = 1;
                TempData["khachhang"] = 1;
                return RedirectToAction("customer-personal/106", "product-services");

            }
            catch (Exception e)
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                Logger.Error(typeof(KhaosatController), e.Message);
                return View();
            }
        }

        public ActionResult KSTraiPhieuCBNV()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KSTraiPhieuCBNV(FormCollection fc)
        {
            try
            {
                
                var kyTuBm = fc["kytubaomatcn"] ?? string.Empty;
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
                {
                    ViewBag.captcha = "Lỗi ký tự bảo mật";
                    return View();
                }
                else
                {
                    if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kyTuBm)
                    {
                        ViewBag.captcha = "Ký tự bảo mật không đúng";
                        return View();
                    }
                }
                var tenCbnv = fc["tencbnv"] ?? string.Empty;
                var phongBan = fc["phongbancbnv"] ?? string.Empty;
                var lamViecVoiTvsi = fc["LamViecVoiTvsi"] ?? string.Empty;
                var kHCaNhanHayToChucText = fc["khcanhanhaytochuc"] ?? string.Empty; // textarea
            
                // A
                var traiPhieuCanhTranhA = fc["traiphieucanhtranhcao"] ?? string.Empty;
                var traiPhieuCanhTranhLyDoTextA = fc["traiphieucanhtranhcaolydo"] ?? string.Empty; // textarea
                var cBnvDanhGiaDichVuTpA = fc["cbnvdanhgiadichvutraiphieu"] ?? string.Empty;
                var loiTucHapDanCanhTranhA = fc["loituchapdancanhtranh"] ?? string.Empty;
                var loiIchDiKemSpA = fc["loiichdikemsanpham"] ?? string.Empty;
                var phiDichVuUdA = fc["phidichvuuudai"] ?? string.Empty;
                var thuTucDonGianNhanhA = fc["thutucdongiannhanh"] ?? string.Empty;
                var yKienKhacAppTpnvA = fc["ykienkhacapptraiphieucbnv"] ?? string.Empty;// textarea
                var yKienKhacTieuChiQtAppTpnvA = fc["ykienkhactieuchiquantrong"] ?? string.Empty;// textarea
                var tieuChiQtA = fc["tieuchiquantrong"] ?? string.Empty; // list
                var danhMucSptvsiccA = fc["danhmucsanphamtvsicc"] ?? string.Empty; // list
                var canBoNvMongMuonCtA = fc["cbnvmongmuoncaitien"] ?? string.Empty;// textarea
                var lyDoKhKMuonTGTPA = fc["lydokhkmuonthamgiatraiphieu"] ?? string.Empty; // list
                var yKienkhackoTGTPA = fc["ykienhacvekhkothamgiatraiphieu"] ?? string.Empty; // textarea
                var khCuaCBNVGapKKA = fc["khcuacbnvgapkhokhan"] ?? string.Empty;
                var khCuaCBNVGapKKTextA = fc["khcbnvgapkhokhantext"] ?? string.Empty; // textarea
                var khGioiThieuThemKhChoCbnvA = fc["khgioithieuthemchocbnv"] ?? string.Empty; 
                var hoTroSpChoCbnvA = fc["hotrospchocbnv"] ?? string.Empty; 
                var hoTroSpChoCbnvTextA = fc["hotrospchocbnvtext"] ?? string.Empty; // textarea
                var phatTrienBoSungThemSPDVA = fc["phattrienbosungthemsanphamdv"] ?? string.Empty; // textarea
                var diemManhCtYkhacA = fc["diemmanhctykhac"] ?? string.Empty; // textarea



                // B
                var thaiDoHoTro = fc["thaidohotro"] ?? string.Empty; 
                var tocDoXuLyNV = fc["tocdoxulynghiepvu"] ?? string.Empty; 
                var yKienVeThaiDoXulyTVSI = fc["ykienthaidoxulytvsi"] ?? string.Empty; //textare
                var heThongNBTVSI = fc["hethongnoibotvsi"] ?? string.Empty; 
                var deXuatVoiHeThong = fc["dexuatvoihethong"] ?? string.Empty; //textare
                var tuVanChaoBanSP = fc["tuvanchaobansp"] ?? string.Empty; 
                var khoKhanKhiTrienKhai = fc["khokhakhitrienkhai"] ?? string.Empty; //textarea
                var giaoDienAppTP = fc["trainghiemapptraiphieu"] ?? string.Empty;
                var tocDoTruyCapApp = fc["tocdotruycapvaxulyapptraiphieu"] ?? string.Empty;
                var dapUngTTTraCuu = fc["dapunghthongtintracuu"] ?? string.Empty;
                var ykienVeAppTPText = fc["ykienveapptraiphieu"] ?? string.Empty; //textarea
                /*
                var boSungThemMoiChoMucDich = fc["bosungthemmoinhungmuc"] ?? string.Empty; 
                */
            
                var boSungThemMoiChoMucDichText = fc["bosungthemoitext"] ?? string.Empty; //textare
                var cbnvMongMuonVeAppTP = fc["mongmuonveapptraiphieutext"] ?? string.Empty; //textare


                var thongKeDanhMuc = fc["theodoithongkedanhmuc"] ?? string.Empty;
                var theoDoiTTTP = fc["theodoithongtintraiphieu"] ??  string.Empty;
                var datLenhMuaTP = fc["datlenhmuabantraiphieu"] ??  string.Empty;
                var nangCapKhac = fc["nangcapkhac"] ??  string.Empty;
                string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
                var idRegist = string.Empty;
                using (SqlConnection conn = new SqlConnection(AStrWeb))
                {
                    using (SqlCommand command = new SqlCommand("TVSI_sKHAO_SAT_KET_QUA_TRAI_PHIEU_CBNV_INSERT", conn))
                    {
                        command.Parameters.Add("@ten_nhan_vien",  tenCbnv );
                        command.Parameters.Add("@phong_ban",  phongBan );
                        command.Parameters.Add("@lam_viec_voi_tvsi",  lamViecVoiTvsi);
                        command.Parameters.Add("@kh_cua_cbnv",  kHCaNhanHayToChucText );
                        command.Parameters.Add("@cau1", traiPhieuCanhTranhA  );
                        command.Parameters.Add("@cau1_text", traiPhieuCanhTranhLyDoTextA  );
                        command.Parameters.Add("@cau2_1", cBnvDanhGiaDichVuTpA  );
                        command.Parameters.Add("@cau2_2", loiTucHapDanCanhTranhA  );
                        command.Parameters.Add("@cau2_3", loiIchDiKemSpA  );
                        command.Parameters.Add("@cau2_4", phiDichVuUdA  );
                        command.Parameters.Add("@cau2_5", thuTucDonGianNhanhA  );
                        command.Parameters.Add("@cau2_text", yKienKhacAppTpnvA  );
                        command.Parameters.Add("@cau3", tieuChiQtA  );
                        command.Parameters.Add("@cau3_text", yKienKhacTieuChiQtAppTpnvA  );
                        command.Parameters.Add("@cau4", danhMucSptvsiccA  );
                        command.Parameters.Add("@cau4_text", canBoNvMongMuonCtA  );
                        command.Parameters.Add("@cau5", lyDoKhKMuonTGTPA  );
                        command.Parameters.Add("@cau5_text", yKienkhackoTGTPA  );
                        command.Parameters.Add("@cau6", khCuaCBNVGapKKA  );
                        command.Parameters.Add("@cau6_text", khCuaCBNVGapKKTextA  );
                        command.Parameters.Add("@cau7", khGioiThieuThemKhChoCbnvA  );
                        command.Parameters.Add("@cau8", hoTroSpChoCbnvA  );
                        command.Parameters.Add("@cau8_text", hoTroSpChoCbnvTextA  );
                        command.Parameters.Add("@cau9_text", phatTrienBoSungThemSPDVA  );
                        command.Parameters.Add("@cau10_text", diemManhCtYkhacA  );
                        command.Parameters.Add("@cau11_1", thaiDoHoTro  );
                        command.Parameters.Add("@cau11_2", tocDoXuLyNV  );
                        command.Parameters.Add("@cau11_text", yKienVeThaiDoXulyTVSI  );
                        command.Parameters.Add("@cau12", heThongNBTVSI  );
                        command.Parameters.Add("@cau12_text", deXuatVoiHeThong  );
                        command.Parameters.Add("@cau13", tuVanChaoBanSP  );
                        command.Parameters.Add("@cau13_text", khoKhanKhiTrienKhai  );
                        command.Parameters.Add("@cau14_1", giaoDienAppTP  );
                        command.Parameters.Add("@cau14_2", tocDoTruyCapApp  );
                        command.Parameters.Add("@cau14_3", dapUngTTTraCuu  );
                        command.Parameters.Add("@cau14_text", ykienVeAppTPText  );
                        command.Parameters.Add("@cau15_1", thongKeDanhMuc  );
                        command.Parameters.Add("@cau15_2", theoDoiTTTP  );
                        command.Parameters.Add("@cau15_3", datLenhMuaTP  );
                        command.Parameters.Add("@cau15_4", nangCapKhac  );
                        command.Parameters.Add("@cau15_text", boSungThemMoiChoMucDichText  );
                        command.Parameters.Add("@cau16_text", cbnvMongMuonVeAppTP  );
                        command.Parameters.Add("@nguoi_tao", "WEBSITE"  );
                        command.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.ExecuteNonQuery();
                        idRegist = command.Parameters["@id"].Value.ToString();
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                }

                ViewBag.CBNV = 1;
                TempData["CBNV"] = 1;
                return RedirectToAction("customer-personal/106", "product-services");

            }
            catch (Exception e)
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                Logger.Error(typeof(KhaosatController), e.Message);
                return View();
            }

            
            
        }


        public ActionResult Luachonthongtin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Luachonthongtin(FormCollection fc)
        {
            string kytubaomatcn = fc["kytubaomatcn"].ToString();
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View();
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View();
                }
            }
            string AccNumber = fc["sotaikhoan"].ToString().Trim();
            string MucDoHaiLong = fc["MucDoHaiLong"].ToString().Trim();
            //string NangCap = fc["LamGiDeNangCap"].ToString().Trim();
            string TiepTucSuDung = fc["TiepTucSuDung"].ToString().Trim();
            string khachhangchuacotaikhoantvsi = fc["khachhangchuacotaikhoantvsi"].ToString().Trim();


            string queryTotal = "select so_tai_khoan from TVSI_KHAO_SAT_KET_QUA where so_tai_khoan = '" + AccNumber + "'";

            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryTotal))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        ViewBag.captcha = "Số tài khoản " + AccNumber + " đã thực hiện khảo sát.";
                        return View();
                    }
                }
            }

            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            SqlConnection con = new SqlConnection(AStrWeb);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "TVSI_sKHAO_SAT_KET_QUA_INSERT";

            cmd.Parameters.Add("@khao_sat_type_code", SqlDbType.VarChar).Value = khachhangchuacotaikhoantvsi;
            cmd.Parameters.Add("@so_tai_khoan", SqlDbType.NVarChar).Value = AccNumber;
            cmd.Parameters.Add("@muc_do_hai_long", SqlDbType.Int).Value = int.Parse(MucDoHaiLong);
            cmd.Parameters.Add("@lam_gi_de_nang_cao", SqlDbType.NVarChar).Value = "";
            cmd.Parameters.Add("@tiep_tuc_su_dung", SqlDbType.Int).Value = int.Parse(TiepTucSuDung);
            cmd.Parameters.Add("@nguoi_tao", SqlDbType.NVarChar).Value = "WEB";
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                string id = cmd.Parameters["@id"].Value.ToString();
                if (!String.IsNullOrEmpty(id))
                {
                    this.Session["id"] = id;
                    this.Session["AccountNumber"] = AccNumber;
                }
                
                if (khachhangchuacotaikhoantvsi == "B")
                {
                    return RedirectToAction("KhachHangDauTuCoPhieu");
                }
                if (khachhangchuacotaikhoantvsi == "C")
                {
                    return RedirectToAction("KhachHangDauTuTraiPhieu");
                }
                if (khachhangchuacotaikhoantvsi == "D")
                {
                    return RedirectToAction("KhachHangDauTuCoPhieuVaTraiPhieu");
                }

                return View();
            }
            catch
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }

        [HttpGet]
        public ActionResult Thongtinchung()
        {

            KhaoSatYkienKhachHangModel model = new KhaoSatYkienKhachHangModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Thongtinchung(KhaoSatYkienKhachHangModel ksykienkhachhang)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            //if (TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG.ExistsSTK(ksykienkhachhang.sotaikhoan.ToString(), AStrWeb))
            //{
            //    ViewBag.Message = "Số tài khoản " + ksykienkhachhang.sotaikhoan + " của quý khách đã thực hiện khảo sát. Vui lòng liên hệ với TVSI để biết thêm thông tin. ";
            //    return View(ksykienkhachhang);
            //}
            //SqlParameter para_hoten = new SqlParameter("@ho_ten", ksykienkhachhang.hoten);
            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", ksykienkhachhang.sotaikhoan);
            //SqlParameter para_email_sdt = new SqlParameter("@so_dien_thoai_email", ksykienkhachhang.emailortel);
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);
            //DataTable so_tai_khoan = TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG.LayTheo_so_tai_khoan(ksykienkhachhang.sotaikhoan);


            this.Session["so_tai_khoan"] = Utilities.GenerateOTP();
            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", this.Session["so_tai_khoan"].ToString());
            SqlParameter para_thoigiangiaodich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 1);
            SqlParameter para_thoigiangiaodich = new SqlParameter("@cau_tra_loiid", Request["thoigiangiaodich"]);

            SqlParameter para_kenhthongtin_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 2);

            SqlParameter para_luachondichvu_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 3);

            SqlParameter para_quatangtvsi_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 4);
            SqlParameter para_quatangtvsi = new SqlParameter("@cau_tra_loiid", Request["quatangtvsi"]);

            SqlParameter para_chuongtrinhtrian_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 72);
            SqlParameter para_chuongtrinhtrian = new SqlParameter("@cau_tra_loiid", Request["chuongtrinhtrian"]);

            SqlParameter para_chuongtrinhhoithao_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 73);
            SqlParameter para_chuongtrinhhoithao = new SqlParameter("@cau_tra_loiid", Request["chuongtrinhhoithao"]);

            SqlParameter para_ykiendichvuchamsoc_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 74);
            SqlParameter para_ykiendichvuchamsoc_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["ykiendichvuchamsoc"]);

            SqlParameter para_trienkhaictht_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 90);
            SqlParameter para_trienkhaictht_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["trienkhaictht"]);

            try
            {
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thoigiangiaodich_cau_hoi, para_thoigiangiaodich, para_sotaikhoan, "", para_ngaykhaosat);
                string strKenhThongTin = Request["kenhthongtin"];
                foreach (string s in strKenhThongTin.Split(','))
                {
                    if (s.Trim().Length > 0)
                    {
                        SqlParameter para_kenhthongtin = new SqlParameter("@cau_tra_loiid", s);
                        SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_kenhthongtin_cau_hoi, para_kenhthongtin, para_sotaikhoan, "", para_ngaykhaosat);
                    }
                }
                string strLuachondichvu = Request["luachondichvu"];
                foreach (string s in strLuachondichvu.Split(','))
                {
                    if (s.Trim().Length > 0)
                    {
                        SqlParameter para_luachondichvu = new SqlParameter("@cau_tra_loiid", s);
                        SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_luachondichvu_cau_hoi, para_luachondichvu, para_sotaikhoan, "", para_ngaykhaosat);
                    }
                }
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_quatangtvsi_cau_hoi, para_quatangtvsi, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_chuongtrinhtrian_cau_hoi, para_chuongtrinhtrian, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_chuongtrinhhoithao_cau_hoi, para_chuongtrinhhoithao, para_sotaikhoan, "", para_ngaykhaosat);
                if (Request["ykiendichvuchamsoc"].ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykiendichvuchamsoc_cau_hoi, 28, para_sotaikhoan, para_ykiendichvuchamsoc_noi_dung, para_ngaykhaosat);
                }
                if (Request["trienkhaictht"].ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_trienkhaictht_cau_hoi, 355, para_sotaikhoan, para_trienkhaictht_noi_dung, para_ngaykhaosat);
                }

                return RedirectToAction("dich-vu-moi-gioi", "khao-sat");
            }
            catch
            {

                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View(ksykienkhachhang);
            }

        }

        public ActionResult Dichvumoigioi()
        {
            KhaoSatYkienKhachHangModel model = new KhaoSatYkienKhachHangModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Dichvumoigioi(KhaoSatYkienKhachHangModel dichvumoigioi)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();

            //Them thong tin khach hang
            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", Session["so_tai_khoan"].ToString());
            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", "9999");
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);

            SqlParameter para_tkduocnvmoigioicsoc_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 6);
            SqlParameter para_tkduocnvmoigioicsoc = new SqlParameter("@cau_tra_loiid", Request["tkduocnvmoigioicsoc"]);

            SqlParameter para_mongmuontkduocqlyboinvmoigioi_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 7);
            SqlParameter para_mongmuontkduocqlyboinvmoigioi = new SqlParameter("@cau_tra_loiid", Request["mongmuontkduocqlyboinvmoigioi"]);


            SqlParameter para_duytrilienhekhachhang_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 75);
            SqlParameter para_duytrilienhekhachhang = new SqlParameter("@cau_tra_loiid", Request["duytrilienhekhachhang"]);

            SqlParameter para_thaidophucvu_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 76);
            SqlParameter para_thaidophucvu = new SqlParameter("@cau_tra_loiid", Request["thaidophucvu"]);


            SqlParameter para_khananghotrogiaodich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 77);
            SqlParameter para_khananghotrogiaodich = new SqlParameter("@cau_tra_loiid", Request["khananghotrogiaodich"]);

            SqlParameter para_tuvandautu_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 78);
            SqlParameter para_tuvandautu = new SqlParameter("@cau_tra_loiid", Request["tuvandautu"]);

            SqlParameter para_ykien_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 9);
            SqlParameter para_ykien_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["ykiendanhgiadvmg"]);
            try
            {

                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tkduocnvmoigioicsoc_cau_hoi, para_tkduocnvmoigioicsoc, para_sotaikhoan, "", para_ngaykhaosat);
                if (int.Parse(Request["tkduocnvmoigioicsoc"]) == 29)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_duytrilienhekhachhang_cau_hoi, para_duytrilienhekhachhang, para_sotaikhoan, "", para_ngaykhaosat);
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thaidophucvu_cau_hoi, para_thaidophucvu, para_sotaikhoan, "", para_ngaykhaosat);
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_khananghotrogiaodich_cau_hoi, para_khananghotrogiaodich, para_sotaikhoan, "", para_ngaykhaosat);
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tuvandautu_cau_hoi, para_tuvandautu, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (int.Parse(Request["tkduocnvmoigioicsoc"]) == 30)
                {

                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_mongmuontkduocqlyboinvmoigioi_cau_hoi, para_mongmuontkduocqlyboinvmoigioi, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (Request["ykiendanhgiadvmg"].ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykien_cau_hoi, 54, para_sotaikhoan, para_ykien_noi_dung, para_ngaykhaosat);

                }

                return RedirectToAction("giao-dich-chung-khoan", "khao-sat");
            }
            catch
            {

                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View(dichvumoigioi);
            }
        }
        public ActionResult Giaodichchungkhoan()
        {
            KhaoSatYkienKhachHangModel model = new KhaoSatYkienKhachHangModel();
            return View(model);
            //return RedirectToAction("Dichvuhotrovon");
        }
        [HttpPost]
        public ActionResult Giaodichchungkhoan(KhaoSatYkienKhachHangModel giaodichchungkhoan)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();


            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", Session["so_tai_khoan"].ToString());
            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", "999999");
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);

            SqlParameter para_kenhgiaodichchungkhoan_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 10);

            SqlParameter para_quatangtvsi_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 4);
            SqlParameter para_quatangtvsi = new SqlParameter("@cau_tra_loiid", Request["quatangtvsi"]);

            SqlParameter para_kenhthongtin_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 2);

            SqlParameter para_luachondichvu_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 3);

            SqlParameter para_hinhthucgiaodien_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 79);
            SqlParameter para_hinhthucgiaodien = new SqlParameter("@cau_tra_loiid", Request["hinhthucgiaodien"]);

            SqlParameter para_noidungthongtin_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 12);
            SqlParameter para_noidungthongtin = new SqlParameter("@cau_tra_loiid", Request["noidungthongtin"]);

            SqlParameter para_bocuc_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 13);
            SqlParameter para_bocuc = new SqlParameter("@cau_tra_loiid", Request["bocuc"]);


            SqlParameter para_tinhnang_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 14);
            SqlParameter para_tinhnang = new SqlParameter("@cau_tra_loiid", Request["tinhnang"]);

            SqlParameter para_tocdotruycap_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 15);
            SqlParameter para_tocdotruycap = new SqlParameter("@cau_tra_loiid", Request["tocdotruycap"]);

            SqlParameter para_gdtt_tinhnangdatlenh_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 16);
            SqlParameter para_gdtt_tinhnangdatlenh = new SqlParameter("@cau_tra_loiid", Request["gdtt_tinhnangdatlenh"]);

            SqlParameter para_giaodichtienchungkhoan_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 17);
            SqlParameter para_giaodichtienchungkhoan = new SqlParameter("@cau_tra_loiid", Request["giaodichtienchungkhoan"]);

            SqlParameter para_thuchienquyentt_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 18);
            SqlParameter para_thuchienquyentt = new SqlParameter("@cau_tra_loiid", Request["thuchienquyentt"]);

            SqlParameter para_banlolett_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 19);
            SqlParameter para_banlolett = new SqlParameter("@cau_tra_loiid", Request["banlolett"]);

            SqlParameter para_ttdanhmuctonghoptsan_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 20);
            SqlParameter para_ttdanhmuctonghoptsan = new SqlParameter("@cau_tra_loiid", Request["ttdanhmuctonghoptsan"]);

            SqlParameter para_saokegiaodich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 21);
            SqlParameter para_saokegiaodich = new SqlParameter("@cau_tra_loiid", Request["saokegiaodich"]);

            SqlParameter para_tinhnanglailotaikhoanqkhu_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 22);
            SqlParameter para_tinhnanglailotaikhoanqkhu = new SqlParameter("@cau_tra_loiid", Request["tinhnanglailotaikhoanqkhu"]);


            SqlParameter para_ykienbosungphanmemgdtt_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 80);
            SqlParameter para_ykienbosungphanmemgdtt_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["ykienbosungphanmemgdtt"]);

            SqlParameter para_datlenhgiaodichtongdai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 24);
            SqlParameter para_datlenhgiaodichtongdai = new SqlParameter("@cau_tra_loiid", Request["datlenhgiaodichtongdai"]);

            SqlParameter para_hotrogiaidapquatongdai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 25);
            SqlParameter para_hotrogiaidapquatongdai = new SqlParameter("@cau_tra_loiid", Request["hotrogiaidapquatongdai"]);

            SqlParameter para_hotrogiaidapquaemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 26);
            SqlParameter para_hotrogiaidapquaemail = new SqlParameter("@cau_tra_loiid", Request["hotrogiaidapquaemail"]);

            SqlParameter para_thaidophucvunvientdai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 27);
            SqlParameter para_thaidophucvunvientdai = new SqlParameter("@cau_tra_loiid", Request["thaidophucvunvientdai"]);

            SqlParameter para_tracuuthongtintudong_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 28);
            SqlParameter para_tracuuthongtintudong = new SqlParameter("@cau_tra_loiid", Request["tracuuthongtintudong"]);

            SqlParameter para_ykienbosungtongdai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 23);
            SqlParameter para_ykienbosungtongdai_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["ykienbosungtongdai"]);

            SqlParameter para_datlenhdichvusmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 30);
            SqlParameter para_datlenhdichvusmsemail = new SqlParameter("@cau_tra_loiid", Request["datlenhdichvusmsemail"]);

            SqlParameter para_gdtienbiendongsodusmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 31);
            SqlParameter para_gdtienbiendongsodusmsemail = new SqlParameter("@cau_tra_loiid", Request["gdtienbiendongsodusmsemail"]);

            SqlParameter para_tbaothuchienquyensmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 32);
            SqlParameter para_tbaothuchienquyensmsemail = new SqlParameter("@cau_tra_loiid", Request["tbaothuchienquyensmsemail"]);

            SqlParameter para_tonghopgiaodichsmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 33);
            SqlParameter para_tonghopgiaodichsmsemail = new SqlParameter("@cau_tra_loiid", Request["tonghopgiaodichsmsemail"]);

            SqlParameter para_saokegiaodichhangthangsmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 34);
            SqlParameter para_saokegiaodichhangthangsmsemail = new SqlParameter("@cau_tra_loiid", Request["saokegiaodichhangthangsmsemail"]);

            SqlParameter para_truyvantttdongsmsemail_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 35);
            SqlParameter para_truyvantttdongsmsemail = new SqlParameter("@cau_tra_loiid", Request["truyvantttdongsmsemail"]);

            SqlParameter para_thaidonviengdichtaiquay_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 37);
            SqlParameter para_thaidonviengdichtaiquay = new SqlParameter("@cau_tra_loiid", Request["thaidonviengdichtaiquay"]);

            SqlParameter para_cnghieptocdoxlygiaodichtaiquay_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 38);
            SqlParameter para_cnghieptocdoxlygiaodichtaiquay = new SqlParameter("@cau_tra_loiid", Request["cnghieptocdoxlygiaodichtaiquay"]);

            SqlParameter para_bmauhdongdkygiaodichtaiquay_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 39);
            SqlParameter para_bmauhdongdkygiaodichtaiquay = new SqlParameter("@cau_tra_loiid", Request["bmauhdongdkygiaodichtaiquay"]);

            try
            {

                string strkenhgiaodichchungkhoan = Request["kenhgiaodichchungkhoan"];
                foreach (string s in strkenhgiaodichchungkhoan.Split(','))
                {
                    if (s.Trim().Length > 0)
                    {
                        SqlParameter para_kenhgiaodichchungkhoan = new SqlParameter("@cau_tra_loiid", s);
                        SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_kenhgiaodichchungkhoan_cau_hoi, para_kenhgiaodichchungkhoan, para_sotaikhoan, "", para_ngaykhaosat);
                    }

                }
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hinhthucgiaodien_cau_hoi, para_hinhthucgiaodien, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_noidungthongtin_cau_hoi, para_noidungthongtin, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_bocuc_cau_hoi, para_bocuc, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tinhnang_cau_hoi, para_tinhnang, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tocdotruycap_cau_hoi, para_tocdotruycap, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_gdtt_tinhnangdatlenh_cau_hoi, para_gdtt_tinhnangdatlenh, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_giaodichtienchungkhoan_cau_hoi, para_giaodichtienchungkhoan, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thuchienquyentt_cau_hoi, para_thuchienquyentt, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_banlolett_cau_hoi, para_banlolett, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ttdanhmuctonghoptsan_cau_hoi, para_ttdanhmuctonghoptsan, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_saokegiaodich_cau_hoi, para_saokegiaodich, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tinhnanglailotaikhoanqkhu_cau_hoi, para_tinhnanglailotaikhoanqkhu, para_sotaikhoan, "", para_ngaykhaosat);
                if (Request["ykienbosungphanmemgdtt"].ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykienbosungphanmemgdtt_cau_hoi, 329, para_sotaikhoan, para_ykienbosungphanmemgdtt_noi_dung, para_ngaykhaosat);
                }

                /**Tong dai**/
                if (Request["datlenhgiaodichtongdai"] != null)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_datlenhgiaodichtongdai_cau_hoi, para_datlenhgiaodichtongdai, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (Request["hotrogiaidapquatongdai"] != null)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hotrogiaidapquatongdai_cau_hoi, para_hotrogiaidapquatongdai, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (Request["hotrogiaidapquaemail"] != null)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hotrogiaidapquaemail_cau_hoi, para_hotrogiaidapquaemail, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (Request["thaidophucvunvientdai"] != null)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thaidophucvunvientdai_cau_hoi, para_thaidophucvunvientdai, para_sotaikhoan, "", para_ngaykhaosat);
                }
                if (Request["tracuuthongtintudong"] != null)
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tracuuthongtintudong_cau_hoi, para_tracuuthongtintudong, para_sotaikhoan, "", para_ngaykhaosat);
                }

                /***************/
                if (Request["ykienbosungtongdai"].ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykienbosungtongdai_cau_hoi, 121, para_sotaikhoan, para_ykienbosungtongdai_noi_dung, para_ngaykhaosat);
                }
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_datlenhdichvusmsemail_cau_hoi, para_datlenhdichvusmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_gdtienbiendongsodusmsemail_cau_hoi, para_gdtienbiendongsodusmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tbaothuchienquyensmsemail_cau_hoi, para_tbaothuchienquyensmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tonghopgiaodichsmsemail_cau_hoi, para_tonghopgiaodichsmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_saokegiaodichhangthangsmsemail_cau_hoi, para_saokegiaodichhangthangsmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_truyvantttdongsmsemail_cau_hoi, para_truyvantttdongsmsemail, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thaidonviengdichtaiquay_cau_hoi, para_thaidonviengdichtaiquay, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_cnghieptocdoxlygiaodichtaiquay_cau_hoi, para_cnghieptocdoxlygiaodichtaiquay, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_bmauhdongdkygiaodichtaiquay_cau_hoi, para_bmauhdongdkygiaodichtaiquay, para_sotaikhoan, "", para_ngaykhaosat);

                return RedirectToAction("dich-vu-ho-tro-von", "khao-sat");
            }
            catch
            {

                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View(giaodichchungkhoan);
            }

        }
        [HttpGet]
        public ActionResult Dichvuhotrovon()
        {
            KhaoSatYkienKhachHangModel model = new KhaoSatYkienKhachHangModel();
            return View(model);
            //return RedirectToAction("Giaodichtructuyen");
        }
        [HttpPost]
        public ActionResult Dichvuhotrovon(KhaoSatYkienKhachHangModel dichvuhotrovon)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();

            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", "666966");
            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", Session["so_tai_khoan"].ToString());
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);

            SqlParameter para_sudungdichvugdkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 40);
            SqlParameter para_sudungdichvugdkyquy = new SqlParameter("@cau_tra_loiid", Request["sudungdichvugdkyquy"]);

            SqlParameter para_lydokhongconhucaugdkquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 41);
            SqlParameter para_dichvuhotrovon_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 42);
            SqlParameter para_goidichvuhotrovongdkquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 43);

            SqlParameter para_tieuchidichvugdkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 44);
            SqlParameter para_tieuchidichvugdkyquy = new SqlParameter("@cau_tra_loiid", Request["tieuchidichvugdkyquy"]);

            SqlParameter para_thutucgiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 46);
            SqlParameter para_thutucgiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["thutucgiaodichkyquy"]);

            SqlParameter para_phidichvugiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 47);
            SqlParameter para_phidichvugiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["phidichvugiaodichkyquy"]);

            SqlParameter para_capnhatdanhmucgiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 48);
            SqlParameter para_capnhatdanhmucgiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["capnhatdanhmucgiaodichkyquy"]);

            SqlParameter para_tylehtrovontkgiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 49);
            SqlParameter para_tylehtrovontkgiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["tylehtrovontkgiaodichkyquy"]);

            SqlParameter para_goisphamgiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 50);
            SqlParameter para_goisphamgiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["goisphamgiaodichkyquy"]);

            SqlParameter para_hthonghotrogiaodichkyquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 51);
            SqlParameter para_hthonghotrogiaodichkyquy = new SqlParameter("@cau_tra_loiid", Request["hthonghotrogiaodichkyquy"]);

            SqlParameter para_guithongtintraodoitsdb_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 52);
            SqlParameter para_guithongtintraodoitsdb = new SqlParameter("@cau_tra_loiid", Request["guithongtintraodoitsdb"]);

            SqlParameter para_chinhsachchovaygdichkquy_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 53);
            SqlParameter para_chinhsachchovaygdichkquy = new SqlParameter("@cau_tra_loiid", Request["chinhsachchovaygdichkquy"]);

            SqlParameter para_sdungdvctyngoai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 91);
            SqlParameter para_sdungdvctyngoai_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["sdungdvctyngoai"]);

            SqlParameter para_lydoluachonctykhac_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 92);

            SqlParameter para_goidvutchinhctyngoai_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 93);
            SqlParameter para_goidvutchinhctyngoai_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["goidvutchinhctyngoai"]);

            SqlParameter para_donggopykienkhac_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 94);
            SqlParameter para_donggopykienkhac_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["donggopykienkhac"]);
            try
            {
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_sudungdichvugdkyquy_cau_hoi, para_sudungdichvugdkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                if (int.Parse(Request["sudungdichvugdkyquy"]) == 168)
                {
                    string strlydokhongconhucaugdkquy = Request["lydokhongconhucaugdkquy"];
                    foreach (string s in strlydokhongconhucaugdkquy.Split(','))
                    {
                        if (s.Trim().Length > 0)
                        {
                            SqlParameter para_lydokhongconhucaugdkquy = new SqlParameter("@cau_tra_loiid", s);
                            SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_lydokhongconhucaugdkquy_cau_hoi, para_lydokhongconhucaugdkquy, para_sotaikhoan, "", para_ngaykhaosat);
                        }
                    }
                }
                if (int.Parse(Request["sudungdichvugdkyquy"]) == 167)
                {
                    string strdichvuhotrovon = Request["dichvuhotrovon"];
                    foreach (string s in strdichvuhotrovon.Split(','))
                    {
                        if (s.Trim().Length > 0)
                        {
                            SqlParameter para_dichvuhotrovon = new SqlParameter("@cau_tra_loiid", s);
                            SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_dichvuhotrovon_cau_hoi, para_dichvuhotrovon, para_sotaikhoan, "", para_ngaykhaosat);
                        }
                    }
                }
                if (Request["goidichvuhotrovongdkquy"].ToString() != "")
                {
                    string strgoidichvuhotrovongdkquy = Request["goidichvuhotrovongdkquy"];
                    foreach (string s in strgoidichvuhotrovongdkquy.Split(','))
                    {
                        if (s.Trim().Length > 0)
                        {
                            SqlParameter para_goidichvuhotrovongdkquy = new SqlParameter("@cau_tra_loiid", s);
                            SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_goidichvuhotrovongdkquy_cau_hoi, para_goidichvuhotrovongdkquy, para_sotaikhoan, "", para_ngaykhaosat);
                        }
                    }
                }
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tieuchidichvugdkyquy_cau_hoi, para_tieuchidichvugdkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thutucgiaodichkyquy_cau_hoi, para_thutucgiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_phidichvugiaodichkyquy_cau_hoi, para_phidichvugiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_capnhatdanhmucgiaodichkyquy_cau_hoi, para_capnhatdanhmucgiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tylehtrovontkgiaodichkyquy_cau_hoi, para_tylehtrovontkgiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_goisphamgiaodichkyquy_cau_hoi, para_goisphamgiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hthonghotrogiaodichkyquy_cau_hoi, para_hthonghotrogiaodichkyquy, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_guithongtintraodoitsdb_cau_hoi, para_guithongtintraodoitsdb, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_chinhsachchovaygdichkquy_cau_hoi, para_chinhsachchovaygdichkquy, para_sotaikhoan, "", para_ngaykhaosat);

                if (Request["sdungdvctyngoai"].Trim().ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_sdungdvctyngoai_cau_hoi, 356, para_sotaikhoan, para_sdungdvctyngoai_noi_dung, para_ngaykhaosat);
                    string strlydoluachonctykhac = Request["lydoluachonctykhac"];
                    if (strlydoluachonctykhac.ToString() != "")
                    {
                        foreach (string s in strlydoluachonctykhac.Split(','))
                        {
                            if (s.Trim().Length > 0)
                            {
                                SqlParameter para_lydoluachonctykhac = new SqlParameter("@cau_tra_loiid", s);
                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_lydoluachonctykhac_cau_hoi, para_lydoluachonctykhac, para_sotaikhoan, "", para_ngaykhaosat);

                            }
                        }
                    }
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_goidvutchinhctyngoai_cau_hoi, 362, para_sotaikhoan, para_goidvutchinhctyngoai_noi_dung, para_ngaykhaosat);
                }
                if (Request["donggopykienkhac"].Trim().ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_donggopykienkhac_cau_hoi, 363, para_sotaikhoan, para_donggopykienkhac_noi_dung, para_ngaykhaosat);
                }
                return RedirectToAction("giao-dich-truc-tuyen", "khao-sat");
            }
            catch
            {

                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View(dichvuhotrovon);
            }
        }
        public ActionResult Giaodichtructuyen()
        {
            KhaoSatYkienKhachHangModel model = new KhaoSatYkienKhachHangModel();
            return View(model);
            //return RedirectToAction("Ykienkhachhang");
        }
        [HttpPost]
        public ActionResult Giaodichtructuyen(KhaoSatYkienKhachHangModel giaodichtructuyen)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();

            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", Session["so_tai_khoan"].ToString());
            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", "9999");
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);


            SqlParameter para_hinhthucgiaodienweb_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 55);
            SqlParameter para_hinhthucgiaodienweb = new SqlParameter("@cau_tra_loiid", Request["hinhthucgiaodienweb"]);

            SqlParameter para_noidungweb_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 56);
            SqlParameter para_noidungweb = new SqlParameter("@cau_tra_loiid", Request["noidungweb"]);

            SqlParameter para_bocucchucnangweb_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 57);
            SqlParameter para_bocucchucnangweb = new SqlParameter("@cau_tra_loiid", Request["bocucchucnangweb"]);

            SqlParameter para_tocdoweb_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 58);
            SqlParameter para_tocdoweb = new SqlParameter("@cau_tra_loiid", Request["tocdoweb"]);

            SqlParameter para_tinhcxaccapnhatbgia_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 59);
            SqlParameter para_tinhcxaccapnhatbgia = new SqlParameter("@cau_tra_loiid", Request["tinhcxaccapnhatbgia"]);

            SqlParameter para_tocdobgia_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 60);
            SqlParameter para_tocdobgia = new SqlParameter("@cau_tra_loiid", Request["tocdobgia"]);

            SqlParameter para_giaodienbgia_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 61);
            SqlParameter para_giaodienbgia = new SqlParameter("@cau_tra_loiid", Request["giaodienbgia"]);

            SqlParameter para_ttthitruongttptich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 62);
            SqlParameter para_ttthitruongttptich = new SqlParameter("@cau_tra_loiid", Request["ttthitruongttptich"]);

            SqlParameter para_loccophieuttptich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 63);
            SqlParameter para_loccophieuttptich = new SqlParameter("@cau_tra_loiid", Request["loccophieuttptich"]);

            SqlParameter para_bieudobolocttptich_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 64);
            SqlParameter para_bieudobolocttptich = new SqlParameter("@cau_tra_loiid", Request["bieudobolocttptich"]);

            SqlParameter para_bantinphantichttruong_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 65);
            SqlParameter para_bantinphantichttruong = new SqlParameter("@cau_tra_loiid", Request["bantinphantichttruong"]);

            SqlParameter para_thamkhaobantinphantichttruong_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 66);
            SqlParameter para_thamkhaobantinphantichttruong = new SqlParameter("@cau_tra_loiid", Request["thamkhaobantinphantichttruong"]);

            SqlParameter para_noidungbantinphantichttruong_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 68);
            SqlParameter para_noidungbantinphantichttruong = new SqlParameter("@cau_tra_loiid", Request["noidungbantinphantichttruong"]);

            SqlParameter para_khuyennghidautubantinpttt_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 69);
            SqlParameter para_khuyennghidautubantinpttt = new SqlParameter("@cau_tra_loiid", Request["khuyennghidautubantinpttt"]);

            SqlParameter para_hieuquadautubantinpttt_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 70);
            SqlParameter para_hieuquadautubantinpttt = new SqlParameter("@cau_tra_loiid", Request["hieuquadautubantinpttt"]);

            SqlParameter para_ykienbantinpttt_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 81);
            SqlParameter para_ykienbantinpttt_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["ykienbantinpttt"]);

            try
            {
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hinhthucgiaodienweb_cau_hoi, para_hinhthucgiaodienweb, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_noidungweb_cau_hoi, para_noidungweb, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_bocucchucnangweb_cau_hoi, para_bocucchucnangweb, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tocdoweb_cau_hoi, para_tocdoweb, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tinhcxaccapnhatbgia_cau_hoi, para_tinhcxaccapnhatbgia, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_tocdobgia_cau_hoi, para_tocdobgia, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_giaodienbgia_cau_hoi, para_giaodienbgia, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ttthitruongttptich_cau_hoi, para_ttthitruongttptich, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_loccophieuttptich_cau_hoi, para_loccophieuttptich, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_bieudobolocttptich_cau_hoi, para_bieudobolocttptich, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_bantinphantichttruong_cau_hoi, para_bantinphantichttruong, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_thamkhaobantinphantichttruong_cau_hoi, para_thamkhaobantinphantichttruong, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_noidungbantinphantichttruong_cau_hoi, para_noidungbantinphantichttruong, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_khuyennghidautubantinpttt_cau_hoi, para_khuyennghidautubantinpttt, para_sotaikhoan, "", para_ngaykhaosat);
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_hieuquadautubantinpttt_cau_hoi, para_hieuquadautubantinpttt, para_sotaikhoan, "", para_ngaykhaosat);
                if (Request["ykienbantinpttt"].Trim().ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykienbantinpttt_cau_hoi, 323, para_sotaikhoan, para_ykienbantinpttt_noi_dung, para_ngaykhaosat);
                }

                //TVSI.DAL.WebsiteDB.ActionQuery.TVSI_sQEM_NOI_DUNG_EMAIL_AND_LOG_EMAIL_INSERT(strSubject, strMT564Body, 14, 1, "System", DateTime.Now, "Winservices", DateTime.Now, DateTime.Now, DateTime.Now, 0, strEmail[j].ToString(), 0, DateTime.Now, DateTime.Now, "", "9999999", "MT564", 99);
                return RedirectToAction("y-kien-khach-hang", "khao-sat");
            }
            catch
            {

                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View(giaodichtructuyen);
            }
        }

        public ActionResult Ykienkhachhang()
        {
            if (this.Session["so_tai_khoan"] != null)
            {
                ViewBag.success = this.Session["so_tai_khoan"];

            }
            else
            {
                ViewBag.success = false;
            }

            return View();
        }

        [HttpPost]
        public ActionResult Ykienkhachhang(KhaoSatYkienKhachHangModel ykienkhachhang)
        {
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            //if (TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG.ExistsSTK(ykienkhachhang.sotaikhoan.ToString(), AStrWeb))
            //{
            //    ViewBag.Message = "Số tài khoản " + ykienkhachhang.sotaikhoan + " của quý khách đã thực hiện khảo sát. Vui lòng liên hệ với TVSI để biết thêm thông tin. ";
            //    return View(ykienkhachhang);
            //}
            //SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", ykienkhachhang.sotaikhoan.ToString());
            SqlParameter para_sotaikhoan = new SqlParameter("@so_tai_khoan", Session["so_tai_khoan"].ToString());
            SqlParameter para_ngaykhaosat = new SqlParameter("@ngay_khao_sat", DateTime.Now);
            SqlParameter para_ykienkhachhang_cau_hoi = new SqlParameter("@cau_hoi_khao_satid", 71);
            SqlParameter para_ykienkhachhang_noi_dung = new SqlParameter("@noi_dung_y_kien", Request["gopytvsi"]);
            try
            {

                if (Request["gopytvsi"].Trim().ToString() != "")
                {
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_INSERT", para_ykienkhachhang_cau_hoi, 297, para_sotaikhoan, para_ykienkhachhang_noi_dung, para_ngaykhaosat);
                }
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKET_QUA_KHAO_SAT_ACCOUNT_UPDATE", ykienkhachhang.sotaikhoan, Session["so_tai_khoan"].ToString().Trim());
                log.Info("Session Account" + Session["so_tai_khoan"].ToString());
                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_THONG_TIN_KHACH_HANG_INSERT", "", ykienkhachhang.sotaikhoan, "", para_ngaykhaosat);

                DataTable dia_chi_email = TVSI_KHAO_SAT_THONG_TIN_KHACH_HANG.Lay_thong_tin_email(ykienkhachhang.sotaikhoan.Trim() + "1");

                if (int.Parse(dia_chi_email.Rows.Count.ToString()) > 0)
                {
                    //Insert thông tin gửi mail
                    string str_dia_chi_email = dia_chi_email.Rows[0]["dia_chi_email"].ToString();
                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sQEM_KET_QUA_KHAO_SAT_EMAIL_INSERT", str_dia_chi_email, "3581281");
                    log.Info("Send Email " + str_dia_chi_email);
                }
                return RedirectToAction("Redirect");
            }
            catch (Exception ex)
            {
                log.Error("SUBMIT THONG TIN KHAO SAT ERROR  " + ykienkhachhang.sotaikhoan + "---" + ex.ToString());
                ViewBag.Message = "Số tài khoản bị sai, Quý khách vui lòng nhập lại số tài khoản bao gồm 6 chữ số.";
                return View(ykienkhachhang);
            }
        }
        //Khách hàng chưa có thông tin tài khoản
        public ActionResult KhachHangChuaCoTaiKhoan()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KhachHangChuaCoTaiKhoan(FormCollection fcl)
        {
            string kytubaomatcn = fcl["kytubaomatcn"].ToString();
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View();
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View();
                }
            }
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            SqlConnection con = new SqlConnection(AStrWeb);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "TVSI_sKHAO_SAT_KET_QUA_INSERT";

            cmd.Parameters.Add("@khao_sat_type_code", SqlDbType.VarChar).Value = "A";
            cmd.Parameters.Add("@ho_ten", SqlDbType.NVarChar).Value = fcl["hoten"].ToString();
            cmd.Parameters.Add("@dien_thoai", SqlDbType.NVarChar).Value = fcl["dienthoai"].ToString();
            cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = fcl["email"].ToString();
            cmd.Parameters.Add("@nguoi_tao", SqlDbType.NVarChar).Value = "WEB";
            cmd.Parameters.Add("@id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                string id = cmd.Parameters["@id"].Value.ToString();
                foreach (string key in fcl.Keys)
                {
                    if (key.Contains("A"))
                    {
                        string strAValue = fcl[key] ?? "";
                        if (strAValue != "")
                        {
                            foreach (string s in strAValue.Split(','))
                            {
                                if (s.Trim().Length > 0)
                                {
                                    SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                    SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                    SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", s.ToString());
                                    SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", "");
                                    SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                    SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id, khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code,
                                        khao_sat_cau_tra_loi_text, nguoi_taoA);
                                }
                            }
                        }
                    }
                }

                return RedirectToAction("Redirect/1");
            }
            catch
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View();
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public ActionResult KhachHangDauTuCoPhieu()
        {
            int id = 0;
            if (this.Session["id"] == null)
                return Redirect("/khao-sat/");
            else
                id = int.Parse(this.Session["id"].ToString());
            string queryCkNew = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'B' and khao_sat_ket_qua_id =" + id;
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryCkNew))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count <= 0)
                    {
                        return Redirect("/khao-sat/");
                    }
                }
            }

            ViewBag.idks = id;
            return View();
        }
        [HttpPost]
        public ActionResult KhachHangDauTuCoPhieu(FormCollection fcl)
        {
            string kytubaomatcn = fcl["kytubaomatcn"].ToString();
            ViewBag.idks = fcl["idks"];
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View();
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View();
                }
            }
            string queryTotal = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA_CHI_TIET where khao_sat_ket_qua_id = " + fcl["idks"];
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryTotal))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        ViewBag.captcha = "Số tài khoản của bạn đã thực hiện khảo sát từ trước đó.";
                        return View();
                    }
                }
            }

            #region Google captcha
            //string captchaResponse = Request.Form["g-Recaptcha-Response"];
            //ViewBag.idks = fcl["idks"];
            //if (!String.IsNullOrEmpty(captchaResponse))
            //{
            //    ReCaptchaValidationResult result = ReCaptchaValidator.IsValid(captchaResponse);
            //    if (!result.Success)
            //    {
            //        if (result.ErrorCodes != null)
            //        {
            //            foreach (string err in result.ErrorCodes)
            //            {
            //                ModelState.AddModelError("", err);
            //            }

            //        }
            //        ModelState.AddModelError("", "Vui lòng xác nhận không phải là người máy.");
            //        return View();

            //    }
            //}
            //else
            //{
            //    ModelState.AddModelError("", "Vui lòng xác nhận không phải là người máy.");
            //    return View();
            //}
            #endregion

            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            string id = fcl["idks"];
            try
            {
                foreach (string key in fcl.Keys)
                {
                    if (key.Contains("B"))
                    {
                        string strAValue = fcl[key] ?? "";
                        if (key.Contains("_text"))
                        {
                            if (!String.IsNullOrEmpty(strAValue))
                            {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", "");
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", strAValue);
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id, khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code,
                                    khao_sat_cau_tra_loi_text, nguoi_taoA);
                            }
                        }
                        else
                        {
                            if (strAValue != "")
                            {
                                //foreach (string s in strAValue.Split(','))
                                //{
                                //    if (s.Trim().Length > 0)
                                //    {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", strAValue.ToString());
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", "");
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id,
                                    khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code, khao_sat_cau_tra_loi_text, nguoi_taoA);
                                //    }
                                //}
                            }
                        }

                    }
                }
                return RedirectToAction("Redirect/1");
            }
            catch (Exception)
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View();
            }
        }

        public ActionResult KhachHangDauTuTraiPhieu()
        {
            int id = 0;
            if (this.Session["id"] == null)
                return Redirect("/khao-sat/");
            else
                id = int.Parse(this.Session["id"].ToString());
            string queryCkNew = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'C' and  khao_sat_ket_qua_id =" + id;
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryCkNew))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count <= 0)
                    {
                        return Redirect("/khao-sat/");
                    }
                }
            }
            ViewBag.idks = id;
            return View();
        }
        [HttpPost]
        public ActionResult KhachHangDauTuTraiPhieu(FormCollection fcl)
        {
            string kytubaomatcn = fcl["kytubaomatcn"].ToString();
            ViewBag.idks = fcl["idks"];
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View();
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View();
                }
            }
            string queryTotal = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA_CHI_TIET where khao_sat_ket_qua_id = " + fcl["idks"];
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryTotal))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        ViewBag.captcha = "Số tài khoản của bạn đã thực hiện khảo sát từ trước đó.";
                        return View();
                    }
                }
            }

            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            string id = fcl["idks"];


            try
            {
                foreach (string key in fcl.Keys)
                {
                    if (key.Contains("C"))
                    {
                        string strAValue = fcl[key] ?? "";
                        if (key.Contains("_text"))
                        {
                            if (!String.IsNullOrEmpty(strAValue))
                            {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", "");
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", strAValue);
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id, khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code,
                                    khao_sat_cau_tra_loi_text, nguoi_taoA);
                            }
                        }
                        else
                        {

                            if (strAValue != "")
                            {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", strAValue.ToString());
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", "");
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id,
                                    khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code, khao_sat_cau_tra_loi_text, nguoi_taoA);
                            }
                        }

                    }
                }
                return RedirectToAction("Redirect/1");
            }
            catch (Exception)
            {
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View();
            }
        }

        public ActionResult KhachHangDauTuCoPhieuVaTraiPhieu()
        {
            int id = 0;
            if (this.Session["id"] == null)
                return Redirect("/khao-sat/");
            else
                id = int.Parse(this.Session["id"].ToString());

            string queryCkNew = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'D' and  khao_sat_ket_qua_id =" + id;
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryCkNew))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count <= 0)
                    {
                        return Redirect("/khao-sat/");
                    }
                }
            }
            ViewBag.idks = id;
            return View();
        }
        [HttpPost]
        public ActionResult KhachHangDauTuCoPhieuVaTraiPhieu(FormCollection fcl)
        {
            string kytubaomatcn = fcl["kytubaomatcn"].ToString();
            ViewBag.idks = fcl["idks"];
            if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN] == null)
            {
                ViewBag.captcha = "Lỗi ký tự bảo mật";
                return View();
            }
            else
            {
                if (this.Session[ConstantsFAQ.KY_TU_BAO_MAT_CA_NHAN].ToString() != kytubaomatcn)
                {
                    ViewBag.captcha = "Ký tự bảo mật không đúng";
                    return View();
                }
            }
            string queryTotal = "select khao_sat_ket_qua_id from TVSI_KHAO_SAT_KET_QUA_CHI_TIET where khao_sat_ket_qua_id = " + fcl["idks"];
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryTotal))
            {
                if (dsData.Tables.Count > 0)
                {
                    if (dsData.Tables[0].Rows.Count > 0)
                    {
                        ViewBag.captcha = "Số tài khoản của bạn đã thực hiện khảo sát từ trước đó.";
                        return View();
                    }
                }
            }
            string AStrWeb = ConfigurationManager.ConnectionStrings["ActionQuery_Website"].ToString();
            string id = fcl["idks"];


            try
            {
                foreach (string key in fcl.Keys)
                {
                    if (key.Contains("D"))
                    {
                        string strAValue = fcl[key] ?? "";
                        if (key.Contains("_text"))
                        {
                            if (!String.IsNullOrEmpty(strAValue))
                            {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", "");
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", strAValue);
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id, khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code,
                                    khao_sat_cau_tra_loi_text, nguoi_taoA);
                            }
                        }
                        else
                        {

                            if (strAValue != "")
                            {
                                SqlParameter khao_sat_ket_qua_id = new SqlParameter("@khao_sat_ket_qua_id", int.Parse(id.ToString()));
                                SqlParameter khao_sat_cau_hoi_code = new SqlParameter("@khao_sat_cau_hoi_code", key.ToString());
                                SqlParameter khao_sat_cau_tra_loi_code = new SqlParameter("@khao_sat_cau_tra_loi_code", strAValue.ToString());
                                SqlParameter khao_sat_cau_tra_loi_text = new SqlParameter("@khao_sat_cau_tra_loi_text", "");
                                SqlParameter nguoi_taoA = new SqlParameter("@nguoi_tao", "WEB");

                                SqlHelper.ExecuteNonQuery(AStrWeb, "TVSI_sKHAO_SAT_KET_QUA_CHI_TIET_INSERTT", khao_sat_ket_qua_id,
                                    khao_sat_cau_hoi_code, khao_sat_cau_tra_loi_code, khao_sat_cau_tra_loi_text, nguoi_taoA);
                            }
                        }

                    }
                }
                return RedirectToAction("Redirect/1");
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.ToString();
                ViewBag.Message = "Quý khách vui lòng điền đầy đủ thông tin & thực hiện lại";
                return View();
            }
        }

        public ActionResult Redirect()
        {
            return View();
        }

        public ActionResult kqkskhachhangchuacotkhaontvsi()
        {
            return View();
        }

        public ActionResult Ketquakhaosat()
        {


            string queryTotal = "select (select count(*) from TVSI_KHAO_SAT_KET_QUA) as total,";
            queryTotal += "(select  count(*) from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'A') as totalA ,";
            queryTotal += "(select  count(*) from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'B') as totalB ,";
            queryTotal += "(select  count(*) from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'C') as totalC ,";
            queryTotal += "(select  count(*) from TVSI_KHAO_SAT_KET_QUA where khao_sat_type_code = 'D') as totalD ";
            using (DataSet dsData = SqlHelper.ExecuteDataset(ConnectionString.SelectQuery_Website, CommandType.Text, queryTotal))
            {
                if (dsData.Tables.Count > 0)
                {

                    var tbData = dsData.Tables[0];
                    ViewBag.Total = tbData.Rows[0]["total"].ToString();
                    ViewBag.totalA = tbData.Rows[0]["totalA"].ToString();
                    ViewBag.totalB = tbData.Rows[0]["totalB"].ToString();
                    ViewBag.totalC = tbData.Rows[0]["totalC"].ToString();
                    ViewBag.totalD = tbData.Rows[0]["totalD"].ToString();
                }
            }
            return View();
        }
    }

    public class ReCaptchaValidationResult
    {
        public bool Success { get; set; }
        public string HostName { get; set; }
        [JsonProperty("challenge_ts")]
        public string TimeStamp { get; set; }
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }

    public class ReCaptchaValidator
    {
        public static ReCaptchaValidationResult IsValid(string captchaResponse)
        {
            if (string.IsNullOrWhiteSpace(captchaResponse))
            {
                return new ReCaptchaValidationResult() { Success = false };
            }

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://www.google.com");

            var values = new List<KeyValuePair<string, string>>();
            values.Add(new KeyValuePair<string, string>
            ("secret", "6LcIJsYZAAAAABtA61jiSdC3nTjhfZLkda4rNnQe"));
            values.Add(new KeyValuePair<string, string>
             ("response", captchaResponse));
            FormUrlEncodedContent content = new FormUrlEncodedContent(values);

            HttpResponseMessage response = client.PostAsync
            ("/recaptcha/api/siteverify", content).Result;

            string verificationResponse = response.Content.
            ReadAsStringAsync().Result;

            var verificationResult = JsonConvert.DeserializeObject
            <ReCaptchaValidationResult>(verificationResponse);

            return verificationResult;
        }
    }
}
