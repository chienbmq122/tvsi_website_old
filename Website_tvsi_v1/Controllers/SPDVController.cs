﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.WebsiteDAL;
using Website_tvsi_v1.Common;

namespace Website_tvsi_v1.Controllers
{
    public class SPDVController : Controller
    {
        //
        // GET: /SPDV/
        private string name_service = "product services";
        public SPDVController()
        {
            ViewBag.spdv_menu = "dropdown active";
            ViewBag.hotro_menu = "dropdown";
            ViewBag.gttvsi_menu = "dropdown ";
            ViewBag.title = "Công Ty Cổ Phần Chứng Khoán Tân Việt (TVSI)- Sản phẩm, dịch vụ của TVSI, Sản phẩm trực tuyến, dịch vụ môi giới, dịch vụ tài chính, dịch vụ doanh nghiệp";
        }
        public ActionResult Index()
        {
            return View();
        }
        // menu khach hang ca nhan
        public ActionResult KhachhangCanhan(int id=66)
        {
            
            // danh muc cha tieng viet la 11, tieng anh la 19
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            var thongtin_spdv = TVSI_THONG_TIN_SPDV_v2.LayTheo_thong_tin_spdvid_2017(id);
            ViewBag.thongtin_spdv_select = thongtin_spdv;
            int danhmuc_cha=11;
            if (ngonngu_id == 2)
            {
                danhmuc_cha = 19;
            }
            var danhsach_spdv = TVSI_DANHMUC_SPDV_TVSI_V3.Laytheo_chuyenmuc_phuthuocid(ngonngu_id, danhmuc_cha);
            // menu khach hang ca nhan
            int danhmuc_id1 = int.Parse(danhsach_spdv.Rows[0]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv1 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[1]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv2 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[2]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv3 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[3]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv4 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[4]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv5 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[5]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv6 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[6]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv7 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[7]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv8 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);

            danhmuc_id1 = int.Parse(danhsach_spdv.Rows[8]["danh_muc_spdvid"].ToString());
            ViewBag.thongtin_spdv9 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);


            return View();
        }

        // khach hang doanh nghiep
        public ActionResult Khachhangdoanhnghiep(int id=75)
        {
            // danh muc khach hang doanh nghiep id tieng viet la 17, tieng anh la 25
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            var thongtin_spdv = TVSI_THONG_TIN_SPDV_v2.LayTheo_thong_tin_spdvid_2017(id);
            ViewBag.thongtin_spdv_select = thongtin_spdv;
            int danhmuc_id = 17;
            if (ngonngu_id == 2)
            {
                danhmuc_id = 25;
            }
            // menu khach hang doanh nghiep
            ViewBag.Khach_hang_doanh_nghiep = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id);
            return View();
        }
        //menu giao dich truc tuyen
        public ActionResult Giaodichtructuyen(int id = 79)
        {
            // danh muc cong cu giao dich truc tuyen tieng viet id 18 tieng anh id 26
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
            var tvsi_map_thong_tin = TVSI_THONG_TIN_TVSI_v2.LayTheo_tvsi_map_ngon_ngu_id(id, name_service);
            if (ngonngu_id == 1)
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_viet_nam"].ToString());
            }
            else
            {
                id = int.Parse(tvsi_map_thong_tin.Rows[0]["id_english"].ToString());
            }
            var thongtin_spdv = TVSI_THONG_TIN_SPDV_v2.LayTheo_thong_tin_spdvid_2017(id);
            ViewBag.thongtin_spdv_select = thongtin_spdv;
            int danhmuc_id = 18;
            if (ngonngu_id == 2)
            {
                danhmuc_id = 26;
            }
            ViewBag.giao_dich_truc_tuyen = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id);
            return View();
        }
        public ActionResult DichvuTVSIKhdn(int stage = 1)
        {
            int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]), id1 = 1188,id2=1189;
            if (ngonngu_id == 2)
            {
                id1 = 1190;
                id2 = 1191;
            }
            var thong_tin_selected = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(id1);
            var thong_tin_selected_tab2 = TVSI_THONG_TIN_TVSI_v2.ChiTiet_THONG_TIN_TVSI_Theo_Thong_Tin_Tvsiid(id2);
            ViewBag.thong_tin_selected = thong_tin_selected;
            ViewBag.thong_tin_selected_tab2 = thong_tin_selected_tab2;
            ViewBag.stage = stage;
            return View();
        }
        public ActionResult laychuyenmuc_kh()
        {
            int ngongu_id=1;
            int chuyenmuc_phuthuoc_id=13;
            ViewBag.chuyenmuckh = TVSI_DANHMUC_SPDV_TVSI_V3.Laytheo_chuyenmuc_phuthuocid(ngongu_id, chuyenmuc_phuthuoc_id);
            return View();
        }
        //public ActionResult ChatLuongDichVuSLA()
        //{
        //    // danh muc cha tieng viet la 11, tieng anh la 19
        //    int ngonngu_id = CommonFnc.getLanguage(Request.Cookies[CommonConstant.name_lang_tvsi_wb]);
        //    int danhmuc_cha = 11;
        //    if (ngonngu_id == 2)
        //    {
        //        danhmuc_cha = 19;
        //    }
        //    var danhsach_spdv = TVSI_DANHMUC_SPDV_TVSI_V3.Laytheo_chuyenmuc_phuthuocid(ngonngu_id, danhmuc_cha);
        //    // menu khach hang ca nhan
        //    int danhmuc_id1 = int.Parse(danhsach_spdv.Rows[0]["danh_muc_spdvid"].ToString());
        //    ViewBag.thongtin_spdv1 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);
        //    danhmuc_id1 = int.Parse(danhsach_spdv.Rows[1]["danh_muc_spdvid"].ToString());
        //    ViewBag.thongtin_spdv2 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);
        //    danhmuc_id1 = int.Parse(danhsach_spdv.Rows[2]["danh_muc_spdvid"].ToString());
        //    ViewBag.thongtin_spdv3 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);
        //    danhmuc_id1 = int.Parse(danhsach_spdv.Rows[3]["danh_muc_spdvid"].ToString());
        //    ViewBag.thongtin_spdv4 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);
        //    danhmuc_id1 = int.Parse(danhsach_spdv.Rows[4]["danh_muc_spdvid"].ToString());
        //    ViewBag.thongtin_spdv5 = TVSI_THONG_TIN_SPDV_v3.LayTheo_danh_muc_spdvid(ngonngu_id, danhmuc_id1);         
        //    return View();
        //}
    }
}
